`default_nettype none
module str16
  (
   input wire clk,
   input wire in,
   output reg out
   );

   wire       out1;
   wire       out2;
   
   str8 a(.clk(clk), .in(in), .out(out1));
   str8 b(.clk(clk), .in(out1), .out(out2));

   assign out = out1 || out2;

endmodule
