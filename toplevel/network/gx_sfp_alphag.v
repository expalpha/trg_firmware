`default_nettype none
// store rx/tx headers in registers => 1k registers for headers
// also 320 for rx/tx shiftreg and ~500 for storing 4*mac, 4*ipaddr 2*ports     -> ~2k registers atm!
// 
// udp data transfer is poor and we want to use tcp - rewrite this module to allow that.
// 
//   currently do not send arp (only reply with our dst{ip,mac}_arp = ours when our mac is requested)
//   --------- and save single dst{ip,mac}_icmp, dstip_icmpdst{ip,mac}_udpd, and dst{ip,mac}_udpc from requests
//      => only 5   types of pkt to send [arp,icmp,data,cmdok,cmd-reply] cmd-ok is called udpc-req, cmd-reply is called cmd-ready
//         only 3/4 types of pkt to rcv  [arp,icmp,udp-cmd[2]]           udp-cmds can come on 2 ports [data and param]
module gx_sfp (
	       //input wire [1:0]   modulelevel,
	       input wire [7:0]   board_id,
	       output wire 	  datarequest_out,
	       input wire 	  clk,
	       input wire 	  reset,
	       output wire 	  debug,
	       input wire [13:0]  target_pkt_size,
	       input wire [7:0]   RxDataIn,
	       input wire 	  RxCtrlIn, // rename to gx or sfp
	       output reg [7:0]   TxDataOut,
	       output reg 	  TxCtrlOut,
	       input wire [63:0]  CmdDataIn,
	       output wire [63:0] CmdDataOut, // CmdDataOut[63] lasts 1 clk, rest lasts till overwritten
	       input wire [31:0]  TxData,
	       input wire 	  TxDataValid,
	       output wire 	  TxDataAck,
	       output reg         sfp_rx_data_valid_out
	       //output reg 	     autoneg_reset
 );
assign debug = (rx_packet_reg[64:0] != frame_hdr);
assign datarequest_out = udpd_active;

// on grif16, can use chipid to choose localmac and locipadr
//wire [63:0] device_id; wire id_valid; /* valid around 70 clks after end of reset */
//chipid id ( .clkin(clk/*max 100Mhz, but currently 125*/), .reset(reset), .data_valid(id_valid), .chip_id(device_id) );

// static frame elements ...
wire   [7:0]        sop =   8'hfb;
wire  [47:0]   preamble =  48'h555555555555;
wire   [7:0]        sfd =   8'hd5;
wire  [15:0]        eop =  16'hfdf7;
wire  [47:0]   bcastmac =  48'hffffffffffff;
wire  [31:0]   ipv4hdr1 =  32'h4500002e; // length=46 (for 18bytes udpdata)
wire  [31:0]   ipv4hdr2 =  32'h00000000; // can be 4000 0000 => don't fragment
wire  [31:0]   ipv4hdr3 =  32'h40110000; // ttl,udp, 16bit hdrchk
wire  [31:0]   pinghdr3 =  32'h40010000; // as ipv4hdr3 but icmp[01] not udp[11]
wire  [31:0]   arp_hdr1 =  32'h00010800;
wire  [31:0]   arp_hdr2 =  32'h06040002; // 2 => reply (we dont do requests)
wire  [15:0]  etype_arp =  16'h0806;
wire  [15:0]  etype_ip4 =  16'h0800;
wire  [31:0]   pinghdr1 =  32'h00000000; // typ/cod,cksum 0=reply,8=req
//wire  [31:0]   udpports =  32'h22B822B8; // 8888
wire  [31:0]   udplenck =  32'h001a0000;
wire   [7:0]  udp_proto =   8'h11;
wire   [7:0] icmp_proto =   8'h01;

// parameter-set frame elements ...                                // panther: 142.90.97.11 8E.5A.61.B
//wire  [47:0]  localmac = ( board_id == 8'h8 ) ? 48'h420000000401 : // mac on some grifc sfp fmc
//                         ( board_id == 8'h9 ) ? 48'h420000000402 : // 
//                         ( board_id == 8'h0 ) ? 48'h420000000403 : // 
//                                                48'h420000000404;  // 
//wire  [31:0]  locipadr = ( board_id == 8'h8 ) ? 32'h8E5A6207 :     // grifip07 142.90.98.7
//                         ( board_id == 8'h9 ) ? 32'h8E5A6208 :     // grifip08 142.90.98.8   8E.5A.62.8
//                         ( board_id == 8'h0 ) ? 32'h8E5A6209 :     // grifip09 142.90.98.9   8E.5A.62.9
//                                                32'h8E5A6213;      // grifip19 142.90.98.19
//wire  [47:0]  localmac = ( board_id == 8'h6 ) ? 48'h420000000404 : // mac on some grifc sfp fmc
//                         ( board_id == 8'h8 ) ? 48'h420000000401 : // 
//                         ( board_id == 8'h9 ) ? 48'h420000000402 : // 
//                         ( board_id == 8'h0 ) ? 48'h420000000403 : // 
//                         ( board_id == 8'hF9) ? 48'h42000000040B : // 
//                         ( board_id == 8'h8F) ? 48'h42000000040C : // 
//                                                48'h42000000040D;  // 
//wire  [31:0]  locipadr = ( board_id == 8'h6 ) ? 32'h8E5A6206 :     // grifip06
//                         ( board_id == 8'h8 ) ? 32'h8E5A6207 :     // grifip07
//                         ( board_id == 8'h9 ) ? 32'h8E5A6208 :     // grifip08
//                         ( board_id == 8'h0 ) ? 32'h8E5A6209 :     // grifip09
//                         ( board_id == 8'hF9) ? 32'h8E5A620A :     // grifip10
//                         ( board_id == 8'h8F) ? 32'h8E5A620B :     // grifip11
//                                                32'hC0A801FB;      // 192.168.1.251 // grifip19

wire  [47:0]  localmac = ( board_id == 8'h00) ? 48'h420000000400 : //
                         ( board_id == 8'h11) ? 48'h420000000401 : // 
                         ( board_id == 8'h22) ? 48'h420000000402 : // 
                         ( board_id == 8'h33) ? 48'h420000000403 : // 
                         ( board_id == 8'h44) ? 48'h420000000404 : // 
                                                48'h420000000405;  // 

wire  [31:0]  locipadr = ( board_id == 8'h00) ? 32'hC0A80132 :     // 192.168.1.50
                         ( board_id == 8'h11) ? 32'hC0A80133 :     // 192.168.1.51
                         ( board_id == 8'h22) ? 32'hC0A80134 :     // 192.168.1.52
                         ( board_id == 8'h33) ? 32'hC0A80135 :     // 192.168.1.53
                         ( board_id == 8'h44) ? 32'hC0A80136 :     // 192.168.1.54
                                                32'hC0A801FB;      // 192.168.1.251

wire  [15:0] data_port = 16'h2260; // decimal: 8800
wire  [15:0] cmd_port  = 16'h2268; // decimal: 8808

// variable frame elements ...(have separate udp command and data registers)
reg   [47:0] dstmac_arp; reg   [47:0] dstmac_udpd; reg   [47:0] dstmac_udpc; reg   [47:0] dstmac_icmp;
reg   [31:0]  dstip_arp; reg   [31:0]  dstip_udpd; reg   [31:0]  dstip_udpc; reg   [31:0]  dstip_icmp;
reg   [31:0]  udpdports; reg   [31:0]   udpcports;

// commands/responses ...
wire [31:0] response_ok  = 32'h4F4B2020; // ascii "OK  "
wire [31:0] response_err = 32'h45525250; // ascii "ERR "
wire [31:0] datarequest  = 32'h44525120; // ascii "DRQ "
wire [31:0] datastopreq  = 32'h53544F50; // ascii "STOP"
wire [31:0] paramreq     = 32'h5041524D; // ascii "PARM"  followed by 64 bits ...
wire [31:0] paramrdbk    = 32'h5244424B; // ascii "RDBK"  followed by 64 bits ...
//  15bit parnum - (16th bit used for ready)
//  15bit addr   - (16th bit used for R/Wn) - 3bitgrifc, 4bit port, 8bit chan,    32bit val
//wire [31:0] readrequest  = 32'h52454144; // ascii "READ"
//wire [31:0] writerequest = 32'h57524954; // ascii "WRIT"

//////////////////////////////////////////////////////////////////////////////////////
/////////              event data and parameter data handling             ////////////
//////////////////////////////////////////////////////////////////////////////////////

// rxdata 143:128 127:112   111:96 95:80 79:64 63:48   47:32 31:16 15:0
//          "PA"    "RM"    parnum  chan |<-value->|   ---- unused ----

   reg [143:0] RxData;
   reg 	       RxDataValid; // modify chan# to mark readback to come here

//assign sfp_rx_data_valid_out = RxDataValid;
   
//assign CmdDataOut = ( modulelevel == 2'h0 ) ? {RxDataValid,RxData[110:96],8'hFE,RxData[87:48]} : // digitiser
//                    ( modulelevel == 2'h1 ) ? {RxDataValid,RxData[110:96],4'hE, RxData[91:48]} : // grifc
//                                              {RxDataValid,RxData[110:48]};                      // master

   assign CmdDataOut = {RxDataValid,RxData[110:48]}; // master

   reg [63:0] CmdData;
   reg 	      cmd_ready_set;
   reg 	      cmd_ready_clr;

   always @ (posedge clk or posedge reset) begin
      if( reset ) begin
	 cmd_ready_set <= 1'b0; CmdData <= 64'h0;
      end else begin
	 cmd_ready_set <= CmdDataIn[63];
	 if( CmdDataIn[63] ) CmdData <= CmdDataIn; // extra response sent to command port, when data from parameter reads arrives
	 else                CmdData <= CmdData;
      end
   end

   // would like to send full packets - but can send partial packets after waiting some time with no more data ...
   //    tx_data_len set in tx, copied from data_packet_len at start of transmission
   //    busy is set as soon as decide to send(txpkt_ready_set), while waiting to send(txpkt_ready), and while sending(tx_data_len)
   //reg TxDataPrevAck;
   //reg TxDataAck_reg;
   reg [13:0] data_packet_len;
   reg 	      txfifo_wrreq;
   reg [31:0] txfifo_in;
   reg [11:0] txbuf_cnt;
   reg [15:0] txbuf_timeout;
   reg [30:0] pkt_count;
   //wire txfifo_busy         = txpkt_ready || txpkt_ready_set || tx_data_len > 16'h2; // single event use of tx_fifo atm
   wire       txfifo_busy           = txpkt_ready || txpkt_ready_set; // dual event use

   assign TxDataAck = (udpd_active) && (!txfifo_busy) && TxDataValid && (txbuf_cnt != 12'h0);

   always @ (posedge clk or posedge reset) begin
   if( reset ) begin
      //TxDataPrevAck   <=  1'b0;
      //TxDataAck_reg   <=  1'b0;
      txbuf_timeout   <= 16'hA00;
      pkt_count       <= 31'h0;
      txbuf_cnt       <= 12'h0;
      txpkt_ready_set <=  1'b0;
      data_packet_len <= 14'h12; 
      txfifo_in       <= 32'h0;
      txfifo_wrreq    <=  1'b0;  
   end else begin
      //TxDataPrevAck   <= TxDataAck;
      //TxDataAck_reg   <= 1'b0;
      txbuf_timeout   <= txbuf_timeout;
      pkt_count       <= pkt_count;
      txbuf_cnt       <= txbuf_cnt;
      txpkt_ready_set <= 1'b0;
      data_packet_len <= data_packet_len;
      txfifo_in       <= txfifo_in;
      txfifo_wrreq    <= 1'b0;
      if( udpd_active && ! txfifo_busy ) begin
	 if( txbuf_cnt == 12'h0 ) begin // insert packet counter at start of each transmission
	    txfifo_in <= {1'b0,pkt_count};  txfifo_wrreq <= 1'b1;  pkt_count <= pkt_count + 31'h1; txbuf_cnt <= txbuf_cnt + 12'h1;
	    //end else if( TxDataValid && !TxDataAck && !TxDataPrevAck ) begin    // wait couple clocks after each ack ...
	 end else if( TxDataValid ) begin
	    txbuf_cnt <= txbuf_cnt + 12'h1;  txbuf_timeout <= 16'hA00; // reset to 25us, each time we get some data
	    //txfifo_in <= {TxData[7:0],TxData[15:8],TxData[23:16],TxData[31:24]}; // 'correct" order in netwk pkt
	    txfifo_in <= TxData[31:0];                                             // (i.e. look normal in tcpdump)
	    txfifo_wrreq <= 1'b1;
	    //TxDataAck_reg <= 1'b1;
	    if( txbuf_cnt == 12'h7F7 || txfifo_words > 11'h7F0 ) begin // fifo about to overrun - send now (only 4k at the moment)
	       txbuf_cnt <= 12'h0;  data_packet_len <= {txbuf_cnt+12'h1,2'h0};  txpkt_ready_set <=  1'b1;
	    end else if( TxData[31:28] == 4'hE && {txbuf_cnt,2'h0} > target_pkt_size ) begin // end of fragment - end packet here
	       txbuf_cnt <= 12'h0;  data_packet_len <= {txbuf_cnt+12'h1,2'h0};  txpkt_ready_set <=  1'b1;
	    end
	 end else if( txbuf_cnt != 12'h1 ) begin // no more data but we already have some - check timeout
	    txbuf_timeout <= txbuf_timeout - 16'h1;
	    if( txbuf_timeout == 16'h0 ) begin    // timeout waiting for more data - send what we have now
	       txbuf_cnt <= 12'h0;  data_packet_len <= {txbuf_cnt,2'h0};  txpkt_ready_set <= 1'b1;
	    end
	 end
      end
   end
end

reg txfifo_rdreq;
wire [7:0] txfifo_out;  wire txfifo_full, txfifo_empty;  wire [10:0] txfifo_words; // 2k x 32bit [8kbytes]
sfp_txfifo txfifo ( //32bit in 8bit out  wrreq at inc txbuf, rdreq in place of shift 
    .rdclk(clk), .wrclk(clk), .aclr(reset), .wrfull(txfifo_full), .wrusedw(txfifo_words),
	 .data(txfifo_in), .rdreq(txfifo_rdreq), .wrreq(txfifo_wrreq), .q(txfifo_out)
);

//////////////////////////////////////////////////////////////////////////////////////
/////////                     transmit packet selection                   ////////////
//////////////////////////////////////////////////////////////////////////////////////

reg arp_request_clr, udpc_request_clr, ping_request_clr;  // set in tx pkt-setup routine
reg arp_request_set, udpc_request_set, ping_request_set;  // set in rx routine
reg arp_request, udpc_request, ping_request, udpd_active; // status bits
reg txpkt_ready_clr, txpkt_ready_set, txpkt_ready, cmd_ready;

always @ (posedge clk or posedge reset) begin
   if( reset ) begin
      arp_request  <= 1'b0; udpc_request <= 1'b0;
      ping_request <= 1'b0; txpkt_ready  <= 1'b0;
      cmd_ready    <= 1'b0;
   end else begin // set has priority over clear below
      ping_request <= ping_request; txpkt_ready <= txpkt_ready;
      udpc_request <= udpc_request; arp_request  <= arp_request;
      if(      arp_request_set  ) arp_request  <= 1'b1; 
      else if( arp_request_clr  ) arp_request  <= 1'b0;
      if(      udpc_request_set ) udpc_request <= 1'b1;
      else if( udpc_request_clr ) udpc_request <= 1'b0;
      if(      ping_request_set ) ping_request <= 1'b1;
      else if( ping_request_clr ) ping_request <= 1'b0;
      if(      txpkt_ready_set  ) txpkt_ready  <= 1'b1; 
      else if( txpkt_ready_clr  ) txpkt_ready  <= 1'b0;
      if(      cmd_ready_set    ) cmd_ready    <= 1'b1; 
      else if( cmd_ready_clr    ) cmd_ready    <= 1'b0;
   end
end

////////////// transmit packet setup /////////////////////////////
// set pkt_type, lengths, headers, clear *_pkt_rdy and set tx_ready
//    Note data_packet_len will be reused when txpkt_rdy cleared
// 
// minpkt[591:0] -  64[591:528]frm 112[528:416]eth 160[415:256]ipv4 64[255:192]udp 144[191:48]data 48[47:0]trailer
// min pkt has 18(0x12)bytes[144bits] of data, any extra inserted at byte 50(x32)400bits[start of data]

parameter idle_state=1'b0, write_state=1'b1, read_state=1'b1;
parameter pkt_none=2'h0, pkt_arp=2'h1, pkt_udp=2'h2, pkt_icmp=2'h3;
parameter src_reg=2'h0, data_fifo=2'h1, rx_fifo=2'h2;

wire [63:0]  frame_hdr = { sop,preamble,sfd };
reg [111:0]   eth_hdr;
reg [159:0]  ipv4_hdr;
reg [ 63:0] proto_hdr; // usually udp ports+length/cksum
reg [143:0]  data_hdr; // need at least this much data even in min-size pkt

reg [15:0] tx_data_len;  reg [7:0] tx_hdr_len;  reg [ 1:0] pkt_type;  reg tx_ready;  reg [1:0] data_source;
always @ (posedge clk or posedge reset ) begin
   if( reset ) begin
      ping_request_clr <= 1'b0;   arp_request_clr    <= 1'b0;   udpc_request_clr <= 1'b0;
      cmd_ready_clr    <= 1'b0;   txpkt_ready_clr    <= 1'b0;   pkt_type     <= pkt_none;  data_source <= src_reg;
      eth_hdr        <= 112'h0;   ipv4_hdr         <= 160'h0;   proto_hdr       <= 64'h0;  data_hdr <=  144'h0;
      tx_hdr_len       <= 8'h0;   tx_data_len       <= 16'h0;   tx_ready         <= 1'b0;
   end else begin
      ping_request_clr <= 1'b0;   arp_request_clr    <= 1'b0;   udpc_request_clr <= 1'b0;
      cmd_ready_clr    <= 1'b0;   txpkt_ready_clr    <= 1'b0;   pkt_type     <= pkt_type;  data_source <= data_source;
      eth_hdr       <= eth_hdr;   ipv4_hdr       <= ipv4_hdr;   proto_hdr   <= proto_hdr;  data_hdr <= data_hdr;
      tx_hdr_len <= tx_hdr_len;   tx_data_len <= tx_data_len;   tx_ready     <= tx_ready;
      
      if( tx_state == idle_state && tx_ready == 1'b0 ) begin // idle - see if any data ready to send
         tx_hdr_len <=  8'h32;  tx_data_len <= 16'h12;  tx_ready <= 1'b1;  // defaults
         if(           arp_request ) begin
            pkt_type  <= pkt_arp;   arp_request_clr <= 1'b1;
            eth_hdr   <= {dstmac_arp, localmac, etype_arp};
            ipv4_hdr  <= {arp_hdr1, arp_hdr2, localmac, locipadr, dstmac_arp[47:32]}; //    not ipv4
            proto_hdr <= {dstmac_arp[31:0],dstip_arp};                                // but still fits!
	    data_hdr  <= 144'h0;  data_source <= src_reg;
         end else if( ping_request ) begin
            pkt_type  <= pkt_icmp;  ping_request_clr <= 1'b1; 
            eth_hdr   <= {dstmac_icmp, localmac, etype_ip4};
            ipv4_hdr  <= {ipv4hdr1[31:16],{5'h0,rx_data_len[10:0]}+16'h1c, ipv4hdr2, pinghdr3, locipadr, dstip_icmp};  // use proper length
            proto_hdr <= {pinghdr1[31:16],{rxpingcksm+16'h0800},rxpingdata[175:144]};                // also adjust checksum for reply code
	    data_hdr  <= rxpingdata[143:0];  data_source <= rx_fifo;
            tx_data_len <= rx_data_len;
         end else if( udpc_request ) begin
            pkt_type  <= pkt_udp;   udpc_request_clr <= 1'b1;                           // cmd reply: "ok" [udpr]
            eth_hdr   <= {dstmac_udpc, localmac, etype_ip4};
            ipv4_hdr  <= {ipv4hdr1[31:16],16'h20, ipv4hdr2, ipv4hdr3, locipadr, dstip_udpc};   // ipv4 length  32
            proto_hdr <= {udpcports, 16'h0c,udplenck[15:0]};                                   // udp data len 12
	    data_hdr  <= {response_reg, 112'h0};   data_source <= src_reg;
         end else if( cmd_ready ) begin
            pkt_type  <= pkt_udp;   cmd_ready_clr <= 1'b1;                               // param readback [udpp]
            eth_hdr   <= {dstmac_udpc, localmac, etype_ip4};
            ipv4_hdr  <= {ipv4hdr1[31:16],16'h28, ipv4hdr2, ipv4hdr3, locipadr, dstip_udpc};   // ipv4 length  32
            proto_hdr <= {udpcports, 16'h14,udplenck[15:0]};                                   // udp data len 12
	    data_hdr  <= {paramrdbk, CmdData, 48'h0};   data_source <= src_reg;
         end else if( udpd_active && txpkt_ready ) begin
            pkt_type  <= pkt_udp;   txpkt_ready_clr <= 1'b1;
            eth_hdr   <= {dstmac_udpd, localmac, etype_ip4};
            ipv4_hdr  <= {ipv4hdr1[31:16],{2'h0,data_packet_len[13:0]}+16'h1c, ipv4hdr2, ipv4hdr3, locipadr, dstip_udpd};
            proto_hdr <= {udpdports, {2'h0,data_packet_len[13:0]}+16'h8,udplenck[15:0]};
	    data_hdr  <= 144'h0;   data_source <= data_fifo;
            tx_data_len <= {2'h0,data_packet_len};
         end else tx_ready <= 1'b0;
      end else if( tx_state == write_state && tx_ready == 1'b1 ) begin // clear ready when writing starts
	 tx_ready <= 1'b0;
	 if( pkt_type == pkt_udp || pkt_type == pkt_icmp ) ipv4_hdr[79:64] <= ipv4hdrchk; // load ipv4 hdrchksum which took 1 clk to calculate after header was set
      end
   end
end

wire [15:0] ipv4hdrchk;  ipv4_chksum hdr_chk( .clk(clk), .data(ipv4_hdr), .chksum(ipv4hdrchk) ); // 1 clk delay till chksum valid

//////////////////////////////////////////////////////////////////////////////////////
///////////////                  Transmit routine             ////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

// to be ready in time, .data() needs to be byte that will appear on output TxDataOut next clk
wire [31:0] new_txcrc;  crc32_eth tx_crc( .data(tx_packet_reg[159:152]), .prv_crc(txfrmcheck), .new_crc(new_txcrc) );

reg [159:0] tx_packet_reg;  reg [31:0]  txfrmcheck;  reg tx_state;
reg  [31:0]   tx_idle_cnt;  reg [15:0] tx_byte_cnt;  reg [7:0] tx_hdr_cnt;  reg [15:0] tx_data_cnt;
always @ (posedge clk or posedge reset) begin
   if( reset ) begin
      tx_packet_reg <=        160'h0;  txfrmcheck  <=       32'h0;  tx_state   <= idle_state;  txfifo_rdreq <=  1'b0;  rxbuf_rdaddr <= 11'h0;
      tx_idle_cnt   <=         32'h0;  tx_byte_cnt <=       16'h0;  tx_hdr_cnt <=       8'h0;  tx_data_cnt  <= 16'h0;
   end else begin
      tx_packet_reg <= tx_packet_reg;  txfrmcheck  <=  txfrmcheck;  tx_state   <=   tx_state;  txfifo_rdreq <=  1'b0;  rxbuf_rdaddr <= rxbuf_rdaddr;
      tx_idle_cnt   <= tx_idle_cnt+1;  tx_byte_cnt <= tx_byte_cnt;  tx_hdr_cnt <= tx_hdr_cnt;  tx_data_cnt  <= tx_data_cnt;
      case( tx_state )
	idle_state:  // wait for 12byte inter-frame-gap before sending anything
	  begin     // also don't split idle-sequence pairs (may not matter?)
	     TxCtrlOut      <= ( tx_idle_cnt[0] == 1'b0 ) ? 1'b1  : 1'b0 ;
	     TxDataOut[7:0] <= ( tx_idle_cnt[0] == 1'b0 ) ? 8'hBC : 8'h50; // K28.5 or D16.2
             if( tx_ready && tx_idle_cnt > 32'hc && tx_idle_cnt[0] ) begin    // idle-count will wrap + cause extra small waits
                tx_state      <= write_state;
		tx_packet_reg <= {frame_hdr,96'h0}; // but not significant
		tx_byte_cnt   <= 7'h0;
		tx_hdr_cnt    <= tx_hdr_len;
		tx_data_cnt   <= tx_data_len + 16'h6; // include trailer in data_len 
		rxbuf_rdaddr  <= rx_data_addr;
	     end
	  end
	write_state: // load headers as required, then data, then fcs/trailer
	  begin     //   *** hdr,data counters should finish at 1 (not zero) since loaded with full count ***
	     TxDataOut[7:0] <=  tx_packet_reg[159:152];                                       // set outputs
	     TxCtrlOut <= ( tx_byte_cnt == 7'h0 || tx_data_cnt < 7'h3 ) ? 1'b1 : 1'b0;
	     
	     tx_idle_cnt <=               32'h0;  tx_byte_cnt <= tx_byte_cnt + 16'h1;         // update counters
	     tx_hdr_cnt  <= (tx_hdr_cnt != 7'h0) ? tx_hdr_cnt  - 7'h1 : 7'h0;
	     tx_data_cnt <= (tx_hdr_cnt == 7'h0) ? tx_data_cnt - 7'h1 : tx_data_cnt;
	     
	     txfrmcheck  <= (tx_byte_cnt == 16'h7) ? 32'hffffffff : new_txcrc; // init fcs, and below - load various hdr/data ready for use ...
	     if(      tx_hdr_cnt <  8'h3 && tx_data_cnt > 16'h8 && data_source == data_fifo ) txfifo_rdreq <= 1'b1; // shift fifo 1 clk before needed
	     if(      tx_hdr_cnt <  8'h3 && tx_data_cnt > 16'h8                             ) rxbuf_rdaddr <= rxbuf_rdaddr + 11'h1;
	     
             if(      tx_hdr_cnt <  8'h2 && tx_data_cnt > 16'h7 && data_source == data_fifo ) tx_packet_reg <= {txfifo_out,tx_packet_reg[151:0]};
             else if( tx_hdr_cnt <  8'h2 && tx_data_cnt > 16'h7 && data_source == rx_fifo   ) tx_packet_reg <= {rxbuf_out, tx_packet_reg[151:0]};
	     else if( tx_hdr_cnt  ==  8'h2b ) tx_packet_reg <=   {eth_hdr,48'h0};             // load hdr1
             else if( tx_hdr_cnt  ==  8'h1d ) tx_packet_reg <=   ipv4_hdr;                    // load hdr2
             else if( tx_hdr_cnt  ==  8'h9  ) tx_packet_reg <= {proto_hdr,96'h0};             // load hdr3
             else if( tx_hdr_cnt  ==  8'h1  ) tx_packet_reg <=  {data_hdr,16'h0};             // load data if not loaded from fifo above
	     else if( tx_data_cnt == 16'h7  ) tx_packet_reg <= {~new_txcrc[31:0],eop,112'h0}; // load fcs/trailer                        
	     else                             tx_packet_reg <= {tx_packet_reg[151:0],8'h0};   // otherwise shift to next byte
	     
	     if( tx_data_cnt == 7'h1 ) tx_state <= idle_state;                                // transmission done
	  end
      endcase
   end
end

//////////////////////////////////////////////////////////////////////////////////////
///////////////                  Receive routine              ////////////////////////
//////////////////////////////////////////////////////////////////////////////////////

reg [111:0]   rx_eth_hdr;
reg [159:0]  rx_ipv4_hdr;
reg [ 63:0] rx_proto_hdr; // usually udp ports+length/cksum
reg [143:0]  rx_data_hdr; // have at least this much data even in min-size pkt

wire  [47:0]    rxdstmac =   rx_eth_hdr[111: 64];
wire  [47:0]    rxsrcmac =   rx_eth_hdr[ 63: 16];
wire  [15:0] rxethertype =   rx_eth_hdr[ 15:  0];
wire  [31:0]  rxipv4hdr1 =  rx_ipv4_hdr[159:128];
wire  [31:0]  rxipv4hdr2 =  rx_ipv4_hdr[127: 96];
wire  [31:0]  rxipv4hdr3 =  rx_ipv4_hdr[ 95: 64];
wire  [31:0]     rxsrcip =  rx_ipv4_hdr[ 63: 32];
wire  [31:0]     rxdstip =  rx_ipv4_hdr[ 31:  0];
wire  [31:0]  rxudpports = rx_proto_hdr[ 63: 32];
//wire  [31:0]  rxudplenck = rx_proto_hdr[ 31:  0];
wire  [31:0]   rxudpdata =  rx_data_hdr[143:112];
wire   [7:0]   ipv4proto =   rxipv4hdr3[ 23: 16];
// alternate names for arp requests ...
wire  [31:0]  rxarpdstip =  rx_proto_hdr[31:0];
wire  [47:0] rxarpsrcmac = {rxipv4hdr3,rxsrcip[31:16]};
wire  [31:0]  rxarpsrcip = {rxsrcip[15:0],rxdstip[31:16]};
// alternate name for ping replies
wire  [15:0]  rxpingcksm =  rx_proto_hdr[47:32];
wire [175:0]  rxpingdata = {rx_proto_hdr[31:0],rx_data_hdr};

wire mac_address = (rxdstmac == bcastmac || rxdstmac == localmac);
wire  addressed  = (rxdstmac == localmac) && (rxdstip == locipadr);
wire  arp_frame  = (!badframe) && mac_address && (rxethertype == etype_arp);
wire ipv4_frame  = (!badframe) && addressed && (rxethertype == etype_ip4);
wire  udp_frame  = ipv4_frame && (ipv4proto ==  udp_proto);
wire icmp_frame  = ipv4_frame && (ipv4proto == icmp_proto);
wire  cmd_rcv    = (rxudpports[15:0] ==  cmd_port);
wire data_rcv    = (rxudpports[15:0] == data_port);

////////////////   rx header setup etc. /////////////////
reg  [31:0] response_reg;   reg [10:0] rx_data_len;  reg [10:0] rx_data_addr;
always @ (posedge clk or posedge reset) begin
   if( reset ) begin
      arp_request_set   <=  1'b0;
      udpc_request_set  <=  1'b0;
      udpd_active       <=  1'b0;
      ping_request_set  <=  1'b0; // pkt requests
      dstmac_arp        <= 48'h0;
      dstmac_udpd       <= 48'h0;
      dstmac_udpc       <= 48'h0;
      dstmac_icmp       <= 48'h0; // mac
      dstip_arp         <= 32'h0;
      dstip_udpd        <= 32'h0;
      dstip_udpc        <= 32'h0;
      dstip_icmp        <= 32'h0; // ipaddrs
      RxData            <= 144'h0;
      RxDataValid       <=  1'b0;
      response_reg      <= 32'h0;
      // rx data
      udpdports         <= 32'h0;
      udpcports         <= 32'h0;
      rx_data_len       <= 11'h0;
      rx_data_addr      <= 11'h0; // ports
      sfp_rx_data_valid_out <= 0;
   end else begin
      arp_request_set   <= 1'b0;
      udpc_request_set  <= 1'b0;
      udpd_active       <= udpd_active;
      ping_request_set  <= 1'b0;
      dstmac_arp        <= dstmac_arp;
      dstmac_udpd       <= dstmac_udpd;
      dstmac_udpc       <= dstmac_udpc;
      dstmac_icmp       <= dstmac_icmp;
      dstip_arp         <= dstip_arp;
      dstip_udpd        <= dstip_udpd;
      dstip_udpc        <= dstip_udpc;
      dstip_icmp        <= dstip_icmp;
      RxData            <= RxData;
      RxDataValid       <= 1'b0;
      response_reg      <= response_reg;
      udpdports         <= udpdports;
      udpcports         <= udpcports;
      rx_data_len       <= rx_data_len;
      rx_data_addr      <= rx_data_addr;
      sfp_rx_data_valid_out <= 0;
      if ( rx_pkt_rdy ) begin
	 rx_data_len  <= rx_byte_cnt[10:0] - 11'h37; // counts everything inc. sop and byte1 of eop - min x49=73bytes for 18 data bytes
	 rx_data_addr <= rxbuf_wraddr - rx_byte_cnt[10:0] + 11'h37 - 11'h4;  // after data, have fcs and byte1 of eop in buffer
	 if( arp_frame && rxarpdstip == locipadr && rxipv4hdr1 == arp_hdr1 && rxipv4hdr2[0] ) begin // arp - to us
	    dstmac_arp <= rxarpsrcmac;
	    dstip_arp  <= rxarpsrcip;
	    arp_request_set <= 1'b1;
	    sfp_rx_data_valid_out <= 1;
	 end
	 if( icmp_frame && rxudpports[31:16] == 16'h0800 ) begin // icmp type 0x0800 = echo request
	    dstmac_icmp  <= rxsrcmac;
	    dstip_icmp   <= rxsrcip;
	    ping_request_set <= 1'b1;
	    sfp_rx_data_valid_out <= 1;
	 end
	 if( udp_frame && data_rcv ) begin // request on data port: handle, and also send response on command port
	    response_reg <= response_ok;  udpc_request_set  <= 1'b1;
	    if( rxudpdata == datastopreq ) udpd_active <= 1'b0;
	    else if( rxudpdata == datarequest ) begin
	       udpd_active  <= 1'b1;
	       dstmac_udpd  <= rxsrcmac;
	       dstip_udpd <= rxsrcip;
	       udpdports <= {rxudpports[15:0],rxudpports[31:16]};   
	    end else response_reg <= response_err;
	    sfp_rx_data_valid_out <= 1;
	 end // NOTE: sending response to data request on command port requires having already received a packet on the command port
	 //      (or port/ip/mac will not be initialised, and the response will not arrive)
	 if( udp_frame && cmd_rcv ) begin  // request on command port: handle and send response
	    response_reg <= ( rxudpdata == paramreq ) ? response_ok : response_err;
	    dstmac_udpc  <= rxsrcmac;
	    dstip_udpc   <= rxsrcip;
	    udpcports    <= {rxudpports[15:0],rxudpports[31:16]};
	    RxDataValid  <= 1'b1;
	    RxData       <= rx_data_hdr;
	    udpc_request_set <= 1'b1;
	    sfp_rx_data_valid_out <= 1;
	 end
      end
   end
end

// delay crc calculation to check at end of frame
reg [31:0] rxcrcdelay; always @ (posedge clk) rxcrcdelay <= {rxcrcdelay[23:0],RxDataIn[7:0]};
wire [31:0] new_rxcrc; crc32_eth rx_crc( .data(rxcrcdelay[31:24]), .prv_crc(rxframecheck), .new_crc(new_rxcrc) );

reg [159:0] rx_packet_reg;  reg [31:0] rxframecheck;  reg rx_state;  reg badframe;  reg rx_pkt_rdy;
reg  [31:0]   rx_idle_cnt;  reg [15:0]  rx_byte_cnt;  // reg [7:0] rx_hdr_cnt;  reg [15:0] rx_data_cnt;
always @ (posedge clk or posedge reset) begin
   if( reset ) begin
      badframe <=       1'b0;  rxframecheck <=         32'h0;  rx_packet_reg <= 160'h0;         rxbuf_in    <=  8'h0; 
      rx_state <= idle_state;  rx_idle_cnt  <=         32'h0;  rx_pkt_rdy <= 1'b0;              rxbuf_wren   <= 1'b0; rxbuf_wraddr <= 11'h0;
   end else begin
      badframe <=   badframe;  rxframecheck <=  rxframecheck;  rx_packet_reg <= rx_packet_reg;  rxbuf_in <= rxbuf_in;
      rx_state <=   rx_state;  rx_idle_cnt  <= rx_idle_cnt+1;  rx_pkt_rdy <= 1'b0;              rxbuf_wren   <= 1'b0; rxbuf_wraddr <= rxbuf_wraddr;
      case( rx_state )
	idle_state:
	  begin
             if( RxDataIn == 8'hFB && RxCtrlIn == 1'b1 ) begin // K28.7 - start of packet
		rx_packet_reg <= {rx_packet_reg[151:0],RxDataIn[7:0]}; // copy sfd into register
 		rx_state <= read_state;  rx_byte_cnt <= 16'h1;  badframe <= 1'b0; 
	     end
          end
        read_state:
	  begin
	     rx_state <= read_state;   rx_byte_cnt <= rx_byte_cnt + 16'h1;
	     rx_packet_reg <= {rx_packet_reg[151:0],RxDataIn[7:0]};
	     rxbuf_in      <= RxDataIn[7:0];
	     
	     if( RxCtrlIn ) begin // RxDataIn == 8'hF7 => K23.7 - end of packet
		rx_state <= idle_state;
		if( RxDataIn == 8'hFD && ~rxframecheck == rxcrcdelay ) begin badframe <= 1'b0; rx_pkt_rdy <= 1'b1; end // check fcs
		else badframe <= 1'b1;  // early end-of-packet
             end
	     
	     rxframecheck <= ( rx_byte_cnt == 16'hb ) ? 32'hffffffff : new_rxcrc;  // fcs [init was at 16'h7 now +4]
	     if( rx_byte_cnt == 16'h8 && rx_packet_reg[63:0] != frame_hdr ) begin rx_state <= idle_state; badframe <= 1'b1; end // check peamble
	     if( rx_byte_cnt == 16'h16 )   rx_eth_hdr <= rx_packet_reg[111:0];
	     if( rx_byte_cnt == 16'h2a )  rx_ipv4_hdr <= rx_packet_reg[159:0];
	     if( rx_byte_cnt == 16'h32 ) rx_proto_hdr <= rx_packet_reg[ 63:0];
	     if( rx_byte_cnt == 16'h44 )  rx_data_hdr <= rx_packet_reg[143:0];
	     if( rx_byte_cnt >= 16'h32 ) begin rxbuf_wraddr <= rxbuf_wraddr + 11'h1;  rxbuf_wren <= 1'b1; end					
          end
      endcase
   end
end

// network can't be paused, this has to run continuously or data will be dropped
//  [1500byte=20kbit]   also get large number of junk/uninteresting packets
// so just use circular buffer, and note locations of anything of interest
reg [10:0] rxbuf_rdaddr; reg [10:0] rxbuf_wraddr;  reg rxbuf_wren;
reg  [7:0] rxbuf_in;     wire [7:0] rxbuf_out; 
netbuf_dpram rx_netbuf ( .clock(clk),   // memory is 2kx8bits
   .wren(rxbuf_wren), .wraddress(rxbuf_wraddr), .data(rxbuf_in),	
                      .rdaddress(rxbuf_rdaddr),    .q(rxbuf_out)
);

endmodule

