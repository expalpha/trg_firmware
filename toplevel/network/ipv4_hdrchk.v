// udpchk - A[31:0] = a+b+c+d+d+e+f+g[16] B=A[31-16]+A[15-0] chk=~B
module ip4v_hdrchk( input wire [159:0] header, output wire [15:0] chksum );

wire [31:0] sumA = header[159:144] + header[143:128] + header[127: 112] + header[111:96] +
                   header[ 95: 80] + header[ 79: 64] + header[ 63:  48] + header[ 47:32] +
						                                     header[ 31:  16] + header[ 15: 0];
assign chksum = ~(sumA[31:16] + sumA[15:0]);

endmodule
