////////////////////////////////////////////////////////////////////////////////////////
//        decimal                  binary                  CRC-N                      //
//          00823    |          001010101    |   shift message by N bits (* x^N)      //
//         _______   |         ___________   |       _______________  then / GGGG     //
//     15 | 12345    |    101 | 110101101    | GGGG | mmmmmmmmmm000  remainder is CRC //
//  8*15=   120      |          101          |                                        //
//            34     |  1AD/5    *110        |   at each bit of running remainder ... //
//  2*15=     30     |  429/5     101        |      either subtract GGGG (if msb=1)   //
//             45    | = 85 r4     *111      |      or do nothing (* in middle pane)  //
//  3*15=      45    |              101      |       ** => msb -> 0 always **         //
//                   | 85 = x55      1001    |   **But not true long division         //
//                   |                101    |   **use bitwise XOR not subtract       //
// 12345/15 = 823 r0 | 429/5 = 85 r4  100    |     eg last row in prev pane would do  //
//                   |                       |        100^101 not 1001-101            //
//------------------------------------------------------------------------------------//
// CRC-N has N+1 coeffs (above GGGG has 4 bits and N=3)                               //
// use n-bit reg to hold current remainder                                            //
// since upper bit always goes to zero at each step - dont bother storing it          //
//    just shift it out (but remember if it was set)                                  //
//                                                                                    //
// init shiftreg to N-msb of message                                                  //
// loop over message bits                                                             //
//    leftshift - (next message bit in at right)                                      //
//    if bit shifted out was set - xor with GGGG                                      //
//                                                                                    //
//----------------------------    ieee803.2 text      --------------------------------//
// first 32bits of frame inverted, nbits of message are coeffs of M(x) of degree n-1  //
// first message bit is x^(n-1) term, last message bit is x^0 term                    //
// x^32*M(x)/G(x) = ?? Remainder R(x),  R(x) degree <=31 - complement R(x) -> CRC bits//
// CRC bits are transmitted x^31 ... x^0                                              //
// G(X) = x^32+x^26+ ... +x^4+x^2+x+1                                                 //
////////////////////////////////////////////////////////////////////////////////////////

// combinatorial only - should run ok up to ??? Mhz (require 125)
module crc32_eth ( input wire [7:0] data, input wire [31:0] prv_crc, output wire [31:0] new_crc );

wire  [7:0] d = data;
wire [31:0] c = prv_crc;

assign new_crc[ 7] = d[1]^d[7]^                          c[31]^c[25];
assign new_crc[ 6] = d[0]^d[1]^d[6]^d[7]^                c[31]^c[30]^c[25]^c[24];
assign new_crc[ 5] = d[0]^d[1]^d[5]^d[6]^d[7]^           c[31]^c[30]^c[29]^c[25]^c[24];
assign new_crc[ 4] = d[0]^d[4]^d[5]^d[6]^                c[30]^c[29]^c[28]^c[24];
assign new_crc[ 3] = d[1]^d[3]^d[4]^d[5]^d[7]^           c[31]^c[29]^c[28]^c[27]^c[25];
assign new_crc[ 2] = d[0]^d[1]^d[2]^d[3]^d[4]^d[6]^d[7]^ c[31]^c[30]^c[28]^c[27]^c[26]^c[25]^c[24];
assign new_crc[ 1] = d[0]^d[1]^d[2]^d[3]^d[5]^d[6]^      c[30]^c[29]^c[27]^c[26]^c[25]^c[24];
assign new_crc[ 0] = d[0]^d[2]^d[4]^d[5]^d[7]^           c[31]^c[29]^c[28]^c[26]^c[24];

assign new_crc[15] = d[3]^d[4]^d[6]^d[7]^                c[ 7]^c[31]^c[30]^c[28]^c[27];
assign new_crc[14] = d[2]^d[3]^d[5]^d[6]^                c[ 6]^c[30]^c[29]^c[27]^c[26];
assign new_crc[13] = d[2]^d[4]^d[5]^d[7]^                c[ 5]^c[31]^c[29]^c[28]^c[26];
assign new_crc[12] = d[3]^d[4]^d[6]^d[7]^                c[ 4]^c[31]^c[30]^c[28]^c[27];
assign new_crc[11] = d[1]^d[2]^d[3]^d[5]^d[6]^d[7]^      c[ 3]^c[31]^c[30]^c[29]^c[27]^c[26]^c[25];
assign new_crc[10] = d[0]^d[1]^d[2]^d[4]^d[5]^d[6]^      c[ 2]^c[30]^c[29]^c[28]^c[26]^c[25]^c[24];
assign new_crc[ 9] = d[0]^d[1]^d[3]^d[4]^d[5]^           c[ 1]^c[29]^c[28]^c[27]^c[25]^c[24];
assign new_crc[ 8] = d[0]^d[2]^d[3]^d[4]^                c[ 0]^c[28]^c[27]^c[26]^c[24];

assign new_crc[23] = d[2]^d[3]^d[7]^                     c[15]^c[31]^c[27]^c[26];
assign new_crc[22] = d[1]^d[2]^d[6]^                     c[14]^c[30]^c[26]^c[25];
assign new_crc[21] = d[0]^d[1]^d[5]^                     c[13]^c[29]^c[25]^c[24];
assign new_crc[20] = d[0]^d[4]^                          c[12]^c[28]^c[24];
assign new_crc[19] = d[3]^                               c[11]^c[27];
assign new_crc[18] = d[2]^                               c[10]^c[26];
assign new_crc[17] = d[7]^                               c[ 9]^c[31];
assign new_crc[16] = d[1]^d[6]^d[7]^                     c[ 8]^c[31]^c[30]^c[25];

assign new_crc[31] = d[0]^d[5]^d[6]^                     c[23]^c[30]^c[29]^c[24];
assign new_crc[30] = d[4]^d[5]^                          c[22]^c[29]^c[28];
assign new_crc[29] = d[1]^d[3]^d[4]^d[7]^                c[21]^c[31]^c[28]^c[27]^c[25];
assign new_crc[28] = d[0]^d[2]^d[3]^d[6]^                c[20]^c[30]^c[27]^c[26]^c[24];
assign new_crc[27] = d[1]^d[2]^d[5]^                     c[19]^c[29]^c[26]^c[25];
assign new_crc[26] = d[0]^d[1]^d[4]^                     c[18]^c[28]^c[25]^c[24];
assign new_crc[25] = d[0]^d[3]^                          c[17]^c[27]^c[24];
assign new_crc[24] = d[2]^                               c[16]^c[26];

endmodule
