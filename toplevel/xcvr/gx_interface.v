module gx_interface(RSTn,
    rx_inclk, rx_outclk, rx_ctrlin, rx_datain, rx_dataout, rx_ctrlout, 
	 tx_inclk, tx_outclk, tx_ctrlin, tx_datain, tx_dataout, tx_ctrlout
);

input  wire RSTn;
input  wire rx_inclk,  rx_outclk;  input  wire tx_inclk,   tx_outclk;
input  wire rx_ctrlin, tx_ctrlin;  output wire rx_ctrlout, tx_ctrlout;
input  wire [7:0] rx_datain;       input  wire [7:0] tx_datain;
output wire [7:0] rx_dataout;      output wire [7:0] tx_dataout;

sync_fifo9 rx4g_fifo_1 (
   .Rclk(rx_outclk ), .RdEn(1'b1),  .data_out({rx_ctrlout,rx_dataout[7:0]}),
   .Wclk(rx_inclk),   .WrEn(1'b1),  .data_in ({rx_ctrlin, rx_datain [7:0]}),
	.RSTn(RSTn)
);

sync_fifo9 tx4g_fifo_0 (
   .Rclk(tx_outclk),  .RdEn(1'b1),   .data_out({tx_ctrlout,tx_dataout[7:0]}),  
   .Wclk(tx_inclk),   .WrEn(1'b1),   .data_in ({tx_ctrlin, tx_datain [7:0]}),
	.RSTn(RSTn)
);

endmodule
