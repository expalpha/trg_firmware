// not really a fifo - but if read remains a little behind write, behaves like one
module sync_fifo ( Rclk, RdEn, data_out, Wclk, WrEn, data_in, RSTn );

input  wire Rclk, Wclk, RdEn, WrEn, RSTn;
output wire [8:0] data_out = fifo_out[8:0];
input  wire [8:0] data_in;

reg [7:0] read_addr; reg [7:0] write_addr;

always @ (posedge Rclk or negedge RSTn) begin
   if (!RSTn)      read_addr <= 8'h00;
   else if( RdEn ) read_addr <= read_addr + 8'h01;
	else            read_addr <= read_addr;
end

always @ (posedge Wclk or negedge RSTn) begin
   if (!RSTn)      write_addr <= 8'h01; // read_addr must be < write_addr
   else if( WrEn ) write_addr <= write_addr + 8'h01;
	else            write_addr <= write_addr;
end

wire [9:0] fifo_out;  wire [9:0] fifo_in = {1'b0,data_in}; 
fifo_dpram fifo_mem ( // memory is 10x256bits (arria memories are multiples of 10bits)
   .wrclock(Wclk),   .wren(WrEn),   .wraddress(write_addr),   .data(fifo_in),	
   .rdclock(Rclk),                  .rdaddress(read_addr ),     .q(fifo_out)
);

endmodule
