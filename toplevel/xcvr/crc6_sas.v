// 6bit crc crc6-itu[0x21 = 6'b100001 = x^6+x^1[+x^0] - koopman notation] has hd=2, and hd=3 to 57bits
module crc6_sas ( input wire [7:0] data, input wire [5:0] prv_crc, output wire [5:0] new_crc );

wire [7:0] d = data;  wire [6:0] c = prv_crc;

// reverse data bits if D[0] isnt first [7-0 6-1 5-2 4-3 3-4 2-5 1-6 0-7]
// also bit-reversed and byte-swapped crc32 for eth readout order
assign new_crc[0] = d[1]^d[2]^d[7]      ^ c[3]^c[4];
assign new_crc[1] = d[0]^d[2]^d[6]^d[7] ^ c[3]^c[5];
assign new_crc[2] = d[1]^d[5]^d[6]      ^ c[0]^c[4];
assign new_crc[3] = d[0]^d[4]^d[5]      ^ c[0]^c[1]^c[5];
assign new_crc[4] = d[3]^d[4]           ^ c[1]^c[2];
assign new_crc[5] = d[2]^d[3]           ^ c[2]^c[3];

endmodule
