// data sent in reverse order - lsbyte first - 8.25bytes of data+type, then 0.75byte of crc
// so packet is:  fb,[7:0],[15:8]...[63:55],[7:2=crc/1:0=type] (type - data:0 param:2 ack:3)
// param and data both 64bits, ack only 56bits - msb is idle,7'h0 (linkid replaces lower4)
//    so acks are ... fb,7xCnt,IL,?T  I=8/0 L=link T=3,7,b,f
// Update: 
//    up speed to 32bits/clk [5Gbits/s]
//    now want to send variable length using fd for eop
//    to keep message multiple of 32bits, use crc14 and keep 2bit datatype, [crc12 is as good as crc14 up to 2kbits]
//  BUT will require at least one idle between packets -> 0 or 32bits spare, not 16 (unless use two idles)
//    max pkt size? (need enough inter-pkt idle for rate-match to succeed)
//       clkdiff       =    1% => need one idle per 100 byte-pairs 
//              100ppm = 0.01% => one idle per 20kbytes
//       insert/remove SKP symbol or ordered set from the IPG or idle-streams
//       rate-match fifo is 20 words deep
//       PCIe mode: SKP set is K28.5[com],3*K28.0[SKP], which can then be modified to have between one and five SKPs
//       XAUI mode: K28.0[R] are sent during IPG, these can be duplicated/removed as necessary
//       GIGe mode: I1 or I2 sent during IPG, fifo can insert/delete I2 (and part of C2 duing autoneg)
//       basic    : define 2 20bit seqences [10bit ctrl, 10bit skp] - can delete/dup skip part after ctrl part
// 
//         <--------- PMA ------>   <--------------------------------------- PCS --------------------------------->  <- FPGA
// pin <-1bit-> serdes <-8/10/16/20 bits-> WordAlign   DeSkew    RateMatch 8/10 ByteDeser ByteOrder PhaseComp <-8to40 bits->
//                                      mux curr/prv  16wd fifo  20wd Fifo                          4 or 8 wd
//                                       word bits   align lanes
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// receive/send 32/64bit data words, from 8bit+control link 
module gx_sas ( input wire [5:0] id, output reg crcerr,  output wire [383:0] scalar_dataout,
   input  wire clk, input  wire reset, output wire debug, input wire TxData_active,  input wire [23:0] runtime,
   input  wire   [7:0]  RxDataIn,  input  wire          RxCtrlIn,  // from xcvr
   output reg    [7:0] TxDataOut,  output reg          TxCtrlOut,  // to   xcvr
	input  wire  [63:0] CmdDataIn,  output reg  [63:0] CmdDataOut,  // param in and out
 	input  wire  [31:0]    TxData,  input  wire       TxDataReady,  output wire TxDataAck, // event data in
 	output wire  [31:0]    RxData,  output wire       RxDataReady,  input  wire RxDataAck, // event data out
 	input  wire  [10:0]  StDataIn,  output wire [10:0]  StDataOut  // st in and out: 8bit data[7:0], sop[8], eop[9], rdy[10]
);
assign RxDataReady = !databuf_empty;  assign debug = crcerr^chkerr^prev_words[11]^prev_words[1]^prev_words[0];
assign scalar_dataout = {8'h0,remote_lost,rx_scalar,tx_scalar};
wire run_start =  TxData_active && !txac_reg; reg txac_reg; always @ (posedge clk) txac_reg <= TxData_active; // oneshot
wire run_stop  = !TxData_active &&  txac_reg; // oneshot on fall

wire st_remote_ready = st_ready;  // set in rx routine
wire st_ready_out = ( (tx_count == 4'h0) && !ackpkt_ready && !parpkt_ready && !txpkt_ready && st_remote_ready);
wire st_ready_in  = StDataIn[10];  // st_ready_in - communicated to remote during idle periods
assign StDataOut[10] = st_ready_out;

wire st_rx_eop = (st_receive && RxDataIn == 8'hFD && RxCtrlIn == 1'b1);
wire st_rx_sop = (st_receive && !prv_st_receive);
assign StDataOut[7:0] = (st_receive) ? RxDataIn : 8'h0;
assign StDataOut[8] = st_rx_sop;
assign StDataOut[9] = st_rx_eop;

////////////////////////////    data transmission flow control    ////////////////////////////////////////////////
reg [55:0] rxdata_count; // count words read out of databuf [will send in acks to link partner]
always @ (posedge clk) begin
//   if( reset || run_start ) rxdata_count <= 56'h0;
   if( reset || run_start ) rxdata_count <= 56'hFFFFFFFFFFFFFFD; // work round grif16 sas bug till updated
	else if( RxDataAck     ) rxdata_count <= rxdata_count + 56'h1;
	else                     rxdata_count <= rxdata_count;
end
reg [55:0] txdata_count; // count words read into this module
always @ (posedge clk) begin
   if( reset || run_start ) txdata_count <= 56'h0;
	else                     txdata_count <= txdata_count + {54'h0,TxAckCnt};
end
reg  [55:0] remote_lost; // words lost in transmission (inferred from counters and remote state)
reg [31:0] data_idle_cnt;
wire data_idle /* synthesis keep */ = (data_idle_cnt > 32'h100); // no DATA transmission for > 256 clks [2us]
always @ (posedge clk) data_idle_cnt <= ( reset || run_start || TxAckCnt[0] || TxAckCnt[1] ) ? 32'h0 : data_idle_cnt + 32'h1;
always @ (posedge clk) begin
   if(      reset || run_start       ) remote_lost <= 56'h0;
	else if( remote_idle && data_idle ) remote_lost <= txdata_count - ackcnt_in[55:0];
	else                                remote_lost <= remote_lost;
end

wire [15:0] remote_bufsiz = 16'hFF0; // size of databuf below in rx section (minus a few words margin)
reg  [55:0] remote_count;            // words still in remote buffer
always @ (posedge clk) remote_count <= txdata_count - ackcnt_in[55:0] - remote_lost;

/////////////////////    data transmission packet selection [data/param/ack]    ////////////////////////////////////
// data output - get pairs of 32bit data words to make 64bit trasmission
// use "ackcnt_in" from rcv'd acks to avoid overflowing remote buffer - calculations above
reg [1:0] TxAckCnt; // for inserted data
reg [63:0] TxData_reg;   reg [1:0] tx_pkt_count;  reg [11:0] ev_words;  reg [11:0] prev_words; 
wire remote_busy /* synthesis keep */ = (remote_count >= remote_bufsiz);
wire txpkt_busy  = txpkt_ready || txpkt_ready_set;
assign TxDataAck = TxData_active && TxDataReady && !txpkt_busy && !remote_busy;
always @ (posedge clk or posedge reset) begin
   if( reset ) begin
		TxData_reg   <= 64'h0;  TxAckCnt        <= 2'h0;  prev_words <= 12'h0;
		tx_pkt_count <=  2'h0;  txpkt_ready_set <= 1'b0;  ev_words   <= 12'h0;
	end else begin
		TxData_reg   <=   TxData_reg;  TxAckCnt        <= 2'h0;  prev_words <= prev_words;
	   tx_pkt_count <= tx_pkt_count;  txpkt_ready_set <= 1'b0;  ev_words   <= ev_words;
      if( TxData_active && TxDataReady && !txpkt_busy && !remote_busy ) begin
         TxData_reg <= {TxData,TxData_reg[63:32]}; TxAckCnt <= 2'h1; ev_words <= ev_words + 12'h1;
		   if( TxData[31:28] == 4'h8 ) ev_words <= 12'h1;
		   if( TxData[31:28] == 4'hE ) prev_words <= ev_words + 12'h1;
		   if( tx_pkt_count == 2'h0 && TxData[31:28] == 4'hE ) begin // don't wait after endofevent - pad to 64bits and send
			   TxData_reg <= {32'hDEADDEAD,TxData};
			   tx_pkt_count <= 2'h0;  txpkt_ready_set <= 1'b1;
			end else if( tx_pkt_count == 2'h1 ) begin // just added second 32bits
			   tx_pkt_count <= 2'h0;  txpkt_ready_set <= 1'b1;
         end else tx_pkt_count <= tx_pkt_count + 2'h1;
		end
	end
end

// parameter output - single parameter is 64bits
reg [63:0] param_reg;
always @ (posedge clk or posedge reset) begin
   if( reset ) begin
	   param_reg <=     64'h0;  parpkt_ready_set <= 1'b0;
   end else begin
	   param_reg <= param_reg;  parpkt_ready_set <= 1'b0;
		if( CmdDataIn[63] ) begin
		   if( ! parpkt_ready ) begin
		      param_reg <= CmdDataIn;  parpkt_ready_set <= 1'b1;
			end // else - **dropped parameter** 
		end
   end
end

// [on timer send ack every X Clks] send less often when no data has been processed
// data counters are 56bits - leaving 8 spare bits ... use one of the spares to indicate idle state 
//    (empty buffer, no transfer since last ack) allows monitoring of dropped data
reg [63:0] ack_reg;  reg [31:0] ack_timer;  reg [55:0] prev_rxdatacount;  reg rx_idle;
always @ (posedge clk or posedge reset) begin
   if( reset ) begin
	   ack_reg <=   64'h0;  ackpkt_ready_set <= 1'b0;  ack_timer <= 32'h0;
		prev_rxdatacount <= 56'h0;  rx_idle <= 1'b0;
   end else begin
	   ack_reg <= ack_reg;  ackpkt_ready_set <= 1'b0;  ack_timer <= ack_timer + 32'h1;
		prev_rxdatacount <= prev_rxdatacount; // count on prev ack (not on prev clk)
		rx_idle <= (rxdata_count == prev_rxdatacount) && databuf_empty;
		if( ack_timer >= 32'h1000000 ) begin // 10000000=250M[2seconds] 100000=1M[8ms] 1000=4k[32us]
		   if( ! ackpkt_ready ) begin
		      ack_reg <= {rx_idle,7'h0,rxdata_count};  ackpkt_ready_set <= 1'b1;  ack_timer <= 32'h0;  prev_rxdatacount <= rxdata_count;
			end // else - **dropped ack** 
		end else if( ack_timer >= 32'h1000 ) begin // send every 128ms if inactive [30us if active]
		   if( ! ackpkt_ready && rxdata_count != prev_rxdatacount ) begin
		      ack_reg <= {rx_idle,7'h0,rxdata_count};  ackpkt_ready_set <= 1'b1;  ack_timer <= 32'h0;  prev_rxdatacount <= rxdata_count;
			end // else - **dropped/skipped ack** 
		end
   end
end

// transmit data selection
reg parpkt_ready, parpkt_ready_set, parpkt_ready_clr;
reg txpkt_ready,  txpkt_ready_set,  txpkt_ready_clr;
reg ackpkt_ready, ackpkt_ready_set, ackpkt_ready_clr;
always @ (posedge clk or posedge reset) begin
   if( reset ) begin
   	txpkt_ready <= 1'h0;  parpkt_ready <= 1'h0;  ackpkt_ready <= 1'h0;
	end else begin
	   txpkt_ready <= txpkt_ready;  parpkt_ready <= parpkt_ready; ackpkt_ready <= ackpkt_ready;
		if(      ackpkt_ready_set ) ackpkt_ready <= 1'b1;
		else if( ackpkt_ready_clr ) ackpkt_ready <= 1'b0;
		if(      txpkt_ready_set  ) txpkt_ready  <= 1'b1;
		else if( txpkt_ready_clr  ) txpkt_ready  <= 1'b0;
		if(      parpkt_ready_set ) parpkt_ready <= 1'b1;
		else if( parpkt_ready_clr ) parpkt_ready <= 1'b0;
	end
end

/////////////////////////       Sas Transmit routine          /////////////////////////
wire [5:0] new_tx_crc;  wire [5:0] prev_tx_crc = ( tx_count > 4'h1 ) ? tx_crc : 6'h3F;
crc6_sas  tx_crc6 ( .data(TxDataOut), .prv_crc(prev_tx_crc), .new_crc(new_tx_crc) );

reg [7:0] prvchk; reg chkerr;  reg st_transmit; // tx_scalar: idle[31:0] data[63:32] ack[95:64] param[127:96] 
reg [65:0] tx_reg;  reg [5:0] tx_crc;  reg [29:0] tx_idle_cnt;  reg [3:0] tx_count; reg [127:0] tx_scalar;
always @ (posedge clk or posedge reset) begin
   if( reset ) begin
	   tx_reg      <=  66'h0;  parpkt_ready_clr <= 1'b0;  txpkt_ready_clr <=  1'b0;  tx_crc    <= 6'h3F;  prvchk <= 8'h0;  st_transmit <= 1'b0;
		tx_count    <=   4'h0;  ackpkt_ready_clr <= 1'b0;  tx_idle_cnt     <= 30'h0;  tx_scalar <= 128'h0;  chkerr <= 1'b0;
	end else begin
	   tx_reg      <= tx_reg;  parpkt_ready_clr <= 1'b0;  txpkt_ready_clr <=  1'b0;     tx_crc <= new_tx_crc; chkerr <= chkerr;
		tx_count  <= tx_count;  ackpkt_ready_clr <= 1'b0;  tx_idle_cnt <= tx_idle_cnt + 30'h1;  prvchk <= prvchk;    st_transmit <= st_transmit;
      tx_scalar <= ( run_start ) ? 128'h0 : tx_scalar;
		if(  StDataIn[8] /* sop */ ) begin       // treated separately from other items - use K23.7[F7] - CarExt for st-sop
			TxCtrlOut <= 1'b1;  TxDataOut <= 8'hF7;  st_transmit <= 1'b1;
		   tx_reg[10:0] = {1'b0,StDataIn[9:0]};  // save eop if present, also bit[10] set => last data written - end pkt nxt
		end else if( st_transmit  ) begin
		   TxCtrlOut <= 1'b0;  TxDataOut <= tx_reg[7:0];  tx_reg[10:0] = {1'b0,StDataIn[9:0]};
			if(      tx_reg[10] ) begin TxCtrlOut <= 1'b1;  st_transmit <= 1'b0; end
         else if(  tx_reg[9] )       tx_reg[10:0] = {1'b1,2'h0,8'hFD};
		end else if( tx_count == 4'h0 ) begin                            // currently idle
         TxDataOut <= ( tx_idle_cnt[0] == 1'b0 ) ? 8'hBC: ( (st_ready_in) ? 8'h50 : 8'hC5);       // K28.5/D16.2 or D5.6[C5] if not ready
		   TxCtrlOut <= ( tx_idle_cnt[0] == 1'b0 ) ? 1'b1 : 1'b0;                                   // sop is K27.7[FB], eop is K29.7[FD]
			if( 1'b0 && tx_idle_cnt < 30'h4 ) begin                               // remain idle until sent at least 2 K28.5/D16.2 patterns
			   tx_scalar[31:0] <= tx_scalar[31:0] + 32'h1; 
		   end else if( parpkt_ready && !tx_idle_cnt[0] ) begin             // transmit param  ... start on even tx_idle_count 
			   tx_reg <=  {2'h2,param_reg};  tx_count <= 4'h1;  parpkt_ready_clr <= 1'b1;            // i.e. don't split idle group
				TxDataOut <= 8'hFB; TxCtrlOut <= 1'b1;                       // :sop
			   tx_scalar[127:96] <= tx_scalar[127:96] + 32'h1;
		   end else if( ackpkt_ready && !tx_idle_cnt[0] ) begin             // transmit ack 
			   tx_scalar[95:64] <= tx_scalar[95:64] + 32'h1;
			   tx_reg <= {2'h3,ack_reg[63],1'h0,id[5:0],ack_reg[55:0]};  tx_count <= 4'h1;  ackpkt_ready_clr <= 1'b1;
				TxDataOut <= 8'hFB; TxCtrlOut <= 1'b1;
		   end else if( txpkt_ready && !tx_idle_cnt[0] ) begin             // transmit data 
			   tx_scalar[63:32] <= tx_scalar[63:32] + 32'h1;
			   tx_reg <= {2'h0,TxData_reg};  tx_count <= 4'h1;  txpkt_ready_clr <= 1'b1;
				TxDataOut <= 8'hFB; TxCtrlOut <= 1'b1;
				prvchk <= TxData_reg[39:32]; chkerr <= (TxData_reg[7:0] != (prvchk+8'h1)) || (TxData_reg[39:32] != (TxData_reg[7:0]+8'h1));
			end else tx_scalar[31:0] <= tx_scalar[31:0] + 32'h1;            // remain idle
		end else begin
		   tx_idle_cnt <= 30'h0;
			if( tx_count == 4'h9 ) begin // after sop+8 data bytes, send checksum and end transmission
			   TxDataOut <= {new_tx_crc,tx_reg[1:0]};     TxCtrlOut <= 1'b0;   tx_count <= 4'h0; 
			end else begin
   		   tx_count  <= tx_count + 4'h1;   tx_reg    <= {8'h0,tx_reg[65:8]}; // right shift
			   TxDataOut <= tx_reg[7:0];       TxCtrlOut <= 1'b0;
			end
		end
	end
end

/////////////////////////        Sas Receive routine           /////////////////////////
wire databuf_full; wire databuf_empty /* synthesis keep */;
reg [31:0] databuf_in; reg databuf_write;

sczlfifo evbuf ( .clk(clk),  .aclr(reset||run_start||run_stop),   //  4k words - will hold 80us sample!
   .data(databuf_in),   .wrreq(databuf_write), .full(databuf_full),
   .q(RxData),          .rdreq(RxDataAck),     .empty(databuf_empty)
);

// 64bit_link -> 32bit_buffer interface, drop and padding words [32'hDEADDEAD]
// can write 32bits per clk into buffer, link currently provides < 8bits/clk
// plenty of time to add extra diagnostics
reg [63:0] rx_buf_in;  reg rx_buf_rdy_in;  reg [1:0] rx_wrd_cnt;
always @ (posedge clk or posedge reset) begin
   if( reset ) begin
      rx_wrd_cnt <=       2'h0;  databuf_in <=      32'h0;  databuf_write <= 1'b0;
	end else begin
      rx_wrd_cnt <= rx_wrd_cnt;  databuf_in <= databuf_in;  databuf_write <= 1'b0;
		if( rx_buf_rdy_in ) begin
			if( rx_buf_in[31:28] == 4'h8 ) rx_wrd_cnt <= 2'h2; // add diagnostics
		   rx_wrd_cnt <= 2'h1;  databuf_in <= rx_buf_in[31: 0];
			databuf_write <= (rx_buf_in[31: 0] != 32'hDEADDEAD ) ? 1'b1 : 1'b0;
		end else if( rx_wrd_cnt == 2'h1 ) begin
		   rx_wrd_cnt <= 2'h0;  databuf_in <= rx_buf_in[63:32];
			databuf_write <= (rx_buf_in[63:32] != 32'hDEADDEAD ) ? 1'b1 : 1'b0;
		end else if( rx_wrd_cnt == 2'h2 ) begin // diagnostics
		   rx_wrd_cnt <= 2'h1;  databuf_in <= {8'hC3,rx_scalar[183:160]}; databuf_write <= 1'b1;
		end
	end
end

wire [5:0] new_rx_crc;  wire [5:0] prev_rx_crc = ( rx_count != 4'h0 ) ? rx_crc : 6'h3F;
crc6_sas  rx_crc6 ( .data(RxDataIn), .prv_crc(prev_rx_crc), .new_crc(new_rx_crc) );

// rx_scalar: idle[31:0] data[63:32] ack[95:64] param[127:96] unknown[159:128] error[191:160]
wire remote_idle = ackcnt_in[63];  reg st_receive;  reg prv_st_receive;  reg st_ready;   
reg [63:0] rx_reg;  reg [3:0] rx_count;  reg [3:0] rx_idle_count;  reg [63:0] ackcnt_in;  reg [5:0] rx_crc;  reg [191:0] rx_scalar;
always @ (posedge clk or posedge reset) begin
   if( reset ) begin
      rx_reg    <=  64'h0;  rx_count      <= 4'h0;  ackcnt_in  <= 64'h0;  crcerr <= 1'b0;  rx_idle_count <= 4'h0;  st_receive <= 1'b0;  prv_st_receive <= 1'b0;
		rx_buf_in <=  64'h0;  rx_buf_rdy_in <= 1'b0;  CmdDataOut <= 64'h0;  rx_crc <= 6'h3F;  rx_scalar <= 192'h0;   st_ready <= 1'b0; 
	end else begin
      rx_reg    <= rx_reg;     rx_count <= rx_count;   ackcnt_in <= ackcnt_in;  crcerr <= crcerr;   st_receive <= st_receive;   prv_st_receive <= st_receive;  
		rx_buf_in <= rx_buf_in;  rx_buf_rdy_in <= 1'b0;  rx_crc   <= new_rx_crc;  rx_idle_count <= rx_idle_count;    st_ready <= st_ready;
		rx_scalar <= ( run_start ) ? 128'h0 : rx_scalar;  
		CmdDataOut <= {1'b0,CmdDataOut[62:0]};
		if( RxDataIn == 8'hF7 && RxCtrlIn == 1'b1 ) st_receive <= 1'b1; // st sop
		else if( st_receive && RxDataIn == 8'hFD && RxCtrlIn == 1'b1 ) st_receive <= 1'b0;
		else if( rx_count == 4'h0 ) begin // currently idle
			rx_scalar[31:0] <= rx_scalar[31:0] + 32'h1;  rx_idle_count <= rx_idle_count + 4'h1;
			if( rx_idle_count == 4'hF ) crcerr <= 1'b0; // clear 15 clks after bad pkt
		   if( RxDataIn == 8'hFB && RxCtrlIn == 1'b1 ) rx_count <= 4'h1; // K28.7 - start of packet
			if( RxDataIn == 8'h50 && RxCtrlIn == 1'b0 ) st_ready <= 1'b1; // normal idle
			if( RxDataIn == 8'hC5 && RxCtrlIn == 1'b0 ) st_ready <= 1'b0; // alternate idle - remote not ready
 		end else begin // receiving data ...
		   //if( RxCtrlIn == 1'b1 ) // error
	      rx_count <= rx_count + 4'h1;
			if( rx_count == 4'h9 ) begin // last 8 bits of data have now just shifted into rxdata
			   rx_count     <= 4'h0;
				if( |(RxDataIn[7:2] ^ rx_crc) ) begin crcerr <= 1'b1; rx_scalar[191:160] <= rx_scalar[191:160] + 32'h1; end
				else if(     RxDataIn[1:0] == 2'h0 ) begin // event data
				   rx_buf_in <=  rx_reg;  rx_buf_rdy_in <= 1'b1;
					rx_scalar[63:32] <= rx_scalar[63:32] + 32'h1;
				end else if( RxDataIn[1:0] == 2'h2 ) begin // parameter
				   CmdDataOut <= {1'b1,rx_reg[62:0]};
					rx_scalar[127:96] <= rx_scalar[127:96] + 32'h1;
				end else if( RxDataIn[1:0] == 2'h3 ) begin // ack
				   ackcnt_in <= rx_reg[63:0];
					rx_scalar[95:64] <= rx_scalar[95:64] + 32'h1;
				end else begin //error - unknown transmission type
				   rx_scalar[159:128] <= rx_scalar[159:128] + 32'h1;
				end
			end else rx_reg <= {RxDataIn,rx_reg[63:8]}; // right shift ** remove any inserted idle's and adjust rx_count
		end
	end
end

endmodule

