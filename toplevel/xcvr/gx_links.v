// data flow ...   <gxlink>  ->  gx_RxDataOut -> <gx_interface> -> sas_RxDataOut -> <gx_sas> -> dcc_RxDataOut -> <dcc> -> MstData
//                 <gxlink>  <-  gx_TxDataIn  <- <gx_interface> <- sas_TxDataIn  <- <gx_sas> <- dcc_TxDataIn  <- <dcc> <- MstData
// 
// parameter routing ...  (format:  [62]:R/Wn [47:44]:mstprt [43:40]slvprt [39:32]:chan [31;0]:val)
//    if parameter request comes in not via master, it is marked with all 1's for mst/slvport (does not match real configuration)
//    and zero just before start of good portion of address   i.e. dest=grif16-sfp => mstslv pattern is fe ... slv:6X[110xxxx]
//    (master always sends out it's sfp) - this scheme means master port14 is unusable (also port15 is used for param block)
`default_nettype none
module gx_links
  (
   input wire [7:0]   board_id,
   //input wire 	      RSTn,
   input wire 	      RSTn_io_clk,
   input wire 	      sys_clk,
   input wire 	      io_clk,
   input wire 	      io_clk2,
   input wire [63:0]  linkparreply,
   output wire [63:0] linkpar,
   input wire [31:0] avalon_data,
   input wire avalon_sop,
   input wire avalon_eop,
   input wire avalon_valid,
   output wire avalon_ready_out,
   //input wire [23:0]  FMCX_RX,
   //output wire [23:0] FMCX_TX,
   input wire [3:0]   FMCX_RX_block3,
   output wire [3:0]  FMCX_TX_block3,
   input wire [3:0]   FMCX_RX_block4,
   output wire [3:0]  FMCX_TX_block4,
   output wire        eth_rx_clk_out,
   output wire 	      eth_led_out
   );

   assign eth_rx_clk_out = rx_recovclkout[8];

   //wire 	      reset = !RSTn;
   wire 	      reset_io_clk = !RSTn_io_clk;
   
   wire [63:0] sfp_CmdDataOut;
   wire [63:0] sfp_CmdDataIn = linkparreply;
   assign linkpar = sfp_CmdDataOut;
   
   ////////////////////////////////////////////////////////////////////////////////////////////////////////
   /////////////////////////////////     data sink sfp/sas blocks         /////////////////////////////////
   
   wire [31:0] 	 sas_TxDataIn_95_64;
   wire [31:0] 	 sas_RxDataOut_95_64;

   wire [7:0] 	 sas_TxDataIn_71_64;
   wire [7:0] 	 sas_RxDataOut_71_64;

   assign sas_TxDataIn_71_64[7:0] = sas_TxDataIn_95_64[7:0];
   assign sas_RxDataOut_71_64[7:0] = sas_RxDataOut_95_64[7:0];

   wire  [47:0]  localmac = ( board_id == 8'h00) ? 48'h420000000400 : //
                         ( board_id == 8'h11) ? 48'h420000000401 : // 
                         ( board_id == 8'h22) ? 48'h420000000402 : // 
                         ( board_id == 8'h33) ? 48'h420000000403 : // 
                         ( board_id == 8'h44) ? 48'h420000000404 : // 
                                                48'h420000000405;  // 

   //wire [31:0]   locipadr = ( board_id == 8'h00) ? 32'hC0A80132 :     // 192.168.1.50
   //                      ( board_id == 8'h11) ? 32'hC0A80133 :     // 192.168.1.51
   //                      ( board_id == 8'h22) ? 32'hC0A80134 :     // 192.168.1.52
   //                      ( board_id == 8'h33) ? 32'hC0A80135 :     // 192.168.1.53
   //                      ( board_id == 8'h44) ? 32'hC0A80136 :     // 192.168.1.54
   //                                             32'hC0A801FB;      // 192.168.1.251

   reg           xcvr_ready1, xcvr_ready2, xcvr_ready;

   always_ff @ (posedge io_clk) begin
      xcvr_ready1 <= rx_syncstatus[8];
      xcvr_ready2 <= xcvr_ready1;
      xcvr_ready  <= xcvr_ready2;
   end

   gx_sfp sfp9
     (
      .clk(io_clk),
      .reset(reset_io_clk),
      .mac_address(localmac),
      //.RxDataIn (sas_RxDataOut[71:64]),
      .RxDataIn(sas_RxDataOut_71_64),
      .RxCtrlIn (sas_RxCtrlOut[8]),
      .CmdDataIn( sfp_CmdDataIn),
      //.TxDataOut(sas_TxDataIn [71:64]),
      .TxDataOut(sas_TxDataIn_71_64),
      .TxCtrlOut(sas_TxCtrlIn[8] ),
      .CmdDataOut(sfp_CmdDataOut),
      .TxData(avalon_data),
      .TxSop(avalon_sop),
      .TxEop(avalon_eop),
      .TxValid(avalon_valid),
      .TxReadyOut(avalon_ready_out),
      //.xcvr_ready(rx_sigdetct_out[8]),
      .xcvr_ready(xcvr_ready),
      .eth_led_out(eth_led_out)
      );

   ////////////////////////////////////////////////////////////////////////////////////////////////////////
   ///////////////////////////////          data source sfp/sas blocks          ///////////////////////////
   //wire [191:0] sas_TxDataIn;
   //wire [191:0] sas_RxDataOut; // ^^linktx* unconnected: data not sent to data source!
   wire [23:0] 	sas_TxCtrlIn;
   wire [23:0] 	sas_RxCtrlOut;

   //////////////////////////////////////////////////////////////////////////////////////////////////
   // clk transfer fifos to/from gx links
//   gx_interface4 gx_if_a (
//			  .rx_inclk(     rx_clkout[3:0]), .tx_outclk(    tx_clkout[3:0]),  .rx_outclk(io_clk), .tx_inclk(io_clk), .RSTn(pll_locked[0]),
//			  .rx_ctrlin(rx_ctrldetect[3:0]), .rx_ctrlout(sas_RxCtrlOut[3:0]), .rx_datain(gx_RxDataOut[31:0]), .rx_dataout(sas_RxDataOut[31:0]),
//			  .tx_ctrlin( sas_TxCtrlIn[3:0]), .tx_ctrlout(tx_ctrlenable[3:0]), .tx_datain(sas_TxDataIn[31:0]), .tx_dataout(  gx_TxDataIn[31:0])
//			  );
//   gx_interface4 gx_if_b (
//			  .rx_inclk(     rx_clkout[7:4]), .tx_outclk(     tx_clkout[7:4]), .rx_outclk(io_clk), .tx_inclk(io_clk), .RSTn(pll_locked[1]),
//			  .rx_ctrlin(rx_ctrldetect[7:4]), .rx_ctrlout(sas_RxCtrlOut[7:4]), .rx_datain(gx_RxDataOut[63:32]), .rx_dataout(sas_RxDataOut[63:32]),
//			  .tx_ctrlin( sas_TxCtrlIn[7:4]), .tx_ctrlout(tx_ctrlenable[7:4]), .tx_datain(sas_TxDataIn[63:32]), .tx_dataout(  gx_TxDataIn[63:32])
//			  );

   wire [31:0] 	gx_RxDataOut_95_64;
   wire [31:0] 	gx_TxDataIn_95_64;

   gx_interface4 gx_if_c
     (
      .rx_inclk(rx_clkout[11:8]),
      .tx_outclk(tx_clkout[11:8]),
      .rx_outclk(io_clk),
      .tx_inclk(io_clk),
      .RSTn(pll_locked[2]),
      .rx_ctrlin(rx_ctrldetect[11:8]),
      .rx_ctrlout(sas_RxCtrlOut[11:8]),
      //.rx_datain(gx_RxDataOut[95:64]),
      .rx_datain(gx_RxDataOut_95_64),
      //.rx_dataout(sas_RxDataOut[95:64]),
      .rx_dataout(sas_RxDataOut_95_64),
      .tx_ctrlin(sas_TxCtrlIn[11:8]),
      .tx_ctrlout(tx_ctrlenable[11:8]),
      //.tx_datain(sas_TxDataIn[95:64]),
      .tx_datain(sas_TxDataIn_95_64),
      //.tx_dataout(gx_TxDataIn[95:64])
      .tx_dataout(gx_TxDataIn_95_64)
      );

   wire [31:0] 	gx_RxDataOut_127_96;
   wire [31:0] 	gx_TxDataIn_127_96;

//   gx_interface4 gx_if_d
//     (
//      .rx_inclk(rx_clkout[15:12]),
//      .tx_outclk(tx_clkout[15:12]),
//      .rx_outclk(io_clk),
//      .tx_inclk(io_clk),
//      .RSTn(pll_locked[3]),
//      .rx_ctrlin(rx_ctrldetect[15:12]),
//      .rx_ctrlout(sas_RxCtrlOut[15:12]),
//      .rx_datain(gx_RxDataOut[127:96]),
//      .rx_dataout(sas_RxDataOut[127:96]),
//      .tx_ctrlin(sas_TxCtrlIn[15:12]),
//      .tx_ctrlout(tx_ctrlenable[15:12]),
//      .tx_datain(sas_TxDataIn[127:96]),
//      .tx_dataout(gx_TxDataIn[127:96])
//      );

//   gx_interface4 gx_if_e (
//			  .rx_inclk(     rx_clkout[19:16]), .tx_outclk(     tx_clkout[19:16]), .rx_outclk(io_clk), .tx_inclk(io_clk), .RSTn(pll_locked[4]),
//			  .rx_ctrlin(rx_ctrldetect[19:16]), .rx_ctrlout(sas_RxCtrlOut[19:16]), .rx_datain(gx_RxDataOut[159:128]), .rx_dataout(sas_RxDataOut[159:128]),
//			  .tx_ctrlin( sas_TxCtrlIn[19:16]), .tx_ctrlout(tx_ctrlenable[19:16]), .tx_datain(sas_TxDataIn[159:128]), .tx_dataout(  gx_TxDataIn[159:128])
//			  );
//   gx_interface4 gx_if_f (
//			  .rx_inclk(     rx_clkout[23:20]), .tx_outclk(     tx_clkout[23:20]), .rx_outclk(io_clk), .tx_inclk(io_clk), .RSTn(pll_locked[5]),
//			  .rx_ctrlin(rx_ctrldetect[23:20]), .rx_ctrlout(sas_RxCtrlOut[23:20]), .rx_datain(gx_RxDataOut[191:160]), .rx_dataout(sas_RxDataOut[191:160]),
//			  .tx_ctrlin( sas_TxCtrlIn[23:20]), .tx_ctrlout(tx_ctrlenable[23:20]), .tx_datain(sas_TxDataIn[191:160]), .tx_dataout(  gx_TxDataIn[191:160])
//			  );

   //////////////////////////    GX Serial Links    ///////////////////////////////////
   // 1Gbps / 10bits => 100Mhz Parallel clk (8 bits into xcvr, 10 out of link per clk)
   //                =>  50Mhz 16-bit/clk (25Mhz 32-bit/clk)
   // inputs are 32 bit into 4link x 8 bit, at 50Mhz => each data is currently sent twice
   wire [5:0] 	pll_locked;
   wire [23:0] 	rx_freqlocked;
   //wire [191:0] gx_TxDataIn;
   //wire [191:0] gx_RxDataOut;
   wire [23:0] 	tx_ctrlenable;
   wire [23:0] 	rx_ctrldetect;
   wire [23:0] 	tx_clkout;
   wire [23:0] 	rx_clkout;
   assign rx_clkout = tx_clkout; // ratematch fifo now included => now use txclkout only
   
   wire [203:0] FMCX_cfgin;
   wire [23:0] 	FMCX_cfgout;
   wire [5:0] 	reconfig_busy;
   
   wire [23:0] 	rx_digitalreset;
   wire [23:0] 	tx_digitalreset;
   wire [23:0] 	rx_analogreset;
   wire [5:0] 	pll_powerdown;
   
   wire [23:0] 	rx_syncstatus;
   wire [23:0] 	rx_patterndetect;

   wire [23:0] 	rx_disperr_out; // unused
   wire [23:0] 	rx_errdetect_out; // unused
   wire [23:0] 	rx_freqlocked_out; // unused
   //wire [23:0]  rx_sigdetct_out;
   wire [23:0] 	rx_fifodel;
   wire [23:0] 	rx_fifoins;
   wire [23:0] 	rx_fifoful;
   wire [23:0] 	rx_fifoemp;
   wire [11:8]  rx_recovclkout;

   /////////////////////// block 3: channels 1,2,3,4 (-> Master) /////////////////////
   /////////////////////// chan1 is maybe sfp, chan2,3,4,5 mini-sas //////////////////
   // should attach sfp_rstreq somewhere in here (there is no reset port)

   initialise init3
     (
      .clk(io_clk2),
      .reconfig_busy(reconfig_busy[2]),
      .pll_locked(pll_locked[2]),
      .pll_powerdown(pll_powerdown[2]),
      .rx_freqlocked(rx_freqlocked[11:8]),
      .rx_digitalreset(rx_digitalreset[11:8]),
      .tx_digitalreset(tx_digitalreset[11:8]),
      .rx_analogreset(rx_analogreset[11:8])
      );

   FMC_Transceiver block3
     (
      .pll_inclk(                   io_clk2),
      .cal_blk_clk(                 io_clk2),
      .reconfig_clk(                 sys_clk),
      .pll_locked(             pll_locked[2]),
      .pll_powerdown(       pll_powerdown[2]),
      .reconfig_togxb(     FMCX_cfgout[11:8]),
      .reconfig_fromgxb(   FMCX_cfgin[83:68]),
      .rx_datain(              FMCX_RX_block3),
      .tx_dataout(             FMCX_TX_block3),
      //.tx_datain(         gx_TxDataIn[95:64]),
      //.rx_dataout(       gx_RxDataOut[95:64]),
      .tx_datain(         gx_TxDataIn_95_64),
      .rx_dataout(       gx_RxDataOut_95_64),
      .rx_digitalreset(rx_digitalreset[11:8]),
      .tx_digitalreset(tx_digitalreset[11:8]),
      .rx_analogreset(  rx_analogreset[11:8]),
      .rx_cruclk(               {4{io_clk2}}),
      /*.rx_clkout(            rx_clkout[11:8]),*/
      .tx_clkout(            tx_clkout[11:8]),
      .tx_ctrlenable(    tx_ctrlenable[11:8]),
      .rx_ctrldetect(    rx_ctrldetect[11:8]),
      .rx_disperr(      rx_disperr_out[11:8]),
      .rx_errdetect(  rx_errdetect_out[11:8]),
      .rx_freqlocked(    rx_freqlocked[11:8]),
      //.rx_signaldetect(rx_sigdetct_out[11:8]),
      .rx_syncstatus(    rx_syncstatus[11:8]),
      .rx_patterndetect( rx_patterndetect[11:8]),
      .rx_rmfifodatadeleted(rx_fifodel[11:8]),
      .rx_rmfifodatainserted(rx_fifoins[11:8]),
      .rx_rmfifoempty(      rx_fifoemp[11:8]),
      .rx_rmfifofull(       rx_fifoful[11:8]),
      .rx_recovclkout(      rx_recovclkout[11:8])
      );
   
   FMC_Transceiver_reconfig reconfig3
     (
      .reconfig_clk(sys_clk),
      .busy(reconfig_busy[2]),
      .reconfig_fromgxb(FMCX_cfgin[101:68]),
      .reconfig_togxb(FMCX_cfgout[11:8])
      );
   
   /////////////////////// block 4: channels 5-8 /////////////////////
   /////////////////////// chan5 is last on on mini-sas, 6-8 not connected////////////
   initialise init4
     (
      .clk(io_clk),
      .reconfig_busy(reconfig_busy[3]),
      .pll_locked(pll_locked[3]),
      .pll_powerdown(pll_powerdown[3]),
      .rx_freqlocked(rx_freqlocked[15:12]),
      .rx_digitalreset(rx_digitalreset[15:12]),
      .tx_digitalreset(tx_digitalreset[15:12]),
      .rx_analogreset(rx_analogreset[15:12])
      );

   FMC_Transceiver block4
     (
      .pll_inclk(                     io_clk),
      .cal_blk_clk(                   io_clk),  .reconfig_clk(                  sys_clk),
      .pll_locked(              pll_locked[3]),  .pll_powerdown(        pll_powerdown[3]),
      .reconfig_togxb(     FMCX_cfgout[15:12]),  .reconfig_fromgxb(  FMCX_cfgin[117:102]),
      .rx_datain(              FMCX_RX_block4),
      .tx_dataout(             FMCX_TX_block4),
      .tx_datain(         gx_TxDataIn_127_96),
      .rx_dataout(       gx_RxDataOut_127_96),
      .rx_digitalreset(rx_digitalreset[15:12]),  .tx_digitalreset(tx_digitalreset[15:12]),
      .rx_analogreset(  rx_analogreset[15:12]),  .rx_cruclk(                {4{io_clk}}),
      /*.rx_clkout(            rx_clkout[15:12]),*/.tx_clkout(            tx_clkout[15:12]),
      .tx_ctrlenable(    tx_ctrlenable[15:12]),  .rx_ctrldetect(    rx_ctrldetect[15:12]),
      .rx_disperr(      rx_disperr_out[15:12]),  .rx_errdetect(  rx_errdetect_out[15:12]),
      .rx_freqlocked(    rx_freqlocked[15:12]),
      //.rx_signaldetect(rx_sigdetct_out[15:12]),
      .rx_syncstatus(    rx_syncstatus[15:12]),
      .rx_patterndetect( rx_patterndetect[15:12]),
      .rx_rmfifodatadeleted(rx_fifodel[15:12]), .rx_rmfifodatainserted(rx_fifoins[15:12]),
      .rx_rmfifoempty(      rx_fifoemp[15:12]),  .rx_rmfifofull(       rx_fifoful[15:12])
      );

   FMC_Transceiver_reconfig reconfig4
     (
      .reconfig_clk(sys_clk),
      .busy(reconfig_busy[3]),
      .reconfig_fromgxb(FMCX_cfgin[135:102]),
      .reconfig_togxb(FMCX_cfgout[15:12])
      );

//	.reconfig_fromgxb(   FMCX_cfgin[16:0]), ??? 15:0
//	gx_TxDataIn / gx_RxDataOut[31:0]),
//	.reconfig_fromgxb(FMCX_cfgin[33:0]),
// 
//	.reconfig_fromgxb(  FMCX_cfgin[49:34]),
//	gx_TxDataIn / gx_RxDataOut[63:32]),
//	.reconfig_fromgxb(FMCX_cfgin[67:34]),
// 
//	.reconfig_fromgxb(   FMCX_cfgin[83:68]),
//	gx_TxDataIn / gx_RxDataOut[95:64]),
// .reconfig_fromgxb(FMCX_cfgin[101:68]),
// 
//	 .reconfig_fromgxb(  FMCX_cfgin[117:102]),
//	gx_TxDataIn / gx_RxDataOut[127:96]),
//	.reconfig_fromgxb(FMCX_cfgin[135:102]),
// 
//	.reconfig_fromgxb(  FMCX_cfgin[151:136]),
//	gx_TxDataIn / gx_RxDataOut[159:128]),
//	                          [169:136]
// 
//	.reconfig_fromgxb(  FMCX_cfgin[185:170]),
//	gx_TxDataIn / gx_RxDataOut[191:160]),
//                           [203:170]

endmodule
//
