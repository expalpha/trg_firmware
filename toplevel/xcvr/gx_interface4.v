module gx_interface4( RSTn,
    rx_inclk, rx_outclk, rx_ctrlin, rx_datain, rx_dataout, rx_ctrlout, 
	 tx_inclk, tx_outclk, tx_ctrlin, tx_datain, tx_dataout, tx_ctrlout
);

input  wire RSTn;
input  wire  [3:0] rx_inclk;       input  wire        rx_outclk;
input  wire  [3:0] tx_outclk;      input  wire         tx_inclk; 
input  wire  [3:0] rx_ctrlin;      input  wire  [3:0] tx_ctrlin;
output wire  [3:0] rx_ctrlout;     output wire  [3:0] tx_ctrlout;
input  wire [31:0] rx_datain;      input  wire [31:0] tx_datain;
output wire [31:0] rx_dataout;     output wire [31:0] tx_dataout;

sync_fifo rx4g_fifo_in0 (
   .Rclk(rx_outclk  ), .RdEn(1'b1),  .data_out({rx_ctrlout[0],rx_dataout[7:0]}),
   .Wclk(rx_inclk[0]), .WrEn(1'b1),  .data_in ({rx_ctrlin[0], rx_datain [7:0]}),
	.RSTn(RSTn)
);
sync_fifo rx4g_fifo_in1 (
   .Rclk(rx_outclk  ), .RdEn(1'b1),  .data_out({rx_ctrlout[1],rx_dataout[15:8]}),
   .Wclk(rx_inclk[1]), .WrEn(1'b1),  .data_in ({rx_ctrlin[1], rx_datain [15:8]}),
	.RSTn(RSTn)
);
sync_fifo rx4g_fifo_in2 (
   .Rclk(rx_outclk  ), .RdEn(1'b1),  .data_out({rx_ctrlout[2],rx_dataout[23:16]}),
   .Wclk(rx_inclk[2]), .WrEn(1'b1),  .data_in ({rx_ctrlin[2], rx_datain [23:16]}),
	.RSTn(RSTn)
);
sync_fifo rx4g_fifo_in3 (
   .Rclk(rx_outclk  ), .RdEn(1'b1),  .data_out({rx_ctrlout[3],rx_dataout[31:24]}),
   .Wclk(rx_inclk[3]), .WrEn(1'b1),  .data_in ({rx_ctrlin[3], rx_datain [31:24]}),
	.RSTn(RSTn)
);

sync_fifo tx4g_fifo_out0 (
   .Rclk(tx_outclk[0]),  .RdEn(1'b1),   .data_out({tx_ctrlout[0],tx_dataout[7:0]}),  
   .Wclk(tx_inclk    ),  .WrEn(1'b1),   .data_in ({tx_ctrlin[0], tx_datain [7:0]}),
	.RSTn(RSTn)
);
sync_fifo tx4g_fifo_out1 (
   .Rclk(tx_outclk[1]),  .RdEn(1'b1),   .data_out({tx_ctrlout[1],tx_dataout[15:8]}),  
   .Wclk(tx_inclk    ),  .WrEn(1'b1),   .data_in ({tx_ctrlin[1], tx_datain [15:8]}),
	.RSTn(RSTn)
);
sync_fifo tx4g_fifo_out2 (
   .Rclk(tx_outclk[2]),  .RdEn(1'b1),   .data_out({tx_ctrlout[2],tx_dataout[23:16]}),  
   .Wclk(tx_inclk    ),  .WrEn(1'b1),   .data_in ({tx_ctrlin[2], tx_datain [23:16]}),
	.RSTn(RSTn)
);
sync_fifo tx4g_fifo_out3 (
   .Rclk(tx_outclk[3]),  .RdEn(1'b1),   .data_out({tx_ctrlout[3],tx_dataout[31:24]}),  
   .Wclk(tx_inclk    ),  .WrEn(1'b1),   .data_in ({tx_ctrlin[3], tx_datain [31:24]}),
	.RSTn(RSTn)
);

endmodule
