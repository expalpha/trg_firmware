module mux32x16 ( input wire [511:0] data_in, input wire [3:0] sel, output wire [31:0] data_out );

assign data_out = (sel == 4'h0) ? data_in[ 31:  0] : 
                  (sel == 4'h1) ? data_in[ 63: 32] :
                  (sel == 4'h2) ? data_in[ 95: 64] :
                  (sel == 4'h3) ? data_in[127: 96] :
                  (sel == 4'h4) ? data_in[159:128] :
                  (sel == 4'h5) ? data_in[191:160] :
                  (sel == 4'h6) ? data_in[223:192] :
                  (sel == 4'h7) ? data_in[255:224] :
                  (sel == 4'h8) ? data_in[287:256] :
                  (sel == 4'h9) ? data_in[319:288] :
                  (sel == 4'hA) ? data_in[351:320] :
                  (sel == 4'hB) ? data_in[383:352] :
                  (sel == 4'hC) ? data_in[415:384] :
                  (sel == 4'hD) ? data_in[447:416] :
                  (sel == 4'hE) ? data_in[479:448] :
                /* sel == 4'hF */ data_in[511:480];

endmodule
