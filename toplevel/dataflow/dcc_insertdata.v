// modulelevel controls port# recording in the event header (which of the 16 incoming ports the data is read from)
// different bits are used for recording each of the digitiser channel, grifc port and master-grifc port (modulelevel=0,1,2)
// modulelevel=3 => port is not recorded (data passed through unchanged)
// words fadffadf are padding words and will be skipped [must be inside event or will cause errors]
module data_concentrator ( output wire debug,  input wire [63:0] inserted_data,
   input wire clk, input wire reset,  input wire [1:0] modulelevel,
	output reg [31:0] data_out, output reg ready_out, input wire evbuffull_in, 
	input wire [511:0] data_in, input wire [15:0] ready_in, output wire [15:0] ack_out
);
assign debug = ^(ev_size) ^ worderr;

mux32x16    datamux(   .data_in(data_in ), .sel(     channel), .data_out(word_in) );
mux1x16     rdymux(    .data_in(ready_in), .sel(     channel), .data_out(  ready) );
decode1x16  ackdecode( .data_in(ack     ), .sel(prev_channel), .data_out(ack_out) ); // ack was going to wrong chan at end of data

reg   [3:0]   channel; reg   [3:0] prev_channel;
wire [31:0]   word_in; wire  ready;
reg   [1:0] dcc_state;  reg    ack; reg prevack;
parameter dcc_idle=0, dcc_data=1, dcc_insert=2, dcc_error=3;
reg [15:0] timeout; reg [11:0] ev_words;  reg [11:0] ev_size;  reg worderr;

reg [1023:0] evcount;  reg [15:0] evchan;
							  
wire [255:0] evblock = (evchan[15:8] == 8'h0) ? evcount[255:  0] : (evchan[15:8] == 8'h1) ? evcount[511:256] :
                       (evchan[15:8] == 8'h2) ? evcount[767:512] : (evchan[15:8] == 8'h3) ? evcount[1023:768] : 256'h0;

wire [255:0] nxtblock = {
  (evchan[ 3:0] == 4'hF) ? evblock[255:240] + 16'h1 : evblock[255:240],
  (evchan[ 3:0] == 4'hE) ? evblock[239:224] + 16'h1 : evblock[239:224],
  (evchan[ 3:0] == 4'hD) ? evblock[223:208] + 16'h1 : evblock[223:208],
  (evchan[ 3:0] == 4'hC) ? evblock[207:192] + 16'h1 : evblock[207:192],
  (evchan[ 3:0] == 4'hB) ? evblock[191:176] + 16'h1 : evblock[191:176],
  (evchan[ 3:0] == 4'hA) ? evblock[175:160] + 16'h1 : evblock[175:160],
  (evchan[ 3:0] == 4'h9) ? evblock[159:144] + 16'h1 : evblock[159:144],
  (evchan[ 3:0] == 4'h8) ? evblock[143:128] + 16'h1 : evblock[143:128],
  (evchan[ 3:0] == 4'h7) ? evblock[127:112] + 16'h1 : evblock[127:112],
  (evchan[ 3:0] == 4'h6) ? evblock[111: 96] + 16'h1 : evblock[111: 96],
  (evchan[ 3:0] == 4'h5) ? evblock[ 95: 80] + 16'h1 : evblock[ 95: 80],
  (evchan[ 3:0] == 4'h4) ? evblock[ 79: 64] + 16'h1 : evblock[ 79: 64],
  (evchan[ 3:0] == 4'h3) ? evblock[ 63: 48] + 16'h1 : evblock[ 63: 48],
  (evchan[ 3:0] == 4'h2) ? evblock[ 47: 32] + 16'h1 : evblock[ 47: 32],
  (evchan[ 3:0] == 4'h1) ? evblock[ 31: 16] + 16'h1 : evblock[ 31: 16],
  (evchan[ 3:0] == 4'h0) ? evblock[ 15:  0] + 16'h1 : evblock[ 15:  0]
};

wire [1023:0] nxtcount = {
	(evchan[15:8] == 8'h3) ? nxtblock : evcount[1023:768],
   (evchan[15:8] == 8'h2) ? nxtblock : evcount[ 767:512],
	(evchan[15:8] == 8'h1) ? nxtblock : evcount[ 511:256],
   (evchan[15:8] == 8'h0) ? nxtblock : evcount[ 255:  0]
};

wire  [15:0] evwire  = (evchan[ 3:0] == 4'h0) ? evblock[ 15:  0] : (evchan[ 3:0] == 4'h1) ? evblock[ 31: 16] : 
                       (evchan[ 3:0] == 4'h2) ? evblock[ 47: 32] : (evchan[ 3:0] == 4'h3) ? evblock[ 63: 48] :
                       (evchan[ 3:0] == 4'h4) ? evblock[ 79: 64] : (evchan[ 3:0] == 4'h5) ? evblock[ 95: 80] :
                       (evchan[ 3:0] == 4'h6) ? evblock[111: 96] : (evchan[ 3:0] == 4'h7) ? evblock[127:112] :
                       (evchan[ 3:0] == 4'h8) ? evblock[143:128] : (evchan[ 3:0] == 4'h9) ? evblock[159:144] :
                       (evchan[ 3:0] == 4'hA) ? evblock[175:160] : (evchan[ 3:0] == 4'hB) ? evblock[191:176] :
                       (evchan[ 3:0] == 4'hC) ? evblock[207:192] : (evchan[ 3:0] == 4'hD) ? evblock[223:208] :
                       (evchan[ 3:0] == 4'hE) ? evblock[239:224] :                          evblock[255:240];

always @ ( posedge clk or posedge reset ) begin
   if( reset ) begin
      channel      <=      4'h0;  ready_out <=      1'b0; ack      <= 1'b0;   evcount <= 1024'h0;  evchan <= 16'h0;
      dcc_state    <=  dcc_idle;  data_out  <=     32'h0; prevack  <= 1'b0;   worderr <= 1'b0;
		prev_channel <=      4'h0;  timeout   <=     16'h0; ev_words <= 12'h0;  ev_size <= 12'h0;
	end else begin
	   channel      <=   channel;  ready_out <=     1'b0;  ack      <= 1'b0; // default ready/ack is off
      dcc_state    <= dcc_state;  data_out  <= data_out;  prevack  <= ack;       worderr <= worderr; evcount <= evcount;
		prev_channel <=   channel;  timeout   <=  timeout;  ev_words <= ev_words;  ev_size <= ev_size; evchan  <= evchan;
		if( ! ack && !prevack ) begin // wait 1 clk after ack for fifo q to update
  	     case(dcc_state)
		      dcc_idle: // wait for space in this state
			   	if ( ! evbuffull_in && ready ) begin
			   	   if( word_in[31:28] != 4'h8 ) dcc_state <= dcc_error;
					   else begin
						   //dcc_state <= dcc_data; ready_out <= 1'b1; ack <= 1'b1;  ev_words <= 12'h1;
						   dcc_state <= ( modulelevel == 2'h2 && inserted_data[63] ) ? dcc_insert : dcc_data; ready_out <= 1'b1; ack <= 1'b1;  ev_words <= 12'h1;
							data_out <= ( modulelevel == 2'h0 ) ? {word_in[31: 8],channel[3:0],word_in[ 3:0]} :
							            ( modulelevel == 2'h1 ) ? {word_in[31:16],channel[3:0],word_in[11:0]} :
											( modulelevel == 2'h2 ) ? {word_in[31:20],channel[3:0],word_in[15:0]} : word_in[31:0];
							evchan <= {channel[3:0],word_in[15:4]}; // only used on master
                  end
			   	end else channel <= channel + 4'h1;
		      dcc_insert:
				   if ( ! evbuffull_in ) begin
					   ack <= 1'b0;  ev_words <= ev_words + 12'h1;
					   if( ev_words == 12'h1 ) begin
				         dcc_state <= dcc_insert;   data_out <= {16'h0000,inserted_data[15:0]}; // insert fake ppg pattern
							ready_out <= 1'b1;
						end else if( ev_words == 12'h2 ) begin
						   
				         //dcc_state <= dcc_insert;   data_out <= {16'h001D,inserted_data[15:0]}; // insert fake master-id
				         dcc_state <= dcc_data;   data_out <= {16'h001D,evwire[15:0]}; // insert fake master-id
							ready_out <= 1'b1;       evcount <= nxtcount;
						end else begin
				         dcc_state <= dcc_data;     data_out <= {8'hC4,inserted_data[55:32]}; // insert diagnostic count
							ready_out <= 1'b1;
						end
					end
		      dcc_data: // done on 0xe (**inc channel**)
			      if( ! evbuffull_in  && ready ) begin
				      if( word_in[31:0] == 32'hfadffadf ) begin ack <= 1'b1; timeout <= 16'h0; end // skip padding words
						else begin
						   if( word_in[31:28] == 4'hE ) begin
						      dcc_state <= dcc_idle;  channel <= channel + 4'h1;
							   ev_size <= ev_words + 12'h1;  worderr <= (ev_size != ev_words + 12'h1);
						   end
				   	   data_out  <= word_in[31:0];  ev_words <= ev_words + 12'h1;
				         ready_out <= 1'b1; ack <= 1'b1; timeout <= 16'h0;
						end
			   	end else if( ! evbuffull_in  && ! ready ) begin // don't normally enter this state
					   timeout <= timeout + 16'h1;
						if( timeout == 16'hffff ) begin
						   dcc_state <= dcc_idle;
				   	   data_out  <=  32'hEFFFFFFF; ready_out <= 1'b1; ack <= 1'b0;
						end
					end
		      dcc_error: // write an error word, then read rest of bad event
			      if( ! evbuffull_in  && ready ) begin
				   	dcc_state <= dcc_data;
				   	data_out  <=  {4'hF,word_in[31:28],word_in[23:0]}; ready_out <= 1'b1; ack <= 1'b1;
			   	end else if( ! evbuffull_in  && ! ready ) begin // don't normally enter this state
					   timeout <= timeout + 16'h1;
						if( timeout == 16'hffff ) begin
						   dcc_state <= dcc_idle;
				   	   data_out  <=  32'hEEFFFFFF; ready_out <= 1'b1; ack <= 1'b0;
						end
					end
		      default: dcc_state <= dcc_idle;
		   endcase
		end
   end
end

endmodule
