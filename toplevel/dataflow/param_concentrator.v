// multiplex parameter i/o from 16 channels to single port
module param_concentrator ( input wire clk,  input wire [1:0] modulelevel,
   input wire [  63:0]    param_in,  output reg  [  63:0]    param_out,
	input wire [1023:0] chan_par_in,  output wire [1023:0] chan_par_out
);
// read and write are independant - allow different channel on input/output
//  => can't use input-channel to select output [just use valid strobe]
wire [7:0] channel =  ( modulelevel == 2'h0 ) ?       param_in[39:32]  :
                      ( modulelevel == 2'h1 ) ? {4'h0,param_in[43:40]} :
							                           {4'h0,param_in[47:44]};

assign chan_par_out[  63:  0] = (channel == 8'h0) ? param_in : 64'h0;
assign chan_par_out[ 127: 64] = (channel == 8'h1) ? param_in : 64'h0;
assign chan_par_out[ 191:128] = (channel == 8'h2) ? param_in : 64'h0;
assign chan_par_out[ 255:192] = (channel == 8'h3) ? param_in : 64'h0;
assign chan_par_out[ 319:256] = (channel == 8'h4) ? param_in : 64'h0;
assign chan_par_out[ 383:320] = (channel == 8'h5) ? param_in : 64'h0;
assign chan_par_out[ 447:384] = (channel == 8'h6) ? param_in : 64'h0;
assign chan_par_out[ 511:448] = (channel == 8'h7) ? param_in : 64'h0;
assign chan_par_out[ 575:512] = (channel == 8'h8) ? param_in : 64'h0;
assign chan_par_out[ 639:576] = (channel == 8'h9) ? param_in : 64'h0;
assign chan_par_out[ 703:640] = (channel == 8'hA) ? param_in : 64'h0;
assign chan_par_out[ 767:704] = (channel == 8'hB) ? param_in : 64'h0;
assign chan_par_out[ 831:768] = (channel == 8'hC) ? param_in : 64'h0;
assign chan_par_out[ 895:832] = (channel == 8'hD) ? param_in : 64'h0;
assign chan_par_out[ 959:896] = (channel == 8'hE) ? param_in : 64'h0;
assign chan_par_out[1023:960] = (channel == 8'hF) ? param_in : 64'h0;

// give priority to lowest # if several channels send at once
always @(posedge clk) begin
   param_out <= param_out;
   if(      chan_par_in[  63] ) param_out <= chan_par_in[  63:  0];
   else if( chan_par_in[ 127] ) param_out <= chan_par_in[ 127: 64];
   else if( chan_par_in[ 191] ) param_out <= chan_par_in[ 191:128];
   else if( chan_par_in[ 255] ) param_out <= chan_par_in[ 255:192];
   else if( chan_par_in[ 319] ) param_out <= chan_par_in[ 319:256];
   else if( chan_par_in[ 383] ) param_out <= chan_par_in[ 383:320];
   else if( chan_par_in[ 447] ) param_out <= chan_par_in[ 447:384];
   else if( chan_par_in[ 511] ) param_out <= chan_par_in[ 511:448];
   else if( chan_par_in[ 575] ) param_out <= chan_par_in[ 575:512];
   else if( chan_par_in[ 639] ) param_out <= chan_par_in[ 639:576];
   else if( chan_par_in[ 703] ) param_out <= chan_par_in[ 703:640];
   else if( chan_par_in[ 767] ) param_out <= chan_par_in[ 767:704];
   else if( chan_par_in[ 831] ) param_out <= chan_par_in[ 831:768];
   else if( chan_par_in[ 895] ) param_out <= chan_par_in[ 895:832];
   else if( chan_par_in[ 959] ) param_out <= chan_par_in[ 959:896];
   else if( chan_par_in[1023] ) param_out <= chan_par_in[1023:960];
	else param_out[63] <= 1'b0;
end

endmodule
