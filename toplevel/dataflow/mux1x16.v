module mux1x16 ( input wire [15:0] data_in, input wire [3:0] sel, output wire data_out );

assign data_out = (sel == 4'h0) ? data_in[ 0] : 
                  (sel == 4'h1) ? data_in[ 1] :
                  (sel == 4'h2) ? data_in[ 2] :
                  (sel == 4'h3) ? data_in[ 3] :
                  (sel == 4'h4) ? data_in[ 4] :
                  (sel == 4'h5) ? data_in[ 5] :
                  (sel == 4'h6) ? data_in[ 6] :
                  (sel == 4'h7) ? data_in[ 7] :
                  (sel == 4'h8) ? data_in[ 8] :
                  (sel == 4'h9) ? data_in[ 9] :
                  (sel == 4'hA) ? data_in[10] :
                  (sel == 4'hB) ? data_in[11] :
                  (sel == 4'hC) ? data_in[12] :
                  (sel == 4'hD) ? data_in[13] :
                  (sel == 4'hE) ? data_in[14] :
                /* sel == 4'hF */ data_in[15];

endmodule

