`default_nettype none
// this module runs on io clock at the moment
module param_io
  (
   input wire 	     clk,
   input wire 	     reset,
   output reg 	     reconfig_out,

   input wire [63:0] param_in,  // command from UDP communication module
   output reg [63:0] param_out, // reply to UDP communication module

   //input wire [7:0]  csr_in,
   //output reg [31:0] csr, // **csr[7:0] autocleared after 1 clk**
   //output reg [15:0] num_sample, // control UDP packet size

   input wire [31:0] fw_rev, // firmware revision

   output reg [31:0] conf_control,
   output reg [31:0] conf_mlu,
   output reg [31:0] conf_drift_width,
   output reg [31:0] conf_scaledown,
   output reg [31:0] conf_trig_delay,
   output reg [31:0] conf_trig_width,
   output reg [31:0] conf_busy_width,
   output reg [31:0] conf_pulser_width,
   output reg [31:0] conf_pulser_period,
   output reg [31:0] conf_pulser_burst_ctrl,
   output reg [31:0] conf_trig_enable,
   output reg [31:0] conf_nim_mask,
   output reg [31:0] conf_esata_mask,
   output reg [31:0] conf_adc16_masks[7:0],
   output reg [31:0] conf_adc32_masks[15:0],
   //output reg [31:0] conf_coinc_a,
   //output reg [31:0] conf_coinc_b,
   //output reg [31:0] conf_coinc_c,
   //output reg [31:0] conf_coinc_d,
   output reg [31:0] conf_counter_adc_select,
   output reg [31:0] conf_bsc_control,
   output reg [31:0] conf_coinc_control,
   output reg [31:0] conf_trig_timeout,

   output reg 	     reset_out,
   output reg 	     latch_out,
   output reg 	     trigger_out,

   input wire [31:0] pll_625_status,
   input wire [31:0] clk_counter,
   input wire [31:0] clk_625_counter,
   input wire [31:0] esata_clk_counter,
   input wire [31:0] esata_clk_esata_counter,
   input wire [31:0] eth_clk_counter,
   input wire [31:0] eth_clk_eth_counter,
   input wire [15:0] sas_sd,
   input wire [63:0] sas_bits[15:0],
   input wire [31:0] sas_sd_counters[15:0],
   input wire [31:0] ts_625,
   input wire [31:0] counter_trig_out,
   input wire [31:0] counter_trig_in,
   input wire [31:0] counter_pulser,
   input wire [31:0] counter_drift,
   input wire [31:0] counter_scaledown,
   input wire [31:0] counter_timeout,
   input wire [31:0] counter_adc16_grand_or,
   input wire [31:0] counter_adc32_grand_or,
   input wire [31:0] counter_adc_grand_or,
   input wire [31:0] counter_esata_nim_grand_or,
   //input wire [31:0] counter_adc16_mult_1ormore,
   //input wire [31:0] counter_adc16_mult_2ormore,
   //input wire [31:0] counter_adc16_mult_3ormore,
   //input wire [31:0] counter_adc16_mult_4ormore,
   input wire [31:0] counter_adc16_or16[15:0],
   input wire [31:0] counter_adc32_or16[31:0],
   input wire [31:0] counter_adc_selected[15:0],
   //input wire [31:0] counter_aw16_mult_1ormore,
   //input wire [31:0] counter_aw16_mult_2ormore,
   //input wire [31:0] counter_aw16_mult_3ormore,
   //input wire [31:0] counter_aw16_mult_4ormore,
   //input wire [31:0] counter_cc_a,
   //input wire [31:0] counter_cc_b,
   //input wire [31:0] counter_cc_c,
   //input wire [31:0] counter_cc_d,
   //input wire [31:0] counter_cc,
   input wire [31:0] counter_aw16_grand_or,
   input wire [31:0] counter_aw16_mlu_start,
   input wire [31:0] counter_aw16_mlu_trig,
   input wire [31:0] counter_bsc_grand_or,
   input wire [31:0] counter_bsc_mult,
   input wire [31:0] counter_bsc_mult_start,
   input wire [31:0] counter_bsc_mult_trig,
   input wire [31:0] counter_bsc64[63:0],
   input wire [31:0] counter_coinc_start,
   input wire [31:0] counter_coinc_trig,

   output reg [31:0] flash_control_out,
   output reg [31:0] flash_a_out,
   output reg [31:0] flash_d_out,
   input  reg [31:0] flash_status,
   input  reg [31:0] flash_a,

   output reg [31:0] cb_control_out,
   input  reg [31:0] cb_status,

   output reg [31:0] cb_invert_a_out,
   output reg [31:0] cb_invert_b_out,
   output reg [31:0] cb_enable_le_a_out,
   output reg [31:0] cb_enable_le_b_out,
   output reg [31:0] cb_enable_te_a_out,
   output reg [31:0] cb_enable_te_b_out,

   output reg [31:0] cb_scaler_addr_out,
   input  reg [31:0] cb_scaler_data,
   input  reg [31:0] cb_input_num,

   output reg [31:0] cb_sync_mask_a_out,
   output reg [31:0] cb_sync_mask_b_out,
   output reg [31:0] cb_sync_reg_out,
   input  reg [31:0] cb_sync_status,

   input  reg [31:0] cb_fifo_data,
   input  reg [31:0] cb_fifo_status,
   output reg        cb_fifo_rdack_out,

   output reg [31:0] cb_latch_period_out,

   output reg [31:0] cb_udp_period_out,
   output reg [31:0] cb_udp_threshold_out
);

wire        par_valid = param_in[63];
wire        readback  = param_in[62];
wire [13:0] par_id    = param_in[61:48];
wire [15:0] chan      = param_in[47:32];
wire [31:0] par_value = param_in[31: 0];

reg [31:0] scratch_reg;

   // kludge to help quartus meet timing:
   // after fifo rdack, we do not need the data to change
   // on the next clock, it is okey if it is delayed
   // because next read from FIFO (via UDP) is coming
   // after a very long time. K.O. Nov 2021
   reg [31:0] cb_fifo_data_kludge;
   always_ff @ ( posedge clk ) begin
      cb_fifo_data_kludge <= cb_fifo_data;
   end


wire par_active = par_valid && !parvalid_reg;  reg parvalid_reg; always @(posedge clk) parvalid_reg <= par_valid;  // oneshot

always_ff @ ( posedge clk or posedge reset ) begin
   if( reset ) begin
      //num_sample    <= 16'h0;
      param_out     <=  64'h0;
      //csr           <= 32'h0;
      scratch_reg        <= 0;
      conf_control       <= 32'h0;
      conf_scaledown     <= 32'h0;
      conf_drift_width   <= 32'hA;
      conf_trig_width    <= 32'h8;
      conf_busy_width    <= 32'h10;
      conf_pulser_width  <= 32'h12;
      conf_pulser_period <= 32'h0;
      conf_pulser_burst_ctrl <= 32'h0;
      conf_trig_enable   <= 0;
      conf_nim_mask      <= 0;
      conf_esata_mask    <= 0;
      reset_out          <= 0;
      latch_out          <= 0;
      trigger_out        <= 0;
      flash_control_out  <= 0;
      flash_a_out        <= 0;
      flash_d_out        <= 0;
      cb_control_out     <= 0;
      cb_invert_a_out    <= 0;
      cb_invert_b_out    <= 0;
      cb_enable_le_a_out <= 0;
      cb_enable_le_b_out <= 0;
      cb_enable_te_a_out <= 0;
      cb_enable_te_b_out <= 0;
      cb_scaler_addr_out <= 0;
      cb_sync_mask_a_out <= 0;
      cb_sync_mask_b_out <= 0;
      cb_sync_reg_out    <= 0;
      cb_fifo_rdack_out  <= 0;
      cb_udp_period_out  <= 0;
      cb_udp_threshold_out <= 0;
      cb_latch_period_out<= 0;
      
   end else begin
      //num_sample    <= num_sample;
      param_out     <= {1'b0,param_out[62:0]};
      //csr           <= {csr[31:8],8'h0};
      reset_out <= 0;
      latch_out <= 0;
      trigger_out <= 0;
      cb_fifo_rdack_out <= 0;
      if( par_active ) begin
	 if( ! readback ) begin
            case( par_id )
              14'h008: scratch_reg <= par_value[31:0];
	      14'h01F: ;

	      // ALPHA-g parameters
              14'h020: conf_trig_width     <= par_value;
              14'h021: conf_busy_width     <= par_value;
              14'h022: conf_pulser_width   <= par_value;
              14'h023: conf_pulser_period  <= par_value;
              14'h024: conf_pulser_burst_ctrl <= par_value;
              14'h025: conf_trig_enable   <= par_value;
	      14'h026: ;
	      14'h027: ;
	      14'h028: ;
	      14'h029: conf_nim_mask <= par_value;
	      14'h02A: conf_esata_mask <= par_value;
	      14'h02B: begin
		 reset_out <= par_value[0];   // 0x01
		 latch_out <= par_value[1];   // 0x02
		 trigger_out <= par_value[2]; // 0x04
	      end
	      //14'h02C: conf_coinc_a <= par_value;
	      //14'h02D: conf_coinc_b <= par_value;
	      //14'h02E: conf_coinc_c <= par_value;
	      //14'h02F: conf_coinc_d <= par_value;

	      14'h030: ;
	      14'h031: ;
	      14'h032: ;
	      14'h033: ;

      	      14'h034: conf_control <= par_value;
      	      14'h035: conf_drift_width <= par_value;
      	      14'h036: conf_scaledown <= par_value;
      	      14'h037: conf_mlu <= par_value;
      	      14'h038: conf_trig_delay <= par_value;

	      14'h039: ;
	      14'h03A: ;

	      14'h03B: conf_counter_adc_select <= par_value;

	      14'h03C: ;

	      // GRIF-C CSR
	      14'h03D: ; // csr <= csr & ~par_value; // Selective clear
	      14'h03E: ; // csr <= csr |  par_value; // Selective set
	      14'h03F: ; // csr <= par_value;

	      14'h040: conf_bsc_control     <= par_value;
	      14'h041: conf_coinc_control   <= par_value;
	      14'h042: reconfig_out <= (par_value == ~fw_rev);
	      14'h043: conf_trig_timeout    <= par_value;

              14'h044:  ; // eth_rx_clk frequency counters
	      14'h045:  ; // eth rx_clk frequency counters

	      14'h050:  flash_control_out   <= par_value;
	      14'h051:  flash_a_out         <= par_value;
	      14'h052:  flash_d_out         <= par_value;

	      14'h060:  cb_control_out   <= par_value;
	      14'h061:  ; // cb_status
              14'h062:  cb_invert_a_out <= par_value;
              14'h063:  cb_invert_b_out <= par_value;
              14'h064:  cb_enable_le_a_out <= par_value;
              14'h065:  cb_enable_le_b_out <= par_value;
              14'h066:  cb_enable_te_a_out <= par_value;
              14'h067:  cb_enable_te_b_out <= par_value;

              14'h068:  cb_scaler_addr_out <= par_value;
              14'h069:  ; //  cb_scaler_data
              14'h06A:  ; //  cb_input_num

              14'h06B:  cb_sync_mask_a_out <= par_value;
              14'h06C:  cb_sync_mask_b_out <= par_value;
              14'h06D:  cb_sync_reg_out <= par_value;
              14'h06E:  ; // cb_sync_status,

              14'h06F:  cb_latch_period_out   <= par_value;

	      14'h070:  ; // cb_fifo_status
	      14'h071:  ; // cb_fifo_data
              14'h072:  cb_udp_period_out     <= par_value;
              14'h073:  cb_udp_threshold_out  <= par_value;
	      
	      14'h200: conf_adc16_masks[0] <= par_value;
	      14'h201: conf_adc16_masks[1] <= par_value;
	      14'h202: conf_adc16_masks[2] <= par_value;
	      14'h203: conf_adc16_masks[3] <= par_value;
	      14'h204: conf_adc16_masks[4] <= par_value;
	      14'h205: conf_adc16_masks[5] <= par_value;
	      14'h206: conf_adc16_masks[6] <= par_value;
	      14'h207: conf_adc16_masks[7] <= par_value;

	      14'h300: conf_adc32_masks[0] <= par_value;
	      14'h301: conf_adc32_masks[1] <= par_value;
	      14'h302: conf_adc32_masks[2] <= par_value;
	      14'h303: conf_adc32_masks[3] <= par_value;
	      14'h304: conf_adc32_masks[4] <= par_value;
	      14'h305: conf_adc32_masks[5] <= par_value;
	      14'h306: conf_adc32_masks[6] <= par_value;
	      14'h307: conf_adc32_masks[7] <= par_value;
	      14'h308: conf_adc32_masks[8] <= par_value;
	      14'h309: conf_adc32_masks[9] <= par_value;
	      14'h30A: conf_adc32_masks[10] <= par_value;
	      14'h30B: conf_adc32_masks[11] <= par_value;
	      14'h30C: conf_adc32_masks[12] <= par_value;
	      14'h30D: conf_adc32_masks[13] <= par_value;
	      14'h30E: conf_adc32_masks[14] <= par_value;
	      14'h30F: conf_adc32_masks[15] <= par_value;
            endcase
	 end else begin
            case( par_id )
              14'h008:  param_out <= {2'h3, par_id, chan,     scratch_reg };
	      14'h01F:  param_out <= {2'h3, par_id, chan,     fw_rev };
	      
	      // ALPHA-g parameters
	      14'h020:  param_out <= {2'h3, par_id, chan,     conf_trig_width };
	      14'h021:  param_out <= {2'h3, par_id, chan,     conf_busy_width };
	      14'h022:  param_out <= {2'h3, par_id, chan,     conf_pulser_width };
	      14'h023:  param_out <= {2'h3, par_id, chan,     conf_pulser_period };
	      14'h024:  param_out <= {2'h3, par_id, chan,     conf_pulser_burst_ctrl };
	      14'h025:  param_out <= {2'h3, par_id, chan,     conf_trig_enable };
	      14'h026:  param_out <= {2'h3, par_id, chan,     32'h0 };
	      14'h027:  param_out <= {2'h3, par_id, chan,     32'h0 };
	      14'h028:  param_out <= {2'h3, par_id, chan,     32'h0 };
	      14'h029:  param_out <= {2'h3, par_id, chan,     conf_nim_mask };
	      14'h02A:  param_out <= {2'h3, par_id, chan,     conf_esata_mask };
	      14'h02B:  param_out <= {2'h3, par_id, chan,     32'h0 };
	      //14'h02C:  param_out <= {2'h3, par_id, chan,     conf_coinc_a };
	      //14'h02D:  param_out <= {2'h3, par_id, chan,     conf_coinc_b };
	      //14'h02E:  param_out <= {2'h3, par_id, chan,     conf_coinc_c };
	      //14'h02F:  param_out <= {2'h3, par_id, chan,     conf_coinc_d };
              
	      14'h030:  param_out <= {2'h3, par_id, chan,     { 16'h0, sas_sd[15:0] } };
	      14'h031:  param_out <= {2'h3, par_id, chan,     pll_625_status };
	      14'h032:  param_out <= {2'h3, par_id, chan,     clk_counter };
	      14'h033:  param_out <= {2'h3, par_id, chan,     clk_625_counter };
	      14'h034:  param_out <= {2'h3, par_id, chan,     conf_control };
	      14'h035:  param_out <= {2'h3, par_id, chan,     conf_drift_width };
	      14'h036:  param_out <= {2'h3, par_id, chan,     conf_scaledown };
	      14'h037:  param_out <= {2'h3, par_id, chan,     conf_mlu };
	      14'h038:  param_out <= {2'h3, par_id, chan,     conf_trig_delay };
	      14'h039:  param_out <= {2'h3, par_id, chan,     esata_clk_counter };
	      14'h03A:  param_out <= {2'h3, par_id, chan,     esata_clk_esata_counter };
	      14'h03B:  param_out <= {2'h3, par_id, chan,     conf_counter_adc_select };
	      14'h03C:  param_out <= {2'h3, par_id, chan,     32'b0 };
              14'h03D:  param_out <= {2'h3, par_id, chan,     32'b0 /*csr[31:8],csr_in*/ };
              14'h03E:  param_out <= {2'h3, par_id, chan,     32'b0 /*csr[31:8],csr_in*/ };
              14'h03F:  param_out <= {2'h3, par_id, chan,     32'b0 /*csr[31:8],csr_in*/ };

              14'h040:  param_out <= {2'h3, par_id, chan,     conf_bsc_control };
              14'h041:  param_out <= {2'h3, par_id, chan,     conf_coinc_control };
              14'h042:  param_out <= {2'h3, par_id, chan,     32'b0 }; // reconfig_out
              14'h043:  param_out <= {2'h3, par_id, chan,     conf_trig_timeout };

              14'h044:  param_out <= {2'h3, par_id, chan,     eth_clk_counter };
	      14'h045:  param_out <= {2'h3, par_id, chan,     eth_clk_eth_counter };

	      14'h050:  param_out <= {2'h3, par_id, chan,     flash_control_out };
	      14'h051:  param_out <= {2'h3, par_id, chan,     flash_a_out };
	      14'h052:  param_out <= {2'h3, par_id, chan,     flash_d_out };
	      14'h053:  param_out <= {2'h3, par_id, chan,     flash_status };
	      14'h054:  param_out <= {2'h3, par_id, chan,     flash_a };

              14'h060:  param_out <= {2'h3, par_id, chan,     cb_control_out };
	      14'h061:  param_out <= {2'h3, par_id, chan,     cb_status };
	      14'h062:  param_out <= {2'h3, par_id, chan,     cb_invert_a_out };
	      14'h063:  param_out <= {2'h3, par_id, chan,     cb_invert_b_out };
	      14'h064:  param_out <= {2'h3, par_id, chan,     cb_enable_le_a_out };
	      14'h065:  param_out <= {2'h3, par_id, chan,     cb_enable_le_b_out };
	      14'h066:  param_out <= {2'h3, par_id, chan,     cb_enable_te_a_out };
	      14'h067:  param_out <= {2'h3, par_id, chan,     cb_enable_te_b_out };

	      14'h068:  param_out <= {2'h3, par_id, chan,     cb_scaler_addr_out };
	      14'h069:  param_out <= {2'h3, par_id, chan,     cb_scaler_data };
	      14'h06A:  param_out <= {2'h3, par_id, chan,     cb_input_num };

              14'h06B:  param_out <= {2'h3, par_id, chan,     cb_sync_mask_a_out };
	      14'h06C:  param_out <= {2'h3, par_id, chan,     cb_sync_mask_b_out };
	      14'h06D:  param_out <= {2'h3, par_id, chan,     cb_sync_reg_out };
	      14'h06E:  param_out <= {2'h3, par_id, chan,     cb_sync_status };

              14'h06F:  param_out <= {2'h3, par_id, chan,     cb_latch_period_out };

	      14'h070:  param_out <= {2'h3, par_id, chan,     cb_fifo_status };
	      14'h071:  begin param_out <= {2'h3, par_id, chan,     cb_fifo_data_kludge }; cb_fifo_rdack_out <= 1; end
              14'h072:  param_out <= {2'h3, par_id, chan,     cb_udp_period_out };
              14'h073:  param_out <= {2'h3, par_id, chan,     cb_udp_threshold_out };

              14'h100:  param_out <= {2'h3, par_id, chan, ts_625 };
              14'h101:  param_out <= {2'h3, par_id, chan, counter_trig_out };
              14'h102:  param_out <= {2'h3, par_id, chan, counter_trig_in };
              14'h103:  param_out <= {2'h3, par_id, chan, counter_pulser };

              14'h104:  param_out <= {2'h3, par_id, chan, counter_adc16_grand_or };
              14'h105:  param_out <= {2'h3, par_id, chan, counter_adc32_grand_or };
              14'h106:  param_out <= {2'h3, par_id, chan, counter_adc_grand_or };
              14'h107:  param_out <= {2'h3, par_id, chan, counter_esata_nim_grand_or };

              //14'h108:  param_out <= {2'h3, par_id, chan, counter_adc16_mult_1ormore };
              //14'h109:  param_out <= {2'h3, par_id, chan, counter_adc16_mult_2ormore };
              //14'h10A:  param_out <= {2'h3, par_id, chan, counter_adc16_mult_3ormore };
              //14'h10B:  param_out <= {2'h3, par_id, chan, counter_adc16_mult_4ormore };

              //14'h10C:  param_out <= {2'h3, par_id, chan, counter_cc_a };
              //14'h10D:  param_out <= {2'h3, par_id, chan, counter_cc_b };
              //14'h10E:  param_out <= {2'h3, par_id, chan, counter_cc_c };
              //14'h10F:  param_out <= {2'h3, par_id, chan, counter_cc_d };

	      14'h110:  param_out <= {2'h3, par_id, chan, counter_drift };
              14'h111:  param_out <= {2'h3, par_id, chan, counter_scaledown };

	      //14'h112:  param_out <= {2'h3, par_id, chan, counter_cc };
              14'h113:  param_out <= {2'h3, par_id, chan, counter_aw16_mlu_trig };

              14'h114:  param_out <= {2'h3, par_id, chan, counter_aw16_grand_or };
              //14'h115:  param_out <= {2'h3, par_id, chan, counter_aw16_mult_2ormore };
              //14'h116:  param_out <= {2'h3, par_id, chan, counter_aw16_mult_3ormore };
              //14'h117:  param_out <= {2'h3, par_id, chan, counter_aw16_mult_4ormore };

              14'h118:  param_out <= {2'h3, par_id, chan, counter_bsc_grand_or };
              14'h119:  param_out <= {2'h3, par_id, chan, counter_bsc_mult };
              14'h11A:  param_out <= {2'h3, par_id, chan, counter_coinc_trig };

              14'h11B:  param_out <= {2'h3, par_id, chan, counter_timeout };

              14'h11C:  param_out <= {2'h3, par_id, chan, counter_bsc_mult_start };
              14'h11D:  param_out <= {2'h3, par_id, chan, counter_bsc_mult_trig };

              14'h11E:  param_out <= {2'h3, par_id, chan, counter_coinc_start };

              14'h11F:  param_out <= {2'h3, par_id, chan, counter_aw16_mlu_start };

              14'h200:  param_out <= {2'h3, par_id, chan, conf_adc16_masks[0] };
              14'h201:  param_out <= {2'h3, par_id, chan, conf_adc16_masks[1] };
              14'h202:  param_out <= {2'h3, par_id, chan, conf_adc16_masks[2] };
              14'h203:  param_out <= {2'h3, par_id, chan, conf_adc16_masks[3] };
              14'h204:  param_out <= {2'h3, par_id, chan, conf_adc16_masks[4] };
              14'h205:  param_out <= {2'h3, par_id, chan, conf_adc16_masks[5] };
              14'h206:  param_out <= {2'h3, par_id, chan, conf_adc16_masks[6] };
              14'h207:  param_out <= {2'h3, par_id, chan, conf_adc16_masks[7] };

              14'h300:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[0] };
              14'h301:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[1] };
              14'h302:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[2] };
              14'h303:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[3] };
              14'h304:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[4] };
              14'h305:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[5] };
              14'h306:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[6] };
              14'h307:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[7] };
              14'h308:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[8] };
              14'h309:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[9] };
              14'h30A:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[10] };
              14'h30B:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[11] };
              14'h30C:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[12] };
              14'h30D:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[13] };
              14'h30E:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[14] };
              14'h30F:  param_out <= {2'h3, par_id, chan, conf_adc32_masks[15] };
	      
              14'h400:  param_out <= {2'h3, par_id, chan, sas_bits[0][31: 0] };
              14'h401:  param_out <= {2'h3, par_id, chan, sas_bits[0][63:32] };
              14'h402:  param_out <= {2'h3, par_id, chan, sas_bits[1][31: 0] };
              14'h403:  param_out <= {2'h3, par_id, chan, sas_bits[1][63:32] };
              14'h404:  param_out <= {2'h3, par_id, chan, sas_bits[2][31: 0] };
              14'h405:  param_out <= {2'h3, par_id, chan, sas_bits[2][63:32] };
              14'h406:  param_out <= {2'h3, par_id, chan, sas_bits[3][31: 0] };
              14'h407:  param_out <= {2'h3, par_id, chan, sas_bits[3][63:32] };
              14'h408:  param_out <= {2'h3, par_id, chan, sas_bits[4][31: 0] };
              14'h409:  param_out <= {2'h3, par_id, chan, sas_bits[4][63:32] };
              14'h40A:  param_out <= {2'h3, par_id, chan, sas_bits[5][31: 0] };
              14'h40B:  param_out <= {2'h3, par_id, chan, sas_bits[5][63:32] };
              14'h40C:  param_out <= {2'h3, par_id, chan, sas_bits[6][31: 0] };
              14'h40D:  param_out <= {2'h3, par_id, chan, sas_bits[6][63:32] };
              14'h40E:  param_out <= {2'h3, par_id, chan, sas_bits[7][31: 0] };
              14'h40F:  param_out <= {2'h3, par_id, chan, sas_bits[7][63:32] };
              14'h410:  param_out <= {2'h3, par_id, chan, sas_bits[8][31: 0] };
              14'h411:  param_out <= {2'h3, par_id, chan, sas_bits[8][63:32] };
              14'h412:  param_out <= {2'h3, par_id, chan, sas_bits[9][31: 0] };
              14'h413:  param_out <= {2'h3, par_id, chan, sas_bits[9][63:32] };
              14'h414:  param_out <= {2'h3, par_id, chan, sas_bits[10][31: 0] };
              14'h415:  param_out <= {2'h3, par_id, chan, sas_bits[10][63:32] };
              14'h416:  param_out <= {2'h3, par_id, chan, sas_bits[11][31: 0] };
              14'h417:  param_out <= {2'h3, par_id, chan, sas_bits[11][63:32] };
              14'h418:  param_out <= {2'h3, par_id, chan, sas_bits[12][31: 0] };
              14'h419:  param_out <= {2'h3, par_id, chan, sas_bits[12][63:32] };
              14'h41A:  param_out <= {2'h3, par_id, chan, sas_bits[13][31: 0] };
              14'h41B:  param_out <= {2'h3, par_id, chan, sas_bits[13][63:32] };
              14'h41C:  param_out <= {2'h3, par_id, chan, sas_bits[14][31: 0] };
              14'h41D:  param_out <= {2'h3, par_id, chan, sas_bits[14][63:32] };
              14'h41E:  param_out <= {2'h3, par_id, chan, sas_bits[15][31: 0] };
              14'h41F:  param_out <= {2'h3, par_id, chan, sas_bits[15][63:32] };

              14'h420:  param_out <= {2'h3, par_id, chan, sas_sd_counters[0] };
              14'h421:  param_out <= {2'h3, par_id, chan, sas_sd_counters[1] };
              14'h422:  param_out <= {2'h3, par_id, chan, sas_sd_counters[2] };
              14'h423:  param_out <= {2'h3, par_id, chan, sas_sd_counters[3] };
              14'h424:  param_out <= {2'h3, par_id, chan, sas_sd_counters[4] };
              14'h425:  param_out <= {2'h3, par_id, chan, sas_sd_counters[5] };
              14'h426:  param_out <= {2'h3, par_id, chan, sas_sd_counters[6] };
              14'h427:  param_out <= {2'h3, par_id, chan, sas_sd_counters[7] };
              14'h428:  param_out <= {2'h3, par_id, chan, sas_sd_counters[8] };
              14'h429:  param_out <= {2'h3, par_id, chan, sas_sd_counters[9] };
              14'h42A:  param_out <= {2'h3, par_id, chan, sas_sd_counters[10] };
              14'h42B:  param_out <= {2'h3, par_id, chan, sas_sd_counters[11] };
              14'h42C:  param_out <= {2'h3, par_id, chan, sas_sd_counters[12] };
              14'h42D:  param_out <= {2'h3, par_id, chan, sas_sd_counters[13] };
              14'h42E:  param_out <= {2'h3, par_id, chan, sas_sd_counters[14] };
              14'h42F:  param_out <= {2'h3, par_id, chan, sas_sd_counters[15] };

              14'h430:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[0] };
              14'h431:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[1] };
              14'h432:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[2] };
              14'h433:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[3] };
              14'h434:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[4] };
              14'h435:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[5] };
              14'h436:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[6] };
              14'h437:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[7] };
              14'h438:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[8] };
              14'h439:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[9] };
              14'h43A:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[10] };
              14'h43B:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[11] };
              14'h43C:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[12] };
              14'h43D:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[13] };
              14'h43E:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[14] };
              14'h43F:  param_out <= {2'h3, par_id, chan, counter_adc16_or16[15] };

              14'h440:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[0] };
              14'h441:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[1] };
              14'h442:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[2] };
              14'h443:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[3] };
              14'h444:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[4] };
              14'h445:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[5] };
              14'h446:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[6] };
              14'h447:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[7] };
              14'h448:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[8] };
              14'h449:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[9] };
              14'h44A:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[10] };
              14'h44B:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[11] };
              14'h44C:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[12] };
              14'h44D:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[13] };
              14'h44E:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[14] };
              14'h44F:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[15] };
              14'h450:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[16] };
              14'h451:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[17] };
              14'h452:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[18] };
              14'h453:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[19] };
              14'h454:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[20] };
              14'h455:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[21] };
              14'h456:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[22] };
              14'h457:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[23] };
              14'h458:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[24] };
              14'h459:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[25] };
              14'h45A:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[26] };
              14'h45B:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[27] };
              14'h45C:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[28] };
              14'h45D:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[29] };
              14'h45E:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[30] };
              14'h45F:  param_out <= {2'h3, par_id, chan, counter_adc32_or16[31] };

              14'h460:  param_out <= {2'h3, par_id, chan, counter_adc_selected[0] };
              14'h461:  param_out <= {2'h3, par_id, chan, counter_adc_selected[1] };
              14'h462:  param_out <= {2'h3, par_id, chan, counter_adc_selected[2] };
              14'h463:  param_out <= {2'h3, par_id, chan, counter_adc_selected[3] };
              14'h464:  param_out <= {2'h3, par_id, chan, counter_adc_selected[4] };
              14'h465:  param_out <= {2'h3, par_id, chan, counter_adc_selected[5] };
              14'h466:  param_out <= {2'h3, par_id, chan, counter_adc_selected[6] };
              14'h467:  param_out <= {2'h3, par_id, chan, counter_adc_selected[7] };
              14'h468:  param_out <= {2'h3, par_id, chan, counter_adc_selected[8] };
              14'h469:  param_out <= {2'h3, par_id, chan, counter_adc_selected[9] };
              14'h46A:  param_out <= {2'h3, par_id, chan, counter_adc_selected[10] };
              14'h46B:  param_out <= {2'h3, par_id, chan, counter_adc_selected[11] };
              14'h46C:  param_out <= {2'h3, par_id, chan, counter_adc_selected[12] };
              14'h46D:  param_out <= {2'h3, par_id, chan, counter_adc_selected[13] };
              14'h46E:  param_out <= {2'h3, par_id, chan, counter_adc_selected[14] };
              14'h46F:  param_out <= {2'h3, par_id, chan, counter_adc_selected[15] };

              14'h470:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+0] };
              14'h471:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+1] };
              14'h472:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+2] };
              14'h473:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+3] };
              14'h474:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+4] };
              14'h475:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+5] };
              14'h476:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+6] };
              14'h477:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+7] };
              14'h478:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+8] };
              14'h479:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+9] };
              14'h47A:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+10] };
              14'h47B:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+11] };
              14'h47C:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+12] };
              14'h47D:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+13] };
              14'h47E:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+14] };
              14'h47F:  param_out <= {2'h3, par_id, chan, counter_bsc64[0+15] };

              14'h480:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+0] };
              14'h481:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+1] };
              14'h482:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+2] };
              14'h483:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+3] };
              14'h484:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+4] };
              14'h485:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+5] };
              14'h486:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+6] };
              14'h487:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+7] };
              14'h488:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+8] };
              14'h489:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+9] };
              14'h48A:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+10] };
              14'h48B:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+11] };
              14'h48C:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+12] };
              14'h48D:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+13] };
              14'h48E:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+14] };
              14'h48F:  param_out <= {2'h3, par_id, chan, counter_bsc64[16+15] };

              14'h490:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+0] };
              14'h491:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+1] };
              14'h492:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+2] };
              14'h493:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+3] };
              14'h494:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+4] };
              14'h495:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+5] };
              14'h496:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+6] };
              14'h497:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+7] };
              14'h498:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+8] };
              14'h499:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+9] };
              14'h49A:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+10] };
              14'h49B:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+11] };
              14'h49C:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+12] };
              14'h49D:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+13] };
              14'h49E:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+14] };
              14'h49F:  param_out <= {2'h3, par_id, chan, counter_bsc64[32+15] };

              14'h4A0:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+0] };
              14'h4A1:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+1] };
              14'h4A2:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+2] };
              14'h4A3:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+3] };
              14'h4A4:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+4] };
              14'h4A5:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+5] };
              14'h4A6:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+6] };
              14'h4A7:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+7] };
              14'h4A8:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+8] };
              14'h4A9:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+9] };
              14'h4AA:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+10] };
              14'h4AB:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+11] };
              14'h4AC:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+12] };
              14'h4AD:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+13] };
              14'h4AE:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+14] };
              14'h4AF:  param_out <= {2'h3, par_id, chan, counter_bsc64[48+15] };

	      default:  param_out <= {2'h3, par_id, chan, 32'h0 };
           endcase
	 end
      end
   end
end

endmodule
