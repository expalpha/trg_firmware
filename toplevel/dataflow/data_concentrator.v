// modulelevel controls port# recording in the event header (which of the 16 incoming ports the data is read from)
// different bits are used for recording each of the digitiser channel, grifc port and master-grifc port (modulelevel=0,1,2)
// modulelevel=3 => port is not recorded (data passed through unchanged)
// words fadffadf are padding words and will be skipped [must be inside event or will cause errors]
module data_concentrator ( output wire debug,
   input wire clk, input wire reset,  input wire [1:0] modulelevel,
	output reg [31:0] data_out, output reg ready_out, input wire evbuffull_in, 
	input wire [511:0] data_in, input wire [15:0] ready_in, output wire [15:0] ack_out
);
assign debug = ^(ev_size) ^ worderr;

mux32x16    datamux(   .data_in(data_in ), .sel(     channel), .data_out(word_in) );
mux1x16     rdymux(    .data_in(ready_in), .sel(     channel), .data_out(  ready) );
decode1x16  ackdecode( .data_in(c_ack     ), .sel(   channel), .data_out(ack_out) );

reg   [3:0]   channel; reg   [3:0] prev_channel;
wire [31:0]   word_in; wire  ready;
reg   [1:0] dcc_state;  reg    ack; reg [1:0] prevack;
parameter dcc_idle=0, dcc_data=1, dcc_insert=2, dcc_error=3;
reg [15:0] timeout; reg [11:0] ev_words;  reg [11:0] ev_size;  reg worderr;

wire c_ack = ( ! evbuffull_in && ready );

always @ ( posedge clk or posedge reset ) begin
   if( reset ) begin 
      channel      <=      4'h0;  ready_out <=      1'b0; ack      <= 1'b0;
      dcc_state    <=  dcc_idle;  data_out  <=     32'h0; prevack  <= 2'b0;   worderr <= 1'b0;
		prev_channel <=      4'h0;  timeout   <=     16'h0; ev_words <= 12'h0;  ev_size <= 12'h0;
	end else begin
	   channel      <=   channel;  ready_out <=     1'b0;  ack      <= 1'b0; // default ready/ack is off
      dcc_state    <= dcc_state;  data_out  <= data_out;  prevack  <= {prevack[0],ack};  worderr <= worderr;
		prev_channel <=   channel;  timeout   <=  timeout;  ev_words <= ev_words;  ev_size <= ev_size;
		//if( ! ack && !prevack[0] && !prevack[1] ) begin // wait 1-2 clks after ack for fifo q to update
		//if( ! ack ) begin
  	     case(dcc_state)
		      dcc_idle: // wait for space in this state
			   	if ( ! evbuffull_in && ready ) begin
			   	   if( word_in[31:28] != 4'h8 ) dcc_state <= dcc_error;
					   else begin
						   dcc_state <= dcc_data; ready_out <= 1'b1; ack <= 1'b1;  ev_words <= 12'h1;
							data_out <= ( modulelevel == 2'h0 ) ? {word_in[31: 8],channel[3:0],word_in[ 3:0]} :
							            ( modulelevel == 2'h1 ) ? {word_in[31:16],channel[3:0],word_in[11:0]} :
											( modulelevel == 2'h2 ) ? {word_in[31:20],channel[3:0],word_in[15:0]} : word_in[31:0];
                  end
			   	end else channel <= channel + 4'h1;
		      dcc_data: // done on 0xe (**inc channel**)
			      if( ! evbuffull_in  && ready ) begin
				      if( word_in[31:0] == 32'hfadffadf ) begin ack <= 1'b1; timeout <= 16'h0; end // skip padding words
						else begin
						   if( word_in[31:28] == 4'hE ) begin
						      dcc_state <= dcc_idle;  channel <= channel + 4'h1;
							   ev_size <= ev_words + 12'h1;  worderr <= (ev_size != ev_words + 12'h1);
						   end
				   	   data_out  <= word_in[31:0];  ev_words <= ev_words + 12'h1;
				         ready_out <= 1'b1; ack <= 1'b1; timeout <= 16'h0;
						end
			   	end else if( ! evbuffull_in  && ! ready ) begin // don't normally enter this state
					   timeout <= timeout + 16'h1;
						if( timeout == 16'hffff ) begin
						   dcc_state <= dcc_idle;
				   	   data_out  <=  32'hEFFFFFFF; ready_out <= 1'b1; ack <= 1'b0;
						end
					end
		      dcc_error: // write an error word, then read rest of bad event
			      if( ! evbuffull_in  && ready ) begin
				   	dcc_state <= dcc_data;
				   	data_out  <=  {4'hF,word_in[31:28],word_in[23:0]}; ready_out <= 1'b1; ack <= 1'b1;
			   	end else if( ! evbuffull_in  && ! ready ) begin // don't normally enter this state
					   timeout <= timeout + 16'h1;
						if( timeout == 16'hffff ) begin
						   dcc_state <= dcc_idle;
				   	   data_out  <=  32'hEEFFFFFF; ready_out <= 1'b1; ack <= 1'b0;
						end
					end
		      default: dcc_state <= dcc_idle;
		   endcase
		//end
   end
end

endmodule
