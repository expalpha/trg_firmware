// 3-wire spi [scl,sdio,csn] 24bits per transaction ... clk,Datain,trig   dataout,busy     
//     idle:       wait for trigger
//     start:      load data, drive init signals *wait for hold time*
//     setup/hold: set signals for #clks
//     finish:     drive high+turn off
module spi3_24 (
   input  wire             clk, input  wire        reset,
	input  wire [27:0]  data_in, input  wire dataready_in,
	output wire [ 7:0] data_out, output wire     busy_out,
   output wire             sck, output wire  [3:0]   csn,
   inout  wire            sdio, output wire        debug
);
assign debug = sdio_in;

assign sdio    = ( sdio_en ) ? sdio_reg : 1'bz;
assign sdio_in = ( sdio_en ) ? sdio_reg : sdio;
reg sdio_en, sdio_reg, sdio_in;

wire [7:0] clk_time = 8'h02;
wire [7:0] num_bits = 8'h17;

parameter idle=3'h0, start=3'h1, setup=3'h2, hold=3'h3, finish=3'h4;

reg  [2:0]    state;  reg  [3:0]   device;  reg read_bit;
reg [23:0]  command;  reg  [7:0] response;
reg  [7:0]  bit_cnt;  reg  [7:0]  clk_cnt;

always @ (posedge clk or posedge reset) begin
   if( reset ) begin
		sck <= 1'b1; sdio_reg <= 1'b1; sdio_en <= 1'b0;  csn <= 1'b1;
		command  <=  24'h0;	 response <=  8'h0;  read_bit <= 1'b0;
		bit_cnt  <=   8'h0;	 clk_cnt  <=  8'h0;  busy_out <= 1'b0;
	   state    <=   idle;   device   <=  4'h0;
	end else begin
		sck <= 1'b0; sdio_reg <= 1'b1; sdio_en <= 1'b0; csn <= 4'hF; // default I/O state: all off, clk-low
		command  <=  command;   response <= response;  clk_cnt  <= clk_cnt - 8'h1;
		read_bit <= read_bit;   bit_cnt  <=  bit_cnt;  busy_out <= busy_out;  
		state    <=    state;   device   <=   device;
	   case( state )
	      idle:     // wait for trigger
			    if( dataready_in ) begin
				    response <=     8'h0;  bit_cnt  <= num_bits;
					 clk_cnt  <= clk_time;  command  <= data_in[23: 0];
				    busy_out <=     1'b1;  device   <= data_in[27:24];
					 state    <=    start;  read_bit <= data_in[23];
				 end
	      start:    // set chipselect and wait for hold time
			   begin
         		csn <= ~device;
				   if ( clk_cnt == 8'h0 ) begin
	    	         clk_cnt  <= clk_time;
             	   state    <=    setup;
				   end
				end
	      setup:    // set current data/clk bits and wait for setup time
			   begin
					sdio_en  <= ! ( read_bit && bit_cnt < 8'h8 ); // leave off if reading ( read && data bits)
			   	sdio_reg <= command[23];
         		csn      <=     ~device;
					sck      <=        1'b0;  
				   if ( clk_cnt == 8'h0 ) begin
          		   response <= {response[6:0],1'b0};
	    	         clk_cnt  <= clk_time;					   
             	   state    <=     hold;
					end
				end
	      hold:    // hold current data/clk bits for hold time, then go to nextbit/end
			   begin
					sdio_en     <= ! ( read_bit && bit_cnt < 8'h8 ); // leave off if reading
			   	sdio_reg    <= command[23];
			      response[0] <= sdio;                        // could load on a specific clk
         		csn         <= ~device;
					sck         <=   1'b1;
				   if ( clk_cnt == 8'h0 ) begin
					   if( bit_cnt == 8'h0 ) begin
						   state <= finish;
						end else begin
						   command <= {command[22:0],1'b0};
						   bit_cnt <=       bit_cnt - 8'h1;
   	    	         clk_cnt <=             clk_time;
                	   state   <=                setup;
						end
					end
				end
	       finish: begin state <= idle; busy_out <= 1'b0; end
	      default: begin state <= idle; busy_out <= 1'b0; end
	   endcase
	end
end

endmodule
