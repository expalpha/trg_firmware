`default_nettype none
module str0
  (
   input wire clk,
   input wire in,
   output reg out
   );
   
   assign out = in;

endmodule
