`default_nettype none
module mult64
  (
   input wire [63:0] in,
   output wire [7:0] out
   );

   add64 add64_inst
     (
      .data0x(in[0]),
      .data1x(in[1]),
      .data2x(in[2]),
      .data3x(in[3]),
      .data4x(in[4]),
      .data5x(in[5]),
      .data6x(in[6]),
      .data7x(in[7]),

      .data8x(in[8]),
      .data9x(in[9]),
      .data10x(in[10]),
      .data11x(in[11]),
      .data12x(in[12]),
      .data13x(in[13]),
      .data14x(in[14]),
      .data15x(in[15]),

      .data16x(in[16+0]),
      .data17x(in[16+1]),
      .data18x(in[16+2]),
      .data19x(in[16+3]),
      .data20x(in[16+4]),
      .data21x(in[16+5]),
      .data22x(in[16+6]),
      .data23x(in[16+7]),

      .data24x(in[16+8]),
      .data25x(in[16+9]),
      .data26x(in[16+10]),
      .data27x(in[16+11]),
      .data28x(in[16+12]),
      .data29x(in[16+13]),
      .data30x(in[16+14]),
      .data31x(in[16+15]),

      .data32x(in[32+0]),
      .data33x(in[32+1]),
      .data34x(in[32+2]),
      .data35x(in[32+3]),
      .data36x(in[32+4]),
      .data37x(in[32+5]),
      .data38x(in[32+6]),
      .data39x(in[32+7]),

      .data40x(in[32+8]),
      .data41x(in[32+9]),
      .data42x(in[32+10]),
      .data43x(in[32+11]),
      .data44x(in[32+12]),
      .data45x(in[32+13]),
      .data46x(in[32+14]),
      .data47x(in[32+15]),

      .data48x(in[48+0]),
      .data49x(in[48+1]),
      .data50x(in[48+2]),
      .data51x(in[48+3]),
      .data52x(in[48+4]),
      .data53x(in[48+5]),
      .data54x(in[48+6]),
      .data55x(in[48+7]),

      .data56x(in[48+8]),
      .data57x(in[48+9]),
      .data58x(in[48+10]),
      .data59x(in[48+11]),
      .data60x(in[48+12]),
      .data61x(in[48+13]),
      .data62x(in[48+14]),
      .data63x(in[48+15]),

      .result(out)
      );
   
endmodule
