`default_nettype none
module ag_bsc_remap
  (
   input wire [15:0]  adc,
   output wire [7:0]  bsc_bot,
   output wire [7:0]  bsc_top
   );

   assign bsc_top[3] = adc[0];
   assign bsc_top[2] = adc[1];
   assign bsc_top[1] = adc[2];
   assign bsc_top[0] = adc[3];

   assign bsc_top[7] = adc[4];
   assign bsc_top[6] = adc[5];
   assign bsc_top[5] = adc[6];
   assign bsc_top[4] = adc[7];

   assign bsc_bot[0] = adc[8];
   assign bsc_bot[1] = adc[9];
   assign bsc_bot[2] = adc[10];
   assign bsc_bot[3] = adc[11];

   assign bsc_bot[4] = adc[12];
   assign bsc_bot[5] = adc[13];
   assign bsc_bot[6] = adc[14];
   assign bsc_bot[7] = adc[15];

endmodule
