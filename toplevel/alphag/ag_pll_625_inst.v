ag_pll_625	ag_pll_625_inst (
	.clkswitch ( clkswitch_sig ),
	.inclk0 ( inclk0_sig ),
	.inclk1 ( inclk1_sig ),
	.activeclock ( activeclock_sig ),
	.c0 ( c0_sig ),
	.c1 ( c1_sig ),
	.clkbad0 ( clkbad0_sig ),
	.clkbad1 ( clkbad1_sig ),
	.locked ( locked_sig )
	);
