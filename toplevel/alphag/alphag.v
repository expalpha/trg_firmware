`default_nettype none
module alphag
  (
   input wire 		   clk,
   input wire 		   clk_local_625,

   output wire             clk_625_out,
   output wire             clk_10_out,

   // controls and configurations

   input wire [31:0] 	   conf_fw_rev,
   input wire [31:0] 	   conf_control,
   input wire [31:0] 	   conf_mlu,
   input wire [31:0] 	   conf_drift_width,
   input wire [31:0] 	   conf_scaledown,
   input wire [31:0] 	   conf_trig_delay,
   input wire [31:0] 	   conf_trig_width,
   input wire [31:0] 	   conf_busy_width,
   input wire [31:0] 	   conf_pulser_width,
   input wire [31:0] 	   conf_pulser_period,
   input wire [31:0] 	   conf_pulser_burst_ctrl,
   input wire [31:0] 	   conf_trig_enable,
   input wire [31:0] 	   conf_nim_mask,
   input wire [31:0] 	   conf_esata_mask,
   input wire [31:0] 	   conf_adc16_masks[7:0],
   input wire [31:0] 	   conf_adc32_masks[15:0],
   //input wire [31:0] 	   conf_aw16_coinc_a,
   //input wire [31:0] 	   conf_aw16_coinc_b,
   //input wire [31:0] 	   conf_aw16_coinc_c,
   //input wire [31:0] 	   conf_aw16_coinc_d,
   input wire [31:0] 	   conf_counter_adc_select,
   input wire [31:0] 	   conf_bsc_control,
   input wire [31:0] 	   conf_coinc_control,
   input wire [31:0] 	   conf_trig_timeout,

   input wire 		   reset,
   input wire 		   latch,
   input wire 		   sw_trigger,

   // esata link

   input wire 		   esata_clk,
   //input wire 	     esata_run,

   // ethernet clock

   input wire              eth_clk,

   // trigger links

   input wire [3:0] 	   rx_sd_0,
   input wire [3:0] 	   rx_sd_1,
   input wire [3:0] 	   rx_sd_2,
   input wire [3:0] 	   rx_sd_3,

   input wire [15:0][63:0] sas_bits,
   
   // ethernet UDP data

   output reg [31:0]       udp_data_out,
   output reg              udp_sop_out,
   output reg              udp_eop_out,
   output reg              udp_valid_out,
   input wire              udp_ready,

   // output for UDP readout

   output reg [31:0] 	   pll_status_out, // latched

   output reg [31:0] 	   clk_counter_out, // latched
   output reg [31:0] 	   clk_625_counter_out, // latched
   output reg [31:0] 	   esata_clk_counter_out, // latched
   output reg [31:0] 	   esata_clk_esata_counter_out, // latched
   output reg [31:0] 	   eth_clk_counter_out, // latched
   output reg [31:0] 	   eth_clk_eth_counter_out, // latched

   output reg [15:0] 	   sas_sd_out, // latched
   output reg [63:0] 	   sas_bits_out[15:0], // latched
   output reg [31:0] 	   sas_sd_counters_out[15:0], // latched

   output reg [31:0] 	   ts_625_out, // latched
   output reg [31:0] 	   counter_trig_in_out, // latched
   output reg [31:0] 	   counter_drift_out, // latched
   output reg [31:0] 	   counter_scaledown_out, // latched
   output reg [31:0] 	   counter_trig_out_out, // latched
   output reg [31:0] 	   counter_pulser_out, // latched
   output reg [31:0] 	   counter_timeout_out, // latched

   output reg [31:0] 	   counter_adc16_grand_or_out, // latched
   output reg [31:0] 	   counter_adc32_grand_or_out, // latched
   output reg [31:0] 	   counter_adc_grand_or_out, // latched
   output reg [31:0] 	   counter_esata_nim_grand_or_out, // latched

   output reg [31:0] 	   counter_adc_selected_out[15:0], // latched

   //output reg [31:0] 	   counter_adc16_mult_1ormore_out, // latched
   //output reg [31:0] 	   counter_adc16_mult_2ormore_out, // latched
   //output reg [31:0] 	   counter_adc16_mult_3ormore_out, // latched
   //output reg [31:0] 	   counter_adc16_mult_4ormore_out, // latched

   output reg [31:0] 	   counter_adc16_or16_out[15:0], // latched
   output reg [31:0] 	   counter_adc32_or16_out[31:0], // latched

   //output reg [31:0] 	   counter_aw16_mult_1ormore_out, // latched
   //output reg [31:0] 	   counter_aw16_mult_2ormore_out, // latched
   //output reg [31:0] 	   counter_aw16_mult_3ormore_out, // latched
   //output reg [31:0] 	   counter_aw16_mult_4ormore_out, // latched

   //output reg [31:0] 	   counter_aw16_coinc_a_out, // latched
   //output reg [31:0] 	   counter_aw16_coinc_b_out, // latched
   //output reg [31:0] 	   counter_aw16_coinc_c_out, // latched
   //output reg [31:0] 	   counter_aw16_coinc_d_out, // latched
   //output reg [31:0] 	   counter_aw16_coinc_out, // latched

   output reg [31:0] 	   counter_aw16_grand_or_out, // latched
   output reg [31:0] 	   counter_aw16_mlu_start_out, // latched
   output reg [31:0] 	   counter_aw16_mlu_trig_out, // latched

   output reg [31:0] 	   counter_bsc_grand_or_out, // latched
   output reg [31:0] 	   counter_bsc_mult_out, // latched
   output reg [31:0] 	   counter_bsc_mult_start_out, // latched
   output reg [31:0] 	   counter_bsc_mult_trig_out, // latched
   output reg [31:0] 	   counter_bsc64_out[63:0], // latched

   output reg [31:0] 	   counter_coinc_start_out, // latched
   output reg [31:0] 	   counter_coinc_trig_out, // latched

   // board outputs

   //output wire 		   clk_out, // clk_625
   //output wire 		   pulser_out, // clk_625
   //output wire 		   out2,

   // signals sent to the chronobox

   output wire             sw_trig_out,
   output wire             pulser_trig_out_clk_625,
   output wire             esata_nim_trig_out,
   output wire             adc16_grand_or_trig_out,
   output wire             adc32_grand_or_trig_out,

   output wire             aw16_grand_or_trig_out,
   //output wire             aw16_mult_1ormore_out,
   //output wire             aw16_mult_2ormore_out,
   //output wire             aw16_mult_3ormore_out,
   //output wire             aw16_mult_4ormore_out,
   output wire             aw16_mlu_trig_out,

   output wire             bsc64_grand_or_trig_out,
   //output wire             bsc64_mult_1ormore_out,
   //output wire             bsc64_mult_2ormore_out,
   //output wire             bsc64_mult_3ormore_out,
   //output wire             bsc64_mult_4ormore_out,
   output wire             bsc64_mult_trig_out,

   output wire             coinc_trig_out,

   output wire             timeout_trig_out,

   output wire             trig_received_out_clk_625,  // received triggers
   output wire             trig_drift_out_clk_625,     // triggers survived drift time veto
   output wire             trig_scaledown_out_clk_625, // triggers survived scaledown
   output wire             trig_out_clk_625,           // triggers survived busy veto

   output wire             busy_out_clk_625,
   output wire             drift_out_clk_625
   );

   ///////////////////////////////////////////////////////////
   //                   General control bits
   ///////////////////////////////////////////////////////////

   wire 		   conf_clock_select     = conf_control[0];
   wire 		   conf_enable_aw_adc16  = conf_control[1];
   wire 		   conf_enable_aw_adc32a = conf_control[2];
   wire 		   conf_enable_aw_adc32b = conf_control[3];

   wire [7:0] 		   conf_mlu_prompt     = conf_control[23:16];
   wire [7:0] 		   conf_mlu_wait       = conf_control[31:24];

   ///////////////////////////////////////////////////////////
   //                   External 62.5 MHz clock
   ///////////////////////////////////////////////////////////

   wire 		   clk_625 = clk_625_out;
   wire 		   clkbad0_sig;
   wire 		   clkbad1_sig;
   wire 		   locked_sig;
   wire 		   activeclock_sig;

   ag_pll_625 ag_pll_625_inst
     (
      .clkswitch ( conf_clock_select ),
      .inclk0 ( clk_local_625 ),
      .inclk1 ( esata_clk ),
      .activeclock ( activeclock_sig ),
      .c0 ( clk_625_out ),
      .c1 ( clk_10_out ),
      .clkbad0 ( clkbad0_sig ),
      .clkbad1 ( clkbad1_sig ),
      .locked ( locked_sig )
      );

   // counter for locked_sig

   reg [15:0] 		   pll_locked_counter;
   reg 			   prev_locked_sig;

   always_ff @ (posedge clk) begin
      prev_locked_sig <= locked_sig;
      if (prev_locked_sig == 0 && locked_sig == 1) begin
	 pll_locked_counter <= pll_locked_counter + 16'h1;
      end
   end

   // ethernet clock frequency counter

   wire [31:0] 		   eth_clk_counter;
   wire [31:0] 		   eth_clk_eth_counter;

   frequency_counter fc_eth
     (
      .clk1(clk),
      .clk2(eth_clk),
      .clk1_counter_out(eth_clk_counter),
      .clk2_counter_out(eth_clk_eth_counter)
      );

   // external clock frequency counter

   wire [31:0] 		   esata_clk_counter;
   wire [31:0] 		   esata_clk_esata_counter;

   frequency_counter fc_esata
     (
      .clk1(clk),
      .clk2(esata_clk),
      .clk1_counter_out(esata_clk_counter),
      .clk2_counter_out(esata_clk_esata_counter)
      );

   // selected 62.5 MHz clock frequency counter

   wire [31:0] 		   clk_counter;
   wire [31:0] 		   clk_625_counter;

   frequency_counter fc
     (
      .clk1(clk),
      .clk2(clk_625),
      .clk1_counter_out(clk_counter),
      .clk2_counter_out(clk_625_counter)
      );

   wire [31:0] 		   pll_status;

   assign pll_status[31] = locked_sig;
   assign pll_status[30] = activeclock_sig;
   assign pll_status[29] = clkbad1_sig;
   assign pll_status[28] = clkbad0_sig;
   
   assign pll_status[27:16] = 12'h0;
   assign pll_status[15:0] = pll_locked_counter;

   always_ff @ (posedge clk) begin
      if (latch) begin
	 pll_status_out <= pll_status;
	 clk_counter_out <= clk_counter;
	 clk_625_counter_out <= clk_625_counter;
	 esata_clk_counter_out <= esata_clk_counter;
	 esata_clk_esata_counter_out <= esata_clk_esata_counter;
	 eth_clk_counter_out <= eth_clk_counter;
	 eth_clk_eth_counter_out <= eth_clk_eth_counter;
      end
   end

   ///////////////////////////////////////////////////////////
   //                   
   ///////////////////////////////////////////////////////////

   genvar 		   i;

   ///////////////////////////////////////////////////////////
   //                   Mini-SAS trigger logic
   ///////////////////////////////////////////////////////////

   wire 		   adc16_grand_or;
   wire 		   adc32_grand_or;
   wire 		   adc_grand_or;
   wire 		   esata_nim_grand_or;

   //wire [7:0]              adc16_mult;
   //wire 		   adc16_mult_1ormore;
   //wire 		   adc16_mult_2ormore;
   //wire 		   adc16_mult_3ormore;
   //wire 		   adc16_mult_4ormore;

   wire [15:0] 		   adc16_bits[15:0];
   wire [15:0] 		   adc32_bits[31:0];

   wire [31:0] 		   nim_bits;
   wire [31:0] 		   esata_bits;

   wire [15:0] 		   adc16_or16;
   wire [31:0] 		   adc32_or16;

   ag_sas_proc ag_sas_proc
     (
      .clk(clk),
      .reset(reset),
      .latch(latch),

      .conf_nim_mask(conf_nim_mask),
      .conf_esata_mask(conf_esata_mask),
      .conf_adc16_masks(conf_adc16_masks),
      .conf_adc32_masks(conf_adc32_masks),

      .rx_sd_0(rx_sd_0),
      .rx_sd_1(rx_sd_1),
      .rx_sd_2(rx_sd_2),
      .rx_sd_3(rx_sd_3),
      .sas_bits(sas_bits),

      .sas_sd_out(sas_sd_out),
      .sas_bits_out(sas_bits_out),
      .sas_sd_counters_out(sas_sd_counters_out),

      .nim_bits_out(nim_bits),
      .esata_bits_out(esata_bits),

      .adc16_bits_out(adc16_bits),
      .adc32_bits_out(adc32_bits),

      .counter_adc16_grand_or_out(counter_adc16_grand_or_out),
      .counter_adc32_grand_or_out(counter_adc32_grand_or_out),
      .counter_adc_grand_or_out(counter_adc_grand_or_out),
      .counter_esata_nim_grand_or_out(counter_esata_nim_grand_or_out),

      .adc16_grand_or_out(adc16_grand_or),
      .adc32_grand_or_out(adc32_grand_or),
      .adc_grand_or_out(adc_grand_or),
      .esata_nim_grand_or_out(esata_nim_grand_or),

      //.adc16_mult_out(adc16_mult),
      //.adc16_mult_1ormore_out(adc16_mult_1ormore),
      //.adc16_mult_2ormore_out(adc16_mult_2ormore),
      //.adc16_mult_3ormore_out(adc16_mult_3ormore),
      //.adc16_mult_4ormore_out(adc16_mult_4ormore),
      //
      //.counter_adc16_mult_1ormore_out(counter_adc16_mult_1ormore_out),
      //.counter_adc16_mult_2ormore_out(counter_adc16_mult_2ormore_out),
      //.counter_adc16_mult_3ormore_out(counter_adc16_mult_3ormore_out),
      //.counter_adc16_mult_4ormore_out(counter_adc16_mult_4ormore_out),

      .adc16_or16_out(adc16_or16),
      .adc32_or16_out(adc32_or16),

      .counter_adc16_or16_out(counter_adc16_or16_out),
      .counter_adc32_or16_out(counter_adc32_or16_out)
   );

   ///////////////////////////////////////////////////////////
   //           Counters for selected group of 16 ADCs
   ///////////////////////////////////////////////////////////

   reg [15:0] 		   adc_selected;
   wire [1:0] 		   conf_counter_adc_select_group = conf_counter_adc_select[5:4];

   always_ff @ (posedge clk) begin
      if (conf_counter_adc_select_group == 2'b00) begin
	 adc_selected <= adc16_bits[conf_counter_adc_select[3:0]];
      end else if (conf_counter_adc_select_group == 2'b01) begin
	 adc_selected <= adc32_bits[{1'b0,conf_counter_adc_select[3:0]}]; // quartus warning is correct, we can only address adc32_bits[15 through 0] here. K.O.
      end else if (conf_counter_adc_select_group == 2'b10) begin
	 adc_selected <= adc32_bits[{1'b1,conf_counter_adc_select[3:0]}]; // we address adc32_bits[31 through 8], somehow there is no quartus warning here. K.O.
      end else begin
	 adc_selected <= 16'h0000;
      end
   end
 		   
   wire [31:0] 		   counter_adc_selected[15:0];

   generate
      for (i=0; i<16; i=i+1) begin: cnt_adc_selected_block
	 counter cnt_adc_selected_i(.clk(clk), .reset(reset), .in(adc_selected[i]), .out(counter_adc_selected[i]));
      end
   endgenerate

   always_ff @ (posedge clk) begin
      if (latch) begin
	 counter_adc_selected_out <= counter_adc_selected;
      end
   end
	 
   ///////////////////////////////////////////////////////////
   //           TPC Anode Wire trigger logic (aw16)
   ///////////////////////////////////////////////////////////

   // map adc signals into aw16

   wire [15:0] 		   aw16_bus = ({16{conf_enable_aw_adc16}} & adc16_or16[15:0]) |
			   ({16{conf_enable_aw_adc32a}} & adc32_or16[15:0]) |
			   ({16{conf_enable_aw_adc32b}} & adc32_or16[31:16]);

   wire                    aw16_grand_or = |aw16_bus;
   

//   // condition the aw16 signals
//
//   wire [15:0] 		   aw16c;
//
//   ag_conditioner cond_adc16_or16_0(.clk(clk), .in(aw16_bus[0]), .out(aw16c[0]));
//   ag_conditioner cond_adc16_or16_1(.clk(clk), .in(aw16_bus[1]), .out(aw16c[1]));
//   ag_conditioner cond_adc16_or16_2(.clk(clk), .in(aw16_bus[2]), .out(aw16c[2]));
//   ag_conditioner cond_adc16_or16_3(.clk(clk), .in(aw16_bus[3]), .out(aw16c[3]));
//
//   ag_conditioner cond_adc16_or16_4(.clk(clk), .in(aw16_bus[4]), .out(aw16c[4]));
//   ag_conditioner cond_adc16_or16_5(.clk(clk), .in(aw16_bus[5]), .out(aw16c[5]));
//   ag_conditioner cond_adc16_or16_6(.clk(clk), .in(aw16_bus[6]), .out(aw16c[6]));
//   ag_conditioner cond_adc16_or16_7(.clk(clk), .in(aw16_bus[7]), .out(aw16c[7]));
//
//   ag_conditioner cond_adc16_or16_8(.clk(clk), .in(aw16_bus[8]), .out(aw16c[8]));
//   ag_conditioner cond_adc16_or16_9(.clk(clk), .in(aw16_bus[9]), .out(aw16c[9]));
//   ag_conditioner cond_adc16_or16_10(.clk(clk), .in(aw16_bus[10]), .out(aw16c[10]));
//   ag_conditioner cond_adc16_or16_11(.clk(clk), .in(aw16_bus[11]), .out(aw16c[11]));
//
//   ag_conditioner cond_adc16_or16_12(.clk(clk), .in(aw16_bus[12]), .out(aw16c[12]));
//   ag_conditioner cond_adc16_or16_13(.clk(clk), .in(aw16_bus[13]), .out(aw16c[13]));
//   ag_conditioner cond_adc16_or16_14(.clk(clk), .in(aw16_bus[14]), .out(aw16c[14]));
//   ag_conditioner cond_adc16_or16_15(.clk(clk), .in(aw16_bus[15]), .out(aw16c[15]));
//
//   // latch aw16c signals into aw16c_bus
//
//   reg [15:0] 		   aw16c_bus;
//
//   always_ff @ (posedge clk) begin
//      aw16c_bus <= aw16c;
//   end

   ///////////////////////////////////////////////////////////
   //           TPC Anode Wire (aw16) multiplicity
   ///////////////////////////////////////////////////////////

//   wire [7:0] aw16_mult_wire;
//
//   mult16 xadc16_mult_inst(.in(aw16c_bus), .out(aw16_mult_wire));
//
//   reg [7:0] aw16_mult;
//   reg 	     aw16_mult_1ormore;
//   reg 	     aw16_mult_2ormore;
//   reg 	     aw16_mult_3ormore;
//   reg 	     aw16_mult_4ormore;
//
//   always_ff @ (posedge clk) begin
//      aw16_mult <= aw16_mult_wire;
//      aw16_mult_1ormore <= aw16_mult_wire >= 1;
//      aw16_mult_2ormore <= aw16_mult_wire >= 2;
//      aw16_mult_3ormore <= aw16_mult_wire >= 3;
//      aw16_mult_4ormore <= aw16_mult_wire >= 4;
//   end
//
//   wire [31:0] counter_aw16_mult_1ormore;
//   wire [31:0] counter_aw16_mult_2ormore;
//   wire [31:0] counter_aw16_mult_3ormore;
//   wire [31:0] counter_aw16_mult_4ormore;
//
//   counter cnt_aw16_mult_1(.clk(clk), .reset(reset), .in(aw16_mult_1ormore), .out(counter_aw16_mult_1ormore));
//   counter cnt_aw16_mult_2(.clk(clk), .reset(reset), .in(aw16_mult_2ormore), .out(counter_aw16_mult_2ormore));
//   counter cnt_aw16_mult_3(.clk(clk), .reset(reset), .in(aw16_mult_3ormore), .out(counter_aw16_mult_3ormore));
//   counter cnt_aw16_mult_4(.clk(clk), .reset(reset), .in(aw16_mult_4ormore), .out(counter_aw16_mult_4ormore));
//
//   always_ff @ (posedge clk) begin
//      if (latch) begin
//	 counter_aw16_mult_1ormore_out <= counter_aw16_mult_1ormore;
//	 counter_aw16_mult_2ormore_out <= counter_aw16_mult_2ormore;
//	 counter_aw16_mult_3ormore_out <= counter_aw16_mult_3ormore;
//	 counter_aw16_mult_4ormore_out <= counter_aw16_mult_4ormore;
//      end
//   end

   wire [31:0] counter_aw16_grand_or;

   counter cnt_aw16_grand_or(.clk(clk), .reset(reset), .in(aw16_grand_or), .out(counter_aw16_grand_or));

   always_ff @ (posedge clk) begin
      if (latch) begin
	 counter_aw16_grand_or_out <= counter_aw16_grand_or;
      end
   end

   ///////////////////////////////////////////////////////////
   //           TPC Anode Wire (aw16) coincidence
   ///////////////////////////////////////////////////////////

//   wire [15:0] conf_aw16_coinc_mask_0 = conf_aw16_coinc_a[15:0];
//   wire [15:0] conf_aw16_coinc_mask_1 = conf_aw16_coinc_a[31:16];
//   wire [15:0] conf_aw16_coinc_mask_2 = conf_aw16_coinc_b[15:0];
//   wire [15:0] conf_aw16_coinc_mask_3 = conf_aw16_coinc_b[31:16];
//   wire [15:0] conf_aw16_coinc_mask_4 = conf_aw16_coinc_c[15:0];
//   wire [15:0] conf_aw16_coinc_mask_5 = conf_aw16_coinc_c[31:16];
//   wire [15:0] conf_aw16_coinc_mask_6 = conf_aw16_coinc_d[15:0];
//   wire [15:0] conf_aw16_coinc_mask_7 = conf_aw16_coinc_d[31:16];
//
//   wire cc0 = |(aw16c_bus & conf_aw16_coinc_mask_0);
//   wire cc1 = |(aw16c_bus & conf_aw16_coinc_mask_1);
//   wire cc2 = |(aw16c_bus & conf_aw16_coinc_mask_2);
//   wire cc3 = |(aw16c_bus & conf_aw16_coinc_mask_3);
//
//   wire cc4 = |(aw16c_bus & conf_aw16_coinc_mask_4);
//   wire cc5 = |(aw16c_bus & conf_aw16_coinc_mask_5);
//   wire cc6 = |(aw16c_bus & conf_aw16_coinc_mask_6);
//   wire cc7 = |(aw16c_bus & conf_aw16_coinc_mask_7);
//
//   wire	cc_a_wire = cc0 & cc1;
//   wire	cc_b_wire = cc2 & cc3;
//   wire	cc_c_wire = cc4 & cc5;
//   wire	cc_d_wire = cc6 & cc7;
//
//   wire xcc_a;
//   wire xcc_b;
//   wire xcc_c;
//   wire xcc_d;
//   
//   ag_conditioner cond_cc_a(.clk(clk), .in(cc_a_wire), .out(xcc_a));
//   ag_conditioner cond_cc_b(.clk(clk), .in(cc_b_wire), .out(xcc_b));
//   ag_conditioner cond_cc_c(.clk(clk), .in(cc_c_wire), .out(xcc_c));
//   ag_conditioner cond_cc_d(.clk(clk), .in(cc_d_wire), .out(xcc_d));
//
//   reg 	aw16_coinc_a;
//   reg 	aw16_coinc_b;
//   reg 	aw16_coinc_c;
//   reg 	aw16_coinc_d;
//
//   always_ff @ (posedge clk) begin
//      aw16_coinc_a <= xcc_a;
//      aw16_coinc_b <= xcc_b;
//      aw16_coinc_c <= xcc_c;
//      aw16_coinc_d <= xcc_d;
//   end
//   
//   reg 	aw16_coinc_trig;
//   
//   always_ff@ (posedge clk) begin
//      aw16_coinc_trig <= 1'h0 |
//			 (aw16_coinc_a & conf_enable_aw16_coinc_a) |
//			 (aw16_coinc_b & conf_enable_aw16_coinc_b) |
//			 (aw16_coinc_c & conf_enable_aw16_coinc_c) |
//			 (aw16_coinc_d & conf_enable_aw16_coinc_d) |
//			 1'h0;
//   end
//
//   // counters for the coincidence trigger
//
//   reg [31:0] counter_aw16_coinc_a;
//   reg [31:0] counter_aw16_coinc_b;
//   reg [31:0] counter_aw16_coinc_c;
//   reg [31:0] counter_aw16_coinc_d;
//   reg [31:0] counter_aw16_coinc;
//
//   counter cnt_aw16_coinc_a(.clk(clk), .reset(reset), .in(aw16_coinc_a), .out(counter_aw16_coinc_a));
//   counter cnt_aw16_coinc_b(.clk(clk), .reset(reset), .in(aw16_coinc_b), .out(counter_aw16_coinc_b));
//   counter cnt_aw16_coinc_c(.clk(clk), .reset(reset), .in(aw16_coinc_c), .out(counter_aw16_coinc_c));
//   counter cnt_aw16_coinc_d(.clk(clk), .reset(reset), .in(aw16_coinc_d), .out(counter_aw16_coinc_d));
//   counter cnt_aw16_coinc(  .clk(clk), .reset(reset), .in(aw16_coinc_trig),   .out(counter_aw16_coinc));
//
//   always_ff @ (posedge clk) begin
//      if (latch) begin
//	 counter_aw16_coinc_a_out <= counter_aw16_coinc_a;
//	 counter_aw16_coinc_b_out <= counter_aw16_coinc_b;
//	 counter_aw16_coinc_c_out <= counter_aw16_coinc_c;
//	 counter_aw16_coinc_d_out <= counter_aw16_coinc_d;
//	 counter_aw16_coinc_out <= counter_aw16_coinc;
//      end
//   end

   ///////////////////////////////////////////////////////////
   //                   AW Prompt bitmap
   ///////////////////////////////////////////////////////////

   wire aw16_or = |aw16_bus[15:0];

   reg 	      aw16_prompt_run;
   reg [7:0]  aw16_prompt_cnt;

   reg 	      aw16_prompt_wait;
   reg [7:0]  aw16_prompt_wait_cnt;
   
   reg [15:0] aw16_prompt;
   reg 	      aw16_prompt_out;

   always_ff @ (posedge clk) begin
      if (aw16_prompt_run) begin
	 if (aw16_prompt_cnt == 0) begin
	    if (aw16_or) begin
	       aw16_prompt_wait_cnt <= conf_mlu_wait;
	       aw16_prompt_wait <= 1;
	       aw16_prompt_run <= 1;
	    end else if (aw16_prompt_wait_cnt == 0) begin
	       aw16_prompt_wait <= 0;
	       aw16_prompt_run <= 0;
	    end else if (aw16_prompt_wait) begin
	       aw16_prompt_wait_cnt <= aw16_prompt_wait_cnt - 8'h1;
	       aw16_prompt_wait <= 1;
	       aw16_prompt_run <= 1;
	    end
	    aw16_prompt_out <= 0;
	 end else if (aw16_prompt_cnt == 1) begin
	    aw16_prompt_out <= 1;
	    aw16_prompt_cnt <= aw16_prompt_cnt - 8'h1;
	    // we switch from prompt_run state to prompt_wait state
	    aw16_prompt_wait <= 1;
	    aw16_prompt_wait_cnt <= conf_mlu_wait;
	 end else begin
	    aw16_prompt_cnt <= aw16_prompt_cnt - 8'h1;
	    aw16_prompt_out <= 0;
	    aw16_prompt <= aw16_prompt | aw16_bus;
	 end
      end else if (aw16_or) begin
	 aw16_prompt_out <= 0;
	 aw16_prompt_run <= 1;
	 aw16_prompt_cnt <= conf_mlu_prompt;
	 aw16_prompt <= aw16_bus;
      end else begin
	 aw16_prompt_out <= 0;
	 aw16_prompt_run <= 0;
      end
   end

   ///////////////////////////////////////////////////////////
   //                   AW MLU
   ///////////////////////////////////////////////////////////

   //wire			    mlu_reset = conf_mlu[31]; // MLU reset to zero
   wire 		    mlu_write = conf_mlu[30]; // write to MLU ram
   wire 		    mlu_write_data = conf_mlu[16];
   wire [15:0] 		    mlu_write_addr = conf_mlu[15:0];

   wire [15:0] mlu_read_addr = aw16_prompt[15:0];

   wire [15:0] mlu_addr = (mlu_write ? mlu_write_addr : mlu_read_addr);

   wire        mlu_out;

   ag_mlu_ram ag_mlu_ram_inst
     (
      .address (mlu_addr),
      .clock (clk),
      .data (mlu_write_data),
      .wren (mlu_write),
      .q (mlu_out)
      );

   reg 	       aw16_mlu_trig;

   reg [15:0]  udp_aw16_prompt;
   reg 	       udp_aw16_mlu_out;
   
   always_ff @ (posedge clk) begin
      if (aw16_prompt_out) begin
	 aw16_mlu_trig <= mlu_out;
	 udp_aw16_prompt <= aw16_prompt;
	 udp_aw16_mlu_out <= mlu_out;
      end else begin
	 aw16_mlu_trig <= 0;
      end
   end

   reg [31:0] counter_aw16_mlu_start;
   reg [31:0] counter_aw16_mlu_trig;

   counter cnt_aw16_mlu_start(.clk(clk),   .reset(reset), .in(aw16_prompt_run), .out(counter_aw16_mlu_start));
   counter cnt_aw16_mlu_trig( .clk(clk),   .reset(reset), .in(aw16_mlu_trig),   .out(counter_aw16_mlu_trig));

   always_ff @ (posedge clk) begin
      if (latch) begin
	 counter_aw16_mlu_start_out <= counter_aw16_mlu_start;
	 counter_aw16_mlu_trig_out <= counter_aw16_mlu_trig;
      end
   end

   ///////////////////////////////////////////////////////////
   //           Barrel scintillator trigger
   ///////////////////////////////////////////////////////////

   wire       conf_bsc_adc16_a = conf_bsc_control[0];
   wire       conf_bsc_adc16_b = conf_bsc_control[1];

   wire       conf_bsc64_bot_only    = conf_bsc_control[2];
   wire       conf_bsc64_top_only    = conf_bsc_control[3];
   wire       conf_bsc64_bot_top_or  = conf_bsc_control[4];
   wire       conf_bsc64_bot_top_and = conf_bsc_control[5];

   wire [7:0] conf_bsc64_mult        = conf_bsc_control[15:8];
   wire [7:0] conf_bsc64_empty_window= conf_bsc_control[23:16];
   wire [7:0] conf_bsc64_mult_window = conf_bsc_control[31:24];

   wire [15:0] bsc_adc16[7:0];

   always_comb begin
      if (conf_bsc_adc16_a) begin
	 bsc_adc16[0] = adc16_bits[0];
	 bsc_adc16[1] = adc16_bits[1];
	 bsc_adc16[2] = adc16_bits[2];
	 bsc_adc16[3] = adc16_bits[3];
	 bsc_adc16[4] = adc16_bits[4];
	 bsc_adc16[5] = adc16_bits[5];
	 bsc_adc16[6] = adc16_bits[6];
	 bsc_adc16[7] = adc16_bits[7];
      end else if (conf_bsc_adc16_b) begin
	 bsc_adc16[0] = adc16_bits[8];
	 bsc_adc16[1] = adc16_bits[9];
	 bsc_adc16[2] = adc16_bits[10];
	 bsc_adc16[3] = adc16_bits[11];
	 bsc_adc16[4] = adc16_bits[12];
	 bsc_adc16[5] = adc16_bits[13];
	 bsc_adc16[6] = adc16_bits[14];
	 bsc_adc16[7] = adc16_bits[15];
      end else begin
	 bsc_adc16[0] = 0;
	 bsc_adc16[1] = 0;
	 bsc_adc16[2] = 0;
	 bsc_adc16[3] = 0;
	 bsc_adc16[4] = 0;
	 bsc_adc16[5] = 0;
	 bsc_adc16[6] = 0;
	 bsc_adc16[7] = 0;
      end
   end

   wire [63:0] bsc64_top;
   wire [63:0] bsc64_bot;

   ag_bsc_remap bsc_remap_a(.adc(bsc_adc16[0]), .bsc_bot(bsc64_bot[7:0]), .bsc_top(bsc64_top[7:0]));
   ag_bsc_remap bsc_remap_b(.adc(bsc_adc16[1]), .bsc_bot(bsc64_bot[8+7:8+0]), .bsc_top(bsc64_top[8+7:8+0]));
   ag_bsc_remap bsc_remap_c(.adc(bsc_adc16[2]), .bsc_bot(bsc64_bot[2*8+7:2*8+0]), .bsc_top(bsc64_top[2*8+7:2*8+0]));
   ag_bsc_remap bsc_remap_d(.adc(bsc_adc16[3]), .bsc_bot(bsc64_bot[3*8+7:3*8+0]), .bsc_top(bsc64_top[3*8+7:3*8+0]));

   ag_bsc_remap bsc_remap_e(.adc(bsc_adc16[4]), .bsc_bot(bsc64_bot[4*8+7:4*8+0]), .bsc_top(bsc64_top[4*8+7:4*8+0]));
   ag_bsc_remap bsc_remap_f(.adc(bsc_adc16[5]), .bsc_bot(bsc64_bot[5*8+7:5*8+0]), .bsc_top(bsc64_top[5*8+7:5*8+0]));
   ag_bsc_remap bsc_remap_g(.adc(bsc_adc16[6]), .bsc_bot(bsc64_bot[6*8+7:6*8+0]), .bsc_top(bsc64_top[6*8+7:6*8+0]));
   ag_bsc_remap bsc_remap_h(.adc(bsc_adc16[7]), .bsc_bot(bsc64_bot[7*8+7:7*8+0]), .bsc_top(bsc64_top[7*8+7:7*8+0]));

   reg [63:0] bsc64_bus;

   always_ff @ (posedge clk) begin
      if (conf_bsc64_bot_only) begin
	 bsc64_bus <= bsc64_bot;
      end else if (conf_bsc64_top_only) begin
	 bsc64_bus <= bsc64_top;
      end else if (conf_bsc64_bot_top_or) begin
	 bsc64_bus <= bsc64_bot | bsc64_top;
      end else if (conf_bsc64_bot_top_and) begin
	 bsc64_bus <= bsc64_bot & bsc64_top;
      end else begin
	 bsc64_bus <= 64'b0;
      end
   end // always_ff @

   wire bsc64_grand_or = |bsc64_bus;

   wire [7:0] bsc64_mult;

   mult64 mult64_inst
     (
      .in(bsc64_bus),
      .out(bsc64_mult)
      );

   reg 	bsc_grand_or_trig;
   reg 	bsc_mult_trig;
   
   always_ff @ (posedge clk) begin
      bsc_grand_or_trig <= bsc64_grand_or;
      if (bsc64_mult >= conf_bsc64_mult) begin
         bsc_mult_trig <= 1;
      end else begin
         bsc_mult_trig <= 0;
      end

//      bsc64_mult_1ormore_out <= (bsc64_mult >= 1);
//      bsc64_mult_2ormore_out <= (bsc64_mult >= 2);
//      bsc64_mult_3ormore_out <= (bsc64_mult >= 3);
//      bsc64_mult_4ormore_out <= (bsc64_mult >= 4);
   end
   
   reg [31:0] 	   counter_bsc_grand_or;
   reg [31:0] 	   counter_bsc_mult;
   reg [31:0] 	   counter_bsc_mult_start;
   reg [31:0] 	   counter_bsc_mult_trig;

   ///////////////////////////////////////////////////////////
   //           Barrel scintillator multiplicity
   ///////////////////////////////////////////////////////////


   // new bsc64 multiplicty:
   // first hit opens multiplicity window
   // window runs for a fixed number of clocks, collects bsc64_bus hits
   // after window closes, multiplicity is computed
   // and if conditions are satisfied, bsc64 multiplicity trigger is fired.

   reg [63:0]      bsc64_mult_latch;
   wire [7:0]      bsc64_mult_latch_mult_wire;

   mult64 bsc64_mult_mult64_inst
     (
      .in(bsc64_mult_latch),
      .out(bsc64_mult_latch_mult_wire)
      );

   reg [63:0]      bsc64_mult_l1; // bsc64 bus
   reg [7:0]       bsc64_mult_m1; // bsc64 multiplicity

   always_ff @ (posedge clk) begin
      bsc64_mult_l1 <= bsc64_mult_latch;
      bsc64_mult_m1 <= bsc64_mult_latch_mult_wire;
   end

   reg [7:0]       bsc64_mult_window;
   reg [7:0]       bsc64_empty_window;

   reg             bsc64_mult_trig; // trigger output
   reg [63:0]      bsc64_mult_trig_latch; // latched bsc64_bus
   reg [7:0]       bsc64_mult_trig_mult;  // latched multiplicity

   // state machine on bsc64_mult_window:
   // 0 - idle
   //     if (bsc64_grand_or) load conf_bsc64_mult_window
   //
   // conf_bsc64_mult_window..3 - run the multiplicity window
   //     if (latch_mult > conf_bsc64_mult) bsc64_mult_window <= 3
   //     subtract 1
   //     latch |= bsc64_bus
   //     latch_mult = multiplicty(latch)
   //
   // 3 - wait for bsc64_bus empty
   //     if (!bsc64_grand_or) bsc64_mult_window <= 2
   //
   // 2 - run the "empty" window
   //     if (bsc64_grand_or) bsc64_mult_window <= 3
   //     if (bsc64_empty_window == 0) bsc64_mult_window <= 1
   //     else bsc64_empty_window--;
   //
   // 1 - end of window, prepare for idle
   //     bsc64_mult_window <= 0
   //

   always_ff @ (posedge clk) begin
      if (reset) begin
         // reset
         counter_bsc_mult_start <= 0;
         counter_bsc_mult_trig  <= 0;
         bsc64_empty_window <= 8'd0;
         bsc64_mult_window <= 8'd0;
         bsc64_mult_latch <= 64'd0;
         bsc64_mult_trig <= 0;
         bsc64_mult_trig_latch <= 64'd0;
         bsc64_mult_trig_mult <= 8'd0;
      end else if (conf_bsc64_mult_window == 8'd0) begin
         // window is disabled
         bsc64_mult_latch <= 64'd0;
         bsc64_mult_trig <= 0;
      end else if (bsc64_mult_window == 8'd0) begin
         // window is not running
         bsc64_mult_trig <= 0;
         bsc64_mult_latch <= 64'd0;
         if (bsc64_grand_or) begin
            // window opens
            bsc64_mult_window <= conf_bsc64_mult_window;
            bsc64_mult_latch <= bsc64_bus;
            counter_bsc_mult_start <= counter_bsc_mult_start + 1;
         end
      end else if (bsc64_mult_window == 8'd1) begin
         // end of window
         bsc64_mult_window <= 8'd0;
         bsc64_mult_trig <= 0;
         bsc64_mult_latch <= 64'd0;
      end else if (bsc64_mult_window == 8'd2) begin
         bsc64_mult_trig <= 0;
         // wait while idle
         if (bsc64_grand_or) begin
            // bsc64_bus has stuff
            bsc64_mult_window <= 8'd3;
         end else begin
            // bsc64_bus is empty
            if (bsc64_empty_window == 8'd0) begin
               bsc64_empty_window <= 8'd0;
               bsc64_mult_window <= 8'd1;
            end else begin
               bsc64_empty_window <= bsc64_empty_window - 8'd1;
            end
         end
      end else if (bsc64_mult_window == 8'd3) begin
         bsc64_mult_trig <= 0;
         // wait for bsc64_bus empty
         if (bsc64_grand_or) begin
            // bsc64_bus still has stuff
         end else begin
            // bsc64_bus is empty
            bsc64_mult_window <= 8'd2;
            bsc64_empty_window <= conf_bsc64_empty_window;
         end
      end else begin
         // window is running
         bsc64_mult_window <= bsc64_mult_window - 8'd1;
         bsc64_mult_latch <= bsc64_mult_latch | bsc64_bus;

         if (bsc64_mult_m1 >= conf_bsc64_mult) begin
            // trigger conditions satisifed
            bsc64_mult_trig <= 1;
            bsc64_mult_trig_latch <= bsc64_mult_l1;
            bsc64_mult_trig_mult  <= bsc64_mult_m1;
            counter_bsc_mult_trig <= counter_bsc_mult_trig + 1;
            bsc64_mult_window <= 8'd3; // wait for bsc64_bus empty
         end
      end
   end

   counter cnt_bsc_grand_or(.clk(clk), .reset(reset), .in(bsc_grand_or_trig), .out(counter_bsc_grand_or));
   counter cnt_bsc_mult(    .clk(clk), .reset(reset), .in(bsc_mult_trig),     .out(counter_bsc_mult));

   always_ff @ (posedge clk) begin
      if (latch) begin
	 counter_bsc_grand_or_out <= counter_bsc_grand_or;
	 counter_bsc_mult_out <= counter_bsc_mult;
         counter_bsc_mult_start_out <= counter_bsc_mult_start;
         counter_bsc_mult_trig_out <= counter_bsc_mult_trig;
      end
   end

   reg [31:0] 	   counter_bsc64[63:0];

   generate
      for (i=0; i<64; i=i+1) begin: cnt_bsc64_block
	 counter cnt_bsc64_i(.clk(clk), .reset(reset), .in(bsc64_bus[i]), .out(counter_bsc64[i]));
      end
   endgenerate
   
   always_ff @ (posedge clk) begin
      if (latch) begin
	 counter_bsc64_out <= counter_bsc64;
      end
   end

   ///////////////////////////////////////////////////////////
   //                   Coincidence trigger
   ///////////////////////////////////////////////////////////

   wire [7:0] conf_coinc_require = conf_coinc_control[7:0];
   wire [7:0] conf_coinc_window  = conf_coinc_control[15:8];
   wire [7:0] conf_coinc_start   = conf_coinc_control[23:16];

   wire [7:0] coinc_input;
   
   assign coinc_input[0] = aw16_grand_or; // aw16_mult_1ormore;
   assign coinc_input[1] = aw16_mlu_trig;
   assign coinc_input[2] = bsc_grand_or_trig;
   assign coinc_input[3] = bsc64_mult_trig;
   assign coinc_input[4] = esata_nim_grand_or;
   assign coinc_input[5] = 0;
   assign coinc_input[6] = 0;
   assign coinc_input[7] = 0;

   reg [7:0]  coinc_is; // input start
   reg [7:0]  coinc_ir; // input required

   reg [7:0]  coinc_is1;
   reg [7:0]  coinc_ir1;
   reg [7:0]  coinc_is2;
   reg [7:0]  coinc_ir2;

   always_ff @ (posedge clk) begin
      coinc_is1 <= coinc_input & conf_coinc_start;
      coinc_ir1 <= coinc_input & conf_coinc_require;
      coinc_is2 <= coinc_is1;
      coinc_ir2 <= coinc_ir1;
      coinc_is <= coinc_is1 & ~coinc_is2;
      coinc_ir <= coinc_ir1 & ~coinc_ir2;
   end
   
   reg [7:0]  coinc_window;
   reg [7:0]  coinc_latch;
   reg        coinc_trig;

   reg [31:0] counter_coinc_start;
   reg [31:0] counter_coinc_trig;
   
   reg [63:0] coinc_bsc64_latch;
   reg [7:0]  coinc_bsc64_mult;

   reg [15:0] coinc_aw16_latch;
   reg [7:0]  coinc_aw16_mult;

   always_ff @ (posedge clk) begin
      if (conf_coinc_window == 8'd0 || reset) begin
         // coincidence is disabled
         coinc_window <= 8'd0;
         coinc_latch <= 8'd0;
         coinc_trig <= 1'd0;
         coinc_bsc64_latch <= 64'd0;
         coinc_bsc64_mult <= 8'd0;
         coinc_aw16_latch <= 16'd0;
         coinc_aw16_mult <= 8'd0;
         counter_coinc_start <= 0;
         counter_coinc_trig <= 0;
      end else if (coinc_window == 0) begin
         // coincidence is idle
	 if (coinc_is != 8'd0) begin
	    coinc_window <= conf_coinc_window;
	    coinc_latch <= coinc_ir;
            counter_coinc_start <= counter_coinc_start + 1;
            if (coinc_is[1]) begin // aw16 mlu
               coinc_aw16_latch <= aw16_prompt;
               coinc_aw16_mult <= 8'hE2;
            end else if (coinc_is[0]) begin // aw16 grand or
               coinc_aw16_latch <= aw16_bus;
               coinc_aw16_mult <= 8'hE1;
            end else begin
               coinc_aw16_latch <= 16'd0;
               coinc_aw16_mult <= 8'hE0;
            end
            if (coinc_is[3]) begin // bsc64 mult
               coinc_bsc64_latch <= bsc64_mult_trig_latch;
               coinc_bsc64_mult <= bsc64_mult_trig_mult;
            end else if (coinc_is[2]) begin // bsc64 grand or
               coinc_bsc64_latch <= bsc64_bus;
               coinc_bsc64_mult <= 8'hE4;
            end else begin
               coinc_bsc64_latch <= 64'd0;
               coinc_bsc64_mult <= 8'hE3;
            end
	 end else begin
	    coinc_window <= 8'd0;
	    coinc_latch <= 8'd0;
	 end
         coinc_trig <= 0;
      end else if (coinc_window == 2) begin
         // wait for empty
         coinc_trig <= 0;
	 coinc_latch <= 8'd0;
	 if (coinc_ir != 8'd0) begin
            // wait
         end else if (coinc_is != 8'd0) begin
            // wait
         end else begin
	    coinc_window <= 8'd1;
         end
      end else if (coinc_window == 1) begin
         // reset the coincidence
         coinc_trig <= 0;
	 coinc_window <= 8'd0;
	 coinc_latch <= 8'd0;
      end else begin
	 coinc_window <= coinc_window - 8'd1;
	 coinc_latch <= coinc_latch | coinc_ir;
         if (coinc_ir[1]) begin // aw16 mlu
            coinc_aw16_latch <= aw16_prompt;
            coinc_aw16_mult <= 8'hE6;
         end else if (coinc_ir[0]) begin // aw16 grand or
            coinc_aw16_latch <= aw16_bus;
            coinc_aw16_mult <= 8'hE5;
         end
         if (coinc_ir[3]) begin // bsc64 mult
            coinc_bsc64_latch <= bsc64_mult_trig_latch;
            coinc_bsc64_mult <= bsc64_mult_trig_mult;
         end else if (coinc_ir[2]) begin // bsc64 grand or
            coinc_bsc64_latch <= bsc64_bus;
            coinc_bsc64_mult <= 8'hE7;
         end
         if (coinc_latch == conf_coinc_require) begin
            coinc_trig <= 1;
            coinc_window <= 8'd2;
            counter_coinc_trig <= counter_coinc_trig + 1;
         end
      end
   end
   
   always_ff @ (posedge clk) begin
      if (latch) begin
	 counter_coinc_start_out <= counter_coinc_start;
	 counter_coinc_trig_out <= counter_coinc_trig;
      end
   end

   ///////////////////////////////////////////////////////////
   //                   Pulser
   ///////////////////////////////////////////////////////////

   wire pulser_clk_625;
   
   ag_pulser ag_pulser
     (
      .clk(clk_625),
      .reset(reset),
      .conf_pulser_width(conf_pulser_width),
      .conf_pulser_period(conf_pulser_period),
      .conf_pulser_burst_ctrl(conf_pulser_burst_ctrl),
      .conf_pulser_run(conf_pulser_run),
      .pulser_out(pulser_clk_625),
      .counter_pulser_out(counter_pulser_clk_625)
   );

   ///////////////////////////////////////////////////////////
   //                   UDP packet transmitter
   ///////////////////////////////////////////////////////////

   // latched data for udp packet
   
   reg [31:0] 	    udp_trig_ts_625;
   reg [31:0] 	    udp_counter_trig_in;
   reg [31:0] 	    udp_counter_drift;
   reg [31:0] 	    udp_counter_scaledown;
   reg [31:0] 	    udp_counter_trig_out;
   reg [31:0] 	    udp_counter_pulser;
   reg [31:0] 	    udp_trig_bits;
   reg [31:0] 	    udp_nim_bits;
   reg [31:0] 	    udp_esata_bits;
   reg [15:0] 	    udp_aw16_bus;
   reg [7:0]        udp_aw16_mult;
   //reg [7:0]        udp_adc16_mult;
   reg [63:0] 	    udp_bsc64_bus;
   reg [7:0] 	    udp_bsc64_mult;
   reg [7:0] 	    udp_coinc_latch;

   parameter idle_state   = 8'h00;
   parameter writeW1_state = 8'h81;
   parameter writeW2_state = 8'h82;
   parameter write0_state = 8'hE0;
   parameter write1_state = 8'h01;
   parameter write2_state = 8'h02;
   parameter write3_state = 8'h03;
   parameter write4_state = 8'h04;
   parameter write5_state = 8'h05;
   parameter write6_state = 8'h06;
   parameter write7_state = 8'h07;
   parameter write8_state = 8'h08;
   parameter write9_state = 8'h09;
   parameter writeA_state = 8'h0A;
   parameter writeB_state = 8'h0B;
   parameter writeC_state = 8'h0C;
   parameter writeD_state = 8'h0D;
   parameter writeE_state = 8'h0E;
   parameter writeF_state = 8'h0F;
   parameter write10_state = 8'h10;
   parameter writeY_state = 8'hDD;
   parameter writeX_state = 8'hEE;

   reg [7:0]   tx_state;

   reg 	       start_udp;

   always @ (posedge clk) begin
      case (tx_state)
	idle_state: begin
	   if (start_udp) begin
	      tx_state <= writeW1_state;
	      udp_valid_out <= 0;
              udp_sop_out   <= 0;
              udp_eop_out   <= 0;
	   end else begin
	      tx_state <= tx_state;
	      udp_valid_out <= 0;
              udp_sop_out   <= 0;
              udp_eop_out   <= 0;
	   end
	end
	writeW1_state: begin
	   tx_state <= writeW2_state;
	   udp_valid_out <= 0;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	writeW2_state: begin
	   tx_state <= write0_state;
	   udp_valid_out <= 0;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	write0_state: begin
	   tx_state <= write1_state;
	   udp_data_out  <= {4'h8,udp_counter_trig_out[27:0]};
	   udp_valid_out <= 1;
           udp_sop_out   <= 1;
           udp_eop_out   <= 0;
	end
	write1_state: begin
	   tx_state <= write2_state;
	   udp_data_out <= udp_trig_ts_625;
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	write2_state: begin
	   tx_state <= write3_state;
	   udp_data_out  <= udp_counter_trig_out;
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	write3_state: begin
	   tx_state <= write4_state;
	   udp_data_out  <= udp_counter_trig_in;
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	write4_state: begin
	   tx_state <= write5_state;
	   udp_data_out  <= udp_counter_pulser;
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	write5_state: begin
	   tx_state <= write6_state;
	   udp_data_out  <= udp_trig_bits;
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	write6_state: begin
	   tx_state <= write7_state;
	   udp_data_out  <= udp_nim_bits;
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	write7_state: begin
	   tx_state <= write8_state;
	   udp_data_out  <= udp_esata_bits;
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	write8_state: begin
	   tx_state <= write9_state;
	   udp_data_out  <= { udp_aw16_mlu_out, 7'h00, 8'h00, udp_aw16_prompt[15:0]};
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	write9_state: begin
	   tx_state <= writeA_state;
	   udp_data_out  <= udp_counter_drift;
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	writeA_state: begin
	   tx_state <= writeB_state;
	   udp_data_out  <= udp_counter_scaledown;
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	writeB_state: begin
	   tx_state <= writeC_state;
	   //udp_data_out  <= { 8'h00, udp_adc16_mult[7:0], 16'h0000 };
	   udp_data_out  <= { 8'h00, 8'h00, 16'h0000 };
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	writeC_state: begin
	   tx_state <= writeD_state;
	   udp_data_out  <= { 8'h00, udp_aw16_mult[7:0], udp_aw16_bus[15:0]};
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	writeD_state: begin
	   tx_state <= writeE_state;
	   udp_data_out  <= { udp_bsc64_bus[31:0]};
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	writeE_state: begin
	   tx_state <= writeF_state;
	   udp_data_out  <= { udp_bsc64_bus[63:32]};
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	writeF_state: begin
	   tx_state <= write10_state;
	   udp_data_out  <= { 8'h00, 8'h00, 8'h00, udp_bsc64_mult[7:0] };
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	write10_state: begin
	   tx_state <= writeY_state;
	   udp_data_out  <= { 8'h00, 8'h00, 8'h00, udp_coinc_latch[7:0] };
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	writeY_state: begin
	   tx_state <= writeX_state;
	   udp_data_out  <= conf_fw_rev;
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 0;
	end
	writeX_state: begin	
	   tx_state <= idle_state;
	   udp_data_out  <= {4'hE,udp_counter_trig_out[27:0]};
	   udp_valid_out <= 1;
           udp_sop_out   <= 0;
           udp_eop_out   <= 1;
	end
      endcase
   end // always @ (posedge clk)

   ///////////////////////////////////////////////////////////
   //                trigger select and enable
   ///////////////////////////////////////////////////////////

//   wire conf_enable_sw_trigger = conf_trig_enable[0];
   wire conf_enable_pulser = conf_trig_enable[1];
//   //wire conf_enable_sas_or = conf_trig_enable[2];
   wire conf_pulser_run = conf_trig_enable[3];
//   //wire conf_output_pulser = conf_trig_enable[4];
//   wire conf_enable_esata_nim = conf_trig_enable[5];
//   wire conf_enable_adc16 = conf_trig_enable[6];
//   wire conf_enable_adc32 = conf_trig_enable[7];
//   wire conf_enable_adc16_1ormore = conf_trig_enable[8];
//   wire conf_enable_adc16_2ormore = conf_trig_enable[9];
//   wire conf_enable_adc16_3ormore = conf_trig_enable[10];
//   wire conf_enable_adc16_4ormore = conf_trig_enable[11];
//   //wire conf_enable_adc16_coinc = conf_trig_enable[12];
   wire conf_enable_udp     = conf_trig_enable[13];
//   //wire conf_enable_busy    = conf_trig_enable[14];
//   wire conf_enable_adc     = conf_trig_enable[15];
//   wire conf_enable_aw16_coinc_a = conf_trig_enable[16];
//   wire conf_enable_aw16_coinc_b = conf_trig_enable[17];
//   wire conf_enable_aw16_coinc_c = conf_trig_enable[18];
//   wire conf_enable_aw16_coinc_d = conf_trig_enable[19];
//   //wire conf_clock_select   = conf_trig_enable[20];
//   wire conf_enable_aw16_grand_or= conf_trig_enable[20];
//   wire conf_enable_aw16_coinc   = conf_trig_enable[21];
//   wire conf_enable_aw16_mlu     = conf_trig_enable[22];
   wire conf_enable_timeout      = conf_trig_enable[23];
//   wire conf_enable_aw16_1ormore = conf_trig_enable[24];
//   wire conf_enable_aw16_2ormore = conf_trig_enable[25];
//   wire conf_enable_aw16_3ormore = conf_trig_enable[26];
//   wire conf_enable_aw16_4ormore = conf_trig_enable[27];
//   wire conf_enable_bsc_grand_or = conf_trig_enable[28];
//   wire conf_enable_bsc_mult     = conf_trig_enable[29];
//   wire conf_enable_coinc        = conf_trig_enable[30];

   //wire conf_trig_enable_xxx = |conf_trig_enable;

   wire timeout_trig;
   wire [31:0] trig32_in;
   
   assign trig32_in[0]  = sw_trigger; // software trigger
   assign trig32_in[1]  = 0; // not used pulser trigger
   assign trig32_in[2]  = 0; // not used conf_enable_sas_or
   assign trig32_in[3]  = 0; // not used conf_pulser_run
   assign trig32_in[4]  = 0; // not used conf_output_pulser
   assign trig32_in[5]  = esata_nim_grand_or;
   assign trig32_in[6]  = adc16_grand_or;
   assign trig32_in[7]  = adc32_grand_or;
   assign trig32_in[8]  = 0; // adc16_mult_1ormore;
   assign trig32_in[9]  = 0; // adc16_mult_2ormore;
   assign trig32_in[10] = 0; // adc16_mult_3ormore;
   assign trig32_in[11] = 0; // adc16_mult_4ormore;
   assign trig32_in[12] = 0; // not used conf_enable_adc16_coinc
   assign trig32_in[13] = 0; // not used conf_enable_udp
   assign trig32_in[14] = 0; // not used conf_enable_busy
   assign trig32_in[15] = adc_grand_or;
   assign trig32_in[16] = 0; // aw16_coinc_a;
   assign trig32_in[17] = 0; // aw16_coinc_b;
   assign trig32_in[18] = 0; // aw16_coinc_c;
   assign trig32_in[19] = 0; // aw16_coinc_d;
   assign trig32_in[20] = aw16_grand_or; // was not used conf_clock_select
   assign trig32_in[21] = 0; // aw16_coinc_trig;
   assign trig32_in[22] = aw16_mlu_trig;
   assign trig32_in[23] = timeout_trig;
   assign trig32_in[24] = 0; // aw16_mult_1ormore;
   assign trig32_in[25] = 0; // aw16_mult_2ormore;
   assign trig32_in[26] = 0; // aw16_mult_3ormore;
   assign trig32_in[27] = 0; // aw16_mult_4ormore;
   assign trig32_in[28] = bsc_grand_or_trig;
   assign trig32_in[29] = bsc64_mult_trig;
   assign trig32_in[30] = coinc_trig;
   assign trig32_in[31] = 0; // not used

   wire trig_in_wire = |(trig32_in & conf_trig_enable);
		     
//   wire trig_in =
//	//(conf_enable_pulser & pulser) |
//	(conf_enable_sw_trigger & sw_trigger) |
//	(esata_nim_grand_or & conf_enable_esata_nim) |
//	(adc16_grand_or & conf_enable_adc16) |
//	(adc32_grand_or & conf_enable_adc32) |
//	(adc_grand_or & conf_enable_adc) |
//	(adc16_mult_1ormore & conf_enable_adc16_1ormore) |
//	(adc16_mult_2ormore & conf_enable_adc16_2ormore) |
//	(adc16_mult_3ormore & conf_enable_adc16_3ormore) |
//	(adc16_mult_4ormore & conf_enable_adc16_4ormore) |
//	(aw16_mult_1ormore & conf_enable_aw16_1ormore) |
//	(aw16_mult_2ormore & conf_enable_aw16_2ormore) |
//	(aw16_mult_3ormore & conf_enable_aw16_3ormore) |
//	(aw16_mult_4ormore & conf_enable_aw16_4ormore) |
//	(aw16_grand_or & conf_enable_aw16_grand_or) |
//	(aw16_coinc_trig & conf_enable_aw16_coinc) |
//	(aw16_mlu_trig & conf_enable_aw16_mlu) |
//	(bsc_grand_or_trig & conf_enable_bsc_grand_or) |
//	(bsc64_mult_trig & conf_enable_bsc_mult) |
//	(coinc_trig & conf_enable_coinc) |
//	(timeout_trig & conf_enable_timeout) |
//	1'h0;

   ///////////////////////////////////////////////////////////
   //                     data to cbtrg
   ///////////////////////////////////////////////////////////
   
   assign sw_trig_out             = sw_trigger;
   assign pulser_trig_out_clk_625 = pulser_clk_625 & conf_enable_pulser;
   assign esata_nim_trig_out      = esata_nim_grand_or;
   assign adc16_grand_or_trig_out = adc16_grand_or;
   assign adc32_grand_or_trig_out = adc32_grand_or;

   assign aw16_grand_or_trig_out  = aw16_grand_or;
   //assign aw16_mult_1ormore_out   = 0; // aw16_mult_1ormore;
   //assign aw16_mult_2ormore_out   = 0; // aw16_mult_2ormore;
   //assign aw16_mult_3ormore_out   = 0; // aw16_mult_3ormore;
   //assign aw16_mult_4ormore_out   = 0; // aw16_mult_4ormore;
   assign aw16_mlu_trig_out       = aw16_mlu_trig;

   assign bsc64_grand_or_trig_out = bsc_grand_or_trig;
   //assign bsc64_mult_1ormore_out  = ...;
   //assign bsc64_mult_2ormore_out  = ...;
   //assign bsc64_mult_3ormore_out  = ...;
   //assign bsc64_mult_4ormore_out  = ...;
   assign bsc64_mult_trig_out     = bsc64_mult_trig;

   assign coinc_trig_out = coinc_trig;

   ///////////////////////////////////////////////////////////
   //      latch data into the UDP trigger packet
   ///////////////////////////////////////////////////////////

   reg 	trig_in1;
   always_ff @ (posedge clk) begin
      trig_in1 <= trig_in_wire;
      if ((trig_in_wire==1) & (trig_in1==0)) begin
	 // latch all the trigger bits
	 udp_nim_bits   <= nim_bits;
	 udp_esata_bits <= esata_bits;
	 udp_trig_bits  <= trig32_in;
         if (coinc_trig) begin
	    udp_aw16_bus   <= coinc_aw16_latch;
	    udp_aw16_mult  <= coinc_aw16_mult;
         end else if (aw16_mlu_trig) begin
	    udp_aw16_bus   <= aw16_prompt;
	    udp_aw16_mult  <= 8'hFD;
         end else if (aw16_grand_or) begin
	    udp_aw16_bus   <= aw16_bus;
	    udp_aw16_mult  <= 8'hFE;
         end else begin
	    udp_aw16_bus   <= 16'h00;
	    udp_aw16_mult  <= 8'hFF;
         end
         if (coinc_trig) begin
	    udp_bsc64_bus  <= coinc_bsc64_latch;
	    udp_bsc64_mult <= coinc_bsc64_mult;
         end else if (bsc64_mult_trig) begin
	    udp_bsc64_bus  <= bsc64_mult_trig_latch;
	    udp_bsc64_mult <= bsc64_mult_trig_mult;
         end else if (bsc_grand_or_trig) begin
	    udp_bsc64_bus  <= bsc64_bus;
	    udp_bsc64_mult <= 8'hFE;
         end else begin
	    udp_bsc64_bus  <= 64'd0;
	    udp_bsc64_mult <= 8'hFF;
         end
	 udp_coinc_latch <= coinc_latch;
      end
   end

   ///////////////////////////////////////////////////////////
   //              62.5 MHz clock logic
   ///////////////////////////////////////////////////////////

   wire [31:0] ts_clk_625;
   wire [31:0] trig_ts_clk_625;
   wire [31:0] counter_pulser_clk_625;
   wire [31:0] counter_trig_in_clk_625;
   wire [31:0] counter_drift_clk_625;
   wire [31:0] counter_scaledown_clk_625;
   wire [31:0] counter_trig_out_clk_625;

   ag_clk_625 ag_clk_625
     (
      .clk(clk_625),
      .reset(reset),
      .conf_drift_width(conf_drift_width),
      .conf_scaledown(conf_scaledown),
      .conf_trig_delay(conf_trig_delay),
      .conf_trig_width(conf_trig_width),
      .conf_busy_width(conf_busy_width),
      .trigger_in(trig_in_wire),
      .pulser_in(pulser_clk_625 & conf_enable_pulser),
      .ts_625_out(ts_clk_625),
      .trig_ts_625_out(trig_ts_clk_625),
      .counter_trig_in(counter_trig_in_clk_625),
      .counter_drift(counter_drift_clk_625),
      .counter_scaledown(counter_scaledown_clk_625),
      .counter_trig_out(counter_trig_out_clk_625),
      .trig_received_out(trig_received_out_clk_625),
      .trig_drift_out(trig_drift_out_clk_625),
      .trig_scaledown_out(trig_scaledown_out_clk_625),
      .trig_out(trig_out_clk_625),
      .busy_out(busy_out_clk_625),
      .drift_out(drift_out_clk_625)
      );

   ///////////////////////////////////////////////////////////
   //      transfer trig_out back to 125 MHz clock domain
   ///////////////////////////////////////////////////////////

   wire        trig_out_sync_1;
   wire        trig_out_sync_a;
   wire        trig_out_sync_b;
   wire        trig_out_sync_c;
   wire        trig_out_sync_d;
   wire        trig_out_sync;

   sync_ss s1zz(.in(trig_out_clk_625), .clk(clk), .out(trig_out_sync_1));
   oneshot s2zz(.clk(clk), .in(trig_out_sync_1), .out(trig_out_sync_a));

   // need to delay trig_out_sync because we are racing
   // gainst the update of counter_trig_out_clk_625 which
   // is on the 62.5MHz clock. We should issue trig_out_sync
   // and attempt to latch 62.5MHz counters and timestamps
   // long after they have settled. K.O. 13jun2018

   always_ff @ (posedge clk) begin
      trig_out_sync_b <= trig_out_sync_a;
      trig_out_sync_c <= trig_out_sync_b;
      trig_out_sync_d <= trig_out_sync_c;
      trig_out_sync <= trig_out_sync_d;
   end   
   
   ///////////////////////////////////////////////////////////
   //              trigger timeout counter
   ///////////////////////////////////////////////////////////

   // generates a special timeout trigger if there is no triggers
   // within the specified time. This is needed to keep
   // the event builder synchronized. timeout has to be set
   // to a value smaller than the wrap around time
   // of the event timestamps seen by the event builder.
   // usually it is 32 bit 125 MHz timestamp, wrap around time
   // is 34 sec.
   
   // trig_out_sync starts the timeout counter
   // timeout counter counts from conf_trig_timeout down to zero
   // if conf_trig_timeout (and so trig_timeout) are set to zero, nothing happens (timeout is disabled)
   // once trig_timeout counts down and reaches value 1, timeout trigger is generated
   // timeout counter is reset back to conf_trig_timeout to generate the next timeout trigger.

   reg [31:0] trig_timeout;
   reg [31:0] counter_timeout;
   
   always_ff @ (posedge clk) begin
      if (reset) begin
	 trig_timeout <= conf_trig_timeout;
	 timeout_trig <= 0;
         counter_timeout <= 0;
      end else if (!conf_enable_timeout) begin
	 trig_timeout <= conf_trig_timeout;
	 timeout_trig <= 0;
      end else if (trig_out_sync) begin
	 trig_timeout <= conf_trig_timeout;
	 timeout_trig <= 0;
      end else if (trig_timeout == 1) begin
	 trig_timeout <= conf_trig_timeout;
	 timeout_trig <= 1;
         if (conf_enable_timeout) begin
            counter_timeout <= counter_timeout + 1;
         end
      end else if (trig_timeout == 0) begin
	 trig_timeout <= 0;
	 timeout_trig <= 0;
      end else begin
	 trig_timeout <= trig_timeout - 1;
	 timeout_trig <= 0;
      end
   end // always_ff @ (posedge clk)

   assign timeout_trig_out = timeout_trig;

   always_ff @ (posedge clk) begin
      if (latch) begin
	 counter_timeout_out <= counter_timeout;
      end
   end

   ///////////////////////////////////////////////////////////
   //              latch data for the UDP packet
   ///////////////////////////////////////////////////////////

   reg 	       trig_out_sync_delayed;

   always_ff @ (posedge clk) begin
      if (trig_out_sync) begin
	 udp_trig_ts_625 <= trig_ts_clk_625;
	 udp_counter_trig_in <= counter_trig_in_clk_625;
	 udp_counter_drift <= counter_drift_clk_625;
	 udp_counter_scaledown <= counter_scaledown_clk_625;
 	 udp_counter_trig_out <= counter_trig_out_clk_625;
 	 udp_counter_pulser <= counter_pulser_clk_625;
      end
      trig_out_sync_delayed <= trig_out_sync;
      start_udp <= trig_out_sync_delayed & conf_enable_udp;
   end
      

   ///////////////////////////////////////////////////////////
   //                      latch clk_625 counters
   ///////////////////////////////////////////////////////////

   reg 	       latch1;
   reg 	       latch2;
   reg 	       latch3;
   reg 	       latchX;
   wire        latch_clk_625_1;
   wire        latch_clk_625;

   always_ff @ (posedge clk) begin
      latch1 <= latch;
      latch2 <= latch1;
      latch3 <= latch2;
      latchX <= latch1 | latch2 | latch3;
   end

   sync_ss s1xx(.in(latchX), .clk(clk_625), .out(latch_clk_625_1));
   oneshot s2xx(.clk(clk_625), .in(latch_clk_625_1), .out(latch_clk_625));
   
   always_ff @ (posedge clk_625) begin
      if (latch_clk_625) begin
	 ts_625_out <= ts_clk_625;
	 counter_trig_out_out <= counter_trig_out_clk_625;
	 counter_trig_in_out <= counter_trig_in_clk_625;
    	 counter_pulser_out <= counter_pulser_clk_625;
    	 counter_drift_out <= counter_drift_clk_625;
    	 counter_scaledown_out <= counter_scaledown_clk_625;
      end
   end
   
   ///////////////////////////////////////////////////////////
   //                        outputs
   ///////////////////////////////////////////////////////////
   
   //assign trig_out = trig_out_clk_625;
   //assign clk_out = clk_625;
   //assign pulser_out = pulser_clk_625 & conf_output_pulser;
   //assign out2 = conf_trig_enable_xxx;

endmodule
