module mult16
  (
   input wire [15:0] in,
   output reg [7:0]  out
   );

   add16 add16_inst
     (
      .data0x({7'b0,in[0]}),
      .data1x({7'b0,in[1]}),
      .data2x({7'b0,in[2]}),
      .data3x({7'b0,in[3]}),
      .data4x({7'b0,in[4]}),
      .data5x({7'b0,in[5]}),
      .data6x({7'b0,in[6]}),
      .data7x({7'b0,in[7]}),
      .data8x({7'b0,in[8]}),
      .data9x({7'b0,in[9]}),
      .data10x({7'b0,in[10]}),
      .data11x({7'b0,in[11]}),
      .data12x({7'b0,in[12]}),
      .data13x({7'b0,in[13]}),
      .data14x({7'b0,in[14]}),
      .data15x({7'b0,in[15]}),
      .result(out)
      );
   
endmodule
