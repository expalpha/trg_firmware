module oneshot
  (
   input wire 	     clk,
   input wire 	     in,
   output wire       out
   );

   // Convert signal "in" into a single-shot pulse:
   //
   // in, in1 -> in_pulse -> next clock in1, in_pulse
   // 0, 0 -> 0 -> 0, 0
   // 1, 0 -> 1 -> 1, 0
   // 1, 1 -> 0 -> 1, 0
   // 0, 1 -> 0 -> 0, 0

   reg 		     in1;

   always @ (posedge clk) begin
      in1 <= in;
   end

   assign out = in & !in1;

endmodule
