`default_nettype none
module ag_clk_625
  (
   input wire 	     clk,
   input wire 	     reset,

   // controls and configurations

   input wire [31:0] conf_drift_width,
   input wire [31:0] conf_scaledown,
   input wire [31:0] conf_trig_delay,
   input wire [31:0] conf_trig_width,
   input wire [31:0] conf_busy_width,

   // trigger inputs

   input wire 	     trigger_in, // some other clock
   input wire 	     pulser_in, // same clock as clk

   // counters output

   output reg [31:0] ts_625_out, // running clk_625 counter
   output reg [31:0] trig_ts_625_out, // timestamp of accepted trigger
   output reg [31:0] counter_trig_in, // counter of input triggers: trigger_in and pulser_in
   output reg [31:0] counter_drift, // counter of input triggers that made it past the drift blank-off
   output reg [31:0] counter_scaledown, // counter of input triggers that made it past the scaledown
   output reg [31:0] counter_trig_out, // counter of generated triggers (trig_out)

   // outputs

   output wire 	     trig_out,
   output wire 	     busy_out,
   output wire 	     drift_out,

   // outputs to chronobox

   output wire trig_received_out,
   output wire trig_drift_out,
   output wire trig_scaledown_out
   );

   ///////////////////////////////////////////////////////////
   //                        timestamp
   ///////////////////////////////////////////////////////////

   reg [31:0] ts_625;

   always_ff @ (posedge clk or posedge reset) begin
      if (reset) begin
	 ts_625 <= 0;
      end else begin
	 ts_625 <= ts_625 + 1;
      end
   end

   assign ts_625_out = ts_625;

   ///////////////////////////////////////////////////////////
   //               Normalize the inputs
   ///////////////////////////////////////////////////////////

   wire trigger_in1;
   wire trigger;
   wire pulser;

   // bring trig_in into the 62.5 MHz clock domain, as a one-shot pulse

   sync_fast_to_slow_clk s1(.in(trigger_in), .clk(clk), .out(trigger_in1));
   oneshot s2(.clk(clk), .in(trigger_in1), .out(trigger));
   oneshot s3(.clk(clk), .in(pulser_in), .out(pulser));

   ///////////////////////////////////////////////////////////
   //                    drift blank-off
   ///////////////////////////////////////////////////////////

   wire start_drift = trigger | pulser;
   
   reg [31:0] drift_run_counter;
   reg 	      drift;
   reg 	      start_scaledown;
   
   always_ff @ (posedge clk or posedge reset) begin
      if (reset) begin
	 drift_run_counter <= 0;
	 drift <= 0;
	 start_scaledown <= 0;
      end else if (drift_run_counter == 0) begin
	 if (start_drift) begin
	    drift_run_counter <= conf_drift_width;
	    drift <= 1;
	    start_scaledown <= 1;
	 end else begin
	    drift <= 0;
	    start_scaledown <= 0;
	 end
      end else begin
	 drift_run_counter <= drift_run_counter - 1;
	 drift <= 1;
	 start_scaledown <= 0;
      end
   end

   assign drift_out = drift;
   
   ///////////////////////////////////////////////////////////
   //                    scale down
   ///////////////////////////////////////////////////////////

   reg [31:0] scaledown_run_counter;
   reg 	      start_delay;
   
   always_ff @ (posedge clk or posedge reset) begin
      if (reset) begin
	 scaledown_run_counter <= 0;
	 start_delay <= 0;
      end else if (start_scaledown) begin
	 if ((scaledown_run_counter == conf_scaledown) || (conf_scaledown == 0)) begin
	    scaledown_run_counter <= 0;
	    start_delay <= 1;
	 end else begin
	    scaledown_run_counter <= scaledown_run_counter + 1;
	    start_delay <= 0;
	 end
      end else begin
	 start_delay <= 0;
      end
   end
   
   ///////////////////////////////////////////////////////////
   //                    trigger delay
   ///////////////////////////////////////////////////////////

   reg [31:0] delay_run_counter;
   reg 	      start_trigger;
   
   always_ff @ (posedge clk or posedge reset) begin
      if (reset) begin
	 delay_run_counter <= 0;
	 start_trigger <= 0;
      end else if (conf_trig_delay == 0) begin
	 start_trigger <= start_delay;
      end else if (start_delay) begin
	 delay_run_counter <= conf_trig_delay;
	 start_trigger <= 0;
      end else if (delay_run_counter == 1) begin
	 delay_run_counter <= delay_run_counter - 1;
	 start_trigger <= 1;
      end else if (delay_run_counter == 0) begin
	 start_trigger <= 0;
      end else begin
	 delay_run_counter <= delay_run_counter - 1;
	 start_trigger <= 0;
      end
   end

   ///////////////////////////////////////////////////////////
   //                    main state machine
   ///////////////////////////////////////////////////////////

   reg [31:0] trig_run_counter;

   reg        trig;
   reg 	      busy;
   reg 	      start_busy;

   always_ff @ (posedge clk or posedge reset) begin
      if (reset) begin
	 trig <= 0;
	 trig_ts_625_out <= 0;
	 trig_run_counter <= 0;
	 start_busy <= 0;
      end else if (trig_run_counter == 0) begin
	 if (start_trigger & !busy) begin
	    trig_run_counter <= conf_trig_width;
	    trig <= 1;
	    trig_ts_625_out <= ts_625;
	    start_busy <= 1;
	 end else begin
	    trig <= 0;
	    start_busy <= 0;
	 end
      end else if (trig_run_counter == 1) begin
	 trig_run_counter <= 0;
	 trig <= 1;
	 start_busy <= 0;
      end else begin
	 trig_run_counter <= trig_run_counter - 1;
	 trig <= 1;
	 start_busy <= 0;
      end // else: !if(trig_counter == 0)
   end // always @ (posedge clk_625 || posedge reset)

   ///////////////////////////////////////////////////////////
   //                       busy logic
   ///////////////////////////////////////////////////////////

   reg [31:0] busy_run_counter;
   
   always_ff @ (posedge clk or posedge reset) begin
      if (reset) begin
	 busy_run_counter <= 0;
	 busy <= 0;
      end else if (busy_run_counter == 0) begin
	 if (start_busy) begin
	    busy_run_counter <= conf_busy_width;
	    busy <= 1;
	 end else begin
	    busy <= 0;
	 end
      end else begin
	 busy_run_counter <= busy_run_counter - 1;
	 busy <= 1;
      end // else: !if(busy_counter == 0)
   end // always @ (posedge clk_625 || posedge reset)

   ///////////////////////////////////////////////////////////
   //                        outputs
   ///////////////////////////////////////////////////////////
   
   assign trig_out = trig;
   assign busy_out = busy;

   ///////////////////////////////////////////////////////////
   //                       counters
   ///////////////////////////////////////////////////////////
   
   counter c1(.clk(clk), .reset(reset), .in(start_drift), .out(counter_trig_in));
   counter c2(.clk(clk), .reset(reset), .in(start_scaledown), .out(counter_drift));
   counter c3(.clk(clk), .reset(reset), .in(start_trigger), .out(counter_scaledown));
   counter c4(.clk(clk), .reset(reset), .in(trig_out), .out(counter_trig_out));

   ///////////////////////////////////////////////////////////
   //                       sent to chronobox
   ///////////////////////////////////////////////////////////

   assign trig_received_out  = start_drift;
   assign trig_drift_out     = start_scaledown;
   assign trig_scaledown_out = start_trigger;

endmodule
