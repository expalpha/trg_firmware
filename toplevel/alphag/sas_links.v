`default_nettype none
module sas_links
  (
   input wire 		   reset,
   input wire 		   sys_clk,
   input wire 		   io_clk,
   input wire 		   io_clk2,

   //input wire [23:0]  FMCX_RX,
   //output wire [23:0] FMCX_TX,

   input wire [3:0] 	   FMCX_RX_block1,
   input wire [3:0] 	   FMCX_RX_block2,
   input wire [3:0] 	   FMCX_RX_block5,
   input wire [3:0] 	   FMCX_RX_block6,

   output wire [3:0] 	   FMCX_TX_block1,
   output wire [3:0] 	   FMCX_TX_block2,
   output wire [3:0] 	   FMCX_TX_block5,
   output wire [3:0] 	   FMCX_TX_block6,

   output wire [31:0] 	   rx_data_0,
   output wire [31:0] 	   rx_data_1,
   output wire [31:0] 	   rx_data_2,
   output wire [31:0] 	   rx_data_3,

   output wire [3:0] 	   rx_sd_out_0, // rx_signal_detect[]
   output wire [3:0] 	   rx_sd_out_1,
   output wire [3:0] 	   rx_sd_out_2,
   output wire [3:0] 	   rx_sd_out_3,

   //output wire [15:0] 	   sas_trig,
   output reg [15:0][63:0] sas_bits
   );

   //////////////////////////    GX Serial Links    ///////////////////////////////////
   // 1Gbps / 10bits => 100Mhz Parallel clk (8 bits into xcvr, 10 out of link per clk)
   //                =>  50Mhz 16-bit/clk (25Mhz 32-bit/clk)
   // inputs are 32 bit into 4link x 8 bit, at 50Mhz => each data is currently sent twice

   wire [5:0] 	 pll_locked;
   wire [23:0] 	 rx_freqlocked;
   wire [191:0]  gx_TxDataIn;
   //wire [191:0]  gx_RxDataOut;
   wire [23:0] 	 tx_ctrlenable;
   wire [23:0] 	 rx_ctrldetect;
   wire [23:0] 	 rx_errdetect;
   wire [23:0] 	 rx_sigdetct;
   wire [23:0] 	 tx_clkout;

   wire [3:0] 	 rx_clkout_block1;
   wire [3:0] 	 rx_clkout_block2;
   wire [3:0] 	 rx_clkout_block5;
   wire [3:0] 	 rx_clkout_block6;
   
   wire [203:0]  FMCX_cfgin;
   wire [23:0] 	 FMCX_cfgout;
   wire [5:0] 	 reconfig_busy;
   
   wire [23:0] 	 rx_digitalreset;
   wire [23:0] 	 tx_digitalreset;
   wire [23:0] 	 rx_analogreset;
   wire [5:0] 	 pll_powerdown;
   
   wire [23:0] 	 rx_disperr_out; // unused
   wire [23:0] 	 rx_freqlocked_out; // unused
   wire [23:0] 	 rx_fifodel;
   wire [23:0] 	 rx_fifoins;
   wire [23:0] 	 rx_fifoful;
   wire [23:0] 	 rx_fifoemp;

   //assign rx_data_0[31:0] = gx_RxDataOut[31:0]; // block1: channels 0,1,2,3 (Upper mini-sas on FMC0)
   //assign rx_data_1[31:0] = gx_RxDataOut[63:32]; // block 2: channels 4,5,6,7 (Lower mini-sas on FMC0)
   //assign rx_data_2[31:0] = gx_RxDataOut[159:128]; // block 5: channels 0,1,2,3 (Upper mini-sas on FMC2)
   //assign rx_data_3[31:0] = gx_RxDataOut[191:160]; // block 6: channels 4,5,6,7 (Lower mini-sas on FMC2)
   
   wire [3:0] rx_sd_0 = rx_sigdetct[3:0]; // block 1
   wire [3:0] rx_sd_1 = rx_sigdetct[7:4]; // block 2
   wire [3:0] rx_sd_2 = rx_sigdetct[19:16]; // block 5
   wire [3:0] rx_sd_3 = rx_sigdetct[23:20]; // block 6
   
   wire [3:0] rx_cd_0 = rx_ctrldetect[3:0]; // block 1
   wire [3:0] rx_cd_1 = rx_ctrldetect[7:4]; // block 2
   wire [3:0] rx_cd_2 = rx_ctrldetect[19:16]; // block 5
   wire [3:0] rx_cd_3 = rx_ctrldetect[23:20]; // block 6

   wire [3:0] rx_err_0 = rx_errdetect[3:0]; // block 1
   wire [3:0] rx_err_1 = rx_errdetect[7:4]; // block 2
   wire [3:0] rx_err_2 = rx_errdetect[19:16]; // block 5
   wire [3:0] rx_err_3 = rx_errdetect[23:20]; // block 6

   //wire [3:0] rx_pll_locked_0; // block 1
   //wire [3:0] rx_pll_locked_1; // block 2
   //wire [3:0] rx_pll_locked_2; // block 5
   //wire [3:0] rx_pll_locked_3; // block 6

   assign rx_sd_out_0 = rx_sd_0 & ~rx_err_0;
   assign rx_sd_out_1 = rx_sd_1 & ~rx_err_1;
   assign rx_sd_out_2 = rx_sd_2 & ~rx_err_2;
   assign rx_sd_out_3 = rx_sd_3 & ~rx_err_3;

   wire [3:0] ena0 = rx_sd_out_0 & ~rx_cd_0;
   wire [3:0] ena1 = rx_sd_out_1 & ~rx_cd_1;
   wire [3:0] ena2 = rx_sd_out_2 & ~rx_cd_2;
   wire [3:0] ena3 = rx_sd_out_3 & ~rx_cd_3;

   sas_decode_bits b0(.reset(reset), .clk(rx_clkout_block1[0]), .in(rx_data_0[7:0]),   .ena(ena0[0]),  .out(sas_bits[0]));
   sas_decode_bits b1(.reset(reset), .clk(rx_clkout_block1[1]), .in(rx_data_0[15:8]),  .ena(ena0[1]),  .out(sas_bits[1]));
   sas_decode_bits b2(.reset(reset), .clk(rx_clkout_block1[2]), .in(rx_data_0[23:16]), .ena(ena0[2]),  .out(sas_bits[2]));
   sas_decode_bits b3(.reset(reset), .clk(rx_clkout_block1[3]), .in(rx_data_0[31:24]), .ena(ena0[3]),  .out(sas_bits[3]));

   sas_decode_bits b4(.reset(reset), .clk(rx_clkout_block2[0]), .in(rx_data_1[7:0]),   .ena(ena1[0]),  .out(sas_bits[4]));
   sas_decode_bits b5(.reset(reset), .clk(rx_clkout_block2[1]), .in(rx_data_1[15:8]),  .ena(ena1[1]),  .out(sas_bits[5]));
   sas_decode_bits b6(.reset(reset), .clk(rx_clkout_block2[2]), .in(rx_data_1[23:16]), .ena(ena1[2]),  .out(sas_bits[6]));
   sas_decode_bits b7(.reset(reset), .clk(rx_clkout_block2[3]), .in(rx_data_1[31:24]), .ena(ena1[3]),  .out(sas_bits[7]));

   sas_decode_bits b8(.reset(reset), .clk(rx_clkout_block5[0]), .in(rx_data_2[7:0]),   .ena(ena2[0]),  .out(sas_bits[8]));
   sas_decode_bits b9(.reset(reset), .clk(rx_clkout_block5[1]), .in(rx_data_2[15:8]),  .ena(ena2[1]),  .out(sas_bits[9]));
   sas_decode_bits b10(.reset(reset), .clk(rx_clkout_block5[2]), .in(rx_data_2[23:16]), .ena(ena2[2]),  .out(sas_bits[10]));
   sas_decode_bits b11(.reset(reset), .clk(rx_clkout_block5[3]), .in(rx_data_2[31:24]), .ena(ena2[3]),  .out(sas_bits[11]));

   sas_decode_bits b12(.reset(reset), .clk(rx_clkout_block6[0]), .in(rx_data_3[7:0]),   .ena(ena3[0]),  .out(sas_bits[12]));
   sas_decode_bits b13(.reset(reset), .clk(rx_clkout_block6[1]), .in(rx_data_3[15:8]),  .ena(ena3[1]),  .out(sas_bits[13]));
   sas_decode_bits b14(.reset(reset), .clk(rx_clkout_block6[2]), .in(rx_data_3[23:16]), .ena(ena3[2]),  .out(sas_bits[14]));
   sas_decode_bits b15(.reset(reset), .clk(rx_clkout_block6[3]), .in(rx_data_3[31:24]), .ena(ena3[3]),  .out(sas_bits[15]));
   
   /////////////////////// block 1: channels 0,1,2,3 (Upper mini-sas on FMC0) ////
   initialise init1
     (
      .clk(io_clk),
      .reconfig_busy(reconfig_busy[0]),
      .pll_locked(pll_locked[0]),
      .pll_powerdown(  pll_powerdown[0]),
      .rx_freqlocked(  rx_freqlocked[3:0]),
      .rx_digitalreset(rx_digitalreset[3:0]),
      .tx_digitalreset(tx_digitalreset[3:0]),
      .rx_analogreset( rx_analogreset[3:0])
      );
   
   FMC_Transceiver_sas block1
     (
      //.pll_inclk(            FMCX_GBTCLK[0]),
      .pll_inclk(                   io_clk),
      .cal_blk_clk(                 io_clk),
      .reconfig_clk(                sys_clk),
      .pll_locked(            pll_locked[0]),
      .pll_powerdown(      pll_powerdown[0]),
      .reconfig_togxb(     FMCX_cfgout[3:0]),
      .reconfig_fromgxb(   FMCX_cfgin[16:0]),
      //.rx_datain(              FMCX_RX[3:0]),
      //.tx_dataout(             FMCX_TX[3:0]),
      .rx_datain(              FMCX_RX_block1),
      .tx_dataout(             FMCX_TX_block1),
      .tx_datain(         gx_TxDataIn[31:0]),
      //.rx_dataout(       gx_RxDataOut[31:0]),
      .rx_dataout(rx_data_0),
      .rx_digitalreset(rx_digitalreset[3:0]),
      .tx_digitalreset(tx_digitalreset[3:0]),
      .rx_analogreset(  rx_analogreset[3:0]),
      .rx_cruclk(               {4{io_clk}}),
      .rx_clkout(            rx_clkout_block1),
      .tx_clkout(            tx_clkout[3:0]),
      .tx_ctrlenable(    tx_ctrlenable[3:0]),
      .rx_ctrldetect(    rx_ctrldetect[3:0]),
      .rx_disperr(      rx_disperr_out[3:0]),
      .rx_errdetect(      rx_errdetect[3:0]),
      .rx_freqlocked(    rx_freqlocked[3:0]),
      .rx_signaldetect(    rx_sigdetct[3:0])
      //.rx_pll_locked(  rx_pll_locked_0[3:0])
      //.rx_rmfifodatadeleted(rx_fifodel[3:0]),
      //.rx_rmfifodatainserted(rx_fifoins[3:0]),
      //.rx_rmfifoempty(      rx_fifoemp[3:0]),
      //.rx_rmfifofull(       rx_fifoful[3:0])
      );
   
   FMC_Transceiver_reconfig_sas reconfig1
     (
      .reconfig_clk(sys_clk),
      .busy(reconfig_busy[0]),
      .reconfig_fromgxb(FMCX_cfgin[33:0]),
      .reconfig_togxb(FMCX_cfgout[3:0])
      );
   
   /////////////////////// block 2: channels 4,5,6,7 (Lower mini-sas on FMC0) ///////
   initialise init2
     (
      .clk(io_clk),               .reconfig_busy(reconfig_busy[1]),
      .pll_locked(pll_locked[1]), .pll_powerdown(pll_powerdown[1]),
      .rx_freqlocked(    rx_freqlocked[7:4]),
      .rx_digitalreset(rx_digitalreset[7:4]),
      .tx_digitalreset(tx_digitalreset[7:4]),
      .rx_analogreset(  rx_analogreset[7:4])
      );
   
   FMC_Transceiver_sas block2
     (
      .pll_inclk(                    io_clk),
      .cal_blk_clk(                  io_clk),  .reconfig_clk(                 sys_clk),
      .pll_locked(            pll_locked[1]),  .pll_powerdown(      pll_powerdown[1]),
      .reconfig_togxb(     FMCX_cfgout[7:4]),  .reconfig_fromgxb(  FMCX_cfgin[49:34]),
      //.rx_datain(              FMCX_RX[7:4]),
      //.tx_dataout(             FMCX_TX[7:4]),
      .rx_datain(              FMCX_RX_block2),
      .tx_dataout(             FMCX_TX_block2),
      .tx_datain(        gx_TxDataIn[63:32]),
      //.rx_dataout(      gx_RxDataOut[63:32]),
      .rx_dataout(rx_data_1),
      .rx_digitalreset(rx_digitalreset[7:4]),
      .tx_digitalreset(tx_digitalreset[7:4]),
      .rx_analogreset(  rx_analogreset[7:4]),
      .rx_cruclk(               {4{io_clk}}),
      .rx_clkout(            rx_clkout_block2),
      .tx_clkout(            tx_clkout[7:4]),
      .tx_ctrlenable(    tx_ctrlenable[7:4]),
      .rx_ctrldetect(    rx_ctrldetect[7:4]),
      .rx_disperr(      rx_disperr_out[7:4]),
      .rx_errdetect(      rx_errdetect[7:4]),
      .rx_freqlocked(    rx_freqlocked[7:4]),
      .rx_signaldetect(    rx_sigdetct[7:4])
      //.rx_rmfifodatadeleted(rx_fifodel[7:4]), .rx_rmfifodatainserted(rx_fifoins[7:4]),
      //.rx_rmfifoempty(      rx_fifoemp[7:4]),  .rx_rmfifofull(       rx_fifoful[7:4])
      );
   
   FMC_Transceiver_reconfig_sas reconfig2
     (
      .reconfig_clk(sys_clk),                .busy(reconfig_busy[1]),
      .reconfig_fromgxb(FMCX_cfgin[67:34]), .reconfig_togxb(FMCX_cfgout[7:4])
      );

/////////////////////// block 5: channels 0,1,2,3 (Upper mini-sas on FMC2) ////
   initialise init5
     (
      .clk(io_clk2),              .reconfig_busy(reconfig_busy[4]),
      .pll_locked(pll_locked[4]), .pll_powerdown(pll_powerdown[4]),
      .rx_freqlocked(    rx_freqlocked[19:16]),
      .rx_digitalreset(rx_digitalreset[19:16]),
      .tx_digitalreset(tx_digitalreset[19:16]),
      .rx_analogreset(  rx_analogreset[19:16])
      );
   
   FMC_Transceiver_sas block5
     (
      //.pll_inclk(            FMCX_GBTCLK[0]),
      .pll_inclk(                   io_clk2),
      .cal_blk_clk(                 io_clk2),  .reconfig_clk(                sys_clk),
      .pll_locked(            pll_locked[4]),  .pll_powerdown(      pll_powerdown[4]),
      .reconfig_togxb(     FMCX_cfgout[19:16]),  .reconfig_fromgxb(   FMCX_cfgin[151:136]),
      //.rx_datain(              FMCX_RX[19:16]),
      //.tx_dataout(             FMCX_TX[19:16]),
      .rx_datain(              FMCX_RX_block5),
      .tx_dataout(             FMCX_TX_block5),
      .tx_datain(         gx_TxDataIn[159:128]),
      //.rx_dataout(       gx_RxDataOut[159:128]),
      .rx_dataout(rx_data_2),
      .rx_digitalreset(rx_digitalreset[19:16]),  .tx_digitalreset(tx_digitalreset[19:16]),
      .rx_analogreset(  rx_analogreset[19:16]),  .rx_cruclk(               {4{io_clk2}}),
      .rx_clkout(            rx_clkout_block5),
      .tx_clkout(            tx_clkout[19:16]),
      .tx_ctrlenable(    tx_ctrlenable[19:16]),
      .rx_ctrldetect(    rx_ctrldetect[19:16]),
      .rx_disperr(      rx_disperr_out[19:16]),
      .rx_errdetect(      rx_errdetect[19:16]),
      .rx_freqlocked(    rx_freqlocked[19:16]),
      .rx_signaldetect(    rx_sigdetct[19:16])
      //.rx_rmfifodatadeleted(rx_fifodel[19:16]), .rx_rmfifodatainserted(rx_fifoins[19:16]),
      //.rx_rmfifoempty(      rx_fifoemp[19:16]),  .rx_rmfifofull(       rx_fifoful[19:16])
      );
   
   FMC_Transceiver_reconfig_sas reconfig5
     (
      .reconfig_clk(sys_clk),               .busy(reconfig_busy[4]),
      .reconfig_fromgxb(FMCX_cfgin[169:136]), .reconfig_togxb(FMCX_cfgout[19:16])
      );
   /////////////////////// block 6: channels 4,5,6,7 (Lower mini-sas on FMC2) ///////
   initialise init6
     (
      .clk(io_clk2),              .reconfig_busy(reconfig_busy[5]),
      .pll_locked(pll_locked[5]), .pll_powerdown(pll_powerdown[5]),
      .rx_freqlocked(    rx_freqlocked[23:20]),
      .rx_digitalreset(rx_digitalreset[23:20]),
      .tx_digitalreset(tx_digitalreset[23:20]),
      .rx_analogreset(  rx_analogreset[23:20])
      );
   
   FMC_Transceiver_sas block6
     (
      //.pll_inclk(            FMCX_GBTCLK[0]),
      .pll_inclk(                   io_clk2),
      .cal_blk_clk(                 io_clk2),  .reconfig_clk(                sys_clk),
      .pll_locked(            pll_locked[5]),  .pll_powerdown(      pll_powerdown[5]),
      .reconfig_togxb(     FMCX_cfgout[23:20]),  .reconfig_fromgxb(   FMCX_cfgin[185:170]),
      //.rx_datain(              FMCX_RX[23:20]),
      //.tx_dataout(             FMCX_TX[23:20]),
      .rx_datain(              FMCX_RX_block6),
      .tx_dataout(             FMCX_TX_block6),
      .tx_datain(         gx_TxDataIn[191:160]),
      //.rx_dataout(       gx_RxDataOut[191:160]),
      .rx_dataout(rx_data_3),
      .rx_digitalreset(rx_digitalreset[23:20]),  .tx_digitalreset(tx_digitalreset[23:20]),
      .rx_analogreset(  rx_analogreset[23:20]),  .rx_cruclk(               {4{io_clk2}}),
      .rx_clkout(            rx_clkout_block6),
      .tx_clkout(            tx_clkout[23:20]),
      .tx_ctrlenable(    tx_ctrlenable[23:20]),
      .rx_ctrldetect(    rx_ctrldetect[23:20]),
      .rx_disperr(      rx_disperr_out[23:20]),
      .rx_errdetect(      rx_errdetect[23:20]),
      .rx_freqlocked(    rx_freqlocked[23:20]),
      .rx_signaldetect(    rx_sigdetct[23:20])
      //.rx_rmfifodatadeleted(rx_fifodel[23:20]), .rx_rmfifodatainserted(rx_fifoins[23:20]),
      //.rx_rmfifoempty(      rx_fifoemp[23:20]),  .rx_rmfifofull(       rx_fifoful[23:20])
      );
   
   FMC_Transceiver_reconfig_sas reconfig6
     (
      .reconfig_clk(sys_clk),               .busy(reconfig_busy[5]),
      .reconfig_fromgxb(FMCX_cfgin[203:170]), .reconfig_togxb(FMCX_cfgout[23:20])
      );

endmodule
//
