`default_nettype none
module ag_conditioner
  (
   input wire clk,
   input wire in,
   output reg out
   );
   
   reg 	      in1;
   reg 	      in2;
   reg 	      in3;
   reg 	      in4;
   reg        xout;
   
   always_ff @ (posedge clk) begin
      in1 <= in;
      in2 <= in1;
      in3 <= in2;
      in4 <= in3;
      if (in1==0 && in2==0 && in3==0 && in4 == 0) xout <= 0;
      else xout <= 1;
      out <= xout;
   end

endmodule
