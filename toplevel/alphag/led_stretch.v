`default_nettype none
module led_stretch
  (
   input wire clk,
   input wire in_async,
   output reg out
   );

   reg 		     in_async1;
   reg 		     in_async2;
   reg 		     in;

   reg [23:0] 	     counter;

   always_ff @ (posedge clk) begin
      in_async1 <= in_async;
      in_async2 <= in_async1;
      in <= in_async2;

      if (in) begin
	 out <= 1;
	 counter <= 24'd12500000;
      end else if (counter != 0) begin
	 out <= 1;
	 counter <= counter - 24'd1;
      end else begin
	 out <= 0;
      end
   end

endmodule
