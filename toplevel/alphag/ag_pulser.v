`default_nettype none
module ag_pulser
  (
   input wire 	      clk,
   input wire 	      reset,
   
   // controls and configurations
   
   input wire [31:0]  conf_pulser_width,
   input wire [31:0]  conf_pulser_period,
   input wire [31:0]  conf_pulser_burst_ctrl,

   input wire 	      conf_pulser_run,

   // outputs
   
   output wire 	      pulser_out,
   output wire [31:0] counter_pulser_out	     
   );

   wire [7:0]         conf_burst_count  = conf_pulser_burst_ctrl[31:24];
   wire [23:0]        conf_burst_period = conf_pulser_burst_ctrl[23:0];

   // sync reset to the clock

   reg                reset_sync;
   reg                reset1;
   
   always_ff @ (posedge clk) begin
      reset1 <= reset;
      reset_sync <= reset1;
   end

   // hold pulser in reset if conf_pulser_run is false

   wire 	     xreset = reset_sync | ~conf_pulser_run;

   // generate start_pulser with required period

   reg 		     start_pulser;
   reg [31:0] 	     pulser_run_counter;
   
   always_ff @ (posedge clk or posedge xreset) begin
      if (xreset) begin
	 pulser_run_counter <= 0;
	 start_pulser <= 0;
      end else if (!conf_pulser_run) begin
	 pulser_run_counter <= 0;
	 start_pulser <= 0;
      end else if (pulser_run_counter == 0) begin
	 pulser_run_counter <= conf_pulser_period;
	 start_pulser <= 1;
      end else begin
	 pulser_run_counter <= pulser_run_counter - 1;
	 start_pulser <= 0;
      end // else: !if(run_pulser_counter == 0)
   end // always @ (posedge clk_625 || posedge reset)

   // generate burst of pulses

   reg [7:0]  burst_counter;
   reg [23:0] burst_wait;
   reg        start_burst;

   always_ff @ (posedge clk or posedge xreset) begin
      if (xreset) begin
	 start_burst <= 0;
         burst_counter <= 0;
         burst_wait <= 0;
      end else if (conf_burst_count == 0) begin
         // burst pulser disabled
	 start_burst <= 0;
         burst_counter <= 0;
         burst_wait <= 0;
      end else if (conf_burst_period == 0) begin
         // burst pulser disabled
	 start_burst <= 0;
         burst_counter <= 0;
         burst_wait <= 0;
      end else if (burst_counter == 8'd0) begin
         // burst pulser is idle
         if (start_pulser) begin
            burst_counter <= conf_burst_count + 8'd2;
         end
         burst_wait <= 24'd0;
	 start_burst <= 0;
      end else if (burst_counter == 1) begin
         // prepare to be idle
         burst_counter <= 0;
         burst_wait <= 0;
	 start_burst <= 0;
      end else if (burst_wait == 24'd0) begin
         burst_counter <= burst_counter - 8'd1;
         burst_wait <= conf_burst_period - 24'd1;
	 start_burst <= 0;
      end else if (burst_wait == 24'd2) begin
         burst_wait <= 24'd1;
         start_burst <= 1;
      end else if (burst_wait == 24'd1) begin
         burst_wait <= 24'd0;
         start_burst <= 0;
      end else begin
         burst_wait <= burst_wait - 24'd1;
         start_burst <= 0;
      end
   end

   // generate pulse_out of required width
   
   reg [31:0] pulser_on_counter;
   reg [31:0] counter_pulser;

   always_ff @ (posedge clk or posedge xreset) begin
      if (xreset) begin
	 pulser_on_counter <= 0;
	 pulser_out <= 0;
	 counter_pulser <= 0;
      end else if (pulser_on_counter == 0) begin
	 if (start_pulser | start_burst) begin
	    pulser_on_counter <= conf_pulser_width;
	    pulser_out <= 1;
	    counter_pulser <= counter_pulser + 1;
	 end else begin
	    pulser_out <= 0;
	 end
      end else begin
	 pulser_on_counter <= pulser_on_counter - 1;
	 pulser_out <= 1;
      end // else: !if(pulser_counter == 0)
   end // always @ (posedge clk_625 || posedge reset)

   assign counter_pulser_out = counter_pulser;

endmodule
