`default_nettype none
module ag_sas_proc
  (
   input wire 		   clk,
   input wire 		   reset,
   input wire 		   latch,

   // configuration

   input wire [31:0] 	   conf_nim_mask,
   input wire [31:0] 	   conf_esata_mask,
   input wire [31:0] 	   conf_adc16_masks[7:0],
   input wire [31:0] 	   conf_adc32_masks[15:0],

   // trigger links from ADCs

   input wire [3:0] 	   rx_sd_0,
   input wire [3:0] 	   rx_sd_1,
   input wire [3:0] 	   rx_sd_2,
   input wire [3:0] 	   rx_sd_3,

   input wire [15:0][63:0] sas_bits,

   // outputs
   
   output reg [15:0] 	   sas_sd_out, // latched
   output reg [63:0] 	   sas_bits_out[15:0], // latched
   output reg [31:0] 	   sas_sd_counters_out[15:0], // latched

   output reg [31:0] 	   nim_bits_out,
   output reg [31:0] 	   esata_bits_out,

   output reg [15:0] 	   adc16_bits_out[15:0],
   output reg [15:0] 	   adc32_bits_out[31:0],

   // grand-or trigger outputs

   output reg 		   adc16_grand_or_out,
   output reg 		   adc32_grand_or_out,
   output reg 		   adc_grand_or_out,
   output reg 		   esata_nim_grand_or_out,

   output reg [31:0] 	   counter_adc16_grand_or_out, // latched
   output reg [31:0] 	   counter_adc32_grand_or_out, // latched
   output reg [31:0] 	   counter_adc_grand_or_out, // latched
   output reg [31:0] 	   counter_esata_nim_grand_or_out, // latched

   // adc16 multiplicity trigger outputs

   //output reg [7:0] 	   adc16_mult_out,
   //output reg 		   adc16_mult_1ormore_out,
   //output reg 		   adc16_mult_2ormore_out,
   //output reg 		   adc16_mult_3ormore_out,
   //output reg 		   adc16_mult_4ormore_out,

   //output reg [31:0] 	   counter_adc16_mult_1ormore_out, // latched
   //output reg [31:0] 	   counter_adc16_mult_2ormore_out, // latched
   //output reg [31:0] 	   counter_adc16_mult_3ormore_out, // latched
   //output reg [31:0] 	   counter_adc16_mult_4ormore_out, // latched

   // adc16 and adc32 per-preamp ORes

   output reg [15:0] 	   adc16_or16_out,
   output reg [31:0] 	   adc32_or16_out,

   output reg [31:0] 	   counter_adc16_or16_out[15:0], // latched
   output reg [31:0] 	   counter_adc32_or16_out[31:0] // latched
   );

   genvar 		   i;

   ///////////////////////////////////////////////////////////
   // Sync sas_bits to our clock
   ///////////////////////////////////////////////////////////
   
   wire [15:0][63:0] sas_bits_async = sas_bits;
   reg [15:0][63:0]  sas_bits_async1;

   reg [15:0][63:0]  sas_bits_sync;

   always @ (posedge clk) begin
      sas_bits_async1 <= sas_bits_async;
      sas_bits_sync <= sas_bits_async1;

      //if (latch) begin
      //  sas_bits_out <= sas_bits_sync;
      //end
   end

   generate
      for (i=0; i<16; i=i+1) begin: sas_sd_out_block
	 always @ (posedge clk) begin
	    if (latch) begin
	       //sas_bits_out <= sas_bits_sync;
	       sas_bits_out[i] <= sas_bits_sync[i];
	    end
	 end
      end
   endgenerate


   ///////////////////////////////////////////////////////////
   // Sync rx_sd_N to our clock
   ///////////////////////////////////////////////////////////

   wire [15:0] rx_sd;

   assign rx_sd[0] = rx_sd_0[0];
   assign rx_sd[1] = rx_sd_0[1];
   assign rx_sd[2] = rx_sd_0[2];
   assign rx_sd[3] = rx_sd_0[3];

   assign rx_sd[4] = rx_sd_1[0];
   assign rx_sd[5] = rx_sd_1[1];
   assign rx_sd[6] = rx_sd_1[2];
   assign rx_sd[7] = rx_sd_1[3];

   assign rx_sd[8] = rx_sd_2[0];
   assign rx_sd[9] = rx_sd_2[1];
   assign rx_sd[10] = rx_sd_2[2];
   assign rx_sd[11] = rx_sd_2[3];

   assign rx_sd[12] = rx_sd_3[0];
   assign rx_sd[13] = rx_sd_3[1];
   assign rx_sd[14] = rx_sd_3[2];
   assign rx_sd[15] = rx_sd_3[3];

   wire [15:0]  rx_sd_async = rx_sd;
   reg [15:0] rx_sd_async1;

   reg [15:0] rx_sd_sync1;
   reg [15:0] rx_sd_sync2;
   reg [15:0] rx_sd_sync3;
   reg [15:0] rx_sd_sync4;
   reg [15:0] rx_sd_sync5;
   reg [15:0] rx_sd_sync6;
   reg [15:0] rx_sd_sync7;
   reg [15:0] rx_sd_sync8;
   reg [15:0] rx_sd_sync;

   always @ (posedge clk) begin
      rx_sd_async1 <= rx_sd_async;
      rx_sd_sync1 <= rx_sd_async1;
      rx_sd_sync2 <= rx_sd_sync1;
      rx_sd_sync3 <= rx_sd_sync2;
      rx_sd_sync4 <= rx_sd_sync3;
      rx_sd_sync5 <= rx_sd_sync4;
      rx_sd_sync6 <= rx_sd_sync5;
      rx_sd_sync7 <= rx_sd_sync6;
      rx_sd_sync8 <= rx_sd_sync7;
      rx_sd_sync <= rx_sd_sync1 & rx_sd_sync2 & rx_sd_sync3 & rx_sd_sync4 & rx_sd_sync5 & rx_sd_sync6 & rx_sd_sync7 & rx_sd_sync8;
      if (latch) begin
         sas_sd_out <= rx_sd_sync;
      end
   end

   ///////////////////////////////////////////////////////////
   // Counters for rx_sd_sync signals to see if sas link
   // signal-detect signal is unstable
   ///////////////////////////////////////////////////////////

   reg [31:0] rx_sd_counters[15:0];

   generate
      for (i=0; i<16; i=i+1) begin: rx_sd_counters_block
	 counter rx_sd_counter_i(.clk(clk), .reset(reset), .in(rx_sd_sync[i]), .out(rx_sd_counters[i]));
      end
   endgenerate

   always @ (posedge clk) begin
      if (latch) begin
	 sas_sd_counters_out <= rx_sd_counters;
      end
   end

   ///////////////////////////////////////////////////////////
   // Apply masks
   ///////////////////////////////////////////////////////////

   wire [15:0][15:0] adc16_bits_masked;
   wire [31:0][15:0] adc32_bits_masked;
   wire [31:0] 	     nim_bits_masked;
   wire [31:0] 	     esata_bits_masked;

   // from ALPHA16 transmitter:
   //
   // assign sas_bits[56] = esata_clk;
   // assign sas_bits[57] = esata_trig;
   // assign sas_bits[58] = nim_clk;
   // assign sas_bits[59] = nim_trig;

   generate
      for (i=0; i<8; i=i+1) begin: adc16_block
	 assign adc16_bits_masked[2*i+0][15:0] = conf_adc16_masks[i][15:0]  & sas_bits_sync[2*i+0][15:0];
	 assign adc16_bits_masked[2*i+1][15:0] = conf_adc16_masks[i][31:16] & sas_bits_sync[2*i+1][15:0];
      end
      for (i=0; i<16; i=i+1) begin: adc32_block
	 assign adc32_bits_masked[2*i+0][15:0] = conf_adc32_masks[i][15:0] & sas_bits_sync[i][31:16];
	 assign adc32_bits_masked[2*i+1][15:0] = conf_adc32_masks[i][31:16] & sas_bits_sync[i][47:32];
      end
      for (i=0; i<16; i=i+1) begin: esata_nim_block
	 assign esata_bits_masked[2*i+0] = conf_esata_mask[2*i+0] & sas_bits_sync[i][56];
	 assign esata_bits_masked[2*i+1] = conf_esata_mask[2*i+1] & sas_bits_sync[i][57];
	 assign nim_bits_masked[2*i+0] = conf_nim_mask[2*i+0] & sas_bits_sync[i][58];
	 assign nim_bits_masked[2*i+1] = conf_nim_mask[2*i+1] & sas_bits_sync[i][59];
      end
   endgenerate

   wire        adc16_grand_or_wire = |adc16_bits_masked;
   wire        adc32_grand_or_wire = |adc32_bits_masked;
   wire        adc_grand_or_wire = adc16_grand_or | adc32_grand_or;
   wire        esata_nim_grand_or_wire = (|esata_bits_masked) | (|nim_bits_masked);

   //
   // Compute the grand-or DFFs
   //

   reg 	       adc16_grand_or;
   reg 	       adc32_grand_or;
   reg 	       adc_grand_or;
   reg 	       esata_nim_grand_or;

   always_ff@ (posedge clk) begin
      adc16_grand_or <= adc16_grand_or_wire;
      adc32_grand_or <= adc32_grand_or_wire;
      adc_grand_or <= adc_grand_or_wire;
      esata_nim_grand_or <= esata_nim_grand_or_wire;
      nim_bits_out <= nim_bits_masked;
      esata_bits_out <= esata_bits_masked;
   end

   generate
      for (i=0; i<16; i=i+1) begin: adc16_bits_out_block
	 always_ff@ (posedge clk) begin
	    adc16_bits_out[i] <= adc16_bits_masked[i];
	 end
      end
      for (i=0; i<32; i=i+1) begin: adc32_bits_out_block
	 always_ff@ (posedge clk) begin
	    adc32_bits_out[i] <= adc32_bits_masked[i];
	 end
      end
   endgenerate

   //ag_conditioner cond_adc_grand_or(.clk(clk), .in(adc_grand_or_wire), .out(adc_grand_or));

   assign adc16_grand_or_out = adc16_grand_or;
   assign adc32_grand_or_out = adc32_grand_or;
   assign adc_grand_or_out = adc_grand_or;
   assign esata_nim_grand_or_out = esata_nim_grand_or;

   //
   // Counters for the grand-or signals
   //

   wire [31:0] counter_adc16_grand_or;
   wire [31:0] counter_adc32_grand_or;
   wire [31:0] counter_adc_grand_or;
   wire [31:0] counter_esata_nim_grand_or;

   counter cx1(.clk(clk), .reset(reset), .in(adc16_grand_or),     .out(counter_adc16_grand_or));
   counter cx2(.clk(clk), .reset(reset), .in(adc32_grand_or),     .out(counter_adc32_grand_or));
   counter cx3(.clk(clk), .reset(reset), .in(adc_grand_or),       .out(counter_adc_grand_or));
   counter cx4(.clk(clk), .reset(reset), .in(esata_nim_grand_or), .out(counter_esata_nim_grand_or));

   always @ (posedge clk) begin
      if (latch) begin
	 counter_adc16_grand_or_out <= counter_adc16_grand_or;
	 counter_adc32_grand_or_out <= counter_adc32_grand_or;
	 counter_adc_grand_or_out <= counter_adc_grand_or;
	 counter_esata_nim_grand_or_out <= counter_esata_nim_grand_or;
      end
   end

   //
   // Compute multiplicity of each adc16
   //
   //
   //wire [7:0]  adc16_mult16_wire[15:0];
   //
   //mult16 adc16m0(.in(adc16_bits_masked[0]), .out(adc16_mult16_wire[0]));
   //mult16 adc16m1(.in(adc16_bits_masked[1]), .out(adc16_mult16_wire[1]));
   //mult16 adc16m2(.in(adc16_bits_masked[2]), .out(adc16_mult16_wire[2]));
   //mult16 adc16m3(.in(adc16_bits_masked[3]), .out(adc16_mult16_wire[3]));
   //mult16 adc16m4(.in(adc16_bits_masked[4]), .out(adc16_mult16_wire[4]));
   //mult16 adc16m5(.in(adc16_bits_masked[5]), .out(adc16_mult16_wire[5]));
   //mult16 adc16m6(.in(adc16_bits_masked[6]), .out(adc16_mult16_wire[6]));
   //mult16 adc16m7(.in(adc16_bits_masked[7]), .out(adc16_mult16_wire[7]));
   //mult16 adc16m8(.in(adc16_bits_masked[8]), .out(adc16_mult16_wire[8]));
   //mult16 adc16m9(.in(adc16_bits_masked[9]), .out(adc16_mult16_wire[9]));
   //mult16 adc16m10(.in(adc16_bits_masked[10]), .out(adc16_mult16_wire[10]));
   //mult16 adc16m11(.in(adc16_bits_masked[11]), .out(adc16_mult16_wire[11]));
   //mult16 adc16m12(.in(adc16_bits_masked[12]), .out(adc16_mult16_wire[12]));
   //mult16 adc16m13(.in(adc16_bits_masked[13]), .out(adc16_mult16_wire[13]));
   //mult16 adc16m14(.in(adc16_bits_masked[14]), .out(adc16_mult16_wire[14]));
   //mult16 adc16m15(.in(adc16_bits_masked[15]), .out(adc16_mult16_wire[15]));
   //
   //
   // Compute the multiplicity DFFs
   //
   //
   //reg [7:0]  adc16_mult16[15:0];
   //
   //always_ff@ (posedge clk) begin
   //   adc16_mult16 <= adc16_mult16_wire;
   //end
   //
   //
   // Compute the full adc16 multiplicity
   //
   //
   //wire [7:0] adc16_mult_wire;
   //
   //add16 adc16_mult_inst
   //  (
   //   .data0x ( adc16_mult16[0] ),
   //   .data1x ( adc16_mult16[1] ),
   //   .data2x ( adc16_mult16[2] ),
   //   .data3x ( adc16_mult16[3] ),
   //   .data4x ( adc16_mult16[4] ),
   //   .data5x ( adc16_mult16[5] ),
   //   .data6x ( adc16_mult16[6] ),
   //   .data7x ( adc16_mult16[7] ),
   //   .data8x ( adc16_mult16[8] ),
   //   .data9x ( adc16_mult16[9] ),
   //   .data10x ( adc16_mult16[10] ),
   //   .data11x ( adc16_mult16[11] ),
   //   .data12x ( adc16_mult16[12] ),
   //   .data13x ( adc16_mult16[13] ),
   //   .data14x ( adc16_mult16[14] ),
   //   .data15x ( adc16_mult16[15] ),
   //   .result (adc16_mult_wire)
   //   );
   //
   //
   // Compute the multiplicity DFFs for adc16 data
   //
   //
   //reg [7:0] adc16_mult;
   //reg 	     adc16_mult_1ormore;
   //reg 	     adc16_mult_2ormore;
   //reg 	     adc16_mult_3ormore;
   //reg 	     adc16_mult_4ormore;
   //
   //always_ff@ (posedge clk) begin
   //   adc16_mult <= adc16_mult_wire;
   //   adc16_mult_1ormore <= (adc16_mult_wire >= 1);
   //   adc16_mult_2ormore <= (adc16_mult_wire >= 2);
   //   adc16_mult_3ormore <= (adc16_mult_wire >= 3);
   //   adc16_mult_4ormore <= (adc16_mult_wire >= 4);
   //end
   //
   //assign adc16_mult_out = adc16_mult;
   //assign adc16_mult_1ormore_out = adc16_mult_1ormore;
   //assign adc16_mult_2ormore_out = adc16_mult_2ormore;
   //assign adc16_mult_3ormore_out = adc16_mult_3ormore;
   //assign adc16_mult_4ormore_out = adc16_mult_4ormore;
   //
   //
   // Counters for adc16 multiplicity
   //
   //
   //wire [31:0] counter_adc16_mult_1ormore;
   //wire [31:0] counter_adc16_mult_2ormore;
   //wire [31:0] counter_adc16_mult_3ormore;
   //wire [31:0] counter_adc16_mult_4ormore;
   //
   //counter counter_adc16_mult_1ormore_inst(.clk(clk), .reset(reset), .in(adc16_mult_1ormore), .out(counter_adc16_mult_1ormore));
   //counter counter_adc16_mult_2ormore_inst(.clk(clk), .reset(reset), .in(adc16_mult_2ormore), .out(counter_adc16_mult_2ormore));
   //counter counter_adc16_mult_3ormore_inst(.clk(clk), .reset(reset), .in(adc16_mult_3ormore), .out(counter_adc16_mult_3ormore));
   //counter counter_adc16_mult_4ormore_inst(.clk(clk), .reset(reset), .in(adc16_mult_4ormore), .out(counter_adc16_mult_4ormore));
   //
   //always @ (posedge clk) begin
   //   if (latch) begin
   //counter_adc16_mult_1ormore_out <= counter_adc16_mult_1ormore;
   //counter_adc16_mult_2ormore_out <= counter_adc16_mult_2ormore;
   //counter_adc16_mult_3ormore_out <= counter_adc16_mult_3ormore;
   //counter_adc16_mult_4ormore_out <= counter_adc16_mult_4ormore;
   //end
   //end

   //
   // Per link ORes and counters
   //

   wire [15:0] adc16_or16_wire;
   wire [31:0] adc32_or16_wire;

   generate
      for (i=0; i<16; i=i+1) begin: adc16_or16_block
	 assign adc16_or16_wire[i] = (|adc16_bits_masked[i]);
      end
      for (i=0; i<32; i=i+1) begin: adc32_or16_block
	 assign adc32_or16_wire[i] = (|adc32_bits_masked[i]);
      end
   endgenerate

   reg [15:0] adc16_or16;
   reg [31:0] adc32_or16;

   always_ff@ (posedge clk) begin
      adc16_or16 <= adc16_or16_wire;
      adc32_or16 <= adc32_or16_wire;
   end

   assign adc16_or16_out = adc16_or16;
   assign adc32_or16_out = adc32_or16;

   //
   // counters for the adc16 and adc32 per-preamp ORes
   //
   
   reg [31:0] counter_adc16_or16[15:0];
   reg [31:0] counter_adc32_or16[31:0];

   //
   //generate
   //   for (i=0; i<16; i=i+1) begin: adc16_or16_counters_block
   //	 counter cnt[i](.clk(clk), .reset(reset), .in(adc16_or16[i]), .out(counter_adc16_or16[i]));
   //   end
   //endgenerate

   counter cnt_0(.clk(clk), .reset(reset), .in(adc16_or16[0]), .out(counter_adc16_or16[0]));
   counter cnt_1(.clk(clk), .reset(reset), .in(adc16_or16[1]), .out(counter_adc16_or16[1]));
   counter cnt_2(.clk(clk), .reset(reset), .in(adc16_or16[2]), .out(counter_adc16_or16[2]));
   counter cnt_3(.clk(clk), .reset(reset), .in(adc16_or16[3]), .out(counter_adc16_or16[3]));

   counter cnt_4(.clk(clk), .reset(reset), .in(adc16_or16[4]), .out(counter_adc16_or16[4]));
   counter cnt_5(.clk(clk), .reset(reset), .in(adc16_or16[5]), .out(counter_adc16_or16[5]));
   counter cnt_6(.clk(clk), .reset(reset), .in(adc16_or16[6]), .out(counter_adc16_or16[6]));
   counter cnt_7(.clk(clk), .reset(reset), .in(adc16_or16[7]), .out(counter_adc16_or16[7]));

   counter cnt_8(.clk(clk), .reset(reset), .in(adc16_or16[8]), .out(counter_adc16_or16[8]));
   counter cnt_9(.clk(clk), .reset(reset), .in(adc16_or16[9]), .out(counter_adc16_or16[9]));
   counter cnt_10(.clk(clk), .reset(reset), .in(adc16_or16[10]), .out(counter_adc16_or16[10]));
   counter cnt_11(.clk(clk), .reset(reset), .in(adc16_or16[11]), .out(counter_adc16_or16[11]));

   counter cnt_12(.clk(clk), .reset(reset), .in(adc16_or16[12]), .out(counter_adc16_or16[12]));
   counter cnt_13(.clk(clk), .reset(reset), .in(adc16_or16[13]), .out(counter_adc16_or16[13]));
   counter cnt_14(.clk(clk), .reset(reset), .in(adc16_or16[14]), .out(counter_adc16_or16[14]));
   counter cnt_15(.clk(clk), .reset(reset), .in(adc16_or16[15]), .out(counter_adc16_or16[15]));

   generate
      for (i=0; i<32; i=i+1) begin: adc32_or16_counters_block
   	 counter cnt32_i(.clk(clk), .reset(reset), .in(adc32_or16[i]), .out(counter_adc32_or16[i]));
      end
   endgenerate

   always @ (posedge clk) begin
      if (latch) begin
	 counter_adc16_or16_out <= counter_adc16_or16;
	 counter_adc32_or16_out <= counter_adc32_or16;
      end
   end

endmodule
