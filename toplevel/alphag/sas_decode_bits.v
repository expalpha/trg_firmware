module sas_decode_bits
  (
   input wire 	     reset,
   input wire 	     clk,
   input wire 	     ena,
   input wire [7:0]  in,
   output reg [63:0] out
   );

   always_ff @(posedge clk or posedge reset) begin
      if (reset) begin
	 out[63:0] <= 64'h0;
      end else if (ena) begin
	case (in[7:4])
	  default: out[63:0] <= 64'hxxxxxxxxxxxxxxxx;

	  4'h0 : out[0*4+3:0*4+0] <= in[3:0];
	  4'h1 : out[1*4+3:1*4+0] <= in[3:0];
	  4'h2 : out[2*4+3:2*4+0] <= in[3:0];
	  4'h3 : out[3*4+3:3*4+0] <= in[3:0];
	  
	  4'h4 : out[4*4+3:4*4+0] <= in[3:0];
	  4'h5 : out[5*4+3:5*4+0] <= in[3:0];
	  4'h6 : out[6*4+3:6*4+0] <= in[3:0];
	  4'h7 : out[7*4+3:7*4+0] <= in[3:0];
	  
	  4'h8 : out[8*4+3:8*4+0] <= in[3:0];
	  4'h9 : out[9*4+3:9*4+0] <= in[3:0];
	  4'hA : out[10*4+3:10*4+0] <= in[3:0];
	  4'hB : out[11*4+3:11*4+0] <= in[3:0];
	  
	  4'hC : out[12*4+3:12*4+0] <= in[3:0];
	  4'hD : out[13*4+3:13*4+0] <= in[3:0];
	  4'hE : out[14*4+3:14*4+0] <= in[3:0];
	  4'hF : out[15*4+3:15*4+0] <= in[3:0];

	endcase // case (in[7:4])
      end // if (ena)
   end // always_ff @

endmodule
