`default_nettype none
module str8
  (
   input wire clk,
   input wire in,
   output reg out
   );
   
   reg 	      in1;
   reg 	      in2;
   reg 	      in3;
   reg 	      in4;
   reg 	      in5;
   reg 	      in6;
   reg 	      in7;
   
   always_ff @ (posedge clk) begin
      in1 <= in;
      in2 <= in1;
      in3 <= in2;
      in4 <= in3;
      in5 <= in4;
      in6 <= in5;
      in7 <= in6;
      out <= in || in1 || in2 || in3 || in4 || in5 || in6 || in7;
   end

endmodule
