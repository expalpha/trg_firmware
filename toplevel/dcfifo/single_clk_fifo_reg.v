// 2^12 [4096] x 32bits
module single_clk_fifo_reg (  input  wire   clk,  input  wire  aclr,  output wire almostfull, output wire almostempty,
   input  wire [31:0]   data, input  wire wrreq,  output wire  full,  output wire wren,  output wire [11:0]  words, 
   output wire [31:0]      q, input  wire rdreq,  output wire empty,  output wire rden,  output reg  [11:0] rdaddr,
	output reg  [11:0] wraddr, output wire error
);
assign almostfull  = (words > 12'hffb);
assign almostempty = (words < 12'h003);
assign wren                      = wrreq & (! full);
assign rden /* synthesis keep */ = rdreq & (! empty);
assign error = (wrreq && full) || (rdreq && empty);

assign words =   wraddr - rdaddr;
assign full  = ((wraddr+12'h1) == rdaddr);
assign empty = ((wraddr      ) == rdaddr);

// mem [4kx32] can be device-specific - name by device
//scfiforam_stratix4gx_unreg fifomem (  .clock(clk), 
//scfiforam_arriav_unreg fifomem (  .clock(clk), 
scfiforam_stratix4gx_reg fifomem ( .clock(clk), 
   .wraddress(wraddr), .wren(wren), .data(data),
   .rdaddress(rdaddr),                 .q(   q)
);

always @(posedge clk or posedge aclr) begin // write side - each write incs write_ptr
   if( aclr ) wraddr <= 12'h0;
	else       wraddr <= ( wren ) ? wraddr + 12'h1 : wraddr;
end

always @(posedge clk or posedge aclr) begin // read side - each read incs read_ptr
   if( aclr ) rdaddr <= 12'h0;
	else       rdaddr <= ( rden ) ? rdaddr + 12'h1 : rdaddr;
end

endmodule
