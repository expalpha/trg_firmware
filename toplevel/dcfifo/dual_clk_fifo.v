// each side is almost independant, running on its own clk, each side maintains read/write address
//   but read/write positions need to be sent to to other side (asynchronous) to check if full/empty
//      since sending a multi-bit address through a synchroniser, need to grey-encode the value to avoid errors
//          bit transitions may each independantly be delayed or not by one clk
//             (do not allow transitions to spread by more than one clk, or grey-coding will still not be sufficient)
// 2^12 [4096] x 32bits
module dual_clk_fifo (      input  wire aclr,  output wire almostfull,
   input  wire [31:0] data, input  wire wrclk, input wire wrreq,  output reg  wrfull, output reg [11:0] wrwords,
   output wire [31:0]    q, input  wire rdclk, input wire rdreq,  output reg rdempty, output reg [11:0] rdwords
);
assign almostfull = (wrwords > 12'hff0);

// memory block [write clk: data/addr/enable  read clk: addr/q]
// rdempty if wraddr   == rdaddr,  rdwords = wraddr - rdaddr (+size if <0)
// wrfull  if wraddr+1 == rdaddr,  wrwords = same as above
//  => max wordcount = size-1 i.e. cannot write last word, 
//     as addresses would be same as when fifo was empty
dcfifo_ram fifomem (
   .wrclock(wrclk), .wraddress(wraddr), .wren(wren), .data(data),
   .rdclock(rdclk), .rdaddress(rdaddr),                 .q(   q)
);

// write side - each write incs write_ptr
reg [11:0]  wraddr;  wire wren = wrreq && ! wrfull;
always @(posedge wrclk or posedge aclr) begin
   if( aclr ) begin
	   wraddr <= 12'h0; wrwords <= 12'h0; wrfull <= 1'b0;
	end else begin
	   wraddr  <=  wraddr;
		wrwords <=  wraddr - sync_rdaddr;
		wrfull  <= ((wraddr+12'h1) == sync_rdaddr);
	   if( wren ) wraddr <= wraddr + 12'h1;
	end
end

// read side - each read incs read_ptr
reg [11:0]  rdaddr;  wire rden = rdreq && ! rdempty;
always @(posedge rdclk or posedge aclr) begin
   if( aclr ) begin
	   rdaddr <= 12'h0; rdwords <= 12'h0; rdempty <= 1'b1;
	end else begin
	   rdaddr  <=  rdaddr;
		rdwords <=  sync_wraddr - rdaddr;
		rdempty <= (rdaddr == sync_wraddr);
	   if( rden ) rdaddr <= rdaddr + 12'h1;
	end
end

// to get word counts and full/empty flags, need to get other sides address value
// transfer to other clock: encode, resync to other clk, decode
// add multicycle paths to sdcfile to ignore timing failure errors on synchronisers

// wraddr -> sync_wraddr_in -> sync_wraddr_a -> sync_wraddr_grey -> sync_wraddr
reg  [11:0] sync_wraddr_grey;  reg  [11:0] sync_wraddr_a;
wire [11:0] sync_wraddr_in;    wire [11:0] sync_wraddr;
bin2grey wrenc( .bin_in(wraddr), .grey_out(sync_wraddr_in) );
always @(posedge rdclk) begin
   sync_wraddr_a    <= sync_wraddr_in;
	sync_wraddr_grey <= sync_wraddr_a;
end
grey2bin wrdec( .grey_in(sync_wraddr_grey), .bin_out(sync_wraddr) );

// rdaddr -> sync_rdaddr_in -> sync_rdaddr_a -> sync_rdaddr_grey -> sync_rdaddr
reg  [11:0] sync_rdaddr_grey;  reg  [11:0] sync_rdaddr_a;
wire [11:0] sync_rdaddr_in;    wire [11:0] sync_rdaddr;
bin2grey rdenc( .bin_in(rdaddr), .grey_out(sync_rdaddr_in) );
always @(posedge wrclk) begin
   sync_rdaddr_a    <= sync_rdaddr_in;
	sync_rdaddr_grey <= sync_rdaddr_a;
end
grey2bin rddec( .grey_in(sync_rdaddr_grey), .bin_out(sync_rdaddr) );

endmodule
