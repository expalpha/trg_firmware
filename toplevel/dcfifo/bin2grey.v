//binary to grey: (x>>1)^x   e.g for 12bit code ...  
//   {0,x[11:1]} ^ x[11:0]  =  {x[11], x[11]^x[10], ..., x[1]^x[0]}

module bin2grey (input wire [11:0] bin_in, output wire [11:0] grey_out);

assign grey_out[11] = bin_in[11];
assign grey_out[10] = bin_in[10] ^ bin_in[11];
assign grey_out[ 9] = bin_in[ 9] ^ bin_in[10];
assign grey_out[ 8] = bin_in[ 8] ^ bin_in[ 9];
assign grey_out[ 7] = bin_in[ 7] ^ bin_in[ 8];
assign grey_out[ 6] = bin_in[ 6] ^ bin_in[ 7];
assign grey_out[ 5] = bin_in[ 5] ^ bin_in[ 6];
assign grey_out[ 4] = bin_in[ 4] ^ bin_in[ 5];
assign grey_out[ 3] = bin_in[ 3] ^ bin_in[ 4];
assign grey_out[ 2] = bin_in[ 2] ^ bin_in[ 3];
assign grey_out[ 1] = bin_in[ 1] ^ bin_in[ 2];
assign grey_out[ 0] = bin_in[ 0] ^ bin_in[ 1];

endmodule
