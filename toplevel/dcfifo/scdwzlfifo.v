// single-clk dual-width no-latency
module scdwzlfifo ( output wire debug,
   input  wire                 clk,  input  wire      aclr,  output wire [13:0] words,   output wire      error,
   input  wire [1023:0]       data,  input  wire     wrreq,  output wire         full,   output wire almostfull,
   output wire  [31:0]           q,  input  wire     rdreq,  output wire        empty,   output wire almostempty
); assign debug = fifo_q_valid_reg[2] ^ fifo_q_valid_reg[3];

assign almostfull   = (wrwords > 9'h1fa);
assign full         = fifo_full;
assign almostempty  = (valid_count < 3'h2); // only 1 word left - if reading on this clk, cannot set read for nxt clk 
assign q            =  q_reg[31:0];
assign words        =  words_int + valid_count;
wire   read_request =  rdreq && !empty; // read_request only affects registers
assign error        = (rdreq && empty);
assign empty        = (valid_count == 3'h0);

 // attempt to keep register full - how to allow for latency on empty (will overrun while empty is about to clear)
wire [3:0] total_count = valid_count + pending_count; // 
wire rdreq_int = ( total_count < 4'h4 ) && (!fifo_empty);

reg [3:0] fifo_q_valid_reg /* synthesis keep */;  wire fifo_q_valid = fifo_q_valid_reg[1];
always @(posedge clk or posedge aclr) fifo_q_valid_reg <= (aclr) ? 4'h0 : {fifo_q_valid_reg[2:0],rdreq_int};

reg [2:0] valid_count;                       // valid count - lose one entry on rdreq, gain one on mem-valid
always @(posedge clk or posedge aclr) begin  // 
   if( aclr ) valid_count <= 3'h0;
   else       valid_count <= valid_count + (fifo_q_valid ? 3'h1 : 3'h0) - (read_request ? 3'h1 : 3'h0);
end

reg [2:0] pending_count;                     // pending count - gain one entry on rdreq_int, lose one on mem-valid
always @(posedge clk or posedge aclr) begin  //  **count is delayed 1clk - current count is pending + rdreq_int**
   if( aclr ) pending_count <= 3'h0;
   else       pending_count <= pending_count + (rdreq_int ? 3'h1 : 3'h0) - (fifo_q_valid ? 3'h1 : 3'h0);
end

reg [127:0] q_reg /* synthesis keep */; 
always @(posedge clk or posedge aclr) begin
   if( aclr ) q_reg <= 128'h0;
   else begin
      q_reg <= read_request ? {32'h0,q_reg[127:32]} : q_reg;
		if( fifo_q_valid ) begin
		   case ( (valid_count - ((read_request) ? 2'h1 : 2'h0)) ) // where to put this fifo_q
			2'h0:  q_reg[ 31: 0] <= fifo_q;
			2'h1:  q_reg[ 63:32] <= fifo_q;
			2'h2:  q_reg[ 95:64] <= fifo_q;
			2'h3:  q_reg[127:96] <= fifo_q;
         endcase
		end
   end
end

//wire [8:0] wrwords;
//wire [11:0] words_int = fifo_words;  assign fifo_wrreq = wrreq;  assign fifo_rdreq = rdreq_int;
//dual_width_scfifo_reg sc_fifo ( .clk(clk),   .aclr(aclr),
//   .data(data),   .wrreq(fifo_wrreq),   .full(fifo_full),   .wren(fifo_wren),  .wraddr(fifo_wraddr),  .wrwords(wrwords),
//   .q(fifo_q),    .rdreq(fifo_rdreq),   .empty(fifo_empty), .rden(fifo_rden),  .rdaddr(fifo_rdaddr),  .rdwords(fifo_words),
//   .q_count(q_count),                   .mem_rden(mem_rden),                   .mem_q(mem_q)
//); 
wire [31:0] fifo_q;  wire fifo_full, fifo_empty;  wire [8:0] wrwords; wire [13:0] words_int /* synthesis keep */;
dual_width_scfifo_reg dwscfifo ( .clk(clk),   .aclr(aclr),  
   .data(data),   .wrreq(wrreq),       .full(fifo_full),   .wrwords(wrwords),
   .q(fifo_q),    .rdreq(rdreq_int),   .empty(fifo_empty), .rdwords(words_int)
);

endmodule
