// simple fifo - unequal widths
module dual_width_scfifo_reg (  input  wire   clk,  input  wire  aclr,   output wire error,
   input  wire [1023:0]   data,   input  wire wrreq,  output wire  full,  output wire wren,  output reg  [8:0] wraddr,  output wire almostfull, 
   output wire   [31:0]      q,   input  wire rdreq,  output wire empty,  output wire rden,  output reg  [8:0] rdaddr,  output wire almostempty,
   output wire   [ 8:0] wrwords,  output wire [13:0] rdwords
);
assign q = q_reg[31:0];
assign wrwords = words;
assign empty = (rdwords == 14'h0);

assign almostfull  = (  words >  9'h1fb);
assign almostempty = (rdwords < 14'h003);
assign wren        = wrreq & (! full );
assign rden        = rdreq & (! empty );
assign error = (wrreq && full) || (rdreq && empty); // ??not sure

assign rdwords    = {words,5'h0} + q_count[5:0];

// with registered memory block, register automatically gets mem.q 2 clks after rdreq
//    here we have to manually load the register at the right time (can't wait for rdreq - too late!)
//    anytime we load reg, need to inc rdaddr for next value, and set q_count (both can be done at on clk as load reg)
wire mem_rden = (q_count[5:0] == 6'h0 || ((q_count[5:0] == 6'h1) && rden) ) && ((wraddr != rdaddr) || wren) &&
                  (mem_rden_del == 2'h0);
reg [1:0] mem_rden_del;  always @(posedge clk) mem_rden_del <= {mem_rden_del[0],mem_rden};
reg [1:0]     rden_del;  always @(posedge clk)     rden_del <= {    rden_del[0],    rden};

///fast fifo timing is read(3F,40,..) 1clk after writes(3e,3f), memq good 3clks after write(41,42,...)
// data on fifoq 1clk after memq good(42,43,...)
//    empty should be on un-delayed numbers
//    q_count also undelayed to keep mem_rden undelayed
//    q_reg control on delayed numbers
//    mem_rdaddr is delayed or will skip data

reg [1023:0] q_reg;
always @(posedge clk or posedge aclr) begin
   if(              aclr    ) q_reg <= 1023'h0;
   else if( mem_rden_del[1] ) q_reg <=  mem_q;  // priority over rden if both set
   else if(     rden_del[1] ) q_reg <= {32'h0,q_reg[1023:32]};
   else                       q_reg <=  q_reg;
end
reg [7:0] q_count;
always @(posedge clk or posedge aclr) begin
   if(          aclr ) q_count <=  8'h0;
   else                q_count <= q_count - (rden ? 8'h1 : 8'h0) + (mem_rden ? 8'h20 : 8'h0);
end  

wire [8:0] words =   wraddr - rdaddr;
assign full      = ((wraddr+9'h1) == rdaddr);

wire [1023:0] mem_q;
// we register data above in large shift-register, do not double-register it
scdwfiforam_stratix4gx_unreg fifomem ( .clock(clk), 
   .wraddress(wraddr), .wren(wren),  .data(data),
   .rdaddress(rdaddr),                 .q(mem_q)
);

always @(posedge clk or posedge aclr) begin // write side - each write incs write_ptr
   if( aclr ) wraddr <= 9'h0;
	else      wraddr <= ( wren ) ? wraddr + 9'h1 : wraddr;
end

always @(posedge clk or posedge aclr) begin // read side - each read incs read_ptr
   if( aclr ) rdaddr <= 9'h0;
	else      rdaddr <= ( mem_rden_del[1] ) ? rdaddr + 9'h1 : rdaddr;
end

endmodule
