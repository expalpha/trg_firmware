//grey to binary ... upper bit unchanged, each lower ^= prev ...
//   x[11:0] ->  {x[11], x[10]^x[11], x[9]^{x[10]^x[11]}, .... }
// 
// to speed up - can break ripple chain in middle, and
// calculate both possibilities for lower half of chain in advance
// (and use actual value to select correct choice when available)
//
module grey2bin (input wire [11:0] grey_in, output wire [11:0] bin_out);

assign bin_out[11] = grey_in[11];
assign bin_out[10] = grey_in[10] ^ bin_out[11];
assign bin_out[ 9] = grey_in[ 9] ^ bin_out[10];
assign bin_out[ 8] = grey_in[ 8] ^ bin_out[ 9];
assign bin_out[ 7] = grey_in[ 7] ^ bin_out[ 8];
assign bin_out[ 6] = grey_in[ 6] ^ bin_out[ 7];
assign bin_out[ 5] = grey_in[ 5] ^ bin_out[ 6];
assign bin_out[ 4] = grey_in[ 4] ^ bin_out[ 5];
assign bin_out[ 3] = grey_in[ 3] ^ bin_out[ 4];
assign bin_out[ 2] = grey_in[ 2] ^ bin_out[ 3];
assign bin_out[ 1] = grey_in[ 1] ^ bin_out[ 2];
assign bin_out[ 0] = grey_in[ 0] ^ bin_out[ 1];

endmodule
