// 2^12 [4096] x 32bits
module single_clk_fifo (  input  wire clk,  input  wire aclr,  output wire almostfull, output wire almostempty,
   input  wire [31:0]   data, input wire wrreq,  output reg  full,  output wire wren,  output reg  [11:0]  words, 
   output wire [31:0]      q, input wire rdreq,  output reg empty,  output wire rden,  output reg  [11:0] rdaddr,
	output reg  [11:0] wraddr
);
assign almostfull  = (words > 12'hffb);
assign almostempty = (words < 12'h003);
assign wren /* synthesis keep */ = wrreq; // & (! full);
assign rden /* synthesis keep */ = rdreq; // & (! empty);

always @(posedge clk or posedge aclr) if( aclr ) words <= 12'h0; else words <= wraddr - rdaddr;

// mem [4kx32] can be device-specific - name by device
//scfiforam_stratix4gx_reg fifomem (
//scfiforam_stratix4gx_unreg fifomem (  .clock(clk), 
scfifo_ram fifomem (  .clock(clk), 
   .wraddress(wraddr), .wren(wren), .data(data),
   .rdaddress(rdaddr),                 .q(   q)
);

always @(posedge clk or posedge aclr) begin // write side - each write incs write_ptr
   if( aclr ) begin
	   wraddr <= 12'h0; full <= 1'b0;
	end else begin
	   wraddr <=  wraddr;
		full   <= ((wraddr+12'h1) == rdaddr);
	   if( wren ) wraddr <= wraddr + 12'h1;
	end
end

always @(posedge clk or posedge aclr) begin // read side - each read incs read_ptr
   if( aclr ) begin
	   rdaddr <= 12'h0; empty <= 1'b1;
	end else begin
	   rdaddr <= rdaddr;
		empty  <= (rdaddr == wraddr);
	   if( rden ) rdaddr <= rdaddr + 12'h1;
	end
end

endmodule
