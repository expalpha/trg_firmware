//
// chronobox timestamp for one channel
//

`default_nettype none
module cb_tsc1
  (
   input wire 	      clk,

   // control

   input wire enable_le,
   input wire enable_te,

   // data input

   input wire in,
   input wire [6:0] chan,
   input wire [23:0] ts,

   // outputs

   output reg [31:0] out,
   output reg out_ready
   );

   reg        in1;

   always_ff @ (posedge clk) begin
      in1 <= in;
      if (in == 1 && in1 == 0 && enable_le) begin
         out <= {1'b1,chan,ts[23:1],1'b0};
         out_ready <= 1;
      end else if (in == 0 && in1 == 1 && enable_te) begin
         out <= {1'b1,chan,ts[23:1],1'b1};
         out_ready <= 1;
      end else begin
         out <= 0;
         out_ready <= 0;
      end
   end
endmodule
