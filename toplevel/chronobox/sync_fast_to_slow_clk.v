`default_nettype none
module sync_fast_to_slow_clk
  (
   input wire in,
   input wire clk,
   output reg out
   );

   wire       in_latched;
   
   dff latch(.d(in),
             .clk(clk),
             .clrn(1),
             .prn(!in),
             .q(in_latched));
             
   reg        in1;
             
   always @(posedge clk) begin
      in1 <= in_latched;
      out <= in1;
   end
endmodule
