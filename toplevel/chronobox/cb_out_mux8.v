//
// output multiplexor
//

`default_nettype none

module cb_out_mux8
  (
   input wire [31:0] ctrl,
   input wire [7:0][15:0] in,
   output wire [7:0] out
   );

   genvar               i;

   generate
      for (i=0; i<8; i=i+1) begin: mux8_loop
         wire [3:0] m = ctrl[i*4+3:i*4];
         assign out[i] = in[i][m];
      end
   endgenerate

endmodule
