`default_nettype none
module counter
  (
   input wire 	     clk,
   input wire 	     reset,
   input wire 	     in,
   output reg [31:0] out
   );

   reg 		     in1;
   wire 	     in_pulse;

   always @ (posedge clk) begin
      in1 <= in;
   end

   assign in_pulse = in & !in1;

   // Convert signal "in" into a single-shot pulse:
   //
   // in, in1 -> in_pulse -> next clock in1, in_pulse
   // 0, 0 -> 0 -> 0, 0
   // 1, 0 -> 1 -> 1, 0
   // 1, 1 -> 0 -> 1, 0
   // 0, 1 -> 0 -> 0, 0
   
   always @ (posedge clk or posedge reset) begin
      if (reset) begin
	 out <= 0;
      end else begin
	 if (in_pulse) begin
	    out <= out + 1;
	 end else begin
	    out <= out;
	 end
      end
   end

endmodule
