//
// chronobox timestamp for one channel with FIFO
//

`default_nettype none
module cb_tsc1f
  (
   input wire 	      clk,
   input wire 	      clk_ts,
   input wire         reset_clk_ts,

   // control

   input wire enable_le,
   input wire enable_te,

   // data input

   input wire in,
   input wire [6:0] chan,
   input wire [23:0] ts,
   input wire fifo_rdacq,

   // outputs

   output wire [31:0] fifo_data,
   output wire fifo_full,
   output wire fifo_empty
   );

   wire [31:0]  tsc_data;
   wire         tsc_ready;

   cb_tsc1 cb_tsc1
     (
      .clk(clk_ts),
      .ts(ts),
      .chan(chan),
      .enable_le(enable_le),
      .enable_te(enable_te),
      .in(in),
      .out(tsc_data),
      .out_ready(tsc_ready)
      );

   reg          reset_clk_ts1;
   reg          reset_clk_ts2;

   always_ff @ (posedge clk_ts) begin
      reset_clk_ts1 <= reset_clk_ts;
      reset_clk_ts2 <= reset_clk_ts1;
   end

   // note on sequencing of reset signal:
   // reset goes up: aclr goes up, we do not care what wrreq does
   // reset goes down: aclr goes down first, 2 clocks later, wrreq becomes enabled.
   // K.O.

   fifo_dual_clock cb_tsc_fifo_0
     (
      .aclr(reset_clk_ts),
      .wrclk(clk_ts),
      .wrreq(tsc_ready & !reset_clk_ts2),
      .data(tsc_data),
      .rdclk(clk),
      .rdreq(fifo_rdacq),
      .q(fifo_data),
      .rdempty(fifo_empty),
      .rdfull(fifo_full)
      );

endmodule
