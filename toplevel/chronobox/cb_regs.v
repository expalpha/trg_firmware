//
// chronobox control registers
//

`default_nettype none
module cb_regs
  (
   input wire 	      clk,
   input wire 	      reset,

   // avalon bus interface

   input  wire [13:0] addr,
   input  wire [31:0] write_data,
   input  wire        write_strobe,
   input  wire        read_strobe,
   output wire [31:0] read_data_out,
   output wire        read_data_valid_out,

   // data input
   
   input wire [31:0]  sof_revision_in,
   input wire [31:0]  switches_in,
   input wire [31:0]  buttons_in,
   input wire [15:0]  flash_programmer_in,
   input wire [31:0]  ecl_in,
   input wire [31:0]  lemo_in,
   input wire [31:0]  gpio_in,
   input wire [31:0]  reg7_test_in,
   input wire [31:0]  reg8_scaler_data_in,
   input wire [31:0]  reg15_input_num_in,
   input wire [31:0]  reg16_fifo_status,
   input wire [31:0]  reg17_fifo_data,
   input wire [31:0]  reg24_fc_ext_clk_100_counter,
   input wire [31:0]  reg25_fc_ext_clk_ext_counter,
   input wire [31:0]  reg26_fc_ts_clk_100_counter,
   input wire [31:0]  reg27_fc_ts_clk_ts_counter,
   input wire [31:0]  reg28_ts_clk_pll_status,
   input wire [31:0]  reg32_cb_sync_status,

   // controls out

   output reg 	      zero_scalers_out,
   output reg 	      latch_scalers_out,
   output reg  [15:0] scaler_addr_out,
   output reg         reconfig_out,
   //output reg         fifo_rdreq_out,
   output wire        fifo_rdack_out,
   output reg         ts_clk_pll_extswitch_out,
   
   // registers

   //output reg [31:0]  reg0, // sof_revision
   output reg [31:0]  reg1_led_out,
   //output reg [31:0]  reg2_switches_in,
   //output reg [31:0]  reg3_buttons_in,
   //output reg [31:0]  reg4_test,
   output reg [15:0]  reg5_flash_programmer_out,
   output reg [31:0]  reg11_lemo_out,
   output reg [31:0]  reg12_gpio_out,
   output reg [31:0]  reg13_out_enable_out,
   output reg [31:0]  reg18_out,
   output reg [31:0]  reg19_out,
   output reg [31:0]  reg20_out,
   output reg [31:0]  reg21_out,
   output reg [31:0]  reg22_out,
   output reg [31:0]  reg23_out,
   output reg [31:0]  reg29_lemo_out_mux_out,
   output reg [31:0]  reg30_cb_sync_a_out,
   output reg [31:0]  reg31_cb_sync_b_out,
   output reg [31:0]  reg32_cb_sync_reg_out,
   output reg [31:0]  reg_last
   );

   assign reg_last = 32'b0;

   reg [31:0] 	      reg4_test;
   reg [31:0] 	      reg14;
   
   always_ff @ (posedge clk or posedge reset) begin
      if (reset) begin
	 read_data_out <= 32'b0;
	 reg1_led_out <= 32'b0;
         reconfig_out <= 0;
         read_data_valid_out <= 0;
         fifo_rdack_out <= 0;
      end else if (write_strobe) begin
         fifo_rdack_out <= 0;
	 case (addr)
	   14'd0: begin
	      latch_scalers_out <= write_data[0];
	      zero_scalers_out <= write_data[1];
	      //fifo_rdreq_out <= write_data[2];
              ts_clk_pll_extswitch_out <= (ts_clk_pll_extswitch_out & !write_data[3]) | write_data[4];
	   end
	   14'd1: reg1_led_out <= write_data;
	   14'd2: ;
	   14'd3: ;
	   14'd4: reg4_test <= write_data;
	   14'd5: reg5_flash_programmer_out <= write_data[15:0];
	   14'd6: ;
	   14'd7: ;
	   14'd8: scaler_addr_out <= write_data[15:0];
	   // 9 is lemo in
	   // 10 is gpio in
	   14'd11: reg11_lemo_out <= write_data;
	   14'd12: reg12_gpio_out <= write_data;
	   14'd13: reg13_out_enable_out <= write_data;
	   14'd14: begin
              reg14 <= write_data;
              reconfig_out <= (write_data == (~sof_revision_in));
           end
	   // 15 is number of inputs
           // 16 is FIFO
           // 17 is FIFO
	   14'd18: reg18_out <= write_data;
	   14'd19: reg19_out <= write_data;
	   14'd20: reg20_out <= write_data;
	   14'd21: reg21_out <= write_data;
	   14'd22: reg22_out <= write_data;
	   14'd23: reg23_out <= write_data;
           // 24 clock counters
           // 25 clock counters
           // 26 clock counters
           // 27 clock counters
           // 28 pll_status
           14'd29: reg29_lemo_out_mux_out <= write_data;
           14'd30: reg30_cb_sync_a_out <= write_data;
           14'd31: reg31_cb_sync_b_out <= write_data;
           14'd32: reg32_cb_sync_reg_out <= write_data;
	   default: ;
	 endcase // case (av_addr)
         read_data_valid_out <= 0;
      end else if (read_strobe) begin
         fifo_rdack_out <= 0;
	 case (addr)
	   14'd0:    read_data_out <= sof_revision_in;
	   14'd1:    read_data_out <= reg1_led_out;
	   14'd2:    read_data_out <= switches_in;
	   14'd3:    read_data_out <= buttons_in;
	   14'd4:    read_data_out <= reg4_test;
	   14'd5:    read_data_out <= { flash_programmer_in, flash_programmer_in };
	   14'd6:    read_data_out <= ecl_in;
	   14'd7:    read_data_out <= reg7_test_in;
	   14'd8:    read_data_out <= reg8_scaler_data_in;
	   14'd9:    read_data_out <= lemo_in;
	   14'd10:   read_data_out <= gpio_in;
	   14'd11:   read_data_out <= reg11_lemo_out;
	   14'd12:   read_data_out <= reg12_gpio_out;
	   14'd13:   read_data_out <= reg13_out_enable_out;
	   14'd14:   read_data_out <= reg14;
	   14'd15:   read_data_out <= reg15_input_num_in;
	   14'd16:   read_data_out <= reg16_fifo_status;
	   14'd17:   begin
              read_data_out <= reg17_fifo_data;
              fifo_rdack_out <= 1;
           end
	   14'd18:   read_data_out <= reg18_out;
	   14'd19:   read_data_out <= reg19_out;
	   14'd20:   read_data_out <= reg20_out;
	   14'd21:   read_data_out <= reg21_out;
	   14'd22:   read_data_out <= reg22_out;
	   14'd23:   read_data_out <= reg23_out;
           14'd24:   read_data_out <= reg24_fc_ext_clk_100_counter;
           14'd25:   read_data_out <= reg25_fc_ext_clk_ext_counter;
           14'd26:   read_data_out <= reg26_fc_ts_clk_100_counter;
           14'd27:   read_data_out <= reg27_fc_ts_clk_ts_counter;
           14'd28:   read_data_out <= reg28_ts_clk_pll_status;
           14'd29:   read_data_out <= reg29_lemo_out_mux_out;
           14'd30:   read_data_out <= reg30_cb_sync_a_out;
           14'd31:   read_data_out <= reg31_cb_sync_b_out;
           14'd32:   read_data_out <= reg32_cb_sync_status;
	   default:  read_data_out <= 32'b0;
	 endcase
         read_data_valid_out <= read_strobe;
      end else begin
         read_data_valid_out <= 0;
         fifo_rdack_out <= 0;
      end
   end // always_ff @ (posedge clk or posedge reset)

endmodule
