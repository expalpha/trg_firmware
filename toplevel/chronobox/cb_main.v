//
// chronobox main block
//

`default_nettype none
module cb_main #(parameter N = 32, S = 4)
  (
   input wire 	      clk,
   input wire 	      clk_ts,

   // data input

   input wire [N-1:0] inputs_async,

   // synchronization

   input wire sync_arm,
   input wire sync_disarm,
   input wire sync_in_async,
   
   // data input controls

   input wire [N-1:0] enable_le,
   input wire [N-1:0] enable_te,

   // controls

   input wire 	      zero_scalers_in,
   input wire 	      latch_scalers_in,
   input wire [15:0]  scaler_addr_in,
   input wire [31:0]  latch_scalers_period_in,
   
   // outputs

   output reg [31:0] input_num_out,

   // output scalers

   output reg [31:0]  scaler_data_out,

   // output fifo

   output reg [31:0] fifo_out,
   output reg fifo_ready_out,
   output reg tsc_fifo_full_out,

   // sync status

   output wire [15:0] sync_status_out,
   output wire        sync_reset_out
   );

   //
   // set number of inputs
   //

   assign input_num_out = N;

   //
   // data path pattern generator
   //

   wire               fifo_pattern_enable = 0;

   reg [7:0]          fifo_pattern;

   always_ff @(posedge clk) begin
      if (fifo_ready_out) begin
         fifo_pattern <= fifo_pattern + 8'd1;
      end
   end

   wire [31:0] fifo_xout;

   always_comb begin
      if (fifo_pattern_enable) begin
         fifo_out = { fifo_xout[31:16],fifo_pattern[7:0],fifo_xout[7:0] };
      end else begin
         fifo_out = fifo_xout;
      end
   end

   //
   // Synchronization circuit
   //

   reg            sync_armed;
   reg            sync_done;
   reg            sync_received;

   reg            sync_armed_clk_ts;
   reg            sync_done_clk_ts;
   reg            sync_received_clk_ts;

   wire           sync_reset = sync_armed;
   assign         sync_reset_out = sync_armed;
   wire           sync_reset_clk_ts = sync_armed_clk_ts;

   wire [15:0]    sync_status;

   assign         sync_status[15]  = sync_armed;
   assign         sync_status[14]  = sync_done;
   assign         sync_status[13]  = sync_received;
   assign         sync_status[12]  = 0;
   assign         sync_status[11]  = sync_armed_clk_ts; // NOTE: wrong clock
   assign         sync_status[10]  = sync_done_clk_ts; // NOTE: wrong clock
   assign         sync_status[9]   = sync_received_clk_ts; // NOTE: wrong clock
   assign         sync_status[8]   = 0;
   assign         sync_status[7:0] = 0;

   always_ff @(posedge clk) begin
      sync_status_out <= sync_status;
   end

   reg sync_in1;
   reg sync_in2;
   reg sync_in;

   always_ff @(posedge clk) begin
      sync_in1 <= sync_in_async;
      sync_in2 <= sync_in1;
      sync_in  <= sync_in2;
   end

   always_ff @(posedge clk) begin
      if (sync_in) begin
         sync_received <= 1;
      end
      if (sync_disarm) begin
         sync_armed <= 0;
         sync_done <= 0;
         sync_received <= 0;
      end else if (sync_armed) begin
         if (sync_in) begin
            sync_armed <= 0;
            sync_done  <= 1;
         end
      end else if (sync_arm) begin
         sync_armed <= 1;
         sync_done  <= 0;
         sync_received <= 0;
      end
   end // always_ff @ (posedge clk)

   reg sync_arm_clk_ts1;
   reg sync_arm_clk_ts;

   reg sync_disarm_clk_ts1;
   reg sync_disarm_clk_ts;

   reg sync_in_clk_ts1;
   reg sync_in_clk_ts2;
   reg sync_in_clk_ts;

   always_ff @(posedge clk_ts) begin
      sync_arm_clk_ts1 <= sync_arm;
      sync_arm_clk_ts  <= sync_arm_clk_ts1;

      sync_disarm_clk_ts1 <= sync_disarm;
      sync_disarm_clk_ts  <= sync_disarm_clk_ts1;

      sync_in_clk_ts1 <= sync_in_async;
      sync_in_clk_ts2 <= sync_in_clk_ts1;
      sync_in_clk_ts  <= sync_in_clk_ts2;
   end

   always_ff @(posedge clk_ts) begin
      if (sync_in_clk_ts) begin
         sync_received_clk_ts <= 1;
      end
      if (sync_disarm_clk_ts) begin
         sync_armed_clk_ts <= 0;
         sync_done_clk_ts <= 0;
         sync_received_clk_ts <= 0;
      end else if (sync_armed_clk_ts) begin
         if (sync_in_clk_ts) begin
            sync_armed_clk_ts <= 0;
            sync_done_clk_ts  <= 1;
         end
      end else if (sync_arm_clk_ts) begin
         sync_armed_clk_ts <= 1;
         sync_done_clk_ts  <= 0;
         sync_received_clk_ts <= 0;
      end
   end

   //
   // Synchronize control signals
   //

   wire       zero_scalers_pulse;
   wire       latch_scalers_pulse1;

   pulse pulse_zero(.clk(clk), .in(zero_scalers_in), .out(zero_scalers_pulse));
   pulse pulse_latch(.clk(clk), .in(latch_scalers_in), .out(latch_scalers_pulse1));

   // scalers latch counter

   reg [31:0] latch_scalers_counter;
   reg        latch_scalers_pulse2;

   always_ff @ (posedge clk) begin
      latch_scalers_pulse2 <= 0;
      if (latch_scalers_period_in == 0) begin
         latch_scalers_counter <= 0;
      end else if (latch_scalers_counter == latch_scalers_period_in) begin
         latch_scalers_pulse2 <= 1;
         latch_scalers_counter <= 0;
      end else begin
         latch_scalers_counter <= latch_scalers_counter + 1;
      end
   end // always_ff @ (posedge clk)

   wire latch_scalers_pulse = latch_scalers_pulse1 || latch_scalers_pulse2;

   //
   // Synchronize the inputs
   //

   reg [N-1:0] inputs_sync;
   reg [N-1:0] inputs_sync_clk_ts;

   genvar      i;

   generate
      for (i=0; i<N; i=i+1) begin: sync_loop
         sync_fast_to_slow_clk sync_clk(.clk(clk), .in(inputs_async[i]), .out(inputs_sync[i]));
         sync_fast_to_slow_clk sync_clk_ts(.clk(clk_ts), .in(inputs_sync[i]), .out(inputs_sync_clk_ts[i]));
      end
   endgenerate

   //
   // Counters for the inputs
   //

   reg [31:0] scalers_latched[N+1];
   reg [31:0] scalers[N+1];

   // counter for the inputs

   generate
      for (i=0; i<N; i=i+1) begin: scalers_loop

	 counter counter(.clk(clk), .reset(sync_reset | zero_scalers_pulse), .in(inputs_sync[i]), .out(scalers[i]));
	    
	 always_ff @ (posedge clk or posedge sync_reset) begin
	    if (sync_reset) begin
	       scalers_latched[i] <= 32'b0;
	    end
	    else begin
	       if (latch_scalers_pulse) begin
		  scalers_latched[i] <= scalers[i];
	       end
	    end
	 end
      end
   endgenerate

   // counter for the clock

   always_ff @ (posedge clk or posedge sync_reset) begin
      if (sync_reset) begin
	 scalers[N] <= 32'b0;
	 scalers_latched[N] <= 32'b0;
      end else begin
	 if (zero_scalers_pulse) begin
	    scalers[N] <= 0;
	 end else begin
	    scalers[N] <= scalers[N] + 1;
	 end
	 if (latch_scalers_pulse) begin
	    scalers_latched[N] <= scalers[N];
	 end
      end
   end // always_ff @ (posedge clk or posedge reset)

   // mux the latched counters into the slow readout bus

   always_ff @ (posedge clk) begin
      scaler_data_out <= scalers_latched[scaler_addr_in];
   end

   // timestamp counter

   reg [31:0] ts_counter;
   reg [23:0] ts;
   reg        ts_overflow_pulse;
   reg        ts_overflow1;
   reg        ts_overflow2;
   reg [22:0] ts_overflow_counter;
   reg [31:0] ts_overflow_data;

   always_ff @ (posedge clk_ts) begin
      if (sync_reset_clk_ts) begin
         ts_counter <= 0;
         ts <= 24'd0;
         ts_overflow1 <= 0;
         ts_overflow2 <= 0;
         ts_overflow_pulse   <= 0;
         ts_overflow_counter <= 23'h000001;
         ts_overflow_data    <= { 8'hFF, 1'h0, 23'h000000 };
      end else begin
         ts_counter   <= ts_counter + 1;
         ts[23:0]     <= ts_counter[23+S:0+S];
         ts_overflow1 <= ts_counter[23+S-1];
         ts_overflow2 <= ts_overflow1;
         ts_overflow_pulse <= (ts_overflow2==1) && (ts_overflow1==0); // 1 clock pulse on drop of MSB from 1 to 0
         //
         // this code is strange: ts_overflow_pulse is generated in the 1st clock
         // latched into 100 MHz tso_flag,
         // old ts_overflow_data is sent to the FIFO
         // on the next clock, ts_overflow_counter and ts_overflow_data is updated,
         // to be sent at the next overflow.
         // so ts_overflow_data is running "one overflow behind".
         //
         // most likely it is coded like this to ensure correct clock domain
         // transfer of ts_overflow_data.
         //
         if (ts_overflow_pulse) begin
            ts_overflow_counter <= ts_overflow_counter + 23'h1;
            ts_overflow_data <= { 8'hFF, ts[23], ts_overflow_counter[22:0] };
         end
      end
   end

   localparam TSC_N = N; // 4;
   localparam TSC_N1 = TSC_N-1;
   localparam TSC_NN = $clog2(TSC_N);
   localparam TSC_NN1 = TSC_NN-1;

   localparam SCL_N = N+1; // last scaler is the 100 MHz clock
   localparam SCL_N1 = SCL_N-1;
   localparam SCL_NN = $clog2(SCL_N);
   localparam SCL_NN1 = SCL_NN-1;

   reg [TSC_NN1:0]   tsc_select;
   reg [SCL_NN1:0]   scl_select;

   wire [TSC_N1:0]  tsc_rdacq;
   wire [31:0]      tsc_data[TSC_N1:0];
   wire [TSC_N1:0]  tsc_empty;
   wire [TSC_N1:0]  tsc_fifo_full;

   generate
      for (i=0; i<N; i=i+1) begin: tsc_loop
         cb_tsc1f cb_tsc_i
           (
            .clk(clk),
            .clk_ts(clk_ts),
            .reset_clk_ts(sync_reset_clk_ts),
            .ts(ts),
            .chan(i),
            .enable_le(enable_le[i]),
            .enable_te(enable_te[i]),
            .in(inputs_sync_clk_ts[i]),
            .fifo_rdacq(tsc_rdacq[i]),
            .fifo_data(tsc_data[i]),
            .fifo_empty(tsc_empty[i]),
            .fifo_full(tsc_fifo_full[i])
            );
      end
   endgenerate

   wire tsc_fifo_full_or = |(tsc_fifo_full);
   
   always_ff @ (posedge clk) begin
      if (sync_reset) begin
         tsc_fifo_full_out <= 0;
      end else begin
         tsc_fifo_full_out <= tsc_fifo_full_out | tsc_fifo_full_or;
      end
   end

   reg       scl_flag;
   reg       scl_header;

   wire      tso_pulse1;
   wire      tso_pulse;
   reg       tso_flag;
   reg [31:0] tso_data;

   sync_slow_to_fast_clk tso_sync_inst(.clk(clk), .in(ts_overflow_pulse), .out(tso_pulse1));
   pulse tso_pulse_inst(.clk(clk), .in(tso_pulse1), .out(tso_pulse));

   // TSC mux

   reg [31:0] tsc_mux_data;
   reg        tsc_mux_have;
   reg        tsc_mux_rdacq;
   
   always_ff @ (posedge clk) begin
      if (sync_reset) begin
         tsc_mux_data <= 0;
         tsc_mux_have <= 0;
         tsc_rdacq    <= {TSC_N{1'b0}};
         tsc_select   <= {TSC_NN{1'b0}};
      end else if (tsc_mux_have) begin
         tsc_rdacq[tsc_select] <= 0;
         if (tsc_mux_rdacq) begin
            tsc_mux_have <= 0;
         end
      end else if (!tsc_empty[tsc_select]) begin
         tsc_mux_data <= tsc_data[tsc_select];
         tsc_mux_have <= 1;
         tsc_rdacq[tsc_select] <= 1;
      end else begin
         tsc_mux_data <= 0;
         tsc_mux_have <= 0;
         if (tsc_select < TSC_N1) begin
            tsc_select <= tsc_select + 1;
         end else begin
            tsc_select <= 0;
         end
      end
   end // always_ff @ (posedge clk)

   reg [3:0] reset_delay;

   always_ff @ (posedge clk) begin
      if (sync_reset) begin
         fifo_xout <= 0;
         fifo_ready_out <= 0;
         reset_delay <= 4'd15;
      end else if (reset_delay == 4'd2) begin
         reset_delay <= reset_delay - 4'd1;
         fifo_xout <= 32'hFD000000;
         fifo_ready_out <= 1;
      end else if (reset_delay == 4'd1) begin
         reset_delay <= reset_delay - 4'd1;
         fifo_xout <= 32'h00000000;
         fifo_ready_out <= 0;
      end else if (reset_delay == 4'd0) begin
      
         if (tso_pulse) begin
            tso_flag <= 1;
            tso_data <= ts_overflow_data;
         end
         
         if (latch_scalers_pulse && !scl_flag) begin
            scl_flag <= 1;
            scl_header <= 1;
            scl_select <= 0;
         end
         
         if (scl_flag) begin
            if (scl_header) begin
               scl_header <= 0;
               fifo_xout <= {8'hFE,8'h00,16'(SCL_N)};
               fifo_ready_out <= 1;
            end else begin
               //fifo_xout <= { 1'b0, scl_select[6:0] , scalers_latched[scl_select][23:0] };
               fifo_xout <= scalers_latched[scl_select];
               fifo_ready_out <= 1;
               if (scl_select < SCL_N1) begin
                  scl_select <= scl_select + 1;
               end else begin
                  scl_select <= 0;
                  scl_flag <= 0;
               end
            end
         end else if (tso_flag) begin
            fifo_xout <= tso_data;
            fifo_ready_out <= 1;
            tso_flag <= 0;
         end else if (tsc_mux_have) begin
            if (tsc_mux_rdacq) begin
               fifo_xout <= 0;
               fifo_ready_out <= 0;
               tsc_mux_rdacq <= 0;
            end else begin
               fifo_xout <= tsc_mux_data;
               fifo_ready_out <= 1;
               tsc_mux_rdacq <= 1;
            end
         end else begin
            fifo_xout <= 0;
            fifo_ready_out <= 0;
         end
      end else begin
         reset_delay <= reset_delay - 4'd1;
      end // else: !if(reset_delay == 0)
   end

endmodule
