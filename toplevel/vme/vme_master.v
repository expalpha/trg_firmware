// vme_a/lwordn should be inout for vme64,  vme_dsn also functions as addressing lines ?
// arbitration, priority interrupt, and utility bus could all go in separate controllers
//    also may need sysclk_out if in slot 1
module vme_master ( input wire clk,  input wire reset,  output wire  debug,  input wire enable,
   output reg  [31:1] vme_a,       output reg vme_lwordn,         output reg [5:0] vme_am,// dtb addr/data
   inout  wire [31:0] vme_d,       output reg vme_d_oe,
   output reg vme_asn,             output reg [1:0] vme_dsn,      input  wire vme_dtackn,  // dtb control
   output reg vme_writen,          input  wire vme_berrn,         // input  wire vme_retryn,
   //inout  wire vme_bbsyn,        inout  wire vme_bclrn,                                  // dtb arbitration
   //inout  wire [3:0] vme_brn,    inout  wire [3:0] vme_bginn,   inout  wire [3:0] vme_bgoutn,
   input  wire [7:1] vme_irqn,                                                           // priority interrupt bus
   output reg vme_iackn,           output reg  vme_iackoutn,      //input  wire vme_iackinn,
   //input  wire vme_sysclk,       input  wire vme_sysfailn,      input  wire vme_sysresetn,// utility bus
   //input  wire vme_acfailn,      inout  wire vme_sera,          inout  wire vme_serb,
	
	input  wire  [5:0] am_requested,
	input  wire [31:0] address_requested,  input  wire [31:0] data_requested,
	input  wire read_request,              input  wire write_request,
	output reg [31:0] readdata,            output reg readdatavalid,
   output reg write_ackn
	//output wire device_wants_bus, input  wire device_granted_bus
);
assign debug = cycle_count[31];
reg [31:0] vme_d_out;  assign vme_d = (vme_d_oe) ? vme_d_out : 32'bz;
wire [31:0] vme_d_in = vme_d;

reg [3:0] state;
parameter idle=0, data_setup=1, address_setup1=2, address_setup2=3, address_setup3=4, initiate_transfer=5,
    wait_for_slave_response=6, as_waitstate1=7, as_waitstate2=8, end_transfer=9, bus_error=10, illegal=11;

wire       interrupt_request        = !( &(vme_irqn[7:1]) ); // request <= nand(vme_irqn);
wire [2:0] interrupt_priority_level = 3'h7; // fix this!!!!! priority decode of vme_irqn.

reg ivme_dtackn, ivme_berrn;   // register asyncronous vme bus signals
always @ (posedge clk) begin ivme_dtackn <= vme_dtackn; ivme_berrn <= vme_berrn; end

reg vmebus_timeout; reg [9:0] timeout_counter; // BUS Timeout, ~10us at 100Mhz
always @ (posedge clk) begin
   vmebus_timeout <= (timeout_counter == 10'h3ff);
   if( state == idle )                  timeout_counter <= 10'b0;
   else if( timeout_counter < 10'h3ff ) timeout_counter <= timeout_counter + 10'h1;
   else                                 timeout_counter <= timeout_counter;
end

reg [31:0] cycle_count;
always @ (posedge clk or posedge reset) begin // logic to advance to the next state
   if( reset ) begin
      vme_asn   <= 1'h1;  vme_dsn      <= 2'h3;   vme_d_oe  <= 1'h0;  readdatavalid <= 1'b0;  cycle_count <= 32'h0;
      vme_iackn <= 1'h1;  vme_iackoutn <= 1'h1;   write_ackn <= 1'b0; // vme_writen <= 1'h0;
      state     <= idle;  vme_d_out    <= 32'hffffffff;      // addr/lword, am, [d gated by oen]
   end else begin
      vme_asn   <= vme_asn;    vme_dsn      <= vme_dsn;      vme_d_oe   <= vme_d_oe;    write_ackn <= write_ackn;
      vme_iackn <= vme_iackn;  vme_iackoutn <= vme_iackoutn; vme_writen <= vme_writen;  cycle_count <= cycle_count;
      state     <= state;      vme_d_out    <= vme_d_out;    readdatavalid <= readdatavalid;
      case( state )
      idle: begin
            vme_asn           <= 1'h1;  vme_dsn         <= 2'h3;   vme_d_oe <= 1'h0;
            vme_iackn         <= 1'h1;  vme_iackoutn    <= 1'h1;   // vme_writen <= 1'h0;
            state <= idle;
            if( interrupt_request && enable ) begin
               vme_iackn  <= 1'h0;  vme_iackoutn <= 1'h0;
               vme_a[3:1] <= interrupt_priority_level;
            end else if( read_request && enable ) begin // read has priority over write
	            vme_am   <= am_requested;   vme_a      <= address_requested[31:1];
               vme_d_oe <= 1'h0;           vme_lwordn <= 1'b0; // no unaligned transfers
               state    <= address_setup1; vme_asn <= 1'h0;
					vme_writen <= 1'h1;         readdatavalid <= 1'b0;
            end else if( write_request && enable ) begin // do not drive data/ds yet
	            vme_am     <= am_requested;  vme_a      <= address_requested[31:1];
               vme_writen <= 1'h0;          vme_lwordn <= 1'b0; // no unaligned transfers
               state      <= data_setup;    vme_asn <= 1'h0;  readdatavalid <= 1'b0;
					cycle_count <= cycle_count + 32'h1;
            end
         end
      data_setup: // master must ensure dtackn and berr are high before driving ds1n, ds0n, or data lines
         if( (ivme_dtackn == 1'b1) && (ivme_berrn == 1'b1) ) begin
            vme_d_out <= data_requested[31:0];  vme_d_oe <= 1'h1;
            state     <= address_setup2;
         end else if( vmebus_timeout == 1'b1 ) state <= bus_error;

      address_setup1: state <= address_setup2; // wait a while for addr/am/writen to propogate
      address_setup2: state <= address_setup3; // should have dropped asn as well? yes
      address_setup3: state <= initiate_transfer; 

      initiate_transfer: begin // dtackn/berr must be high before driving data/ds
            if( (ivme_dtackn == 1'b1) && (ivme_berrn == 1'b1) ) begin
               //vme_dsn[1] <= (avl_byteenable[3:0] == 4'h8) || (avl_byteenable[3:0] == 4'h2);
               //vme_dsn[0] <= (avl_byteenable[3:0] == 4'h4) || (avl_byteenable[3:0] == 4'h1);
               vme_dsn <= 2'h0;
               state   <= wait_for_slave_response;
            end else if( vmebus_timeout == 1'b1 ) state <= bus_error;
         end

      wait_for_slave_response:
         if( ivme_dtackn == 1'b0 ) begin
            vme_asn <= 1'h1; vme_dsn <= 2'h3;
            if( read_request == 1'b1 ) begin
                readdata <= vme_d_in;  readdatavalid <= 1'b1; 
            end
            write_ackn <= 1'h0;
            state <= as_waitstate1;
         end else if( vmebus_timeout == 1'b1 ) state <= bus_error;

      as_waitstate1: begin // asn must be high for 40ns before it can be driven low again.
            write_ackn <= 1'h1;
            state      <= as_waitstate2;
			end
      as_waitstate2: state <= end_transfer;
      end_transfer:  state <= idle;
      bus_error:     begin vme_asn <= 1'h1;  vme_dsn <= 2'h3;  state <= idle; end
      illegal:       state <= idle;
      default:       state <= illegal;
   endcase
   end
end

endmodule
