module initialise (
   input  wire clk,                  input  wire reconfig_busy,
	input  wire pll_locked,           input  wire [3:0] rx_freqlocked,
   output reg pll_powerdown,         output reg [3:0] rx_digitalreset,
   output reg [3:0] tx_digitalreset, output reg [3:0]  rx_analogreset
);

// extra section to trigger a reset on powerup [1us min width]
reg [23:0] pwr_cnt;
reg poweron_reset;
always @ (posedge clk) begin
   if( pwr_cnt < 24'h00FFFF ) pwr_cnt <= pwr_cnt + 1'b1;
	else pwr_cnt <= pwr_cnt;
	
	if( pwr_cnt > 24'h001000 && pwr_cnt <= 24'h001020 ) poweron_reset <= 1;
	else poweron_reset <= 0;
   pll_powerdown <= poweron_reset;
end
///////////////////////////////////////////////
reg [9:0] cnt; reg rx_started;
always @ (posedge clk) begin
   if( ! pll_locked ) begin
	   cnt <= 8'b0;
		rx_started      <= 0;    tx_digitalreset <= 4'hF;
	   rx_digitalreset <= 4'hF; rx_analogreset  <= 4'hF;
   end
	else if ( reconfig_busy ) begin // pll is locked
	   cnt <= 8'b0;
	   rx_started      <= 0;    tx_digitalreset <= 4'h0; 
	   rx_digitalreset <= 4'hF; rx_analogreset  <= 4'hF;
   end
	else if( ! rx_started ) begin  // pll is locked and not busy
	   if( cnt < 10'h3FF ) cnt <= cnt + 1'b1; else cnt <= cnt;
	   if ((cnt > 2) ) begin // min 2 parallel clock cycles
	      rx_started      <= 1;    tx_digitalreset <= 4'h0; 
	      rx_analogreset  <= 4'h0; rx_digitalreset <= 4'hF;
	   end
   end
  	else begin    // rx has started - 4us after freqlckd
	  if( cnt < 10'h3FF ) cnt <= cnt + 1'b1; else cnt <= cnt;
	  rx_started      <= 1;    tx_digitalreset <= 4'h0; 
	  rx_analogreset  <= 4'h0; rx_digitalreset <= ~rx_freqlocked;

	end
end

endmodule
