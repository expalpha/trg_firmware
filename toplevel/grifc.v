`default_nettype none
// Filter duplicates sas buffers - almost direct 1:1 copy for unfiltered data
//    should make filter input spy on unfiltered dcc, then don't need second copy
//       filter only currently pauses if unfilteredbuf is full (=> in use)
// - in diagram below, first dcc -> direct to unfilter-buf
//   [if not active disable write, but dcc still runs as normal]
//   filter grabs results of dcc-out and sends through filter
//   ** except filter inputs supposed to run in parallel, so can't just spy on single dcc
//      ** provided events ~16 clks long, can just spy on link-output and ignore buffers
//         with 16 ports, can take 16clks or so to acknowledge pkt_rdy - so need events
//         separated by this many clks - could use ddrclk -> 8 words min-eventsize
//** ** whatever min-event-gap is, enforce this in link output **
//      - could have small pkt_ptr buf to handle short burst
//        - could use filter-busy signal to pause links (per link or global)
// 
// resources ... Total:7.5mbit   Filter:3.6Mbit    Gx:2.4Mbit    Sigtap:1.2Mbit    rest:0.3
//    [signaltap 136 * m9k]
//    config rom - 16k x 16bits = 32kbytes = 256kbits = 2*m144k [~17.5k used]               256kbit
//    gxlinks 16*m144k and 22*m9k = 2.4Mbits
//       each 4kx32 buffer is 128kbits and gives 30us of transfer and 80us of waveform
//          16 sas rx => 16 of these [m144k]                                   16 * 128kbit = 2Mbit
//       *** phase-match-fifos are too deep (only need mram 9bitsx4words = 36each)
//          currently 256*10bits -> m9k per link tx and rx = 32 (compiler somehow reduces to 22)
//       sfp has 2kx32bit databuf = 8kbytes = 10*m9k
//    toplevel has 1 buf [128k] = 16*m9k                                                    128kbit
//    filter has 357*m9k and 4*m144k = 3.6Mbits
//       outfifo  - 1kevent x 512bits! =  64xM9k                                            512kbit
//       dcc output buf - 1 buf [128k] = 16*m9k
//       cdb 4 channels of (a) unfilt-buf - std 128kbit 16*m9k                4 * 128kbit = 512kbit
//                         (b) packetbuf  - 1kevent * 512bits                 4 * 512kbit =   2Mbit
//       timeslotlist   4k slot x 56bit  - 26*m9k (12bit start/fin slots stored below)      256kbit
//       timeslotmem   16k slot x 32bits - 3*m144k and 1*m9k                                512kbit
//       multiwin - pair of 36 * 256bits - 2*m9k    18kbits
// ...
// timeslotmem needs 36/40bits as 18/20 not enough -> requires 0.75Mbits
// filter packet buffers also need quite a lot
// - reduce back from 512bit (way too much) store as compactly as possible
// - currently header[28], timestamp[28+14], deadtime[14] Trigs[28,14], E[28] T[28] = 6.5*28 = 182
//   also extra 2*28 for descant = 238 total - use 256bits and do not exceed!
//   -> for full 16 links into filter is 4Mbits or 2Mbits for 512 event bufsize
// **** how to tell if losing events due to cdb overrun?
// 
// 
// 
// ------------------------------------------------------------------------------------------------
// 
// 
// 
// -------------------------------------------------------------------------------------------------|
// sas data input rates ~5clk/entry       |<-------------  Filter  ------------->|                  |
//                    ________             ____________/-------------<---------\                    |
// Sas0 -> scfifo ---|   RdX  |--scfifo->A| Rd  cdbuf0 |\  ___   __   __   ____ \          Mst  PC  |
// Sas1 -> scfifo ---|TopLevel|          B| Rd  cdbuf1 |-\|Tsm|_|Ms|_|Ps|_|Mult|_|         Sas/Sfp  |
// Sas2 -> scfifo ---|   dcc  |          C| Rd  cdbuf2 |-/|___| |__| |__| |____|             ---    |
// Sas3 -> scfifo ---|________|          D| Rd  cdbuf3 |/ <- pkt pointers only ->        -->|Rd |-->|
//                                        | 4xScfifo | |                                /    ---    |
//                                         ---|------|-              ______            /            |
//       Rd => read-fifo                      |      |-> filtfifo ->|Filter|          /             |
//        X => not streaming                  |-4x-UnFiltered data->|Rd dcc|->scfifo-/              |
// 1235*M9k 22*M144k 4560*Mlab                                      |-X----|                        |
// -------------------------------------------------------------------------------------------------|
module grifc(
	CLKIN_50,    CLKIN_16,    CLKIN_DDR_50,         /* Clks from 3 oscillators */
	OSC_SCL,     OSC_SDA,               /* Smb [pulled up by 4.7k] for Var Osc */
	CLKIN_VAR_T, CLKIN_VAR_B, CLKIN_VAR_R,  CLKIN_VAR_L, /* fan out from above */

	CFG_RESET,  CFG_SCLK,    CFG_SEL,    CFG_REQUEST,
	CFG_D,      CFG_DIN,     CFG_DOUT,  
	CPU_RESETn,  USER_LED, USER_DIP,
	oct_rdn,     oct_rup,  

	PM_VADJ_PG, PM_VADJ_EN,  PM_DDR3_PG,  PM_DDR3_EN,
	PM_0V9_PG,  PM_1V1_PG,   PM_1V2_PG,   PM_1V5_PG,
	PM_1V8_PG,  PM_2V5_PG,   PM_3V3_PG, 
	PM_PMB_SCL, PM_PMB_SDA,           /* Smb to Mic280 Thermal Monitor -> FPGA */
   FMC_SCL,    FMC_SDA,                  /* Smb [pulled up by 4.7k] to 3 FMCs */

        PM_M2C_PG,
        PM_C2M_PG,
	
	FMC0_MCLK, FMC0_CCLK, FMC0_M2C, FMC0_C2M, /* FMC I/O: Cclk/C2m -> Mez, Mclk, m2c -> Carrier */
	FMC1_MCLK, FMC1_CCLK, FMC1_M2C, FMC1_C2M, /* FMC I/O: Cclk/C2m -> Mez, Mclk, m2c -> Carrier */
	FMC2_MCLK, FMC2_CCLK, FMC2_M2C, FMC2_C2M, /* FMC I/O: Cclk/C2m -> Mez, Mclk, m2c -> Carrier */

	FMC0_RX,   FMC0_TX,   /*FMC0_GBTCLK,*/    /* comment unused rx/tx/gbtclk(Schematic: F0_REFCLK_0) */
	FMC1_RX,   FMC1_TX,   /*FMC1_GBTCLK,*/    /* comment unused rx/tx/gbtclk(                      ) */
	FMC2_RX,   FMC2_TX,   /*FMC2_GBTCLK,*/    /* comment unused rx/tx/gbtclk(                      ) */
   // no location ... FMC0_PG_C2M,    FMC0_PG_M2C,

	FMC0_CFG_PROGn, FMC0_CFG_CCLK, FMC0_CFG_DIN, FMC0_CFG_INITn, FMC0_CFG_DONE, FMC0_PRESENT,
	FMC1_CFG_PROGn, FMC1_CFG_CCLK, FMC1_CFG_DIN, FMC1_CFG_INITn, FMC1_CFG_DONE, FMC1_PRESENT,
	FMC2_CFG_PROGn, FMC2_CFG_CCLK, FMC2_CFG_DIN, FMC2_CFG_INITn, FMC2_CFG_DONE, FMC2_PRESENT,

             // boot flash memory
             FLASH_A, // [26:0]
             FLASH_BUSYn,
             FLASH_BYTEn,
             FLASH_CE0n,
             FLASH_CE1n,
             FLASH_D, // [15:0]
             FLASH_OEn,
             FLASH_RESETn,
             FLASH_WEn,
             FLASH_WPn,

   // VME interface uses TI "8-bit + 2*1-bit" driver (transp/sync/latch mode)
	// external outputs ~50mA, internal outputs (always enabled)(->FPGA)12mA
	// A/D always enabled ?? Still have direction control 
	//  - DIR and'd with OEn for output (must be left as "in" to not drive outputs)
	// have 8 * 8-bit drivers for 64bits Addr+Data, extra 16 1-bits are ...
   //    Berr/Retry ASn/IACKn -/SYSCLK BR1/BR0  WRITEn/DTACKn ACFAIL/SYSFAIL -/- BR2/BR3
	// have 3 more 8-bits for BGout IackOut AM, with RESP/SYSREST BBSY/BCLR DS0/DS1
	// also have 1 extra different driver for IRQio/GAXn (VME_ENABLEn controls this?)
   VME_BGOUTn,    VME_BGINn,                                    /* ** Must be Tied together ** */
	VME_ALEn, VME_DLEn, VME_ARCK, VME_AWCK, VME_DRCK, VME_DWCK,  /* High for Async mode */
	VME_MASTER,      VME_ARBITER,   VME_IACKOE,   VME_SYSCTRL,   /* Low for Slave */
	VME_MASTERn,     VME_ENABLEn,   VME_DTACKOE,                 /* High for Slave */
	VME_SYSFAIL_out, VME_BERR_out,  VME_BBSY_out, VME_PBRESET,   /* Low for Slave (no separate signal: gnd/vcc used) */
 	VME_BR_out,                                                  /*  "                                               */
	
	VME_ASn_out,   VME_DS1n_out,  VME_DS0n_out,  VME_WRITEn_out, /* disabled in Slave Mode, Master=1 connects these 4 to bus */
	VME_RETRYn_in, VME_DTACKn_in, VME_IACKn_in,  VME_IACKINn,    /* disabled in Slave Mode */
	VME_RESPn_out, VME_RESPn_in,  VME_IACKn_out, VME_IACKOUTn,   /* disabled in Slave Mode, MasterN=1 cpmmects Respn_out to bus */
	VME_BCLRn_out,                                               /* disabled in Slave Mode */
	VME_SYSCLK_in, VME_SYSFAILn,  VME_BERRn_in,  VME_BBSYn_in,   /* monitor only - can be ignored */
	VME_SYSRESETn, VME_ACFAILn,   VME_BRn_in,    VME_BCLRn_in,   /* monitor only - can be ignored */
	                             /* SYSCLK_16M -> VME comes From Osc, also goes to Fpga as CLK16M */
	VME_IRQn_in,   VME_IRQn_out,  VME_GAn,       VME_GAPn,       /* Interrupt/Geographical Address - ignore */

	/* Vme Slave Signals */
	VME_ASn_in,     VME_DS1n_in,     VME_DS0n_in,  VME_WRITEn_in,
	VME_AOE,        VME_A,           VME_LWORDn,                 // AOE and DOE set direction of Addr,Data (Both Always enabled)
	VME_DOE,        VME_D,           VME_AM,                     // Master sets Direction of AM
	VME_DTACKn_out, VME_RETRYn_out
);

   input  wire	CLKIN_50,        CLKIN_16,      CLKIN_DDR_50, OSC_SCL;
   input  wire	CLKIN_VAR_T,     CLKIN_VAR_B,   CLKIN_VAR_R,  CLKIN_VAR_L;
   input  wire	CPU_RESETn,      CFG_DOUT,      oct_rdn,      oct_rup;
   output wire	CFG_RESET,       CFG_SCLK,      CFG_DIN,      CFG_REQUEST;
   inout  wire	PM_PMB_SDA,      OSC_SDA,       FMC_SDA;
   output wire  FMC_SCL = 0;
   input  wire	PM_0V9_PG,       PM_1V1_PG,     PM_1V2_PG,    PM_1V5_PG;
   input  wire	PM_1V8_PG,       PM_2V5_PG,     PM_3V3_PG,    PM_DDR3_PG;
   output wire	PM_PMB_SCL = 0,  PM_DDR3_EN = 0;
   input  wire [2:0] PM_M2C_PG;
   input  wire [2:0] PM_C2M_PG;
   input  wire	FMC0_CFG_INITn,  FMC0_CFG_DONE, /* FMC0_PG_M2C, */ FMC0_PRESENT;
   output wire	FMC0_CFG_PROGn,  FMC0_CFG_CCLK, FMC0_CFG_DIN; /* FMC0_PG_C2M, */
   input  wire	FMC1_CFG_INITn,  FMC1_CFG_DONE, /* FMC1_PG_M2C, */ FMC1_PRESENT;
   output wire	FMC1_CFG_PROGn,  FMC1_CFG_CCLK, FMC1_CFG_DIN; /* FMC1_PG_C2M, */
   input  wire	FMC2_CFG_INITn,  FMC2_CFG_DONE, /* FMC2_PG_M2C, */ FMC2_PRESENT;
   output wire	FMC2_CFG_PROGn,  FMC2_CFG_CCLK, FMC2_CFG_DIN; /* FMC2_PG_C2M, */

   assign FMC0_CFG_PROGn = 1'b1;
   assign FMC0_CFG_CCLK  = 1'b0;
   assign FMC0_CFG_DIN   = 1'b0;

   assign FMC2_CFG_PROGn = 1'b1;
   assign FMC2_CFG_CCLK  = 1'b0;
   assign FMC2_CFG_DIN   = 1'b0;

   assign CFG_RESET   = 1'b0;
   assign CFG_SCLK    = 1'b0;
   assign CFG_REQUEST = grifc_reconfig; // requires trg_maxv_firmware tag maxv_29Aug21_KO
   assign CFG_DIN     = 1'b0;

   output wire [8:0] USER_LED;
   input wire [7:0]  USER_DIP;
   input  wire [2:0] PM_VADJ_PG;
   output wire [2:0] PM_VADJ_EN = 3'b000;

   input  wire [7:0] CFG_D;
   input wire [2:0]  CFG_SEL;

   //input wire	[1:0] FMC0_GBTCLK;                        /* comment unused rx/tx/gbtclk */
   input wire [7:0]   FMC0_RX;   output wire	[7:0] FMC0_TX; /* comment unused rx/tx/gbtclk */
   input wire [7:0]   FMC1_RX;   output wire	[7:0] FMC1_TX; /* comment unused rx/tx/gbtclk */
   input wire [7:0]   FMC2_RX;   output wire	[7:0] FMC2_TX; /* comment unused rx/tx/gbtclk */
   
   input  wire [23:0] FMC0_M2C;
   output wire [15:0] FMC0_C2M  = 16'd0;
   input wire [1:0]   FMC0_MCLK;
   output wire [1:0]  FMC0_CCLK = 2'b00;
   
   input  wire [23:0] FMC1_M2C;
   output wire [15:0] FMC1_C2M;
   input wire [1:0]   FMC1_MCLK;
   output wire [1:0]  FMC1_CCLK = 2'b00;

   assign             FMC1_C2M[3:0]  = 4'b0000;
   assign	      FMC1_C2M[4]    = esata_run_out;
   assign             FMC1_C2M[7:5]  = 3'b000;
   assign             FMC1_C2M[15:8] = 8'b00000000;
   
   input  wire [23:0] FMC2_M2C;
   output wire [15:0] FMC2_C2M  = 16'd0;
   input wire [1:0]   FMC2_MCLK;
   output wire [1:0]  FMC2_CCLK = 2'b00;

   // boot flash memory

   inout wire [26:0]  FLASH_A;
   inout wire         FLASH_BUSYn;
   inout wire         FLASH_BYTEn;
   inout wire         FLASH_CE0n;
   inout wire         FLASH_CE1n;
   inout wire [15:0]  FLASH_D;
   inout wire         FLASH_OEn;
   inout wire         FLASH_RESETn;
   inout wire         FLASH_WEn;
   inout wire         FLASH_WPn;

   /////////////////////////////////////         VME             ///////////////////////////////////////////

   output wire 	      VME_ALEn = 1;
   output wire 	      VME_DLEn = 1;
   output wire 	      VME_AOE  = 0;
   output wire 	      VME_DOE  = 0;
   output wire 	      VME_ARCK = 0;
   output wire 	      VME_AWCK = 0;
   output wire 	      VME_DRCK = 0;
   output wire 	      VME_DWCK = 0;
   output wire 	      VME_ARBITER = 0;
   output wire 	      VME_IACKOE  = 0;
   output wire 	      VME_SYSCTRL = 0;
   output wire 	      VME_MASTER  = 0;
   output wire 	      VME_MASTERn = 1;
   output wire 	      VME_ENABLEn = 1;
   output wire 	      VME_DTACKOE = 0;
   output wire 	      VME_BCLRn_out = 1;
   output wire 	      VME_BERR_out  = 0;
   output wire 	      VME_BBSY_out  = 0;
   output wire 	      VME_PBRESET   = 0;
   output wire 	      VME_RESPn_out = 1;
   output wire 	      VME_DS1n_out  = 1;
   output wire 	      VME_DS0n_out  = 1;
   output wire 	      VME_WRITEn_out = 1;
   output wire 	      VME_ASn_out = 1;
   input  wire 	      VME_RETRYn_in, VME_DTACKn_in, VME_IACKn_in,   VME_IACKINn;
   input  wire 	      VME_SYSCLK_in, VME_SYSFAILn,  VME_BERRn_in,   VME_BBSYn_in;
   input  wire 	      VME_SYSRESETn, VME_ACFAILn,   VME_BCLRn_in,   VME_GAPn,       VME_RESPn_in;
   input  wire 	      VME_ASn_in,    VME_DS1n_in,   VME_DS0n_in,    VME_WRITEn_in;
   output wire 	      VME_IACKn_out  = 1;
   output wire 	      VME_IACKOUTn   = 1;
   output wire 	      VME_DTACKn_out = 1;
   output wire 	      VME_RETRYn_out = 1;
   output wire 	      VME_SYSFAIL_out = 0;
   input  wire [3:0]  VME_BGINn;
   output wire [3:0]  VME_BGOUTn;
   input  wire [3:0]  VME_BRn_in;
   output wire [3:0]  VME_BR_out = 0;
   input  wire [7:1]  VME_IRQn_in;
   output wire [7:1]  VME_IRQn_out = 1;
   inout  wire [31:1] VME_A;
   inout  wire [31:0] VME_D;
   inout wire 	      VME_LWORDn;
   inout  wire [5:0]  VME_AM;
   input  wire [4:0]  VME_GAn;
   
   assign VME_BGOUTn   = VME_BGINn; /* tie together if vme-bus-request not implemented */
   assign VME_ALEn     =        1;
   assign VME_DLEn     =       1; // these are the only controllable LEn - all others tied high
   assign VME_MASTER   = 0;
   assign VME_MASTERn  = 1;
   assign VME_DTACKOE = 0;
   assign VME_AOE    = 0;
   assign VME_ARBITER  =        0;
   assign VME_ENABLEn  =       1;
   assign VME_IACKOE  = 0;
   assign VME_SYSCTRL = 0;
   assign VME_BERR_out =        0;
   assign VME_BBSY_out =       0;
   assign VME_SYSFAIL_out = 0;

   wire xxx_vme_a = |VME_A | VME_LWORDn;
   wire xxx_vme_am = |VME_AM;
   wire xxx_vme_d = |VME_D;
   wire xxx_vme = xxx_vme_a | xxx_vme_am | xxx_vme_d;
   
   ///////////////////////////////////////////  PLL/Reset ///////////////////////////////////////////////////
   wire 	       RSTn;
   wire 	       sys_locked;
   wire 	       io_clk /* synthesis keep */ ;
   wire 	       io_ddrclk;
   wire 	       io_clk2;
   wire 	       run_clk;
   wire 	       sys_clk;
   //wire 	       sys_clk2;
   //wire sys_clk0;   // sys:50 io:125 ppg:1 run:100                    
   wire 	       clk_625 /* synthesis keep */ ;
   wire [31:0] 	       poweron_time;
   //wire 	       esata_clk /* synthesis keep */ = FMC1_M2C[8]; // SPARTAN fw with eSATA set to input-only
   wire 	       esata_clk /* synthesis keep */ = FMC1_MCLK[0];
   wire 	       esata_clk2 /* synthesis keep */ = FMC1_MCLK[1];
   //wire 	       esata_run /* synthesis keep */ = FMC1_M2C[7]; // SPARTAN fw with eSATA set to input-only
   
   //pll_reset pll_rst0 ( .inclk1(esata_clk), .clksel(1'b0), .filter_clk(run_clk), .ppg_clk(ppg_clk), .autoreset( 1'b0 ) ); // sys_clk0 for pll1
   
   pll_reset pll_rst1
     (
      .inclk1(CLKIN_50),
      .clksel(1'b0),
      .RSTn(RSTn),
      .clk_625(clk_625),
      .sys_clk(sys_clk),
      .io_clk(io_clk),
      .io_ddrclk(io_ddrclk),
      .poweron_time(poweron_time),
      .reset_req( 1'b0 ),
      .autoreset( 1'b0 )
      );

   pll_reset pll_rst2
     (
      .inclk1(CLKIN_50),
      .clksel(1'b0),
      //.sys_clk(sys_clk2),
      .io_clk(io_clk2),
      .autoreset( 1'b0 )
      ); // sys_clk2 for gx block3

   //
   // Load Xilinx FPGA, etc
   //

   wire 	       init_debug;
   //wire [47:0]         mac_addr;

   module_init init
     (
      .clk(io_clk),
      .reset(!RSTn_io_clk),
      .present(FMC1_PRESENT),
      .debug(init_debug),
      .progn(FMC1_CFG_PROGn),
      .cclk(FMC1_CFG_CCLK),
      .din(FMC1_CFG_DIN),
      .initn(FMC1_CFG_INITn),
      .done(FMC1_CFG_DONE)
      //.mac_clk(FMC1_C2M[0]),
      //.mac_sdi(FMC1_M2C[0]),
      //.mac_sdo(FMC1_C2M[1]),
      //.mac_oen(FMC1_C2M[2])
      //.mac_addr(mac_addr)
      );

   ///////////////////////////       Digitiser Data/Param interfaces       /////////////////////////////////////

   wire [63:0] 	       modpar_in;
   wire [63:0] 	       modpar_out;

   wire [31:0] 	 conf_nim_mask;
   wire [31:0] 	 conf_esata_mask;
   wire [31:0] 	 conf_adc16_masks[7:0];
   wire [31:0] 	 conf_adc32_masks[15:0];
   //wire [31:0] 	 conf_coinc_a;
   //wire [31:0] 	 conf_coinc_b;
   //wire [31:0] 	 conf_coinc_c;
   //wire [31:0] 	 conf_coinc_d;

   wire 	 ag_reset;
   wire 	 ag_latch;
   wire 	 ag_trigger;

   wire [15:0] 	 ag_sas_sd;
   wire [63:0] 	 ag_sas_bits[15:0];

   wire [31:0] 	 ag_sas_sd_counters[15:0];

   wire [31:0] 	 ag_ts_625;
   wire [31:0] 	 ag_counter_trig_out;
   wire [31:0] 	 ag_counter_trig_in;
   wire [31:0] 	 ag_counter_pulser;
   wire [31:0] 	 ag_counter_drift;
   wire [31:0] 	 ag_counter_scaledown;
   wire [31:0] 	 ag_counter_timeout;

   wire [31:0] 	 ag_counter_adc16_grand_or;
   wire [31:0] 	 ag_counter_adc32_grand_or;
   wire [31:0] 	 ag_counter_adc_grand_or;
   wire [31:0] 	 ag_counter_esata_nim_grand_or;

   //wire [31:0]   ag_counter_adc16_mult_1ormore;
   //wire [31:0] 	 ag_counter_adc16_mult_2ormore;
   //wire [31:0] 	 ag_counter_adc16_mult_3ormore;
   //wire [31:0] 	 ag_counter_adc16_mult_4ormore;

   wire [31:0] 	 ag_counter_adc16_or16[15:0];
   wire [31:0] 	 ag_counter_adc32_or16[31:0];

   wire [31:0] 	 ag_counter_adc_selected[15:0];

   //wire [31:0] 	 ag_counter_aw16_mult_1ormore;
   //wire [31:0] 	 ag_counter_aw16_mult_2ormore;
   //wire [31:0] 	 ag_counter_aw16_mult_3ormore;
   //wire [31:0] 	 ag_counter_aw16_mult_4ormore;

   //wire [31:0] 	 ag_counter_cc_a;
   //wire [31:0] 	 ag_counter_cc_b;
   //wire [31:0] 	 ag_counter_cc_c;
   //wire [31:0] 	 ag_counter_cc_d;
   //wire [31:0] 	 ag_counter_cc;

   wire [31:0] 	 ag_counter_aw16_grand_or;
   wire [31:0] 	 ag_counter_aw16_mlu_start;
   wire [31:0] 	 ag_counter_aw16_mlu_trig;

   wire [31:0] 	 ag_counter_bsc_grand_or;
   wire [31:0] 	 ag_counter_bsc_mult;
   wire [31:0] 	 ag_counter_bsc_mult_start;
   wire [31:0] 	 ag_counter_bsc_mult_trig;
   wire [31:0] 	 ag_counter_bsc64[63:0];

   wire [31:0] 	 ag_counter_coinc_start;
   wire [31:0] 	 ag_counter_coinc_trig;

   wire [31:0] 	 ag_pll_625_status;
   wire [31:0] 	 ag_clk_counter;
   wire [31:0] 	 ag_clk_625_counter;
   wire [31:0] 	 ag_esata_clk_counter;
   wire [31:0] 	 ag_esata_clk_esata_counter;
   wire [31:0] 	 ag_eth_clk_counter;
   wire [31:0] 	 ag_eth_clk_eth_counter;

   wire [31:0] 	 conf_control;
   wire [31:0] 	 conf_mlu;
   wire [31:0] 	 conf_scaledown;
   wire [31:0] 	 conf_drift_width;
   wire [31:0] 	 conf_trig_delay;
   wire [31:0] 	 conf_trig_width;
   wire [31:0] 	 conf_busy_width;
   wire [31:0] 	 conf_pulser_width;
   wire [31:0] 	 conf_pulser_period;
   wire [31:0] 	 conf_pulser_burst_ctrl;
   wire [31:0] 	 conf_trig_enable;
   wire [31:0] 	 conf_counter_adc_select;
   wire [31:0] 	 conf_bsc_control;
   wire [31:0] 	 conf_coinc_control;
   wire [31:0] 	 conf_trig_timeout;

   wire [31:0]   fifo_status;
   wire [31:0]   fifo_data;
   wire          fifo_rdack;

   wire [31:0]   cb_latch_period;

   wire [31:0]   cb_udp_period;
   wire [31:0]   cb_udp_threshold;

   reg 		 RSTn_io_clk_tmp;
   reg 		 RSTn_io_clk;
   
   always_ff@ (posedge io_clk) begin
      RSTn_io_clk_tmp <= RSTn;
      RSTn_io_clk <= RSTn_io_clk_tmp;
   end

   wire [31:0] compilation_time;
   timestamp last_compile(.data_out(compilation_time));

   wire        grifc_reconfig;

   param_io par
     (
      //.csr_in({8'h0}),
      .clk(io_clk),
      .reset(!RSTn_io_clk),
      .reconfig_out(grifc_reconfig),
      .param_in(modpar_in),
      .param_out(modpar_out), // param* must be syncd to clk (oneshot)
      //.csr(csr),
      //.num_sample(target_pkt_size),
      .fw_rev(compilation_time),
      .conf_control(conf_control),
      .conf_mlu(conf_mlu),
      .conf_drift_width(conf_drift_width),
      .conf_scaledown(conf_scaledown),
      .conf_trig_delay(conf_trig_delay),
      .conf_trig_width(conf_trig_width),
      .conf_busy_width(conf_busy_width),
      .conf_pulser_width(conf_pulser_width),
      .conf_pulser_period(conf_pulser_period),
      .conf_pulser_burst_ctrl(conf_pulser_burst_ctrl),
      .conf_trig_enable(conf_trig_enable),
      .conf_nim_mask(conf_nim_mask),
      .conf_esata_mask(conf_esata_mask),
      .conf_adc16_masks(conf_adc16_masks),
      .conf_adc32_masks(conf_adc32_masks),
      //.conf_coinc_a(conf_coinc_a),
      //.conf_coinc_b(conf_coinc_b),
      //.conf_coinc_c(conf_coinc_c),
      //.conf_coinc_d(conf_coinc_d),
      .conf_counter_adc_select(conf_counter_adc_select),
      .conf_bsc_control(conf_bsc_control),
      .conf_coinc_control(conf_coinc_control),
      .conf_trig_timeout(conf_trig_timeout),
      .reset_out(ag_reset),
      .latch_out(ag_latch),
      .trigger_out(ag_trigger),
      .sas_sd(ag_sas_sd),
      .sas_bits(ag_sas_bits),
      .sas_sd_counters(ag_sas_sd_counters),
      .ts_625(ag_ts_625),
      .counter_trig_out(ag_counter_trig_out),
      .counter_trig_in(ag_counter_trig_in),
      .counter_pulser(ag_counter_pulser),
      .counter_drift(ag_counter_drift),
      .counter_scaledown(ag_counter_scaledown),
      .counter_timeout(ag_counter_timeout),
      .counter_adc16_grand_or(ag_counter_adc16_grand_or),
      .counter_adc32_grand_or(ag_counter_adc32_grand_or),
      .counter_adc_grand_or(ag_counter_adc_grand_or),
      .counter_esata_nim_grand_or(ag_counter_esata_nim_grand_or),
      //.counter_adc16_mult_1ormore(ag_counter_adc16_mult_1ormore),
      //.counter_adc16_mult_2ormore(ag_counter_adc16_mult_2ormore),
      //.counter_adc16_mult_3ormore(ag_counter_adc16_mult_3ormore),
      //.counter_adc16_mult_4ormore(ag_counter_adc16_mult_4ormore),
      .counter_adc16_or16(ag_counter_adc16_or16),
      .counter_adc32_or16(ag_counter_adc32_or16),
      .counter_adc_selected(ag_counter_adc_selected),
      //.counter_aw16_mult_1ormore(ag_counter_aw16_mult_1ormore),
      //.counter_aw16_mult_2ormore(ag_counter_aw16_mult_2ormore),
      //.counter_aw16_mult_3ormore(ag_counter_aw16_mult_3ormore),
      //.counter_aw16_mult_4ormore(ag_counter_aw16_mult_4ormore),
      //.counter_cc_a(ag_counter_cc_a),
      //.counter_cc_b(ag_counter_cc_b),
      //.counter_cc_c(ag_counter_cc_c),
      //.counter_cc_d(ag_counter_cc_d),
      //.counter_cc(ag_counter_cc),
      .counter_aw16_grand_or(ag_counter_aw16_grand_or),
      .counter_aw16_mlu_start(ag_counter_aw16_mlu_start),
      .counter_aw16_mlu_trig(ag_counter_aw16_mlu_trig),
      .counter_bsc_grand_or(ag_counter_bsc_grand_or),
      .counter_bsc_mult(ag_counter_bsc_mult),
      .counter_bsc_mult_start(ag_counter_bsc_mult_start),
      .counter_bsc_mult_trig(ag_counter_bsc_mult_trig),
      .counter_bsc64(ag_counter_bsc64),
      .counter_coinc_start(ag_counter_coinc_start),
      .counter_coinc_trig(ag_counter_coinc_trig),
      .pll_625_status(ag_pll_625_status),
      .clk_counter(ag_clk_counter),
      .clk_625_counter(ag_clk_625_counter),
      .esata_clk_counter(ag_esata_clk_counter),
      .esata_clk_esata_counter(ag_esata_clk_esata_counter),
      .eth_clk_counter(ag_eth_clk_counter),
      .eth_clk_eth_counter(ag_eth_clk_eth_counter),
      .flash_control_out(flash_control),
      .flash_a_out(flash_a_out),
      .flash_d_out(flash_d_out),
      .flash_status(flash_status),
      .flash_a(flash_a_in),
      .cb_control_out(cb_control),
      .cb_status(cb_status),
      .cb_invert_a_out(cb_invert_a),
      .cb_invert_b_out(cb_invert_b),
      .cb_enable_le_a_out(cb_enable_le_a),
      .cb_enable_le_b_out(cb_enable_le_b),
      .cb_enable_te_a_out(cb_enable_te_a),
      .cb_enable_te_b_out(cb_enable_te_b),
      .cb_scaler_addr_out(cb_scaler_addr),
      .cb_scaler_data(cb_scaler_data),
      .cb_input_num(cb_input_num),
      .cb_sync_mask_a_out(cb_sync_mask_a),
      .cb_sync_mask_b_out(cb_sync_mask_b),
      .cb_sync_reg_out(cb_sync_reg),
      .cb_sync_status(cb_sync_status),
      .cb_fifo_status(fifo_status),
      .cb_fifo_data(fifo_data),
      .cb_fifo_rdack_out(fifo_rdack),
      .cb_udp_period_out(cb_udp_period),
      .cb_udp_threshold_out(cb_udp_threshold),
      .cb_latch_period_out(cb_latch_period)
      );

   //
   // trigger packet FIFO
   //

   wire [31:0] 	ag_udp_data;
   wire 	ag_udp_sop;
   wire 	ag_udp_eop;
   wire 	ag_udp_valid;
   wire 	ag_udp_ready;

   wire [31:0] 	xag_udp_data;
   wire 	xag_udp_sop;
   wire 	xag_udp_eop;
   wire 	xag_udp_valid;
   wire 	xag_udp_ready;

   wire         ag_fifo_empty;
   wire         ag_fifo_full;

   wire         ag_fifo_wrreq = ag_udp_valid & !ag_fifo_full;
   wire         ag_fifo_rdack = !ag_fifo_empty & xag_udp_ready;
   assign       ag_udp_ready = !ag_fifo_full;
   assign       xag_udp_valid = !ag_fifo_empty;

   eth_fifo	ag_fifo
     (
      .clock ( io_clk ),
      .sclr  ( 0 ),
      .data  ( { ag_udp_sop, ag_udp_eop, ag_udp_data } ),
      .rdreq ( ag_fifo_rdack ),
      .wrreq ( ag_fifo_wrreq ),
      .empty ( ag_fifo_empty ),
      .full  ( ag_fifo_full ),
      .q     ( { xag_udp_sop, xag_udp_eop, xag_udp_data } ),
      .usedw ( )
      );

   //
   // UDP packet multiplexor
   //

   wire [31:0] 	cb_udp_data;
   wire 	cb_udp_sop;
   wire 	cb_udp_eop;
   wire 	cb_udp_valid;
   wire 	cb_udp_ready;

   wire [31:0] 	udp_data;
   wire 	udp_sop;
   wire 	udp_eop;
   wire 	udp_valid;
   wire 	udp_ready;

   ethernet_packet_multiplexer udp_mux
     (
      .csi_clock_clk(io_clk),
      .csi_clock_reset_n(RSTn_io_clk),
      // Interface: in0
      .asi_in0_valid(xag_udp_valid),
      .asi_in0_ready(xag_udp_ready),
      .asi_in0_data(xag_udp_data),
      .asi_in0_startofpacket(xag_udp_sop),
      .asi_in0_endofpacket(xag_udp_eop),
      .asi_in0_empty(),
      // Interface: in1
      .asi_in1_valid(cb_udp_valid),
      .asi_in1_ready(cb_udp_ready),
      .asi_in1_data(cb_udp_data),
      .asi_in1_startofpacket(cb_udp_sop),
      .asi_in1_endofpacket(cb_udp_eop),
      .asi_in1_empty(),
      // Interface: out
      .aso_out_valid(udp_valid),
      .aso_out_ready(udp_ready),
      .aso_out_data(udp_data),
      .aso_out_startofpacket(udp_sop),
      .aso_out_endofpacket(udp_eop),
      .aso_out_empty()
      );

   wire 	eth_led;
   wire         eth_rx_clk;

   gx_links fmx_gx
     (
      //.RSTn( RSTn ),
      .RSTn_io_clk( RSTn_io_clk ),
      .board_id(USER_DIP),
      //.target_pkt_size(target_pkt_size[13:0]),
      .sys_clk(sys_clk),
      .io_clk(io_clk),
      .io_clk2(io_clk2),
      //.linkparreply(param_out),
      .linkparreply(modpar_out),
      //.linkpar(linkpar),
      .linkpar(modpar_in),
      //.filterdata_out(udp_data_out),
      //.filterdata_ready(udp_data_ready),
      //.filterdata_ack(udp_data_ack),
      .avalon_data(udp_data),
      .avalon_sop(udp_sop),
      .avalon_eop(udp_eop),
      .avalon_valid(udp_valid),
      .avalon_ready_out(udp_ready),
      //.FMCX_RX({dummy_FMC2_RX,FMC1_RX,dummy_FMC0_RX}),
      //.FMCX_TX({dummy_FMC2_TX,FMC1_TX,dummy_FMC0_TX})
      .FMCX_RX_block3(FMC1_RX[3:0]),
      .FMCX_TX_block3(FMC1_TX[3:0]),
      .FMCX_RX_block4(FMC1_RX[7:4]),
      .FMCX_TX_block4(FMC1_TX[7:4]),
      .eth_rx_clk_out(eth_rx_clk),
      .eth_led_out(eth_led)
      );

   // ALPHA-g connections

   //wire [31:0] rx_data_0;
   //wire [31:0] rx_data_1;
   //wire [31:0] rx_data_2;
   //wire [31:0] rx_data_3;

   wire [3:0]  rx_sd_0;
   wire [3:0]  rx_sd_1;
   wire [3:0]  rx_sd_2;
   wire [3:0]  rx_sd_3;

   wire [15:0][63:0] sas_bits;

   sas_links ag_sas
     (
      .reset(!RSTn),
      .sys_clk(sys_clk),
      .io_clk(io_clk),
      .io_clk2(io_clk2),

      //.FMCX_RX({FMC2_RX,dummy_FMC1_RX,FMC0_RX}),
      //.FMCX_TX({FMC2_TX,dummy_FMC1_TX,FMC0_TX}),

      .FMCX_RX_block1(FMC0_RX[3:0]),
      .FMCX_TX_block1(FMC0_TX[3:0]),

      .FMCX_RX_block2(FMC0_RX[7:4]),
      .FMCX_TX_block2(FMC0_TX[7:4]),

      .FMCX_RX_block5(FMC2_RX[3:0]),
      .FMCX_TX_block5(FMC2_TX[3:0]),

      .FMCX_RX_block6(FMC2_RX[7:4]),
      .FMCX_TX_block6(FMC2_TX[7:4]),
      
      //.rx_data_0(rx_data_0),
      //.rx_data_1(rx_data_1),
      //.rx_data_2(rx_data_2),
      //.rx_data_3(rx_data_3),
      
      .rx_sd_out_0(rx_sd_0),
      .rx_sd_out_1(rx_sd_1),
      .rx_sd_out_2(rx_sd_2),
      .rx_sd_out_3(rx_sd_3),
      
      .sas_bits(sas_bits)
      );

   wire              clk_ag_625;
   wire              clk_ag_10;

   wire              ag_trig_received_out;
   wire              ag_trig_drift_out;
   wire              ag_trig_scaledown_out;
   wire              ag_trig_out;
   wire              ag_busy_out;
   wire              ag_drift_out;

   wire              ag_cb_sw_trig;
   wire              ag_cb_pulser_trig;
   wire              ag_cb_esata_nim_trig;
   wire              ag_cb_adc16_grand_or_trig;
   wire              ag_cb_adc32_grand_or_trig;
   
   wire              ag_cb_aw16_grand_or_trig;
   //wire              ag_cb_aw16_mult_1ormore;
   //wire              ag_cb_aw16_mult_2ormore;
   //wire              ag_cb_aw16_mult_3ormore;
   //wire              ag_cb_aw16_mult_4ormore;
   wire              ag_cb_aw16_mlu_trig;
   
   wire              ag_cb_bsc64_grand_or_trig;
   //wire              ag_cb_bsc64_mult_1ormore;
   //wire              ag_cb_bsc64_mult_2ormore;
   //wire              ag_cb_bsc64_mult_3ormore;
   //wire              ag_cb_bsc64_mult_4ormore;
   wire              ag_cb_bsc64_mult_trig;
   
   wire              ag_cb_coinc_trig;
   wire              ag_cb_timeout_trig;

   alphag ag
     (
      .clk(io_clk),
      .clk_local_625(clk_625),

      .clk_625_out(clk_ag_625),
      .clk_10_out(clk_ag_10),

      //.esata_clk(esata_clk),
      .esata_clk(FMC1_MCLK[0]),
      //.esata_run(),

      .eth_clk(eth_rx_clk),
      
      .conf_fw_rev(compilation_time),
      .conf_control(conf_control),
      .conf_mlu(conf_mlu),
      .conf_drift_width(conf_drift_width),
      .conf_scaledown(conf_scaledown),
      .conf_trig_delay(conf_trig_delay),
      .conf_trig_width(conf_trig_width),
      .conf_busy_width(conf_busy_width),
      .conf_pulser_width(conf_pulser_width),
      .conf_pulser_period(conf_pulser_period),
      .conf_pulser_burst_ctrl(conf_pulser_burst_ctrl),
      .conf_trig_enable(conf_trig_enable),
      .conf_nim_mask(conf_nim_mask),
      .conf_esata_mask(conf_esata_mask),
      .conf_adc16_masks(conf_adc16_masks),
      .conf_adc32_masks(conf_adc32_masks),
      //.conf_aw16_coinc_a(conf_coinc_a),
      //.conf_aw16_coinc_b(conf_coinc_b),
      //.conf_aw16_coinc_c(conf_coinc_c),
      //.conf_aw16_coinc_d(conf_coinc_d),
      .conf_counter_adc_select(conf_counter_adc_select),
      .conf_bsc_control(conf_bsc_control),
      .conf_coinc_control(conf_coinc_control),
      .conf_trig_timeout(conf_trig_timeout),

      .reset(ag_reset),
      .latch(ag_latch),
      .sw_trigger(ag_trigger),
      
      //.rx_data_0(rx_data_0),
      //.rx_data_1(rx_data_1),
      //.rx_data_2(rx_data_2),
      //.rx_data_3(rx_data_3),
      
      .rx_sd_0(rx_sd_0),
      .rx_sd_1(rx_sd_1),
      .rx_sd_2(rx_sd_2),
      .rx_sd_3(rx_sd_3),
      
      .sas_bits(sas_bits),

      .pll_status_out(ag_pll_625_status),
      .clk_counter_out(ag_clk_counter),
      .clk_625_counter_out(ag_clk_625_counter),

      .esata_clk_counter_out(ag_esata_clk_counter),
      .esata_clk_esata_counter_out(ag_esata_clk_esata_counter),
      
      .eth_clk_counter_out(ag_eth_clk_counter),
      .eth_clk_eth_counter_out(ag_eth_clk_eth_counter),
      
      .udp_data_out(ag_udp_data),
      .udp_sop_out(ag_udp_sop),
      .udp_eop_out(ag_udp_eop),
      .udp_valid_out(ag_udp_valid),
      .udp_ready(ag_udp_ready),

      .sas_sd_out(ag_sas_sd),
      .sas_bits_out(ag_sas_bits),
      .sas_sd_counters_out(ag_sas_sd_counters),

      .ts_625_out(ag_ts_625),
      .counter_trig_out_out(ag_counter_trig_out),
      .counter_scaledown_out(ag_counter_scaledown),
      .counter_drift_out(ag_counter_drift),
      .counter_trig_in_out(ag_counter_trig_in),
      .counter_pulser_out(ag_counter_pulser),
      .counter_timeout_out(ag_counter_timeout),

      .counter_adc16_grand_or_out(ag_counter_adc16_grand_or),
      .counter_adc32_grand_or_out(ag_counter_adc32_grand_or),
      .counter_adc_grand_or_out(ag_counter_adc_grand_or),
      .counter_esata_nim_grand_or_out(ag_counter_esata_nim_grand_or),

      //.counter_adc16_mult_1ormore_out(ag_counter_adc16_mult_1ormore),
      //.counter_adc16_mult_2ormore_out(ag_counter_adc16_mult_2ormore),
      //.counter_adc16_mult_3ormore_out(ag_counter_adc16_mult_3ormore),
      //.counter_adc16_mult_4ormore_out(ag_counter_adc16_mult_4ormore),

      .counter_adc16_or16_out(ag_counter_adc16_or16),
      .counter_adc32_or16_out(ag_counter_adc32_or16),

      .counter_adc_selected_out(ag_counter_adc_selected),

      //.counter_aw16_mult_1ormore_out(ag_counter_aw16_mult_1ormore),
      //.counter_aw16_mult_2ormore_out(ag_counter_aw16_mult_2ormore),
      //.counter_aw16_mult_3ormore_out(ag_counter_aw16_mult_3ormore),
      //.counter_aw16_mult_4ormore_out(ag_counter_aw16_mult_4ormore),

      //.counter_aw16_coinc_a_out(ag_counter_cc_a),
      //.counter_aw16_coinc_b_out(ag_counter_cc_b),
      //.counter_aw16_coinc_c_out(ag_counter_cc_c),
      //.counter_aw16_coinc_d_out(ag_counter_cc_d),
      //.counter_aw16_coinc_out(ag_counter_cc),

      .counter_aw16_grand_or_out(ag_counter_aw16_grand_or),
      .counter_aw16_mlu_start_out(ag_counter_aw16_mlu_start),
      .counter_aw16_mlu_trig_out(ag_counter_aw16_mlu_trig),
      
      .counter_bsc_grand_or_out(ag_counter_bsc_grand_or),
      .counter_bsc_mult_out(ag_counter_bsc_mult),
      .counter_bsc_mult_start_out(ag_counter_bsc_mult_start),
      .counter_bsc_mult_trig_out(ag_counter_bsc_mult_trig),
      .counter_bsc64_out(ag_counter_bsc64),

      .counter_coinc_start_out(ag_counter_coinc_start),
      .counter_coinc_trig_out(ag_counter_coinc_trig),

      .sw_trig_out(ag_cb_sw_trig),
      .pulser_trig_out_clk_625(ag_cb_pulser_trig),
      .esata_nim_trig_out(ag_cb_esata_nim_trig),
      .adc16_grand_or_trig_out(ag_cb_adc16_grand_or_trig),
      .adc32_grand_or_trig_out(ag_cb_adc32_grand_or_trig),

      .aw16_grand_or_trig_out(ag_cb_aw16_grand_or_trig),
      //.aw16_mult_1ormore_out(ag_cb_aw16_mult_1ormore),
      //.aw16_mult_2ormore_out(ag_cb_aw16_mult_2ormore),
      //.aw16_mult_3ormore_out(ag_cb_aw16_mult_3ormore),
      //.aw16_mult_4ormore_out(ag_cb_aw16_mult_4ormore),
      .aw16_mlu_trig_out(ag_cb_aw16_mlu_trig),

      .bsc64_grand_or_trig_out(ag_cb_bsc64_grand_or_trig),
      //.bsc64_mult_1ormore_out(ag_cb_bsc64_mult_1ormore),
      //.bsc64_mult_2ormore_out(ag_cb_bsc64_mult_2ormore),
      //.bsc64_mult_3ormore_out(ag_cb_bsc64_mult_3ormore),
      //.bsc64_mult_4ormore_out(ag_cb_bsc64_mult_4ormore),
      .bsc64_mult_trig_out(ag_cb_bsc64_mult_trig),

      .coinc_trig_out(ag_cb_coinc_trig),

      .timeout_trig_out(ag_cb_timeout_trig),

      .trig_received_out_clk_625(ag_trig_received_out),
      .trig_drift_out_clk_625(ag_trig_drift_out),
      .trig_scaledown_out_clk_625(ag_trig_scaledown_out),
      .trig_out_clk_625(ag_trig_out),

      .busy_out_clk_625(ag_busy_out),
      .drift_out_clk_625(ag_drift_out)
      );

   //
   // chronobox main function
   //

   localparam CB_N = 23;
   localparam CB_N1 = CB_N-1;

   wire [CB_N1:0]    cb_xinputs_async;

   str0  s0  (.clk(clk_ag_625), .out(cb_xinputs_async[0]),  .in(ag_trig_out));
   str0  s1  (.clk(clk_ag_625), .out(cb_xinputs_async[1]),  .in(ag_trig_scaledown_out));
   str0  s2  (.clk(clk_ag_625), .out(cb_xinputs_async[2]),  .in(ag_trig_drift_out));
   str0  s3  (.clk(clk_ag_625), .out(cb_xinputs_async[3]),  .in(ag_trig_received_out));
   
   str0  s4  (.clk(io_clk),     .out(cb_xinputs_async[4]),  .in(ag_cb_sw_trig));
   str0  s5  (.clk(clk_ag_625), .out(cb_xinputs_async[5]),  .in(ag_cb_pulser_trig));
   str0  s6  (.clk(io_clk),     .out(cb_xinputs_async[6]),  .in(ag_cb_esata_nim_trig));
   str0  s7  (.clk(io_clk),     .out(cb_xinputs_async[7]),  .in(ag_cb_adc16_grand_or_trig));
   str0  s8  (.clk(io_clk),     .out(cb_xinputs_async[8]),  .in(ag_cb_adc32_grand_or_trig));
   
   str0  s9  (.clk(io_clk),     .out(cb_xinputs_async[9]),  .in(ag_cb_aw16_grand_or_trig));
   //str0  s10 (.clk(io_clk),     .out(cb_xinputs_async[10]), .in(ag_cb_aw16_mult_1ormore));
   //str0 s11 (.clk(io_clk),     .out(cb_xinputs_async[11]), .in(ag_cb_aw16_mult_2ormore));
   //str0 s12 (.clk(io_clk),     .out(cb_xinputs_async[12]), .in(ag_cb_aw16_mult_3ormore));
   //str0 s13 (.clk(io_clk),     .out(cb_xinputs_async[13]), .in(ag_cb_aw16_mult_4ormore));
   str0  s14 (.clk(io_clk),     .out(cb_xinputs_async[14]), .in(ag_cb_aw16_mlu_trig));
   
   str0  s15 (.clk(io_clk),     .out(cb_xinputs_async[15]), .in(ag_cb_bsc64_grand_or_trig));
   //str0  s16 (.clk(io_clk),     .out(cb_xinputs_async[16]), .in(ag_cb_bsc64_mult_1ormore));
   //str0  s17 (.clk(io_clk),     .out(cb_xinputs_async[17]), .in(ag_cb_bsc64_mult_2ormore));
   //str0  s18 (.clk(io_clk),     .out(cb_xinputs_async[18]), .in(ag_cb_bsc64_mult_3ormore));
   //str0  s19 (.clk(io_clk),     .out(cb_xinputs_async[19]), .in(ag_cb_bsc64_mult_4ormore));
   str0  s20 (.clk(io_clk),     .out(cb_xinputs_async[20]), .in(ag_cb_bsc64_mult_trig));
   
   str0  s21 (.clk(io_clk),     .out(cb_xinputs_async[21]), .in(ag_cb_coinc_trig));

   str0  s22 (.clk(io_clk),     .out(cb_xinputs_async[22]), .in(ag_cb_timeout_trig));
   
   wire [CB_N1:0] cb_inputs_async  = cb_xinputs_async ^ cb_inputs_invert[CB_N1:0];

   wire [63:0]    cb_inputs_invert = { cb_invert_b[31:0],    cb_invert_a[31:0] };
   wire [63:0]    cb_sync_mask     = { cb_sync_mask_b[31:0], cb_sync_mask_a[31:0] };
   wire [63:0]    cb_enable_le     = { cb_enable_le_b[31:0], cb_enable_le_a[31:0] };
   wire [63:0]    cb_enable_te     = { cb_enable_te_b[31:0], cb_enable_te_a[31:0] };
   
   wire [CB_N1:0] cb_external_sync_bus = (cb_inputs_async[CB_N1:0] & cb_sync_mask[CB_N1:0]);
   wire           cb_external_sync = (|cb_external_sync_bus);
   wire           cb_sync_in_async = (cb_internal_sync | cb_external_sync);

   wire           cb_sync_arm      = cb_sync_reg[31];
   wire           cb_sync_disarm   = cb_sync_reg[30];
   wire           cb_internal_sync = cb_sync_reg[16];

   wire           cb_sync_reset;

   wire [31:0]    cb_fifo_data;
   wire           cb_fifo_wrreq;

   wire           cb_tsc_fifo_full;

   wire           clk_ts = clk_ag_10;

   // CB control registers

   wire [31:0]    cb_control; // connected
   wire [31:0]    cb_status = 0; // connected
   
   wire           cb_zero_scalers  = cb_control[0];
   wire           cb_latch_scalers = cb_control[1];

   wire [31:0]    cb_scaler_addr; // connected
   wire [31:0]    cb_scaler_data; // connected
   wire [31:0]    cb_input_num; // connected

   wire [31:0]    cb_invert_a, cb_invert_b; // connected
   wire [31:0]    cb_enable_le_a, cb_enable_le_b; // connected
   wire [31:0]    cb_enable_te_a, cb_enable_te_b; // connected
   wire [31:0]    cb_sync_mask_a, cb_sync_mask_b; // connected
   wire [31:0]    cb_sync_reg; // connected
   wire [31:0]    cb_sync_status; // connected

   assign cb_sync_status[31:16] = 16'd0;
  
   cb_main
     #(.N(CB_N), .S(0))
   cb_main
     (
      // control
      .clk(io_clk),
      .clk_ts(clk_ts),
      // sync
      .sync_arm(cb_sync_arm),
      .sync_disarm(cb_sync_disarm),
      .sync_in_async(cb_sync_in_async),
      // control
      .enable_le(cb_enable_le[CB_N1:0] ),
      .enable_te(cb_enable_te[CB_N1:0]),
      // inputs
      .inputs_async(cb_inputs_async),
      .zero_scalers_in(cb_zero_scalers),
      .latch_scalers_in(cb_latch_scalers),
      .scaler_addr_in(cb_scaler_addr),
      .latch_scalers_period_in(cb_latch_period),
      // outputs
      .input_num_out(cb_input_num),
      .scaler_data_out(cb_scaler_data),
      .fifo_out(cb_fifo_data),
      .fifo_ready_out(cb_fifo_wrreq),
      .tsc_fifo_full_out(cb_tsc_fifo_full),
      .sync_status_out(cb_sync_status[15:0]),
      .sync_reset_out(cb_sync_reset)
      );

   fifo_to_udp fifo_to_udp
     (
      .clk(io_clk),
      .reset(cb_sync_reset),
      // configuration
      .udp_period(cb_udp_period),
      .udp_threshold(cb_udp_threshold),
      // data into the fifo
      .fifo_data(cb_fifo_data),
      .fifo_wrreq(cb_fifo_wrreq),
      // fifo status
      .tsc_fifo_full(cb_tsc_fifo_full),
      .fifo_status_out(fifo_status),
      .fifo_data_out(fifo_data),
      .fifo_rdack(fifo_rdack),
      // UDP avalon-st interface
      .av_data_out(cb_udp_data),
      .av_sop_out(cb_udp_sop),
      .av_eop_out(cb_udp_eop),
      .av_valid_out(cb_udp_valid),
      .av_ready(cb_udp_ready)
      );

   // FLASH memory interface

   //inout wire [26:0]  FLASH_A;
   //inout wire [15:0]  FLASH_D;
   //inout wire         FLASH_BUSYn;
   //inout wire         FLASH_BYTEn;
   //inout wire         FLASH_CE0n;
   //inout wire         FLASH_CE1n;
   //inout wire         FLASH_OEn;
   //inout wire         FLASH_RESETn;
   //inout wire         FLASH_WEn;
   //inout wire         FLASH_WPn;

   // from datasheet PC28F256M29EW-MICRON.pdf
   // A : input
   // CE# : chip select
   // OE# : enable output buffer for read operations
   // WE# : control write operation, latch address on falling edge, latch data on leading edge
   // WP# : read about the write protect function
   // BYTE# : switch between 8-bit and 16-bit mode. BYTE# high is 16-bit mode
   // RST#  : reset the device
   // DQ : input for commands during write operations, data during read operations
   // RY#/BY# : low during program and erase operations.
   //
   // actual device is: PC28F512M29EW
   // 512 Mbits = 64 Mbytes: blocks 0..511 (0..0x1FF)
   // block size is 16 bits of address space, so 64 kwords or 128 kbytes.
   // block 0 address 00000..0FFFF
   // block 1 address 10000..1FFFF
   // block 2 address 20000..2FFFF
   // etc
   // last block:   1FF0000..1FFFFFF
   //
   // READ operation: A, CE# low, OE# low -> DQ data output
   // read page mode:
   // 16 words (A[3:0]) can be read without cycling CE#
   //

   wire [31:0] flash_a_in;
   wire [31:0] flash_a_out;
   
   wire [15:0] flash_d_in;
   wire [31:0] flash_d_out;

   wire flash_busyn_in;
   wire flash_byten_in;
   wire flash_ce0n_in;
   wire flash_ce1n_in;
   wire flash_oen_in;
   wire flash_resetn_in;
   wire flash_wen_in;
   wire flash_wpn_in;

   wire flash_busyn_out;
   wire flash_byten_out;
   wire flash_ce0n_out;
   wire flash_ce1n_out;
   wire flash_oen_out;
   wire flash_resetn_out;
   wire flash_wen_out;
   wire flash_wpn_out;

   wire flash_oe_a;
   wire flash_oe_d;
   wire flash_oe_ce0n;
   wire flash_oe_ce1n;
   wire flash_oe_oen;
   wire flash_oe_byten;
   wire flash_oe_busyn;
   wire flash_oe_resetn;
   wire flash_oe_wen;
   wire flash_oe_wpn;
   //wire flash_oe;

   assign flash_a_in[31:27] = 4'd0;

   genvar i;
   
   generate
      for (i=0; i<=26; i++) begin: iobuf_A_block
         iobuf iobuf_A(.dataout(flash_a_in[i]), .oe(flash_oe_a), .dataio(FLASH_A[i]), .datain(flash_a_out[i]));
      end
   endgenerate

   generate
      for (i=0; i<16; i++) begin: iobuf_D_block
         iobuf iobuf_D(.dataout(flash_d_in[i]), .oe(flash_oe_d), .dataio(FLASH_D[i]), .datain(flash_d_out[i]));
      end
   endgenerate
   
   iobuf iobuf_BUSYn(.dataout(flash_busyn_in), .oe(flash_oe_busyn), .dataio(FLASH_BUSYn), .datain(flash_busyn_out));
   iobuf iobuf_BYTEn(.dataout(flash_byten_in), .oe(flash_oe_byten), .dataio(FLASH_BYTEn), .datain(flash_byten_out));
   iobuf iobuf_CE0n(.dataout(flash_ce0n_in), .oe(flash_oe_ce0n), .dataio(FLASH_CE0n), .datain(flash_ce0n_out));
   iobuf iobuf_CE1n(.dataout(flash_ce1n_in), .oe(flash_oe_ce1n), .dataio(FLASH_CE1n), .datain(flash_ce1n_out));
   iobuf iobuf_OEn(.dataout(flash_oen_in), .oe(flash_oe_oen), .dataio(FLASH_OEn), .datain(flash_oen_out));
   iobuf iobuf_RESETn(.dataout(flash_resetn_in), .oe(flash_oe_resetn), .dataio(FLASH_RESETn), .datain(flash_resetn_out));
   iobuf iobuf_WEn(.dataout(flash_wen_in), .oe(flash_oe_wen), .dataio(FLASH_WEn), .datain(flash_wen_out));
   iobuf iobuf_WPn(.dataout(flash_wpn_in), .oe(flash_oe_wpn), .dataio(FLASH_WPn), .datain(flash_wpn_out));

   wire [31:0] flash_control;
   wire [31:0] flash_status;

   assign flash_oe_a       = flash_control[0];
   assign flash_oe_d       = flash_control[1];
   assign flash_oe_ce0n    = flash_control[2];
   assign flash_oe_ce1n    = flash_control[3];
   assign flash_oe_oen     = flash_control[4];
   assign flash_oe_byten   = flash_control[5];
   assign flash_oe_busyn   = flash_control[6];
   assign flash_oe_resetn  = flash_control[7];
   assign flash_oe_wen     = flash_control[8];
   assign flash_oe_wpn     = flash_control[9];
   //assign flash_oe         = flash_control[15];
   assign flash_busyn_out  = ~flash_control[16];
   assign flash_byten_out  = ~flash_control[17];
   assign flash_ce0n_out   = ~flash_control[18];
   assign flash_ce1n_out   = ~flash_control[19];
   assign flash_oen_out    = ~flash_control[20];
   assign flash_resetn_out = ~flash_control[21];
   assign flash_wen_out    = ~flash_control[22];
   assign flash_wpn_out    = ~flash_control[23];

   assign flash_status[15:0] = flash_d_in;
   assign flash_status[16] = ~flash_busyn_in;
   assign flash_status[17] = ~flash_byten_in;
   assign flash_status[18] = ~flash_ce0n_in;
   assign flash_status[19] = ~flash_ce1n_in;
   assign flash_status[20] = ~flash_oen_in;
   assign flash_status[21] = ~flash_resetn_in;
   assign flash_status[22] = ~flash_wen_in;
   assign flash_status[23] = ~flash_wpn_in;
   assign flash_status[31:24] = 0;

   // LEDs, outputs, etc

   led_stretch led_stretch_trig_out(.clk(io_clk), .in_async(ag_trig_out), .out(USER_LED[0]));
   led_stretch led_stretch_eth_led(.clk(io_clk), .in_async(eth_led), .out(USER_LED[1]));
   led_stretch led_stretch_FMC1_CFG_DONE(.clk(io_clk), .in_async(FMC1_CFG_DONE), .out(USER_LED[2]));
   led_stretch led_stretch_unused(.clk(io_clk), .in_async(0), .out(USER_LED[3]));

   assign USER_LED[7:4] = xxx_dummy_out;
   
   wire        esata_run_out = ag_trig_out;

   wire        xxx_dummy_out; // send signals to an output pin to prevent quartus from optimizing them out
   assign xxx_dummy_out = xxx_vme | esata_clk | esata_clk2 | ag_busy_out | ag_drift_out;
   
   assign USER_LED[8] = xxx_dummy_out;

endmodule
