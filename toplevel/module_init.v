//////////////////////////////////////////////////////////////////////////////////////////////////////
// spartan serial_config interface [master:fpga configs self from flash, slave:device writes to fpga]
//       cclk[io]=config_clk din[i]=datain dout[o]=daisychain_dout
//       m[1:0][i]=config_mode  done[io]=config_done init_b[io]=CRCERRn program_b[i]=RSTn
//    m[1:0] pulled up, => default mode is slave serial
//    timing: first rising clk in centre of bit0 [MSB of first byte]
//    on first rise of clk after last bit, done should come up (and init_b stay high)
// Configuration Timing ...
//    Tprogram : min progn pulse-width = 500ns
//    Tpl      : time between releasing progn, and rising edge of initn - up to 5ms
//    cclk/din setup/hold 8ns => ~16ns period or 60Mhz fmax
//////////////////////////////////////////////////////////////////////////////////////////////////////
module module_init (
   input wire clk,   input wire reset,  input wire present,  input wire initn,  input wire done,
   output reg progn, output reg cclk,   output reg din,  output wire debug
	
	//output wire mac_clk, input wire mac_sdi, output wire mac_sdo, output wire mac_oen
	//output wire [47:0] mac_addr
);
assign debug = check_count[31];

// DATA_LENGTH is the last 16bit configuration word (starting at zero) i.e. (config-bytes/2)-1
//localparam DATA_LENGTH = 14'h21CE; // romV1         ends at 8654
//localparam DATA_LENGTH = 14'h221B; // esata_mac rom ends at 8731
//localparam DATA_LENGTH = 14'h21E4; // romV2         ends at 8676
//localparam DATA_LENGTH = 14'h2472;   // romV3         ends at 9330
localparam DATA_LENGTH = 14'h2478;   // drive RateSelect high

// config file is compressed to reduce memory requirement - file mostly contains isolated non-zeroes
//    so encoded file consists of 16bit entries: 8bit data byte and 8 bit count of following-zeroes
//    to write this - first write the data byte, then write count * 8'h0
// 32'h0 is invalid encoded data => end of stream
reg [13:0] rom_addr;  wire [15:0] rom_q;
config_rom spartan_config (  .clock(clk), .address(rom_addr), .q(rom_q)  );

reg [3:0] state;  reg [7:0] data;  reg [7:0] zero_count;  reg [2:0] bit_count; reg clk_count;
parameter init_wait=0, init_start=1, init_setup=2, init_hold=3, init_next=4, init_check=5, init_done=6, init_fail=7, init_error=8;

reg [31:0] check_count;
always @ (posedge clk or posedge reset) begin
   if( reset ) begin
      clk_count  <=       1'h0;  zero_count <=       8'h0;  data  <= 8'h0;  din  <= 1'b0;  check_count <= 32'h0;
		rom_addr   <=      14'h0;  bit_count  <=       3'h0;  progn <= 1'b0;  cclk <= 1'b0;
		state <= init_wait;
   end else begin
      clk_count  <=  clk_count;  zero_count <= zero_count;  data  <= data;  din  <=  din;  check_count <= check_count;
		rom_addr   <=   rom_addr;  bit_count  <=  bit_count;  progn <= 1'b1;  cclk <= cclk;
		state <= state;
		case(state)
      init_wait: if( initn == 1'b1 ) state <= init_start;
      init_start:
         begin
			   data <= rom_q[15:8];  zero_count <= rom_q[7:0];  rom_addr <= rom_addr + 14'h1;
			   bit_count <= 3'h7;  clk_count <= 1'h1;  din <= rom_q[15];  state <= init_setup;
         end
      init_setup: begin
			   cclk <= 1'b0;  din <= data[7]; clk_count <= clk_count - 1'h1;
				if( clk_count == 1'h0 ) begin cclk <= 1'b1; state <= init_hold; end // wraps back to 1
         end
      init_hold: begin
			   cclk <= 1'b1;  din <= data[7]; clk_count <= clk_count - 1'h1;
				if( clk_count == 1'h0 ) begin // wraps back to 1
				   data <= {data[6:0],1'b0};  cclk <= 1'b0; din <= data[6];
					bit_count <= bit_count - 3'h1;
					if( bit_count != 3'h0 ) state <= init_setup;
					else begin              // bit_count wraps back to 7
					   check_count <= check_count + 32'h1;
			         zero_count <= zero_count - 8'h1;
			         if( zero_count == 8'h0 ) state <=  init_next;
				      else                     state <= init_setup; // zero already shifted into data
					end
				end
         end
      init_next: begin
			   data <= rom_q[15:8];  zero_count <= rom_q[7:0];  rom_addr <= rom_addr + 14'h1;
			   din <= rom_q[15]; state <= init_check;
         end
      init_check: begin // "ff00 0000 ff04" at 5838 ends the sequence - rom_q updates 1 clk after test
			   //if( data == 8'h0 && zero_count == 8'h0 && rom_q == 16'h0 ) state <= init_done;
			   if( rom_addr == DATA_LENGTH ) state <= init_done; // end of valid data in rom
			   else if(  rom_addr == 14'h0 ) state <= init_error;   // wrapped memory!
			   else                          state <= init_setup;
         end
      init_done:  state <= init_done; // need to check initn/done for errors
      init_fail:  state <= init_fail;
      init_error: state <= init_error;
      default:    state <= init_error;
      endcase
   end
end

/////////////////////////////////////////////////////////////////////////////////////////////
/////////              access Fmc via spartan (when configured)                 /////////////
/////////////////////////////////////////////////////////////////////////////////////////////
// output wire mac_clk, input wire mac_sdi, output wire mac_sdo, output wire mac_oen,
// output wire [47:0] mac_addr
//spi4 mac (
//   .clk(clk), .reset( reset || (state != init_done) ),  //.debug(),
	//.data_in(), .dataready_in(),  .data_out(), .busy_out(),
//   .sck(mac_clk), .sdo(mac_sdo), .sdi(mac_sdi), .sdoen(mac_oen)
//);

//reg ;
//always @ (posedge clk or posedge reset) begin
//   if( reset ) begin
//   end else begin
//	end
//end
	
endmodule
