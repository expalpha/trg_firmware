// compare 14 paris of 8bit numbers, pass if condition non-zero and all A>=B
module compare_14x8bit (
   input  wire [127:0] mult_in, input  wire [127:0] req,  input wire [31:0] clover_mult,
   output wire result,          output wire result2,      input wire [3:0] dtype
);
// ** There are 2 unused prescale flags: conditions[127:126] which are used to set clover-mult and BGO-suppression
// if these are set, the original Ge multiplicity is replaced with one of the clover_mult multiplicities for that condition

wire [7:0] ge_mult = ( req[127:126] == 2'h0 ) ? mult_in    [15: 8] : // supp=0 clov=0  unsuppressed crystal-mult
                     ( req[127:126] == 2'h1 ) ? clover_mult[ 7: 0] : // supp=0 clov=1  unsuppressed clover-mult
                     ( req[127:126] == 2'h2 ) ? clover_mult[31:24] : // supp=1 clov=0  suppressed   crystal-mult
                                                clover_mult[23:16];  // supp=1 clov=1  suppressed   clover-mult

wire [127:0] mult = {mult_in[127:16],ge_mult[7:0],mult_in[7:0]};

assign result = (req[111:  0] != 112'h0) &&
      (mult[  7:  0] >= req[  7:  0]) && (mult[ 15:  8] >= req[ 15:  8]) && // condition  0, 1
      (mult[ 23: 16] >= req[ 23: 16]) && (mult[ 31: 24] >= req[ 31: 24]) && // condition  2, 3
      (mult[ 39: 32] >= req[ 39: 32]) && (mult[ 47: 40] >= req[ 47: 40]) && // condition  4, 5
      (mult[ 55: 48] >= req[ 55: 48]) && (mult[ 63: 56] >= req[ 63: 56]) && // condition  5, 7
      (mult[ 71: 64] >= req[ 71: 64]) && (mult[ 79: 72] >= req[ 79: 72]) && // condition  6, 9
      (mult[ 87: 80] >= req[ 87: 80]) && (mult[ 95: 88] >= req[ 95: 88]) && // condition 10,11
      (mult[103: 96] >= req[103: 96]) && (mult[111:104] >= req[111:104]);   // condition 12,13

// Now also want det-type to correspond to a passed condition
// - normally for each condition, check all 14 multiplicities
// - now, for each condition, only check (all 14) if our det-type-condition is non-zero
// to do in this module, need properly timed det-type signal
wire [7:0] relevant_condition =
    ( dtype == 4'h0) ? req[  7:  0] : ( dtype == 4'h1) ? req[ 15:  8] :
    ( dtype == 4'h2) ? req[ 23: 16] : ( dtype == 4'h3) ? req[ 31: 24] :
    ( dtype == 4'h4) ? req[ 39: 32] : ( dtype == 4'h5) ? req[ 47: 40] :
    ( dtype == 4'h6) ? req[ 55: 48] : ( dtype == 4'h7) ? req[ 63: 56] :
    ( dtype == 4'h8) ? req[ 71: 64] : ( dtype == 4'h9) ? req[ 79: 72] :
	 ( dtype == 4'hA) ? req[ 87: 80] : ( dtype == 4'hB) ? req[ 95: 88] :
	 ( dtype == 4'hC) ? req[103: 96] : ( dtype == 4'hD) ? req[111:104] :
	 ( dtype == 4'hE) ? req[119:112] :                   req[127:120];
assign result2 = result && (relevant_condition != 8'h0);
		
endmodule
