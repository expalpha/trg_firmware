// file check_coincidence_patterns.v
//
// Compare 8 sets of coincidence paterns of 8 detector types.
// The coincidence patterns are defined as multiplicity thresholds for
// each detector type. each individual multiplicity check compare the multiplicity
// of one detector type within the defined coincidence window to be greater or equal
// to the multiplicity parameter defined in the required coincidence pattern.




/***************************************************************************************
*                         Check ONE of the 8 multiplicity patterns                     *
*                                                                                      *
*       NOTE: the "DONT CARE" condition is defined as 0 in the P field corresponding   *
*       to the detector type                                                           *
*       Example, the P vector defining the requested conditions:                       *
                                                                                       *
*                  Multiplicity of 1 for detector type 0,                              *
*            AND   multiplicity of 2 for detector type 2,                              *
*            AND   "DONT CARE" about the other 6 would be:                             *
*  
*                Detector type:  7  6  5  4  3  2  1  0                                 *                                                   *
*                            P:  00 00 00 00 00 10 00 01                                *
*
*    The pattern is satisfied if multiplicity[detector type] >= P[detector type field]  *
*    for all elements of the vector (AND logic),                                        *
*    as long as the P vector is not identical to zero                                   *
*                                                                                       *
*    Presently, there is a provision to define 8 such P vectors, and the accept flag    *
*    id the OR result                                                                   *
****************************************************************************************/   

module check_coincidence_patterns(
input wire [63:0] m,   // block of 8 values of 8 bits of detector multiplicities
input wire [63:0] mpre,// flagged with preset bit
input wire [63:0] mf1, // flagged with flag1 bit
input wire [63:0] mf2, // flagged with flag2 bit
input wire [4095:0] p_patterns,   // array of 16 vectors of 4 X 8 multiplicities of 8 bits
                       
input wire [15:0]  pattern_select,  // to enable one or more or the 16 vectors
output wire        ccp_ok,    // condition satisfied
output wire [15:0] ccp_cok   // pattern vectors with OK conditions
);

//merge the window multiplicities
wire [255:0] mu;
assign mu = {mf2,mf1,mpre,m};

// Split the P-Pattern array into 16 multiplicity vectors or 4 X 8 multiplicities values
// (one group of 8 for each type of flag)

wire [255:0] m0v;
wire [255:0] m1v;
wire [255:0] m2v;
wire [255:0] m3v;
wire [255:0] m4v;
wire [255:0] m5v;
wire [255:0] m6v;
wire [255:0] m7v;
wire [255:0] m8v;
wire [255:0] m9v;
wire [255:0] m10v;
wire [255:0] m11v;
wire [255:0] m12v;
wire [255:0] m13v;
wire [255:0] m14v;
wire [255:0] m15v;
assign{m15v,m14v,m13v,m12v,m11v,m10v,m9v,m8v,m7v,m6v,m5v,m4v,m3v,m2v,m1v,m0v}= p_patterns;

wire [15:0] ok;    //bit pattern of satisfied coincidence request vectors
// perform the coincidence match AND test within each pattern vectors
testmultiv testmultiv_inst0(.mv(m0v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[0]));
testmultiv testmultiv_inst1(.mv(m1v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[1]));
testmultiv testmultiv_inst2(.mv(m2v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[2]));
testmultiv testmultiv_inst3(.mv(m3v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[3]));
testmultiv testmultiv_inst4(.mv(m4v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[4]));
testmultiv testmultiv_inst5(.mv(m5v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[5]));
testmultiv testmultiv_inst6(.mv(m6v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[6]));
testmultiv testmultiv_inst7(.mv(m7v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[7]));
testmultiv testmultiv_inst8(.mv(m8v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[8]));
testmultiv testmultiv_inst9(.mv(m9v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[9]));
testmultiv testmultiv_inst10(.mv(m10v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[10]));
testmultiv testmultiv_inst11(.mv(m11v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[11]));
testmultiv testmultiv_inst12(.mv(m12v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[12]));
testmultiv testmultiv_inst13(.mv(m13v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[13]));
testmultiv testmultiv_inst14(.mv(m14v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[14]));
testmultiv testmultiv_inst15(.mv(m15v), .mu(mu),.enable(pattern_select[0]), .andOK(ok[15]));

assign ccp_cok = (ok & pattern_select);                 // pattern of successful vectors
assign ccp_ok  = ((ok & pattern_select) != 16'h0);      // One or more valid

endmodule
