// Reorder elements that have the same 10ns time stamp according to the 4-bit time vernier.
//    The packet pointers are split into up to 1,2,4,8, or 16 streams according to the required resolution
//    The streams are then merged to have the correct sequence
// example for 2 streams
//   1) Buffer the packet pointers into FIFO_M (simply to de-randomize the inputs)
//   2) Distribute pointers into the 2 streams according to their vernier bits while main time stamp (bits 17:4) constant
//   3) When the time stamp changes (or if input times out), extract pointers from the 2 FIFOs in the vernier time order.	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
module MicroSequence( 	input  wire reset,  output wire ms_empty,
   input  wire               clk,  input  wire    clk_ddr,
   input  wire [47:0] ll_ptr_out,  input  wire ll_ptr_rdy,
   output reg  [47:0] ms_ptr_out,  output reg  ms_ptr_rdy
);
assign ms_empty = !(stretch || save || ms_ptr_rdy); // must be set correctly for filter to run properly

// clock transferring passthrough from ddrclk to sdr clk
//    stretch ptr/rdy to 2 clks - separate if on consecutive clks
//    first rdy - stretch to 2 clks, if 2nd rdy on next clk during stretch - delay it, (then stretch that)
//       error if 3rd rdy comes on 3rd clk when starting delayed rdy
reg stretch;  reg save;  reg [47:0] ll_ptr_save;
always @ (posedge clk_ddr) begin
   if( reset ) begin
      ms_ptr_out <= 48'h0;       ms_ptr_rdy <= 1'b0;  ll_ptr_save <= 48'h0;        save <= 1'b0;  stretch <= 1'b0;
	end else begin
      ms_ptr_out <= ms_ptr_out;  ms_ptr_rdy <= 1'b0;  ll_ptr_save <= ll_ptr_save;  save <= save;  stretch <= stretch;
	   if ( stretch ) begin
	      if ( ll_ptr_rdy ) begin ll_ptr_save <= ll_ptr_out;  save <= 1'b1; end
			stretch <= 1'b0;
			ms_ptr_out <= ms_ptr_out;  ms_ptr_rdy <= 1'b1;
	   end else if ( save ) begin // error if ll_ptr_rdy set here
		   stretch <= 1'b1;  save <= 1'b0;
			ms_ptr_out <= ll_ptr_save;  ms_ptr_rdy <= 1'b1;  
	   end else if ( ll_ptr_rdy ) begin
		   stretch <= 1'b1;
			ms_ptr_out <= ll_ptr_out;  ms_ptr_rdy <= 1'b1;  
		end
   end
end

//assign ms_ptr_out = ll_ptr_out;  assign ms_ptr_rdy = ll_ptr_rdy;  assign ms_empty = 1'b1; // passthrough
//assign ms_ptr_out = pkt_ptr;  assign ms_ptr_rdy = pkt_ptr_rdy;                          // microsequence
//assign ms_empty   = emptyQ & empty0 & empty8;

reg ptr_rdy;    always @ (posedge clk) ptr_rdy <= (rdreq8 | rdreq0);
reg [39:0] ptr; always @ (posedge clk) ptr <= (rdreq0) ? q0 : ((rdreq8) ? q8 : 48'h0);

reg [39:0] pkt_ptr;  reg pkt_ptr_rdy;
always @ (posedge clk) begin
   pkt_ptr_rdy <= ptr_rdy;
  if (     empty0 & ~ptr[3]) pkt_ptr <= {ptr[39], 1'b1, ptr[37:0]}; // last stamp logic?
  else if (empty8 &  ptr[3]) pkt_ptr <= {ptr[39], 1'b1, ptr[37:0]}; // ?
  else                       pkt_ptr <=  ptr;
end

// events with same timestamp will come close together from linkedlist - only need short timeout to know that no more are coming
reg [7:0] timeout_cnt;  // Timeout counter: zeroed when the Q fifo goes not empty
always @ (posedge clk )begin
   if      (emptyQ == 0)          timeout_cnt  <= 8'h0;
	else if (timeout_cnt != 8'h03) timeout_cnt  <= timeout_cnt + 1'b1;
	else                           timeout_cnt  <= timeout_cnt;
end
wire timeout = timeout_cnt == 8'h03;

//define a queue FIFO and 2 short FIFOS - queue the input stream in order to split only one complete time stamp at a time
wire [3:0] vernier = qQ[3:0]; 
wire wrreqQ = ll_ptr_rdy;
wire wrreq0 = ( vernier[3] ) ?  1'b0  : rdreqQ; // split into 2 streams using only vernier[3]
wire wrreq8 = ( vernier[3] ) ? rdreqQ : 1'b0;
wire rdreqQ = (~emptyQ & ((state[0]) | ((state[4]) & (qQ[17:4] == current_tstamp))));
wire rdreq0 = (         ~empty0 & state[5]); // stream 0 [earlier time] has priority
wire rdreq8 = (empty0 & ~empty8 & state[5]); //    lower priority
wire emptyQ, empty0, empty8;
wire [47:0] q0;  wire [47:0] q8;  wire [47:0] qQ;
ms_FIFO	FIFO_Q (
   .clock(clk       ),  .aclr (reset ),
	.data (ll_ptr_out),  .wrreq(wrreqQ),
	.q    (qQ        ),  .rdreq(rdreqQ),  .empty(emptyQ)
);
ms_FIFO	FIFO_0 (
	.clock(clk                      ),  .aclr (reset ),
	.data ({qQ[39:38],new0,qQ[36:0]}),  .wrreq(wrreq0),
	.q    (q0                       ),  .rdreq(rdreq0),  .empty(empty0)
);
ms_FIFO	FIFO_8 (
	.clock(clk                      ),  .aclr (reset ),
	.data ({qQ[39:38],new8,qQ[36:0]}),  .wrreq(wrreq8),
	.q    (q8                       ),  .rdreq(rdreq8),  .empty(empty8)
);

reg new0;  reg new8;         // new-timestamp flags (one for each stream) - maybe in place of "last stamp"?
always @ (posedge clk) begin // *No - "last stamp" is regenerated below in readout
   new0 <= (wrreq0) ? 1'b0 : new0;  new8 <= (wrreq8) ? 1'b0 : new8; // clear after writing
   if ((state == 6'h1) || (state == 6'h20)) begin new0 <= 1'b1; new8 <= 1'b1; end
end
	
reg [13:0] current_tstamp; // current timestamp
always @ (posedge clk) begin
   if ( ~emptyQ && (state[0]||state[5]) ) current_tstamp <= qQ[17:4]; // going to state 10 (from 1 or 20)
   else                                   current_tstamp <= current_tstamp;
end

reg [5:0] state;
wire new_tstamp = (emptyQ == 0) && (qQ[17:4] != current_tstamp); // time stamp has changed
always @ (posedge clk) begin
   if (reset) state <= 6'h01;
   else begin
      case (state)
      6'h01:  state <= (emptyQ) ? 6'h01 : 6'h02; // idle: all empty 
      6'h02:  state <= 6'h04;                    // 4 clk delay ???
	   6'h04:  state <= 6'h08;
	   6'h08:  state <= 6'h10;
	   6'h10:  state <= (new_tstamp||timeout) ? 6'h20 : 6'h10;  // keep filling input
	   6'h20:  if ( emptyQ & empty0 & empty8)  state <= 6'h01;  // wait for streams to empty
		        else if (     empty0 & empty8)  state <= 6'h10; 
		        else                            state <= 6'h20; 
     default: state <= 6'h01;
     endcase
	end
end

endmodule
