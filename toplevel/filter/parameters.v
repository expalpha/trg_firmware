module parameters(  input  wire clk,
  output wire   [31:0]    ParamOut,  input wire   [31:0] ParamIn,   input wire ParamReady,
  output wire [4095:0]  p_patterns,  output wire [127:0] p_prescales,
  output wire   [13:0] coinc_width,  output wire  [13:0] coinc_width_s,
  output wire   [15:0]   pat_sel_s,  output wire  [15:0] pat_sel,
  output wire        test_startbit
);

assign coinc_width_s = 14'h0C;  //window width for first multi module	
assign coinc_width   = 14'h0C;    
assign test_startbit =  1'b0;

// merge 16 coincidence definition vectors, each with 4 X 8 multiplicities of 8 bits
wire [255:0] m0v;  wire [255:0] m1v;  wire [255:0] m2v;  wire [255:0] m3v;
wire [255:0] m4v;  wire [255:0] m5v;  wire [255:0] m6v;  wire [255:0] m7v;
wire [255:0] m8v;  wire [255:0] m9v;  wire [255:0] m10v; wire [255:0] m11v;
wire [255:0] m12v; wire [255:0] m13v; wire [255:0] m14v; wire [255:0] m15v;
assign p_patterns ={m15v,m14v,m13v,m12v,m11v,m10v,m9v,m8v,m7v,m6v,m5v,m4v,m3v,m2v,m1v,m0v};

// define a few patterns for testing
assign m0v = 256'h200;     // request detector type 1 multiplicity 2
assign m1v = 256'h101000;  // request  coincidence between detector type 1 and 2

// enable these two patterns for the first multiplicity test
assign pat_sel_s = 16'h3;  // select 0 and 1 for test S
assign pat_sel   = 16'h0;  // second unit not enabled

// Pre-scale
wire [15:0] preA;  wire [15:0] preB;  wire [15:0] preC;  wire [15:0] preD;
wire [15:0] preE;  wire [15:0] preF;  wire [15:0] preG;  wire [15:0] preH;
assign preA = 16'h1;
assign preB = 16'h2;
assign preC = 16'h0;
assign preD = 16'h0;
assign preE = 16'h0;
assign preF = 16'h0;
assign preG = 16'h0;
assign preH = 16'h0;
assign p_prescales = {preH,preG,preF,preE,preD,preC,preB,preA};

endmodule
