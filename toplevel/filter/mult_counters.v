// maintain 32 8-bit counters - normal and prescaled - for each of 16 detector types
module mult_counters ( input wire clk,  input wire reset,
   input wire         event_in,         input wire event_out,
   input wire         prescale_ok_in,   input wire prescale_ok_out,
	input wire   [3:0] dettype_in,       input wire [3:0] dettype_out,
	output reg [255:0] multiplicity_out
);

// Don't do prescaled multuplicity > 1, so only need to test yes/no for prescaled events
//  - but still need to count the events in/out to test this
// so there needs to be a counter plus inc plus dec for each dettype and for each prescaled dettype
//genvar i;
//generate
//for(i=0; i<16; i=i+1) begin :coinc
//end
//endgenerate

wire [31:0] inc_det;  wire [31:0] dec_det;
assign inc_det[ 0] = event_in  & (dettype_in  == 4'h0);  assign inc_det[16] = inc_det[ 0] & prescale_ok_in;
assign inc_det[ 1] = event_in  & (dettype_in  == 4'h1);  assign inc_det[17] = inc_det[ 1] & prescale_ok_in;
assign inc_det[ 2] = event_in  & (dettype_in  == 4'h2);  assign inc_det[18] = inc_det[ 2] & prescale_ok_in;
assign inc_det[ 3] = event_in  & (dettype_in  == 4'h3);  assign inc_det[19] = inc_det[ 3] & prescale_ok_in;
assign inc_det[ 4] = event_in  & (dettype_in  == 4'h4);  assign inc_det[20] = inc_det[ 4] & prescale_ok_in;
assign inc_det[ 5] = event_in  & (dettype_in  == 4'h5);  assign inc_det[21] = inc_det[ 5] & prescale_ok_in;
assign inc_det[ 6] = event_in  & (dettype_in  == 4'h6);  assign inc_det[22] = inc_det[ 6] & prescale_ok_in;
assign inc_det[ 7] = event_in  & (dettype_in  == 4'h7);  assign inc_det[23] = inc_det[ 7] & prescale_ok_in;
assign inc_det[ 8] = event_in  & (dettype_in  == 4'h8);  assign inc_det[24] = inc_det[ 8] & prescale_ok_in;
assign inc_det[ 9] = event_in  & (dettype_in  == 4'h9);  assign inc_det[25] = inc_det[ 9] & prescale_ok_in;
assign inc_det[10] = event_in  & (dettype_in  == 4'hA);  assign inc_det[26] = inc_det[10] & prescale_ok_in;
assign inc_det[11] = event_in  & (dettype_in  == 4'hB);  assign inc_det[27] = inc_det[11] & prescale_ok_in;
assign inc_det[12] = event_in  & (dettype_in  == 4'hC);  assign inc_det[28] = inc_det[12] & prescale_ok_in;
assign inc_det[13] = event_in  & (dettype_in  == 4'hD);  assign inc_det[29] = inc_det[13] & prescale_ok_in;
assign inc_det[14] = event_in  & (dettype_in  == 4'hE);  assign inc_det[30] = inc_det[14] & prescale_ok_in;
assign inc_det[15] = event_in  & (dettype_in  == 4'hF);  assign inc_det[31] = inc_det[15] & prescale_ok_in;

assign dec_det[ 0] = event_out & (dettype_out == 4'h0);  assign dec_det[16] = dec_det[ 0] & prescale_ok_out;
assign dec_det[ 1] = event_out & (dettype_out == 4'h1);  assign dec_det[17] = dec_det[ 1] & prescale_ok_out;
assign dec_det[ 2] = event_out & (dettype_out == 4'h2);  assign dec_det[18] = dec_det[ 2] & prescale_ok_out;
assign dec_det[ 3] = event_out & (dettype_out == 4'h3);  assign dec_det[19] = dec_det[ 3] & prescale_ok_out;
assign dec_det[ 4] = event_out & (dettype_out == 4'h4);  assign dec_det[20] = dec_det[ 4] & prescale_ok_out;
assign dec_det[ 5] = event_out & (dettype_out == 4'h5);  assign dec_det[21] = dec_det[ 5] & prescale_ok_out;
assign dec_det[ 6] = event_out & (dettype_out == 4'h6);  assign dec_det[22] = dec_det[ 6] & prescale_ok_out;
assign dec_det[ 7] = event_out & (dettype_out == 4'h7);  assign dec_det[23] = dec_det[ 7] & prescale_ok_out;
assign dec_det[ 8] = event_out & (dettype_out == 4'h8);  assign dec_det[24] = dec_det[ 8] & prescale_ok_out;
assign dec_det[ 9] = event_out & (dettype_out == 4'h9);  assign dec_det[25] = dec_det[ 9] & prescale_ok_out;
assign dec_det[10] = event_out & (dettype_out == 4'hA);  assign dec_det[26] = dec_det[10] & prescale_ok_out;
assign dec_det[11] = event_out & (dettype_out == 4'hB);  assign dec_det[27] = dec_det[11] & prescale_ok_out;
assign dec_det[12] = event_out & (dettype_out == 4'hC);  assign dec_det[28] = dec_det[12] & prescale_ok_out;
assign dec_det[13] = event_out & (dettype_out == 4'hD);  assign dec_det[29] = dec_det[13] & prescale_ok_out;
assign dec_det[14] = event_out & (dettype_out == 4'hE);  assign dec_det[30] = dec_det[14] & prescale_ok_out;
assign dec_det[15] = event_out & (dettype_out == 4'hF);  assign dec_det[31] = dec_det[15] & prescale_ok_out;

always @ (posedge clk or posedge reset) begin
   if( reset ) multiplicity_out <= 256'h0;
	else begin
	         	multiplicity_out <= multiplicity_out;
	   if(      inc_det[ 0] && !dec_det[ 0] ) multiplicity_out[  7:  0] <= multiplicity_out[  7:  0] + 8'h1;
	   else if( dec_det[ 0] && !inc_det[ 0] ) multiplicity_out[  7:  0] <= multiplicity_out[  7:  0] - 8'h1;
	   if(      inc_det[ 1] && !dec_det[ 1] ) multiplicity_out[ 15:  8] <= multiplicity_out[ 15:  8] + 8'h1;
	   else if( dec_det[ 1] && !inc_det[ 1] ) multiplicity_out[ 15:  8] <= multiplicity_out[ 15:  8] - 8'h1;
	   if(      inc_det[ 2] && !dec_det[ 2] ) multiplicity_out[ 23: 16] <= multiplicity_out[ 23: 16] + 8'h1;
	   else if( dec_det[ 2] && !inc_det[ 2] ) multiplicity_out[ 23: 16] <= multiplicity_out[ 23: 16] - 8'h1;
	   if(      inc_det[ 3] && !dec_det[ 3] ) multiplicity_out[ 31: 24] <= multiplicity_out[ 31: 24] + 8'h1;
	   else if( dec_det[ 3] && !inc_det[ 3] ) multiplicity_out[ 31: 24] <= multiplicity_out[ 31: 24] - 8'h1;
	   if(      inc_det[ 4] && !dec_det[ 4] ) multiplicity_out[ 39: 32] <= multiplicity_out[ 39: 32] + 8'h1;
	   else if( dec_det[ 4] && !inc_det[ 4] ) multiplicity_out[ 39: 32] <= multiplicity_out[ 39: 32] - 8'h1;
	   if(      inc_det[ 5] && !dec_det[ 5] ) multiplicity_out[ 47: 30] <= multiplicity_out[ 47: 40] + 8'h1;
	   else if( dec_det[ 5] && !inc_det[ 5] ) multiplicity_out[ 47: 40] <= multiplicity_out[ 47: 40] - 8'h1;
	   if(      inc_det[ 6] && !dec_det[ 6] ) multiplicity_out[ 55: 48] <= multiplicity_out[ 55: 48] + 8'h1;
	   else if( dec_det[ 6] && !inc_det[ 6] ) multiplicity_out[ 55: 48] <= multiplicity_out[ 55: 48] - 8'h1;
	   if(      inc_det[ 7] && !dec_det[ 7] ) multiplicity_out[ 63: 56] <= multiplicity_out[ 63: 56] + 8'h1;
	   else if( dec_det[ 7] && !inc_det[ 7] ) multiplicity_out[ 63: 56] <= multiplicity_out[ 63: 56] - 8'h1;
	   if(      inc_det[ 8] && !dec_det[ 8] ) multiplicity_out[ 71: 64] <= multiplicity_out[ 71: 64] + 8'h1;
	   else if( dec_det[ 8] && !inc_det[ 8] ) multiplicity_out[ 71: 64] <= multiplicity_out[ 71: 64] - 8'h1;
	   if(      inc_det[ 9] && !dec_det[ 9] ) multiplicity_out[ 79: 72] <= multiplicity_out[ 79: 72] + 8'h1;
	   else if( dec_det[ 9] && !inc_det[ 9] ) multiplicity_out[ 79: 72] <= multiplicity_out[ 79: 72] - 8'h1;
	   if(      inc_det[10] && !dec_det[10] ) multiplicity_out[ 87: 80] <= multiplicity_out[ 87: 80] + 8'h1;
	   else if( dec_det[10] && !inc_det[10] ) multiplicity_out[ 87: 80] <= multiplicity_out[ 87: 80] - 8'h1;
	   if(      inc_det[11] && !dec_det[11] ) multiplicity_out[ 95: 88] <= multiplicity_out[ 95: 88] + 8'h1;
	   else if( dec_det[11] && !inc_det[11] ) multiplicity_out[ 95: 88] <= multiplicity_out[ 95: 88] - 8'h1;
	   if(      inc_det[12] && !dec_det[12] ) multiplicity_out[103: 96] <= multiplicity_out[103: 96] + 8'h1;
	   else if( dec_det[12] && !inc_det[12] ) multiplicity_out[103: 96] <= multiplicity_out[103: 96] - 8'h1;
	   if(      inc_det[13] && !dec_det[13] ) multiplicity_out[111:104] <= multiplicity_out[111:104] + 8'h1;
	   else if( dec_det[13] && !inc_det[13] ) multiplicity_out[111:104] <= multiplicity_out[111:104] - 8'h1;
	   if(      inc_det[14] && !dec_det[14] ) multiplicity_out[119:112] <= multiplicity_out[119:112] + 8'h1;
	   else if( dec_det[14] && !inc_det[14] ) multiplicity_out[119:112] <= multiplicity_out[119:112] - 8'h1;
	   if(      inc_det[15] && !dec_det[15] ) multiplicity_out[127:120] <= multiplicity_out[127:120] + 8'h1;
	   else if( dec_det[15] && !inc_det[15] ) multiplicity_out[127:120] <= multiplicity_out[127:120] - 8'h1;

		if(      inc_det[16] && !dec_det[16] ) multiplicity_out[135:128] <= multiplicity_out[135:128] + 8'h1;
	   else if( dec_det[16] && !inc_det[16] ) multiplicity_out[135:128] <= multiplicity_out[135:128] - 8'h1;
	   if(      inc_det[17] && !dec_det[17] ) multiplicity_out[143:136] <= multiplicity_out[143:136] + 8'h1;
	   else if( dec_det[17] && !inc_det[17] ) multiplicity_out[143:136] <= multiplicity_out[143:136] - 8'h1;
	   if(      inc_det[18] && !dec_det[18] ) multiplicity_out[151:144] <= multiplicity_out[151:144] + 8'h1;
	   else if( dec_det[18] && !inc_det[18] ) multiplicity_out[151:144] <= multiplicity_out[151:144] - 8'h1;
	   if(      inc_det[19] && !dec_det[19] ) multiplicity_out[159:152] <= multiplicity_out[159:152] + 8'h1;
	   else if( dec_det[19] && !inc_det[19] ) multiplicity_out[159:152] <= multiplicity_out[159:152] - 8'h1;
	   if(      inc_det[20] && !dec_det[20] ) multiplicity_out[167:160] <= multiplicity_out[167:160] + 8'h1;
	   else if( dec_det[20] && !inc_det[20] ) multiplicity_out[167:160] <= multiplicity_out[167:160] - 8'h1;
	   if(      inc_det[21] && !dec_det[21] ) multiplicity_out[175:168] <= multiplicity_out[175:168] + 8'h1;
	   else if( dec_det[21] && !inc_det[21] ) multiplicity_out[175:168] <= multiplicity_out[175:168] - 8'h1;
	   if(      inc_det[22] && !dec_det[22] ) multiplicity_out[183:176] <= multiplicity_out[183:176] + 8'h1;
	   else if( dec_det[22] && !inc_det[22] ) multiplicity_out[183:176] <= multiplicity_out[183:176] - 8'h1;
	   if(      inc_det[23] && !dec_det[23] ) multiplicity_out[191:184] <= multiplicity_out[191:184] + 8'h1;
	   else if( dec_det[23] && !inc_det[23] ) multiplicity_out[191:184] <= multiplicity_out[191:184] - 8'h1;
	   if(      inc_det[24] && !dec_det[24] ) multiplicity_out[199:192] <= multiplicity_out[199:192] + 8'h1;
	   else if( dec_det[24] && !inc_det[24] ) multiplicity_out[199:192] <= multiplicity_out[199:192] - 8'h1;
	   if(      inc_det[25] && !dec_det[25] ) multiplicity_out[207:200] <= multiplicity_out[207:200] + 8'h1;
	   else if( dec_det[25] && !inc_det[25] ) multiplicity_out[207:200] <= multiplicity_out[207:200] - 8'h1;
	   if(      inc_det[26] && !dec_det[26] ) multiplicity_out[215:208] <= multiplicity_out[215:208] + 8'h1;
	   else if( dec_det[26] && !inc_det[26] ) multiplicity_out[215:208] <= multiplicity_out[215:208] - 8'h1;
	   if(      inc_det[27] && !dec_det[27] ) multiplicity_out[223:216] <= multiplicity_out[223:216] + 8'h1;
	   else if( dec_det[27] && !inc_det[27] ) multiplicity_out[223:216] <= multiplicity_out[223:216] - 8'h1;
	   if(      inc_det[28] && !dec_det[28] ) multiplicity_out[231:224] <= multiplicity_out[231:224] + 8'h1;
	   else if( dec_det[28] && !inc_det[28] ) multiplicity_out[231:224] <= multiplicity_out[231:224] - 8'h1;
	   if(      inc_det[29] && !dec_det[29] ) multiplicity_out[239:232] <= multiplicity_out[239:232] + 8'h1;
	   else if( dec_det[29] && !inc_det[29] ) multiplicity_out[239:232] <= multiplicity_out[239:232] - 8'h1;
	   if(      inc_det[30] && !dec_det[30] ) multiplicity_out[247:240] <= multiplicity_out[247:240] + 8'h1;
	   else if( dec_det[30] && !inc_det[30] ) multiplicity_out[247:240] <= multiplicity_out[247:240] - 8'h1;
	   if(      inc_det[31] && !dec_det[31] ) multiplicity_out[255:248] <= multiplicity_out[255:248] + 8'h1;
	   else if( dec_det[31] && !inc_det[31] ) multiplicity_out[255:248] <= multiplicity_out[255:248] - 8'h1;
	end
end

endmodule
