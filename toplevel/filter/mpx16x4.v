module mpx16x4(
   input wire  [1:0]  sel,     input  wire    enable,
   input wire [63:0] data_in,  output reg [15:0] mpx
);

always @ * begin
  if (enable) begin
    case (sel)
	 0: mpx <= data_in[15: 0];
	 1: mpx <= data_in[31:16];
	 2: mpx <= data_in[47:32];
	 3: mpx <= data_in[63:48];
	 endcase
  end else mpx <= 16'h0;
end

endmodule

  

