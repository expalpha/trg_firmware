//////////////////////////////////////////////////////////////////////////////////////////////////////////
// Store compact data packets in a 512 X 256-bit memory.  512 deep circular buffer can hold ...
//    92 microseconds of backlog at the full link speed.
//   160 microseconds at the maximum design count rate ( 3.2 M-events/sec)
// maximum allowable skew in the data arrival is set to 80 microseconds (cannot fail even at full link speed)
// ERROR TESTS:
//    1) Checksum  2) Late arrival: Time stamp of incoming events must be no later than 81.91 microseconds
//                                   away from the readback time (2**(14 bits-1) units of 10 nanoseconds.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
module cdbuffer ( // incoming data is on clk
   input  wire clk,  input wire reset,  input wire RUN, // Run not used
	input   wire  [31:0] data_in,  input wire  ready_in,  output wire ack_out,
	output  wire  [31:0] data_out, output wire ready_out, input  wire ack_in,
   input  wire  [15:0] runtime,       // time stamp for reading back
   input  wire   [3:0] portID,        // the physical ID of the link port
   input  wire   [9:0] rdaddress,     //
   input  wire         pkt_ptr_ack,   // Acknowledge the output request
   output reg          pkt_ptr_ready, //
   output reg   [47:0] pkt_ptr,       // address where compact buffer is stored, plus timestamp, IDs and flags
   output wire [511:0] data_pkt,      // Packet read back from memory 
   output wire         err_cksum,
   output reg          err_late,
	input  wire         unfiltered,    // enable passthrough of incoming unfiltered data
	input  wire   [5:0] ge_address     // for channel<->clover-number conversion

);	
reg [15:0] cksum;  reg cksumerror; // currently unused ...
assign err_cksum = 1'h0;
wire run_start = RUN && !run_reg;  reg run_reg; always @(posedge clk) run_reg <= RUN;  // oneshot

always @ (posedge clk or posedge reset) begin // *** on clk_ddr ** Set/Clr ready to the multiplexer
    if(                         reset ) pkt_ptr_ready <= 1'b0;
	 //else if ( pkt_ptr_ack || err_late ) pkt_ptr_ready <= 1'b0;
	 else if ( pkt_ptr_ack ) pkt_ptr_ready <= 1'b0;
    else                                pkt_ptr_ready <= (packet_ready) ? 1'b1 : pkt_ptr_ready;
end

////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////          Buffer memories (compact data packet and headers)           ///////////////////
reg [9:0] wraddress;
always @ (posedge clk or posedge reset) begin
    if      ( reset        ) wraddress <= 10'h0;
	 else if ( packet_ready ) wraddress <= wraddress + 1'b1;
	 else                     wraddress <= wraddress;
end

reg [511:0] packet_in;
mem1024X256	/* now 512 */ packet_mem ( .clock( clk ), .wren( packet_ready ), 
	.data( packet_in ), .wraddress( wraddress ), .q( data_pkt ), .rdaddress( rdaddress )
);
////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////          Fill Packet buffer using data from the link           //////////////////////
// -------------------- compact packet format ---------------- | ---------- packet pointer format ---------
// Hdr  :  1stPktFlag(1), Spare(3), PktType(4), #RawPackets(8) | 47-40 packets <= Hdr[7:0]   // #pkts of raw data
// 15: 0:  DetectorType(4), Spare(1), DetectorNumber(11)       | 35-34 PortID   39-36:used later[prescale,last-stamp,X,X]
// 31:16:  Time Stamp Low Bits(16)            10 nsec units    | 33-24 pointer <= wraddress  // Start of block
// 47:32:  TimeStamp Vernier(4), FE flags(2), HitNumber(10)    | 23-20 dettype <= pkt[15:12] // detector ID
// 63:48:  Time Stamp[47:32]                                   | 19-18 FEflags <= pkt[43:42]
// 79:64:  Time Stamp[41:16]                                   | 17- 4 stamp   <= pkt[31:16] // low bits [10 ns]
// 95:80:  Charge/Gain info  ....                              |  3- 0 vernier <= pkt[47:44] // fine time stamp
// Trlr :  Checksum complement                grifhdr:8-Num[4]-Digi[3]-NPileup[3]-Addr[4:4:6]-Det[4]
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// griffin format ...
//   0- 31 0x8 Hdr
//         0x? ppg
//         0x? ID
//  32- 63 0x? Pattern
//  64- 95 0x9 trignum
//  96-127 0xA tstmp
// 128-159 0xB dtime,5'h0,tstmphi
//         0xC
// 160-191 0x?
// 192-223 0x?
// 224-255 0xE
////////////////////////// passthrough buffer for unfiltered data  ///////////////////////////////////
wire passbuf_full;  wire passbuf_empty;
//dual_clk_fifo evbuf ( .aclr(reset||run_start),   // evfifo 4k words - will hold 80us sample!
//   .wrclk(clk),  .data(data_in),  .wrfull(passbuf_full),    .wrreq( ack_out && unfiltered ),
//   .rdclk(clk),  .q(data_out),    .rdempty(passbuf_empty),  .rdreq(ack_in)
//);
sczlfifo evbuf ( .clk(clk),  .aclr(reset||run_start),   //  4k words - will hold 80us sample!
   .data(data_in),  .wrreq( ack_out && unfiltered ), .full(passbuf_full),
   .q(data_out),    .rdreq(ack_in),                  .empty(passbuf_empty)
);
reg prv_empty; always @(posedge clk) prv_empty <= passbuf_empty; // wait for 1 more clk of not empty
assign ready_out = !passbuf_empty && !prv_empty;             // q,rdempty do not change quickly enough?

// new dcc ack was !outbufful && ready
assign ack_out = ready_in && !passbuf_full;

wire [511:0] packet_init = {16{32'hfadffadf}};

reg [1:0] state;  reg prevack;  parameter wait_start=2'h0, mid_packet=2'h1, error=2'h2; // on error wait for new event
reg [2:0] qt_count;  reg hdr_part2;  reg packet_ready;  // grab data words as they come in + flag when complete event is ready
always @ (posedge clk or posedge reset) begin 
   if ( reset ) begin
	   packet_ready <= 1'b0;  state <= wait_start;  packet_in <= packet_init;  qt_count  <= 3'h0;  hdr_part2 <= 1'b0;
   end else begin	 
	   packet_ready <= 1'b0;  state <=      state;  qt_count <= qt_count;                          hdr_part2 <= 1'b0;
		packet_in <= ( state == wait_start ) ? packet_init : packet_in;  
      if( ready_in && !passbuf_full ) begin // full speed fifo - don't have to wait
		   if(  state == wait_start ) state <= ( data_in[31:28] == 4'h8 )  ? mid_packet :      error;
			else if ( state == error ) state <= ( data_in[31:28] == 4'hE )  ? wait_start :      error;
			else /*state=mid_packet*/  state <= ( data_in[31:28] == 4'hE )  ? wait_start : mid_packet;
			if( data_in[31] == 1'b1 ) begin
            case ( data_in[31:28] )
		       4'h8: begin packet_in[ 31:  0] <= data_in; qt_count <= 3'h0; hdr_part2 <= 1'b1; end
				 //                   [ 63: 32] - for ppg/filter // can't do this word
				 //                   [ 95: 64] - for mst-filt-id
		       4'h9:       packet_in[127: 96] <= data_in;
		       4'hA:       packet_in[159:128] <= data_in;
		       4'hB:       packet_in[191:160] <= data_in;
		       4'hD:       packet_in[223:192] <= data_in; // added recently?
		       4'hC:       packet_in[415:224] <= {data_in,packet_in[383:224]}; // room for 6 words (12 samples)
		       4'hE: begin packet_in[511:480] <= data_in;  packet_ready <= ( state == error ) ? 1'b0 : 1'b1; end
		       //default: ; // error bit
		      endcase
			end else if( hdr_part2 == 1'b1 ) begin // word following header (if present)
		      packet_in[ 63: 32] <= data_in;      // if not present, will probably be 0x9 after header
			end else begin
			   qt_count <= qt_count + 3'h1; // E,T,  E,T,  E,T,  ...
				case (qt_count)
				4'h4: packet_in[319:288] <= data_in; // energy 3
				4'h5: packet_in[351:320] <= data_in; // cfd 3
				4'h2: packet_in[383:352] <= data_in; // energy 2
				4'h3: packet_in[415:384] <= data_in; // cfd 2
				4'h0: packet_in[447:416] <= data_in; // energy
				4'h1: packet_in[479:448] <= data_in; // cfd
//				default: // 7,8: too many words
            endcase
			end
		end
	end
end

wire  [7:0] cdb_packets =             8'h0; // number of packets for raw data 
wire  [3:0] dettype     = packet_in[  3: 0]; // detector ID
//wire [15:0] tstamp    = packet_in[111:96]; // Time stamp low bits, 10 nsec units - 111:96 is 0x9 word??
wire [15:0] tstamp      = packet_in[143:128]; // Time stamp low bits, 10 nsec units
wire  [3:0] vernier     = 4'h0; //packet_in[47:44]; // fine time stamp
wire  [3:0] FEflags;            //packet_in[43:42];

wire [3:0] Ge_HighGain = 4'h1;   wire [3:0] Ge_BGO = 4'h7;
wire [7:0] digitiser_chan = packet_in[11: 4];
wire [3:0] grifc_port     = packet_in[15:12];
wire [3:0] master_port    = packet_in[19:16];
wire [3:0] clover_num = {grifc_port[1:0],digitiser_chan[3:2]};   // 0,1,2,3 for grif16chan 0-16
wire [3:0] bgo_num    = {grifc_port[1:0],digitiser_chan[3:2]};   // same as ge for now
wire chan_is_ge  = ({master_port,grifc_port[3:2]} ==  ge_address      );
wire chan_is_bgo = ({master_port,grifc_port[3:2]} == (ge_address+6'h1)); // assumes bgo in next block to ge

// can select master_port, also upper 2 bits of grifc_port with 6bit parameter
// => Ge grif16s need to be consecutive and in ports 0-3 4-7 8-11 or 12-15 of any grifc slave
// order is not really relevant as specific clover#s can be switched without any effect
assign FEflags = ( (dettype == Ge_HighGain) && chan_is_ge  ) ? clover_num :
                 ( (dettype == Ge_BGO     ) && chan_is_bgo ) ?    bgo_num : 4'h0;

always @ (posedge clk or posedge reset) begin // register new packet pointer when ready
   if( reset ) pkt_ptr <= 48'h0;
   else        pkt_ptr <= (!packet_ready) ? pkt_ptr : {cdb_packets, 2'h0, FEflags[3:2], portID[1:0], wraddress, dettype, FEflags[1:0], tstamp[13:0], vernier};
   //else        pkt_ptr <= (!packet_ready) ? pkt_ptr : {8'h0, 4'h0,  portID[1:0], wraddress, packet_in[15:12], packet_in[43:42], packet_in[29:16], packet_in[47:44]};
end

// If bits 15, 14 or 13 of timedif are high, the event is late and should not be stored
wire [15:0] timedif = ( tstamp == 16'h0 ) ? 16'h0 : tstamp - runtime;
always @ (posedge clk) err_late <= 1'b0; //( timedif[15] || (timedif <16'h0080) );

endmodule
