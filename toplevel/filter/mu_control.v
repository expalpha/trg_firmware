// Control read and write of the queue and coincwin FIFOs - CoincWindow is between queue.Q (not included) and coincwin.Q
//    Both are registered - visible data is not in fifo (if add/remove 1 item at time - usually empty+zero words + last item on Q)
// Read queued events into coincwin until next one is outside window (or queue is empty and coincwin events timeout)
//    then remove events from coincwin, until that next queued event comes within window (or if empty+timeout remove all events)
// Each read of coincwin while Q is valid, pops an event.  Last event is popped when it times out (+then Q set invalid)
//    (First read of coincwin is done immediately - Pull is only idle[=0] when all empty)

module mu_control(
   input wire clk,   input wire reset,  input wire [13:0] coinc_width,   input wire pipe_empty,   output wire timeout,
   input wire  [7:0] coinc_words, input wire coinc_empty, input wire [7:0]  queue_words,  input wire queue_empty,
   input wire [13:0] tstamp_in/* to write in m_FIFO */,   input wire [13:0] tstamp_out/* at exit of m_FIFO */,  input wire last_stamp,    
   output wire coinc_read,   output wire queue_read,      output wire coinc_add, output reg mu_strobe,
	output reg RegValid
); // (queue empty similar to queue_words==0 [delayed 1clk] except when fifo full!)
always @ (posedge clk) mu_strobe <= RegValid & (coinc_read | all_done); // (will be no new coinc_read if all done)
wire out_window = (tstamp_in - tstamp_out) >= coinc_width; // register if possible - poor timing
                                                           // used twice just below and once more near eof
//wire [13:0] window = coinc_width - 14'b1;  // NOTE: put multicycle in SDC file
//wire [13:0] timedif = tstamp_in - tstamp_out;
//wire     out_window = timedif > window;
wire   window_avail = (!out_window) | ((coinc_words == 0) & (PullState == 2'h0)); // room for next event or empty

reg [13:0] toutcnt; // time out counter, [10bit i.e. Max range : 5.12 microseconds for ddrclk]
always @ (posedge clk or posedge reset) begin // counts up to coinc_width, when pipe_empty[queue+ms+ll]
   if (reset)                                      toutcnt <= 14'h0;
   else if (pipe_empty && (toutcnt < coinc_width)) toutcnt <= toutcnt + 14'b1;
	else if (pipe_empty)                            toutcnt <= toutcnt;
	else                                            toutcnt <= 14'h0;
end
assign timeout = (toutcnt == coinc_width);

wire all_done = (coinc_empty & queue_empty & pipe_empty & timeout);
always @ (posedge clk) begin
   if      (reset)                            RegValid <= 1'b0;  // registered output of coincwin fifo
	else if (PullState == 2'h0 & ~coinc_empty) RegValid <= 1'b1;
	else if (all_done                        ) RegValid <= 1'b0;
	else                                       RegValid <= RegValid;
end
//    "Queue" state machine
// QueueState == 0: Queue is empty, Queue.Q not valid
// QueueState == 1: delay one clock after queue_read for Q to become valid
// QueueState == 2: Queue.Q valid (copy to coincwin (coinc_add) as soon as any newly out of window events popped)
//                  After copying to coincwin - either queue is empty[->state0], or repeat
assign queue_read = ~queue_empty && ((FillState == 2'h0) | (FillState == 2'h2 & window_avail));
assign coinc_add  =                                        (FillState == 2'h2 & window_avail);
reg [1:0] FillState;
always @ (posedge clk) begin
   if (reset)     FillState <= 2'h0;
	else begin
	  case (FillState)
	  2'h0: if (~queue_empty)            FillState <= 2'h1;
	  2'h1:                              FillState <= 2'h2;
	  2'h2: if (coinc_add & queue_empty) FillState <= 2'h0; 
//	        else if (PullState == 2'h0)  FillState <= 2'h1; // should be !=2?
	        else if (PullState != 2'h2)  FillState <= 2'h1; // PullState=1 until outwindow
			  else                         FillState <= 2'h2;
	  default:                           FillState <= 2'h0;
	  endcase
   end
end
//   "CoincWin" state machine - pulls one time slot worth of elements out of the mu_fifo once the coinc-win is full
// 0:  idle state - nothing in the window [read immediately it comes non empty and go to state1]
// 1:  events in window - new events auto-added while within window width, 1st event outwindow - goto state2
//        if no events come, and current ones timeout, go to state0
//           (check timeout emptying happens correctly with no new data, and with new data while emptying)
// 2:  no more events added from queue till all too-early-events dropped - so drop them (strobe each event as it's dropped)
assign coinc_read = ((PullState == 2'h0) & ~coinc_empty) | (PullState == 2'h2 & out_window & (coinc_words != 0)); // drop from coincwin
reg [1:0] PullState;
always @ (posedge clk) begin
   if (reset) PullState <= 2'h0;
	else begin // originally waited for last-stamp before exiting state 2 or state1->0 but gives hang if last-stamp not set
	  case (PullState) // seems to imply some events are dropped or mis-ordered?
	  2'h0: PullState <= (~coinc_empty) ? 2'h1 : 2'h0;
	  2'h1: if (pipe_empty & timeout) PullState <= 2'h0;
	        else if (out_window)      PullState <= 2'h2;
			  else                      PullState <= 2'h1;
	  2'h2: if ( coinc_words == 0 )   PullState <= 2'h0; // hangs in this state, when outwin=1 (winavail=0)
           //else if ( last_stamp )    PullState <= 2'h1; // and queue.q[38]==0, and coinc_words==0
			  else                      PullState <= 2'h2;
	  default: PullState <= 2'h0;
	  endcase
   end
end
// last-stamp should usually be set - except briefly during rare timeslots with multiple events
// coincwin.q[38] never clear at same time as regValid - as expected
//    queue.q[38] sometimes clear just before regValid rises
//  **But** see rare trigger on both clear - causes the hang
endmodule
