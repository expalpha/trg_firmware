/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Buffers to store compact data packets during the time stamp sequencing process
//    Storage requirements inc BGO, but no SUPERPACES (From GRIFFIN DAQ Arch feb13 garns)
//       4 ports at 6 Gbit/sec  [600Mbytes/s] 32bytes/event => Max 18.75events/us/link
//       Sustained data rates:  Most active port : 144 Mbytes/sec = 4.5events/us
//                              Slowest port     :  32 MBytes/sec
//                              Total            : 420 MBytes/sec = ~15 total/us
//    Max Time skew for burst of full occupancy data ... []
//       In Grif16 assuming slower 1G links [100Mbyte/s, 3.125 evt/us] =>   5us to send  16 events
//       In Grifc, merging 16 1G links to single 6G link [18.75evt/us] => ~15us to send 256 events
//    grif16 and grifc send in parallel => maximum time skew for simultaneous events in 15us
// Allow enough storage for 60us at full link speed (256x4 events of 32bytes) - one buf per link
//    A "compact data packet pointer" to the location of these events is calculated when storing them
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ----------------- compact data packet format -------------- | ---------- packet pointer format ---------
// Hdr  :  1stPktFlag(1), Spare(3), PktType(4), #RawPackets(8) | [46:40] <= Hdr[7:0]   // #pkts of raw data
// 15: 0:  DetectorType(4), Spare(1), DetectorNumber(11)       | [35:34] <= Port       // #port
// 31:16:  Time Stamp Low Bits(16)            10 nsec units    | [33:24] <= wraddress  // Start of block
// 47:32:  TimeStamp Vernier(4), FE flags(2), HitNumber(10)    | [23:20] <= pkt[15:12] // detector ID
// 63:48:  Time Stamp[47:32]                                   | [19:18] <= pkt[43:42] // FEflags
// 79:64:  Time Stamp[41:16]                                   | [17: 4] <= pkt[31:16] // timestamp [10 ns]
// 95:80:  Charge/Gain info  ....                              | [ 3: 0] <= pkt[47:44] // fine time stamp
// Trlr :  Checksum complement   Store 32 bytes of this (1-16) |    14bit timestamp  =>  range = 16.4 us
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//  MstPort  Detector types [type5=reserved]                    also ptr bit 39 is prescale?  38-36:reserved
//     0     0:BGO 
//     1     1:HPGE     [64 crystals x2gains -  8xGRIF16]
//     2     2:SCEPTAR  [20 plastics         -  5xGRIF4G]
//           3:DANTE    [8xE + 8xShields     -  1xGRIF16]
//           4:DANTE-TAC[8xT                 -  1xGRIF16]
//           6:PACES Si [5xE                 -  1xGRIF16]
//     3     7:DESCANT  [72xNeutron          - 18xGRIF4G]
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
module compact_data_buffers (       input  wire [15:0] run_time,       input wire unfiltered,  input wire [5:0] ge_address,
   input  wire clk,                 input  wire        reset,          input wire        RUN,                  
	input  wire [127:0] data_in,     input  wire  [3:0] data_ready_in,  output wire [3:0] data_ack_out, // 4 links in atm ...
	output wire [127:0] data_out,    output wire  [3:0] data_ready_out, input  wire [3:0] data_ack_in, // 4 links out atm ...
   output reg   [47:0] cdb_ptr,     output wire        cdb_ptr_rdy,    output wire [3:0] cksum_error, output wire [3:0] late_error,
   input  wire  [47:0] readbk_ptr,  output wire [511:0] readbk_pkt_out // no pkt_ready signal - continuous readback
);

// multiplex datapacket readback from the buffers (these have been stored for a while)
//    delay pkt/hdr sel by two clks (to allow for readback memory latency)
wire [2047:0] readbk_pkt;  reg [1:0] readbk_port_dly; reg [1:0] readbk_port;
always @ (posedge clk) begin readbk_port_dly <= readbk_port; readbk_port <= readbk_ptr[35:34]; end
assign readbk_pkt_out = ( readbk_port_dly == 2'h0 ) ? readbk_pkt[511:  0] :
                        ( readbk_port_dly == 2'h1 ) ? readbk_pkt[1023:512] :
                        ( readbk_port_dly == 2'h2 ) ? readbk_pkt[1535:1024] : readbk_pkt[2047:1536];

// multiplex packet pointers to filter (available immediately from data as it comes in)
// do a fair selection - do not favour any particular port
reg [3:0] pkt_ptr_ack;  reg [1:0] port; assign cdb_ptr_rdy = |(pkt_ptr_ack);
always @ (posedge clk) begin
   if( reset ) begin
	   pkt_ptr_ack <= 4'h0;   cdb_ptr <=   48'h0;   port <= 2'h0;
	end else begin
	   pkt_ptr_ack <= 4'h0;   cdb_ptr <= cdb_ptr;   port <= port + 2'h1;
	   case (port)
	   2'h0: if( pkt_ptr_rdy[0] ) begin pkt_ptr_ack[0] <= 1'b1; cdb_ptr <= pkt_ptr[ 47:  0]; end
	   2'h1: if( pkt_ptr_rdy[1] ) begin pkt_ptr_ack[1] <= 1'b1; cdb_ptr <= pkt_ptr[ 95: 48]; end
	   2'h2: if( pkt_ptr_rdy[2] ) begin pkt_ptr_ack[2] <= 1'b1; cdb_ptr <= pkt_ptr[143: 96]; end
	   2'h3: if( pkt_ptr_rdy[3] ) begin pkt_ptr_ack[3] <= 1'b1; cdb_ptr <= pkt_ptr[191:144]; end
	   endcase
	end
end

// ------------------------------------------------------------------------------------------------
// 4 compact data buffer memories; 32 bytes X 1024, one for each incoming data link
wire [3:0] pkt_ptr_rdy;  wire [191:0] pkt_ptr;
cdbuffer cdb_buf0 (
   .clk(clk),  .reset(reset), .RUN(RUN),  .runtime(run_time),  .portID(4'h0),  .unfiltered(unfiltered),  .ge_address(ge_address),
   .rdaddress(readbk_ptr[33:24]),  .data_pkt(readbk_pkt[511:0]),
   .data_in(data_in[31: 0]),       .ready_in(data_ready_in[0]),     .ack_out(data_ack_out[0]),
   .data_out(data_out[31: 0]),     .ready_out(data_ready_out[0]),   .ack_in(data_ack_in[0]),
   .pkt_ptr(pkt_ptr[47:0]),        .pkt_ptr_ready(pkt_ptr_rdy[0]),  .pkt_ptr_ack(pkt_ptr_ack[0]), // 40bit pointer - pkt bufaddr/time/id
   .err_cksum(cksum_error[0]),     .err_late(late_error[0])                   
);
cdbuffer cdb_buf1 (
   .clk(clk),  .reset(reset), .RUN(RUN),  .runtime(run_time),  .portID(4'h1),  .unfiltered(unfiltered),  .ge_address(ge_address),
   .rdaddress(readbk_ptr[33:24]),  .data_pkt(readbk_pkt[1023:512]),
   .data_in(data_in[63:32]),       .ready_in(data_ready_in[1]),     .ack_out(data_ack_out[1]),
   .data_out(data_out[63:32]),     .ready_out(data_ready_out[1]),   .ack_in(data_ack_in[1]),
   .pkt_ptr(pkt_ptr[95:48]),       .pkt_ptr_ready(pkt_ptr_rdy[1]),  .pkt_ptr_ack(pkt_ptr_ack[1]),
   .err_cksum(cksum_error[1]),     .err_late(late_error[1])                   
);
cdbuffer cdb_buf2 (
   .clk(clk),  .reset(reset), .RUN(RUN),  .runtime(run_time),  .portID(4'h2),  .unfiltered(unfiltered),  .ge_address(ge_address),
   .rdaddress(readbk_ptr[33:24]),  .data_pkt(readbk_pkt[1535:1024]),
   .data_in(data_in[95:64]),       .ready_in(data_ready_in[2]),     .ack_out(data_ack_out[2]),
   .data_out(data_out[95:64]),     .ready_out(data_ready_out[2]),   .ack_in(data_ack_in[2]),
   .pkt_ptr(pkt_ptr[143:96]),      .pkt_ptr_ready(pkt_ptr_rdy[2]),  .pkt_ptr_ack(pkt_ptr_ack[2]),
   .err_cksum(cksum_error[2]),     .err_late(late_error[2])                   
);
cdbuffer cdb_buf3 (
   .clk(clk),  .reset(reset), .RUN(RUN),  .runtime(run_time),  .portID(4'h3),  .unfiltered(unfiltered),  .ge_address(ge_address),
   .rdaddress(readbk_ptr[33:24]),  .data_pkt(readbk_pkt[2047:1536]),
   .data_in(data_in[127:96]),      .ready_in(data_ready_in[3]),     .ack_out(data_ack_out[3]),
   .data_out(data_out[127:96]),    .ready_out(data_ready_out[3]),   .ack_in(data_ack_in[3]),
   .pkt_ptr(pkt_ptr[191:144]),     .pkt_ptr_ready(pkt_ptr_rdy[3]),  .pkt_ptr_ack(pkt_ptr_ack[3]),
   .err_cksum(cksum_error[3]),     .err_late(late_error[3])                   
);

endmodule	
