module testmultiv(
input wire [255:0] mv,  // requested multiplicity vectors
input wire [255:0] mu,  // window multiplicity array
input wire     enable,  // bit pattern to select from the 16 vectors
output wire     andOK  // AND condition of multiplicity selections satisfied
);

//expand mu
wire [63:0] m;   // block of 8 values of 8 bits of detector multiplicities
wire [63:0] mpre;// flagged with preset bit
wire [63:0] mf1; // flagged with flag1 bit
wire [63:0] mf2;
assign {mf2,mf1,mpre,m} = mu;

//expand mv
wire [63:0] mvv;   // block of 8 values of 8 bits of detector multiplicities
wire [63:0] mvpre;// flagged with preset bit
wire [63:0] mvf1; // flagged with flag1 bit
wire [63:0] mvf2;
assign {mvf2,mvf1,mvpre,mvv} = mv;

// generate the multiplicity match / exceed pattern
// expand
wire [7:0] d0m;   // detector multiplicity in window
wire [7:0] d1m;
wire [7:0] d2m;
wire [7:0] d3m;
wire [7:0] d4m;
wire [7:0] d5m;
wire [7:0] d6m;
wire [7:0] d7m;

wire [7:0] v0m;   // requested multiplicity in window
wire [7:0] v1m;
wire [7:0] v2m;
wire [7:0] v3m;
wire [7:0] v4m;
wire [7:0] v5m;
wire [7:0] v6m;
wire [7:0] v7m;

wire [7:0] dok; // bit pattern for the multiplicity match for unflagged detector
wire [7:0] dnz; // bit pattern of non zero elements of the requested multiplicity vector
wire    andOKm; // condition satisfied for unflagged detector

assign {d7m,d6m,d5m,d4m,d3m,d2m,d1m,d0m} = m;
assign {v7m,v6m,v5m,v4m,v3m,v2m,v1m,v0m} = mvv;

assign dok  = {(d7m >=v7m ),(d6m >=v6m ),(d5m >=v5m ),(d4m >=v4m ),
              (d3m >=v3m ),(d2m >=v2m ),(d1m >=v1m ),(d0m >=v0m )};
assign dnz  = {(d7m != 8'h0 ),(d6m != 8'h0 ),(d5m != 8'h0 ),(d4m != 8'h0 ),
              (d3m != 8'h0 ),(d2m != 8'h0 ),(d1m != 8'h0 ),(d0m != 8'h0 )};
				  
assign andOK = ((dok & dnz) == dnz);   // NOTE: if dnz == 0, andOK = 1 (pass all) 

endmodule
