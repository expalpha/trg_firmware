// now twice as many conditions, AND each condition now has twice #dettypes
//   currently allow 14 conditions - when satisfied, save word_count, and count it down as pkts removed
module coincidence_check (             output wire [15:0]   coinc_ok,
   input wire                  clk,    input wire              reset,
	input wire           use_strict,    input wire   [3:0]   det_type,
	input wire  [255:0] multiplicity,   input  wire  [7:0] word_count,   input wire ptr_ready,
	input wire [2047:0]   conditions,   input wire  [31:0] clover_mult 
);
// parameters [1919:1792] and [2047:1920] are for unused 15th and 16th conditions
// passed, nonstrict, strict [15:14] are corresponding unused flags

// ** There are 2 unused prescale flags: conditions[127:126] which are used to set clover-mult and BGO-suppression
// if these are set, the original Ge multiplicity is replaced with one of the clover_mult multiplicities for that condition

// pkt_count[8*X+7:8*X] is #events still in window that were there when condition X was passed
// i.e. if this count is not zero, the next event out is considered to have passed condition X
assign coinc_ok[ 0] = pkt_count[  7:  0] != 8'h0;
assign coinc_ok[ 1] = pkt_count[ 15:  8] != 8'h0;
assign coinc_ok[ 2] = pkt_count[ 23: 16] != 8'h0;
assign coinc_ok[ 3] = pkt_count[ 31: 24] != 8'h0;
assign coinc_ok[ 4] = pkt_count[ 39: 32] != 8'h0;
assign coinc_ok[ 5] = pkt_count[ 47: 40] != 8'h0;
assign coinc_ok[ 6] = pkt_count[ 55: 48] != 8'h0;
assign coinc_ok[ 7] = pkt_count[ 63: 56] != 8'h0;
assign coinc_ok[ 8] = pkt_count[ 71: 64] != 8'h0;
assign coinc_ok[ 9] = pkt_count[ 79: 72] != 8'h0;
assign coinc_ok[10] = pkt_count[ 87: 80] != 8'h0;
assign coinc_ok[11] = pkt_count[ 95: 88] != 8'h0;
assign coinc_ok[12] = pkt_count[103: 96] != 8'h0;
assign coinc_ok[13] = pkt_count[111:104] != 8'h0;
assign coinc_ok[14] = pkt_count[119:112] != 8'h0;
assign coinc_ok[15] = pkt_count[127:120] != 8'h0;

wire [15:0] nonstrict; wire [15:0] strict;  wire [15:0] prescale_passed;
assign {nonstrict[15:14],strict[15:14],prescale_passed[15:14]} = 6'h0;   // unused conditions
compare_14x8bit condition0  ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[ 127:   0]), .result(nonstrict[ 0]), .result2(strict[ 0]) );
compare_14x8bit condition1  ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[ 255: 128]), .result(nonstrict[ 1]), .result2(strict[ 1]) );
compare_14x8bit condition2  ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[ 383: 256]), .result(nonstrict[ 2]), .result2(strict[ 2]) );
compare_14x8bit condition3  ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[ 511: 384]), .result(nonstrict[ 3]), .result2(strict[ 3]) );
compare_14x8bit condition4  ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[ 639: 512]), .result(nonstrict[ 4]), .result2(strict[ 4]) );
compare_14x8bit condition5  ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[ 767: 640]), .result(nonstrict[ 5]), .result2(strict[ 5]) );
compare_14x8bit condition6  ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[ 895: 768]), .result(nonstrict[ 6]), .result2(strict[ 6]) );
compare_14x8bit condition7  ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[1023: 896]), .result(nonstrict[ 7]), .result2(strict[ 7]) );
compare_14x8bit condition8  ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[1151:1024]), .result(nonstrict[ 8]), .result2(strict[ 8]) );
compare_14x8bit condition9  ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[1279:1152]), .result(nonstrict[ 9]), .result2(strict[ 9]) );
compare_14x8bit condition10 ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[1407:1280]), .result(nonstrict[10]), .result2(strict[10]) );
compare_14x8bit condition11 ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[1535:1408]), .result(nonstrict[11]), .result2(strict[11]) );
compare_14x8bit condition12 ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[1663:1536]), .result(nonstrict[12]), .result2(strict[12]) );
compare_14x8bit condition13 ( .mult_in(multiplicity[127:0]), .clover_mult(clover_mult), .dtype(det_type), .req(conditions[1791:1664]), .result(nonstrict[13]), .result2(strict[13]) );

wire [15:0] passed = (use_strict) ? strict : nonstrict;

// the upper 16 8-bit multiplicities (255:128) are for prescaled events
wire [15:0] prescale_active /* synthesis keep */ = {
   (multiplicity[255:248] != 8'h0),(multiplicity[247:240] != 8'h0),
   (multiplicity[239:232] != 8'h0),(multiplicity[231:224] != 8'h0),
   (multiplicity[223:216] != 8'h0),(multiplicity[215:208] != 8'h0),
   (multiplicity[207:200] != 8'h0),(multiplicity[199:192] != 8'h0),
   (multiplicity[191:184] != 8'h0),(multiplicity[183:176] != 8'h0),
   (multiplicity[175:168] != 8'h0),(multiplicity[167:160] != 8'h0),
   (multiplicity[159:152] != 8'h0),(multiplicity[151:144] != 8'h0),
   (multiplicity[143:136] != 8'h0),(multiplicity[135:128] != 8'h0)};

// prescale_active is bitmask of detector types passing prescale counter [usually 0x2 for dettype1]
// need corresponding bit set in conditions [usually bit57:0x20000000]
// UPDATE - conditions now 128bits each, with upper 16 as prescale bits
assign prescale_passed[ 0] = (conditions[ 127: 112] == 8'h0) || ((prescale_active & conditions[ 127: 112]) != 8'h0);
assign prescale_passed[ 1] = (conditions[ 255: 240] == 8'h0) || ((prescale_active & conditions[ 255: 240]) != 8'h0);
assign prescale_passed[ 2] = (conditions[ 383: 368] == 8'h0) || ((prescale_active & conditions[ 383: 368]) != 8'h0);
assign prescale_passed[ 3] = (conditions[ 511: 496] == 8'h0) || ((prescale_active & conditions[ 511: 496]) != 8'h0);
assign prescale_passed[ 4] = (conditions[ 639: 624] == 8'h0) || ((prescale_active & conditions[ 639: 624]) != 8'h0);
assign prescale_passed[ 5] = (conditions[ 767: 752] == 8'h0) || ((prescale_active & conditions[ 767: 752]) != 8'h0);
assign prescale_passed[ 6] = (conditions[ 895: 880] == 8'h0) || ((prescale_active & conditions[ 895: 880]) != 8'h0);
assign prescale_passed[ 7] = (conditions[1023:1008] == 8'h0) || ((prescale_active & conditions[1023:1008]) != 8'h0);
assign prescale_passed[ 8] = (conditions[1151:1136] == 8'h0) || ((prescale_active & conditions[1151:1136]) != 8'h0);
assign prescale_passed[ 9] = (conditions[1279:1264] == 8'h0) || ((prescale_active & conditions[1279:1264]) != 8'h0);
assign prescale_passed[10] = (conditions[1407:1392] == 8'h0) || ((prescale_active & conditions[1407:1392]) != 8'h0);
assign prescale_passed[11] = (conditions[1535:1520] == 8'h0) || ((prescale_active & conditions[1535:1520]) != 8'h0);
assign prescale_passed[12] = (conditions[1663:1648] == 8'h0) || ((prescale_active & conditions[1663:1648]) != 8'h0);
assign prescale_passed[13] = (conditions[1791:1776] == 8'h0) || ((prescale_active & conditions[1791:1776]) != 8'h0);

// any clock a condition is passed, set count to #events in window (word_count)
reg [127:0] pkt_count;
always @ (posedge clk or posedge reset) begin
   if( reset ) pkt_count <= 64'h0;
	else begin
      pkt_count[ 7: 0] <= ( passed[0]  &&  prescale_passed[0]      ) ? word_count :
	                       ( ptr_ready  &&  pkt_count[ 7: 0] > 8'h0 ) ? pkt_count[ 7: 0] - 8'h1 : pkt_count[ 7: 0];
      pkt_count[15: 8] <= ( passed[1]  &&  prescale_passed[1]      ) ? word_count :
	                       ( ptr_ready  &&  pkt_count[15: 8] > 8'h0 ) ? pkt_count[15: 8] - 8'h1 : pkt_count[15: 8];
      pkt_count[23:16] <= ( passed[2]  &&  prescale_passed[2]      ) ? word_count :
	                       ( ptr_ready  &&  pkt_count[23:16] > 8'h0 ) ? pkt_count[23:16] - 8'h1 : pkt_count[23:16];
      pkt_count[31:24] <= ( passed[3]  &&  prescale_passed[3]      ) ? word_count :
	                       ( ptr_ready  &&  pkt_count[31:24] > 8'h0 ) ? pkt_count[31:24] - 8'h1 : pkt_count[31:24];
      pkt_count[39:32] <= ( passed[4]  &&  prescale_passed[4]      ) ? word_count :
	                       ( ptr_ready  &&  pkt_count[39:32] > 8'h0 ) ? pkt_count[39:32] - 8'h1 : pkt_count[39:32];
      pkt_count[47:40] <= ( passed[5]  &&  prescale_passed[5]      ) ? word_count :
	                       ( ptr_ready  &&  pkt_count[47:40] > 8'h0 ) ? pkt_count[47:40] - 8'h1 : pkt_count[47:40];
      pkt_count[55:48] <= ( passed[6]  &&  prescale_passed[6]      ) ? word_count :
	                       ( ptr_ready  &&  pkt_count[55:48] > 8'h0 ) ? pkt_count[55:48] - 8'h1 : pkt_count[55:48];
      pkt_count[63:56] <= ( passed[7]  &&  prescale_passed[7]      ) ? word_count :
	                       ( ptr_ready  &&  pkt_count[63:56] > 8'h0 ) ? pkt_count[63:56] - 8'h1 : pkt_count[63:56];
	
      pkt_count[ 71: 64] <= ( passed[8]  &&  prescale_passed[8]      ) ? word_count :
	                         ( ptr_ready  &&  pkt_count[ 71: 64] > 8'h0 ) ? pkt_count[ 71: 64] - 8'h1 : pkt_count[ 71: 64];
      pkt_count[ 79: 72] <= ( passed[9]  &&  prescale_passed[9]      ) ? word_count :
	                         ( ptr_ready  &&  pkt_count[ 79: 72] > 8'h0 ) ? pkt_count[ 79: 72] - 8'h1 : pkt_count[ 79: 72];
      pkt_count[ 87: 80] <= ( passed[10]  &&  prescale_passed[10]      ) ? word_count :
	                         ( ptr_ready  &&  pkt_count[ 87: 80] > 8'h0 ) ? pkt_count[ 87: 80] - 8'h1 : pkt_count[ 87: 80];
      pkt_count[ 95: 88] <= ( passed[11]  &&  prescale_passed[11]      ) ? word_count :
	                         ( ptr_ready  &&  pkt_count[ 95: 88] > 8'h0 ) ? pkt_count[ 95: 88] - 8'h1 : pkt_count[ 95: 88];
      pkt_count[103: 96] <= ( passed[12]  &&  prescale_passed[12]      ) ? word_count :
	                         ( ptr_ready  &&  pkt_count[103: 96] > 8'h0 ) ? pkt_count[103: 96] - 8'h1 : pkt_count[103: 96];
      pkt_count[111:104] <= ( passed[13]  &&  prescale_passed[13]      ) ? word_count :
	                         ( ptr_ready  &&  pkt_count[111:104] > 8'h0 ) ? pkt_count[111:104] - 8'h1 : pkt_count[111:104];
      pkt_count[119:112] <= ( passed[14]  &&  prescale_passed[14]      ) ? word_count :
	                         ( ptr_ready  &&  pkt_count[119:112] > 8'h0 ) ? pkt_count[119:112] - 8'h1 : pkt_count[119:112];
      pkt_count[127:120] <= ( passed[15]  &&  prescale_passed[15]      ) ? word_count :
	                         ( ptr_ready  &&  pkt_count[127:120] > 8'h0 ) ? pkt_count[127:120] - 8'h1 : pkt_count[127:120];
	end
end

endmodule
