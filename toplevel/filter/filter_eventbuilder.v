// assemble various data sources into filter output event
module filter_eventbuilder ( input wire clk, input wire reset,
   input  wire   [39:0] mult_ptr_out,       input  wire mult_ptr_rdy,
	input  wire  [511:0] cdb_pkt,            input  wire  [15:0] det_type_mask,   input  wire multi_count,
	output wire [1023:0] filter_evt_data,    output reg  filter_evt_rdy,          output wire filter_passed
);//                               marker         modtype       
assign     filter_passed = (coinc_count != 5'h0) && det_type_passed;
wire [31:0] event_header = {cdb_pkt[31:28],cdb_pkt[27:25],word_count_out,cdb_pkt[19:0]};
assign   filter_evt_data = {
   cdb_pkt[511:96], filter_count_list[511:0], fragment_count, {2'h0,coinc_dly2[13:0],cdb_pkt[47],multi_count,cdb_pkt[45:32]}, event_header
};

wire [4:0] word_count_in = cdb_pkt[24:20];
wire [4:0] word_count_out = word_count_in + (multi_count ? coinc_count : 5'h1);

wire [3:0] det_type = cdb_pkt[3:0];
wire det_type_passed = (det_type == 4'h0) ? det_type_mask[15] : (det_type == 4'h1) ? det_type_mask[14] :
                       (det_type == 4'h2) ? det_type_mask[13] : (det_type == 4'h3) ? det_type_mask[12] :
                       (det_type == 4'h4) ? det_type_mask[11] : (det_type == 4'h5) ? det_type_mask[10] :
                       (det_type == 4'h6) ? det_type_mask[ 9] : (det_type == 4'h7) ? det_type_mask[ 8] :
                       (det_type == 4'h8) ? det_type_mask[ 7] : (det_type == 4'h9) ? det_type_mask[ 6] :
		                 (det_type == 4'hA) ? det_type_mask[ 5] : (det_type == 4'hB) ? det_type_mask[ 4] :
		                 (det_type == 4'hC) ? det_type_mask[ 3] : (det_type == 4'hD) ? det_type_mask[ 2] :
		                 (det_type == 4'hE) ? det_type_mask[ 1] :                      det_type_mask[ 0];

// there is no cdb_pkt_rdy signal - just wait 2 clocks after request data for it to become valid
//  -> add 2 clk delay on event-strobe and coinc-ok before writing fragment out
//     **need to pipeline coinc_ok as can have events on every clock**

reg [15:0] coinc_dly2; reg [15:0] coinc_dly; // *** mult_ptr_out has lower 16 bits replaced with coinc_ok (aligned to mult_ptr_rdy)
always @ (posedge clk) begin coinc_dly2 <= coinc_dly; coinc_dly <= mult_ptr_out[15:0]; end
reg strobe_dly; always @ (posedge clk) begin filter_evt_rdy <= strobe_dly; strobe_dly <= mult_ptr_rdy; end

wire [31:0] padding = 32'hfadffadf; // this padding value is discarded by the data concentrator
wire [511:0] filter_count_list = {
   (coinc_dly2[15] & multi_count ? {1'h0,filter_counts[510:480]} : padding),  (coinc_dly2[14] & multi_count ? {1'h0,filter_counts[478:448]} : padding), 
   (coinc_dly2[13] & multi_count ? {1'h0,filter_counts[446:416]} : padding),  (coinc_dly2[12] & multi_count ? {1'h0,filter_counts[414:384]} : padding), 
   (coinc_dly2[11] & multi_count ? {1'h0,filter_counts[382:352]} : padding),  (coinc_dly2[10] & multi_count ? {1'h0,filter_counts[350:320]} : padding), 
   (coinc_dly2[ 9] & multi_count ? {1'h0,filter_counts[318:288]} : padding),  (coinc_dly2[ 8] & multi_count ? {1'h0,filter_counts[286:256]} : padding), 
   (coinc_dly2[ 7] & multi_count ? {1'h0,filter_counts[254:224]} : padding),  (coinc_dly2[ 6] & multi_count ? {1'h0,filter_counts[222:192]} : padding), 
   (coinc_dly2[ 5] & multi_count ? {1'h0,filter_counts[190:160]} : padding),  (coinc_dly2[ 4] & multi_count ? {1'h0,filter_counts[158:128]} : padding), 
   (coinc_dly2[ 3] & multi_count ? {1'h0,filter_counts[126: 96]} : padding),  (coinc_dly2[ 2] & multi_count ? {1'h0,filter_counts[ 94: 64]} : padding), 
   (coinc_dly2[ 1] & multi_count ? {1'h0,filter_counts[ 62: 32]} : padding),  (coinc_dly2[ 0] & multi_count ? {1'h0,filter_counts[ 30:  0]} : padding)
};
wire [31:0] fragment_count = multi_count ? padding : {1'h0,total_count[30:0]};
// if multi_count set, get full list of counts (no total), otherwise just get total count

wire [4:0] coinc_count /* synthesis keep */ = coinc_dly2[ 0] + coinc_dly2[ 1] + coinc_dly2[ 2] + coinc_dly2[ 3] +
                                              coinc_dly2[ 4] + coinc_dly2[ 5] + coinc_dly2[ 6] + coinc_dly2[ 7] +
                                              coinc_dly2[ 8] + coinc_dly2[ 9] + coinc_dly2[10] + coinc_dly2[11] +
                                              coinc_dly2[12] + coinc_dly2[13] + coinc_dly2[14] + coinc_dly2[15];
reg [511:0] filter_counts;
reg  [31:0] total_count;
always @ (posedge clk) begin
   if( reset ) begin
	   total_count <= 31'h0;  filter_counts <= 512'h0;
	end else begin
   	total_count            <= (strobe_dly && filter_passed ) ? total_count + 31'h1 : total_count;
		filter_counts[511:480] <= (coinc_dly[15]) ? filter_counts[511:480] + 32'h1 : filter_counts[511:480];
		filter_counts[479:448] <= (coinc_dly[14]) ? filter_counts[479:448] + 32'h1 : filter_counts[479:448];
		filter_counts[447:416] <= (coinc_dly[13]) ? filter_counts[447:416] + 32'h1 : filter_counts[447:416];
		filter_counts[415:384] <= (coinc_dly[12]) ? filter_counts[415:384] + 32'h1 : filter_counts[415:384];
		filter_counts[383:352] <= (coinc_dly[11]) ? filter_counts[383:352] + 32'h1 : filter_counts[383:352];
		filter_counts[351:320] <= (coinc_dly[10]) ? filter_counts[351:320] + 32'h1 : filter_counts[351:320];
		filter_counts[319:288] <= (coinc_dly[ 9]) ? filter_counts[319:288] + 32'h1 : filter_counts[319:288];
		filter_counts[287:256] <= (coinc_dly[ 8]) ? filter_counts[287:256] + 32'h1 : filter_counts[287:256];
		filter_counts[255:224] <= (coinc_dly[ 7]) ? filter_counts[255:224] + 32'h1 : filter_counts[255:224];
		filter_counts[223:192] <= (coinc_dly[ 6]) ? filter_counts[223:192] + 32'h1 : filter_counts[223:192];
		filter_counts[191:160] <= (coinc_dly[ 5]) ? filter_counts[191:160] + 32'h1 : filter_counts[191:160];
		filter_counts[159:128] <= (coinc_dly[ 4]) ? filter_counts[159:128] + 32'h1 : filter_counts[159:128];
		filter_counts[127: 96] <= (coinc_dly[ 3]) ? filter_counts[127: 96] + 32'h1 : filter_counts[127: 96];
		filter_counts[ 95: 64] <= (coinc_dly[ 2]) ? filter_counts[ 95: 64] + 32'h1 : filter_counts[ 95: 64];
		filter_counts[ 63: 32] <= (coinc_dly[ 1]) ? filter_counts[ 63: 32] + 32'h1 : filter_counts[ 63: 32];
		filter_counts[ 31:  0] <= (coinc_dly[ 0]) ? filter_counts[ 31:  0] + 32'h1 : filter_counts[ 31:  0];
	end
end

endmodule
