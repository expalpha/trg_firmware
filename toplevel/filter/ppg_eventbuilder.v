// 8[4]  pileup[2] data-type[3] #filters[3] addr[16] det-type[4]        // data-type = master-grifc = 4
// 0x8        00 10           0 000         0xFFFF   0           = 0x820ffff0
// standard event 0x8 [0xPpg 0xMstId] 0x9 0xA 0xB 0xE
 // ppg_event: 0x800000  new-ppg, old-ppg, timestamp, 0xE0000000       [32:0] ppg_readback
 
// ppg_pattern[32]  is flag indicating pattern update
// ppg_readback[32] is flag indicating readback complete
// ppg_readback[33] is ?
module ppg_eventbuilder ( input wire clk, input wire reset,              input wire enable_ppg_events,
   input  wire [32:0] ppg_pattern,     input  wire [33:0] ppg_readback,  input wire [47:0] io_runtime,
   output wire [31:0] ppg_event_data,  output reg  ppg_event_ready,      input wire ppg_event_ack
);
assign ppg_event_data = ppg_event[31:0];

wire ppg_pattern_update = ppg_pattern[32];
wire ppg_readback_valid = ppg_readback[32];
reg [191:0] ppg_event;  reg [31:0] prev_pattern;  reg [2:0] ppg_event_busy;  reg ppg_event_wait;
always @ (posedge clk) begin
   if( reset ) begin ppg_event_busy <= 3'h0;  ppg_event_ready <= 1'b0; ppg_event <= 160'b0; ppg_event_wait <= 1'b0; prev_pattern <= ppg_pattern[31:0]; end
   else begin
	   ppg_event_busy <= ppg_event_busy;  ppg_event_ready <= ppg_event_ready;  ppg_event <= ppg_event; ppg_event_wait <= ppg_event_wait;
		prev_pattern <= ( ppg_pattern_update || ppg_event_wait ) ? prev_pattern : ppg_readback[31:0]; // need to preserve this during change
	   if( ppg_event_busy ) begin                                        // in middle of data readout
		   if( ppg_event_ack ) begin
			   ppg_event_busy  <=  ppg_event_busy - 3'h1;
			   ppg_event       <= (ppg_event_busy == 2'h1) ?  {160'h0, ppg_pattern[31:0]} : {32'h0, ppg_event[191:32]}; 
				ppg_event_ready <= (ppg_event_busy == 2'h1) ? 1'b0 : 1'b1;
			end
		end else if( ppg_event_ready && ppg_event_ack ) begin   // data ready, but not currently reading - start readout
		   ppg_event <= {32'h0, ppg_event[191:32]}; ppg_event_busy <= 3'h5;
		//end else if( (ppg_pattern[32] || ppg_readback[33]) && enable_ppg_events) begin       // new pattern, wait for readback
		end else if( ppg_pattern_update && enable_ppg_events) begin       // new pattern, wait for readback
		   ppg_event_wait <= 1'b1;
		   ppg_event <= { 32'h0, 12'hB00,io_runtime[47:28],  4'hA,io_runtime[27:0], 32'h0, 32'h0, 32'h0 }; // save correct runtime
		end else if( ppg_event_wait && ppg_readback_valid ) begin // readback done - make ppg event and signal ready to dcc
		   ppg_event_ready <= 1'b1;  ppg_event_wait <= 1'b0;
		   ppg_event <= { 4'hE,ppg_readback[27:0], ppg_event[159:96], 4'h9,prev_pattern[27:0], 4'h0,ppg_pattern[27:0], 32'h820ffff0 };
		end
	end
end

// on pattern change - record timestamp, old, new pattern in single wide shift-reg (in place of fifo)
//    mark busy when shifting so don't overwrite with new data while reading
// dcc will check port15 first, so get this event quickly?

endmodule
