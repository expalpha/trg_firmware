module mpx256x4(
   input wire    [1:0]  sel,     input  wire     enable,
   input wire [1023:0] data_in,  output reg [255:0] mpx
);

always @ (sel, enable, data_in) begin
  if (enable) begin
    case (sel)
	 0:       mpx <= data_in[ 255:  0];
	 1:       mpx <= data_in[ 511:256];
	 2:       mpx <= data_in[ 767:512];
	 3:       mpx <= data_in[1023:768];
    default: mpx <= 256'h0;
	 endcase
  end else mpx <= 256'h0;
end

endmodule

  

