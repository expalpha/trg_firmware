// deals only with pointers to compact data:  time order, find multiplicities, check against requirements and accept/reject
module Filter_Core (
   input wire clk,  input wire reset,  input wire RUN,
   input wire   [39:0] cdb_ptr,        input wire cdb_ptr_rdy,
   input wire    [9:0] coinc_width,    input wire   [9:0] coinc_width_s,
   input wire [4095:0] p_patterns,     input wire [127:0] p_prescales,
   input wire   [15:0] pat_sel_s,      input wire  [15:0] pat_sel,  input wire test_startbit,
   output wire  [63:0] multiplicity,   // Divided in 8 detector types, 6 bits for each multiplicity
   output wire  [39:0] pkt_ptr,        output wire pkt_ptr_strobe,
   output wire  [15:0] coinc_vector,   output wire coinc_ok
);
assign pkt_ptr      = mult2_ptr_out;   assign pkt_ptr_strobe = mult2_ptr_rdy;  assign multiplicity = multiplicity2;
assign coinc_vector = ccp_cok1;        assign coinc_ok       = ccp_ok1;   

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////           timeslot/linked-list memories for time-ordering of input events    /////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// write over whole mem depending on timestamp, readback in loop - N us after current-time (use N=80)
// since there can be many events with same timestamp - have to use linked list
//   - each slot now contains list not event, list has pointer to data and pointer to next element
// there is a large memory for data, timeslotmem pointers are addresses pointing into that
//        data-mem size is 4k items of 56bits => 12bit pointers
//           56bits: each 40bit data pkt contains extra 16bit pointer to next element**
//        list-mem size is 16k items of 32bits => 160us worth of 10ns bins
//           32bits: 2x16bit pointers - 12bits + 4bits of flag
//
//        data mem contains pointers => need rewriting [each block is the last at the time it's first written => no next element yet]
//           unless pointer is to previous!
//    4 slot queue fifo on input - write immediately, read when ready (each read inc current mem addr: "tsm_NewListElement")
//
// retreivefifo 256x24bits
wire [11:0]    tsm_head;  wire [11:0]  tsm_tail;  wire [11:0] tsm_prevtail;
wire [11:0]   rdbk_head;  wire [11:0] rdbk_tail;  wire [13:0] tsm_Rtime;
wire [39:0] tsm_ptr_out;  wire         tsm_save;  wire rdbk_valid;    wire update_prev;
TimeSlotMem TimeSlotMem_inst (  /////      Time slot memory - interleave read/write @100 Each  ///////
   .clk(clk),                .reset(reset),              .RUN_in(RUN),
   .cdb_ptr(cdb_ptr),        .cdb_ptr_rdy(cdb_ptr_rdy),  .cdb_ptr_out(tsm_ptr_out),
   .head(tsm_head),          .tail(tsm_tail),            .tsm_save(tsm_save),
   .prev_tail(tsm_prevtail), .update_prev(update_prev),  .rdbk_time(tsm_Rtime), // **internal runtime counter**
   .rdbk_head(rdbk_head),    .rdbk_tail(rdbk_tail),      .rdbk_valid(rdbk_valid)
);
wire [39:0] ll_ptr_out;  wire ll_ptr_rdy; wire ll_empty;
TimeSlotLinkedList TimeSlotLinkedList_inst (	//////    LINKED LIST BUFFER MODULE     /////
   .clk(clk),                   .reset(reset),
   .tsm_head(tsm_head),	        .tsm_tail(tsm_tail),  	  .tsm_save(tsm_save),
	.tsm_prevtail(tsm_prevtail), .tsm_pkt_ptr(tsm_ptr_out),
	.rdbk_head(rdbk_head),       .rdbk_tail(rdbk_tail),     .tsm_rdbk_valid(rdbk_valid),
	.ll_ptr_out(ll_ptr_out),	  .ll_ptr_rdy(ll_ptr_rdy),   .ll_empty(ll_empty)
);	
wire [47:0] ms_ptr_out;  wire ms_ptr_rdy;  wire ms_empty;
MicroSequence MicroSequence_inst(  //////   MICROSEQUENCE   //////
   .clk(clk),   .reset(reset),   .ms_empty(ms_empty),
   .ll_ptr_out(ll_ptr_out),  .ll_ptr_rdy(ll_ptr_rdy),
   .ms_ptr_out(ms_ptr_out),  .ms_ptr_rdy(ms_ptr_rdy)
);
wire [39:0] ps_ptr_out; wire ps_ptr_rdy;
prescale prescale_inst (  //////    PRESCALE    //////
   .clk(clk),   .reset(reset),    .p_prescales(p_prescales),
   .ms_ptr_out(ms_ptr_out),  .ms_ptr_rdy(ms_ptr_rdy),
   .ps_ptr_out(ps_ptr_out),  .ps_ptr_rdy(ps_ptr_rdy)
);
// Multiplicity First stage (can define start for stage 2)
// Output strobed when all available events have been accounted in the time window 
wire [39:0] mult_ptr_out;  wire mult_ptr_rdy;  wire  [63:0] multiplicity1;
Multiplicity Multiplicity_inst (
   .clk(clk),  .reset(reset),    .test_startbit(1'b0 /* not in stage1 */),
	.coinc_width(coinc_width_s),  .flowthrough(1'b0),  .ccp_startOK(1'b0),
	.pkt_ptr_in(ps_ptr_out),      .pkt_ptr_rdy(ps_ptr_rdy),   .ll_empty(ll_empty),  .ms_empty(ms_empty),
   .multiplicity(multiplicity1), .pkt_ptr_out(mult_ptr_out),  .pkt_ptr_strobe(mult_ptr_rdy)
);

wire [7:0] ccp_cok1; // coincidence OK vector (one bit per detector type)
wire ccp_ok1;        // Check Coincidence patterns (for Multiplicity 1 - Use S prefix parameters)
check_coincidence_patterns check_coinc_s (
   .m(multiplicity),   .p_patterns(p_patterns), .pattern_select(pat_sel_s),
   .ccp_cok(ccp_cok1), .ccp_ok(ccp_ok1)
);

wire [39:0] mult2_ptr_out;  wire mult2_ptr_rdy;  wire  [63:0] multiplicity2;
Multiplicity Multiplicity_inst2 ( //  Multiplicity 2 (Can be armed with the START bit from stage A if selected)
   .clk(clk),  .reset(reset),    .test_startbit(test_startbit),
	.coinc_width(coinc_width),    .ccp_startOK(ccp_ok1),
	.pkt_ptr_in(mult_ptr_out),    .pkt_ptr_rdy(mult_ptr_rdy),  
   .multiplicity(multiplicity2), .pkt_ptr_out(mult2_ptr_out), .pkt_ptr_strobe(mult2_ptr_rdy)
);

wire [7:0] ccp_ok; wire ccp_cok;
check_coincidence_patterns check_coinc ( // Check Coincidence patterns (for Multiplicity 2)
   .m(multiplicity2),  .p_patterns(p_patterns),  .pattern_select(pat_sel),
   .ccp_cok(ccp_cok),  .ccp_ok(ccp_ok)
);

endmodule
