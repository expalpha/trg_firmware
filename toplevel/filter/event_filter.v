// Allow extra packet pointers to come in from remote filter blocks - need to identify to avoid readback here
//   (once they have been present in the coincwindow to determine coincidences, they can just be dropped)
//     will need to recalculate buffer sizes to allow for more data
module Event_Filter ( output wire  debug,   output wire [32:0] ppg_pattern,  output wire [31:0] ppg_address,  input wire ppg_events, input wire [33:0] ppg_readback,
   input wire clk,  input wire clk_ddr,  input wire clk_ppg,  input wire RSTn,  input wire RUN,   input wire [47:0] io_runtime, //16bits used in filter, 24 in ppg
   input wire   [63:0]  ParamIn,  output wire  [63:0]     ParamOut, // can re-register both at any point [1clk delay is irrelevant]
   input  wire [127:0]  data_in,  input  wire   [3:0] dataready_in, output wire [3:0] data_ack_out, // up to 4 links in
   output wire  [31:0] data_out,  output wire  dataready_out,  input wire filterack_in,
   //output wire [31:0] ppg_event_out,  output wire ppg_event_ready_out,  input wire ppg_event_ack_in, 
   input wire bypass_filter
); assign debug = filter_full ^ mult_error;
// when filter is bypassed - still would like ppg events - so also send out to toplevel
//    so when filter bypassed, toplevel will ack ppg_data
//wire   ppg_event_ack       = bypass_filter ? ppg_event_ack_in : ppg_event_filterack;
//assign ppg_event_ready_out = bypass_filter ? ppg_event_ready : 1'b0;
//assign ppg_event_out       = ppg_event_data;
wire ppg_event_ack = ppg_event_filterack;

wire run_start_wire = RUN && !run_reg;  reg  run_reg; always @(posedge clk) run_reg <= RUN;  // oneshot
reg run_start; always @(posedge clk) run_start <= run_start_wire; // re-registered oneshot

wire reset = !RSTn;
assign ParamOut = (ppg_par_out[63]) ? ppg_par_out : filter_par_out;

/////////////////////////////        PPG Module        //////////////////////////////////
wire [63:0] ppg_par_out;
wire [63:0] ppg_par_in = ( ParamIn[39:32] == 8'h7E ) ? ParamIn : 64'h0; // use chan=126 for this block;
ppg ppg0 (  .ppg_address(ppg_address),
   .io_clk(clk), .ppg_clk(clk_ppg), .reset(reset), .RUN(RUN), .param_in(ppg_par_in), .param_out(ppg_par_out), .pattern_out(ppg_pattern)
);

wire [31:0] ppg_event_data;  wire ppg_event_ready;
ppg_eventbuilder ppp_evb ( .clk(clk), .reset(reset),                    .enable_ppg_events(ppg_events),     
   .ppg_pattern(ppg_pattern),        .ppg_readback(ppg_readback),       .io_runtime(io_runtime),
   .ppg_event_data(ppg_event_data),  .ppg_event_ready(ppg_event_ready), .ppg_event_ack(ppg_event_ack)
);

/////////////////////////////        data I/O        //////////////////////////////////
// flow control for filter_outfifo is simple - entire events are dropped if writes attempted while full
//    (also nothing else is possible as data cannot be held)
// cdbuffer can control flow for passthrough evbuf - it is aware of event boundaries -
//   if just passing through - stop grabbing data (even in mid event) if outbuf full
//   if filtering only - no problem!
//   if filtering and passing through - reserve a big chunk for filtered data + stop grabbing when full
//    - do not expect dual-stream filtering and passthrough to work well with long waveforms!
// for multiple incoming streams (in grifc) need outfifo within each cdbuffer, and dcc-passthroughevbuf pulls from these

// will not keep up at high rates - need 10G link to keep up with filter (filter ID will flag any any lost packets)
// filter_outfifo ...  cdb_pkt, -> data_out, rdy_out, ack_in

reg [31:0] dead_count; wire dead_wait = ( dead_count != 32'h0 );
always @(posedge clk) begin  // enforced programmable deadtime after each trigger
   if(         dead_wait ) dead_count <= dead_count - 32'h1;
	else if( filter_wrreq ) dead_count <= filter_deadtime;
	else                    dead_count <= dead_count;
end

wire filter_evt_rdy;  wire [1023:0] filter_evt_data;  wire [15:0] det_type_mask;  wire filter_passed;
filter_eventbuilder filt_evb ( .clk(clk), .reset(reset||run_start),    .cdb_pkt(cdb_pkt),
   .mult_ptr_rdy(mult_ptr_rdy),     .mult_ptr_out(mult_ptr_out),       
	.filter_evt_rdy(filter_evt_rdy), .filter_evt_data(filter_evt_data), .det_type_mask(det_type_mask),
	.filter_passed(filter_passed),   .multi_count(multi_filter_cnt)
);
wire filter_wrreq    = (filter_evt_rdy && enable_filter) && (filter_passed || disable_filtering);
wire filter_wrenable = filter_wrreq && !dead_wait;
scdwzlfifo filter_out ( .clk(clk), // 1024x512 - dual width: write 1024 bits and read 32 bits (32x16k)
   .wrreq(filter_wrenable),  .empty(filtered_empty),  .data(filter_evt_data),
   .rdreq(filtered_ack),     .words(filtered_words),  .q(filtered_data)
); wire [13:0] filtered_words;  wire filter_full = (filtered_words >= 14'h3ff0);

wire [ 31:0] filtered_data;    wire         filtered_empty;  wire         filtered_ack;   wire [9:0] dummy;
wire [127:0] unfiltered_data;  wire [3:0] unfiltered_ready;  wire [3:0] unfiltered_ack;   wire ppg_event_filterack;
data_concentrator dcc (
   .clk(clk),                 .reset(reset),                 .modulelevel( 2'h3/* do not modify data*/ ),
	.data_in ({ppg_event_data, 320'h0,filtered_data, unfiltered_data}),
	.ready_in({ppg_event_ready, 10'h0,~filtered_empty,unfiltered_ready}),
	.ack_out({ppg_event_filterack,dummy,filtered_ack,unfiltered_ack}),
   .data_out(outbufdata_in),  .ready_out(outbufdata_ready),  .evbuffull_in(outbuf_full)
);
wire [31:0] outbufdata_in; wire outbufdata_ready;  wire outbuf_full; wire outbuf_empty;
sczlfifo evbuf ( .clk(clk),  .aclr(reset||run_start),    // evfifo 4k words - will hold 80us sample!
   .data(outbufdata_in), .wrreq(outbufdata_ready), .full(outbuf_full),
   .q(data_out),         .rdreq(filterack_in),     .empty(outbuf_empty)
);
reg prv_empty; always @(posedge clk) prv_empty <= outbuf_empty; // wait for 1 more clk of not empty
assign dataready_out = !outbuf_empty && !prv_empty;                // q,rdempty do not change quickly enough?
 
/////////////////////////////        parameters+stats        //////////////////////////////////
// allow 7 det-types [+prescaled], and 8 conditions
// current arrangement is just 8bit multiplicity per detector type (7 atm) -> 56bits
//   also just need single bit per prescale -> 7more bits, or 63bits per condition
// also 8 conditions => 512bits total [requires 16 32bit parameters also 4 for 8x16bit prescales, and 1 param for coincwin]
// UPDATE - doubled above so 14 types and 16 conditions, so 4 param per condition with 16bits prescale at end
wire [255:0] prescales;  wire [2047:0] coinc_param;  wire [9:0] coinc_width;  wire [31:0] filter_csr; wire [63:0] filter_par_out;
wire [31:0] filter_deadtime; wire [63:0] filterpar_in = ( ParamIn[39:32] == 8'h7F ) ? ParamIn : 64'h0; // chan 127 for this block
paramfilter_io par ( .clk(clk),  .reset(reset),  // oneshot on msb of param_in [needs to be syncd to clk correctly]
   .param_in(filterpar_in),          .param_out(filter_par_out),  .csr(filter_csr),
	.trig_deadtime(filter_deadtime),  .coinc_width(coinc_width),   .det_type_mask(det_type_mask),
	.prescales(prescales),            .coinc_param(coinc_param),   .ge_address(ge_address)
);
wire disable_filtering =  filter_csr[ 8]; // do not gate filtered packets on condition passed
wire enable_filter     =  filter_csr[ 9]; // unless set, do not write any filtered packets at all
wire enable_unfiltered = ~filter_csr[10]; // if NOT set, write out unfiltered data
wire strict_filter     =  filter_csr[11]; // if set, use strict filtering (events must pass their own conditions)
wire multi_filter_cnt  =  filter_csr[12]; // if set, provide full list of filter counters

////////////////////     fake data for sim      /////////////////
//wire [127:0] data_in;  wire [3:0] dataready_in;  wire [11:0] dummy;
//reg [11:0] romaddr; always @ (posedge clk or negedge RSTn) if(!RSTn) romaddr <= 10'h0; else romaddr <= romaddr + 10'h1;
//data_rom rom ( .clock(clk), .address(romaddr), .q({dummy,dataready_in,data_in}) );

/////////////////////////////    compact data buffers - rename to data-buffers    //////////////////////////////////
wire [511:0] cdb_pkt;  wire [47:0] cdb_ptr;  wire cdb_ptr_rdy;  wire [5:0] ge_address;
compact_data_buffers cdbuf (  .clk(clk),  .reset(reset),  .RUN(RUN),  .ge_address(ge_address),
	.unfiltered(enable_unfiltered),                                    .run_time(io_runtime[15:0]),
   .data_in(data_in),             .data_ready_in(dataready_in),       .data_ack_out(data_ack_out),  // unfiltered data in
   .data_out(unfiltered_data),    .data_ready_out(unfiltered_ready),  .data_ack_in(unfiltered_ack), // unfiltered data out
   .cdb_ptr(cdb_ptr),             .cdb_ptr_rdy(cdb_ptr_rdy),                                        // ptr to filter
   .readbk_ptr(mult_ptr_out),     .readbk_pkt_out(cdb_pkt)                                          // data packet readback
); // no output valid signal - need to use pkt_ptr_strobe + membuf delay

////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////        Filter Core        /////////////////////////////////////
///////////       timeslot/linked-list memories for time-ordering of input events    ////////////
wire [11:0]    tsm_head;  wire [11:0]  tsm_tail;  wire [11:0] tsm_prevtail;
wire [11:0]   rdbk_head;  wire [11:0] rdbk_tail;  wire [13:0] tsm_Rtime;
wire [39:0] tsm_ptr_out;  wire         tsm_save;  wire rdbk_valid;    wire update_prev;
TimeSlotMem TimeSlotMem_inst (  /////      Time slot memory - interleave read/write @125 Each  ///////
   .clk(clk_ddr),            .reset(reset),              .RUN_in(RUN),
   .cdb_ptr(cdb_ptr),        .cdb_ptr_rdy(cdb_ptr_rdy),  .cdb_ptr_out(tsm_ptr_out),
   .head(tsm_head),          .tail(tsm_tail),            .tsm_save(tsm_save),
   .prev_tail(tsm_prevtail), .update_prev(update_prev),  .rdbk_time(tsm_Rtime), // **internal runtime counter**
   .rdbk_head(rdbk_head),    .rdbk_tail(rdbk_tail),      .rdbk_valid(rdbk_valid) 
);
wire [39:0] ll_ptr_out;  wire ll_ptr_rdy; wire ll_empty;
TimeSlotLinkedList TimeSlotLinkedList_inst (	//////    LINKED LIST BUFFER MODULE  - also using ddrclk   /////
   .clk(clk_ddr),               .reset(reset),
   .tsm_head(tsm_head),	        .tsm_tail(tsm_tail),  	  .tsm_save(tsm_save),
	.tsm_prevtail(tsm_prevtail), .tsm_pkt_ptr(tsm_ptr_out),
	.rdbk_head(rdbk_head),       .rdbk_tail(rdbk_tail),     .tsm_rdbk_valid(rdbk_valid),
	.ll_ptr_out(ll_ptr_out),	  .ll_ptr_rdy(ll_ptr_rdy),   .ll_empty(ll_empty)
);	// ll_ptr_out/ll_ptr_rdy are on consecutive ddr clks [states 0,1 of 0,1,2,3]
   // transfer back to sdr clk in microsequence
wire [47:0] ms_ptr_out;  wire ms_ptr_rdy;  wire ms_empty;
MicroSequence MicroSequence_inst(  //////   MICROSEQUENCE   //////
   .clk(clk),  .clk_ddr(clk_ddr),  .reset(reset),  .ms_empty(ms_empty),
   .ll_ptr_out(ll_ptr_out),  .ll_ptr_rdy(ll_ptr_rdy),
   .ms_ptr_out(ms_ptr_out),  .ms_ptr_rdy(ms_ptr_rdy) // pkt_ptr re-registered on sdr clk here
);
// Set prescale flag-bit every N events (done by detector type)
wire [39:0] ps_ptr_out; wire ps_ptr_rdy;
prescale prescale_inst ( // there is a single "prescale-ok" bit in the packet pointer - control it here
   .clk(clk),  .reset(reset),   .prescales(prescales),
   .ms_ptr_out(ms_ptr_out),  .ms_ptr_rdy(ms_ptr_rdy),
   .ps_ptr_out(ps_ptr_out),  .ps_ptr_rdy(ps_ptr_rdy)
);
// Multiplicity - Window#1 (can have as many as required),  pointers must pass through each window in turn
//    all conditions involving this window must be stored with the packets as they leave
//       as the word counts required to determine the condition pass/fail are only valid at this point
//    pointer timing bits can now be reused to store coinc condition results
wire [39:0] mult_ptr_out;  wire mult_ptr_rdy;  wire  [255:0] multiplicity_out;  wire [7:0] window_events; wire mult_error;
multiplicity multip_window1 ( .clk(clk),  .reset(reset),    .coinc_width(coinc_width),  .coinc_words(window_events),
	.pkt_ptr_in(ps_ptr_out),     .pkt_ptr_rdy(ps_ptr_rdy),    .ll_empty(ll_empty),  .ms_empty(ms_empty),
   .multiplicity_out(multiplicity_out), .mult_ptr_out(mult_ptr_out),  .mult_ptr_rdy(mult_ptr_rdy),
	.coinc_passed(coinc_ok), .error(mult_error),                 .clover_mult_out(clover_mult_out)
); wire [31:0] clover_mult_out; // supp-xtal-mult[8],supp-clov-mult[8],bgo-mult[8],unsup-clov-mult[8]

// multiplicities continuously checked against conditions, "passed" follows multip. 
// every clock, ptr_ready dec's word counts (or if passed theyre updated to "coinc_words")
wire [15:0] coinc_ok; // coincidence OK vector (one bit per condition)
coincidence_check coinc_check (  .clk(clk),  .reset(reset),  .use_strict(strict_filter),
	.ptr_ready(mult_ptr_rdy),         .det_type(mult_ptr_out[23:20]),
   .multiplicity(multiplicity_out),  .word_count(window_events),  
	.conditions(coinc_param),         .coinc_ok(coinc_ok),
	.clover_mult(clover_mult_out)
); // ** det_type is not gated on ptr_rdy (can't use &, as zero is valid det-type)
// ***** so det_type holds its last value, till a new pointer is removed from window
// also timing of det_type was never checked to be correct for condition gating

endmodule
