// each timeslot has a "list" - 12bit head/tail plus flags -> 32bit tsm-entry [bit31=>entry-in-use]
//    tsm_{rd/wr}addr alternate between rdbk_time[counter[13:0]] and tstmp[datapacket[17:4]] (and their delayed versions)
//    12bit head/tail pointers start as "new_entry" - simple counter which runs through mem
// 
// each clk: storenewdata and readbackolddata [set head/tail/valid]
//    actually use 2xClk and alternate

// names ...  tail: address of last cdb_ptr with particular timestamp (end of list)
//        prevtail: storage-address of previous "last cdb_ptr"
//         newtail: storage-address of new cdb_ptr
// prevtailudpdate: strobe to update forward_ptr in prevtail
//         newhead: addr of head of current list
// 
// **NOTE** storing in reverse order allows not updating events after they're stored
// i.e. first element has NULL forward_ptr, second element points to first etc.
//    (=> start of list is last element stored)
// as it is now - each write has 2 parts (occur on consecutive clks) - new elem, prev ptr update[only write 2 of 7 bytes]
// **if there is no previous, 2nd stage is skipped** [ PrvTailUpdate  <= ~(head == tail)]
// 
// linklistmem 4k x 56bits(7bytes=5+2)
//    nextaddr [newhead = tsm_datain[11:0] = new_entryA]
//       starts at 0 and inc's each time a new cdb_ptr seen i.e. every 2 of 4 clks if inqueuefifo not empty
//          - no free list used, so have to have removed old entries before they're overwritten
//             4k entries in 160us => 25/us [have max 18.75/us/link, or ]
// 
// cdb_ptr -> fifo -? pkt_ptr -> 2clk delaypipe -> cdb_ptr_out
// 
// changes ... tsm_state had 5 states - as reset-state was all-zero
module TimeSlotMem(
   input  wire     clk/* ddr */,  input  wire            reset,  input  wire        RUN_in,
   input  wire [39:0]   cdb_ptr,  input  wire      cdb_ptr_rdy,  output reg  [39:0] cdb_ptr_out,
   output reg  [11:0]      head,  output reg  [11:0]      tail,  output reg         tsm_save,  
   output wire      update_prev,  output reg  [12:0] prev_tail,
   output reg  [11:0] rdbk_head,  output reg  [11:0] rdbk_tail,  output reg  rdbk_valid,
   output reg  [13:0] rdbk_time,  // 100 MHz Counter generating the time slot to reteive
   
   output wire queue_empty_out, output wire [39:0] queue_ptr_out, output wire [31:0] tsmq_out,
	output wire [38:0] tsm_prvtail 
);
assign queue_empty_out = queue_empty;  assign queue_ptr_out = pkt_ptr;  assign tsmq_out = tsm_dataout;
assign tsm_prvtail = {prev_tail,prev_tail_dly1,prev_tail_dly2};

assign  update_prev = (prev_tail[12] & tsm_save);

reg RUN; // Check RUN at state=3 (don't change in middle of sequence)
always @ (posedge clk) if (reset) RUN <= 1'b0; else RUN <= (tsm_state[3]) ? RUN_in : RUN;

wire [39:0] pkt_ptr;  wire queue_empty;  wire queue_full;
wire tsm_queue_ack = (~queue_empty & (tsm_state[0] | tsm_state[1]) );
wire tsm_queue_add = (cdb_ptr_rdy  & (tsm_state[0] | tsm_state[2]) ); // cdb_ptr_rdy is on sdr-clk => only write every second state
TSQueueFIFO	TSQueue ( .aclr(reset), .clock(clk), ////////////////////// INPUT QUEUE  ///////////////////////
	.data(cdb_ptr), .wrreq(cdb_ptr_rdy & (tsm_state[0] | tsm_state[2])),   .full(queue_full),
	.q(pkt_ptr),    .rdreq(tsm_queue_ack), .empty(queue_empty)
);
reg [31:0] tsm_datain;  wire tsm_Wren;  wire [31:0] tsm_dataout; 
wire [13:0] tsm_rdaddr; wire [13:0] tsm_wraddr;   ///////////////   Timeslot memory   //////////////////////
TimeSLotMemory TimeSLotMemory_inst( .clock(clk), // 16K X 25bit[BusyFlag,Head(12),Tail(12)] 16k=>160us@10ns
	.data(tsm_datain), .wraddress(tsm_wraddr), .wren(tsm_Wren),
	.q(tsm_dataout),   .rdaddress(tsm_rdaddr)
);
///////////////////////////////////////////////////////////////////////////////////////////////////////////
reg [3:0] tsm_state; // define 4 recurring states [0,1,2,3] to select read/write/source inputs to memory
always @ (posedge clk) if (reset) tsm_state <= 4'b0001; else tsm_state <= {tsm_state[2:0],tsm_state[3]};

reg  [13:0] rdbk_time_dly1;  reg  [13:0] rdbk_time_dly2;
always @ (posedge clk) begin // inc readback time stamp at every other cycle once RUN is asserted
 //if      (reset)                           rdbk_time <= 14'h0;
   if      (reset)                           rdbk_time <= 14'h3F7F;
   else if (RUN && (tsm_state[2:1] != 2'h0)) rdbk_time <= rdbk_time + 14'h1;
	else                                      rdbk_time <= rdbk_time;
end
always @ (posedge clk) begin  rdbk_time_dly2 <= rdbk_time_dly1;  rdbk_time_dly1 <= rdbk_time; end

reg [11:0] new_entry;  
always @ (posedge clk) begin // inc new list address after each input from queue
    if (reset)              new_entry <= 0;
	 else if (tsm_queue_ack) new_entry <= new_entry +1'b1;
	 else                    new_entry <= new_entry;
end
reg           ptr_rdy_dly1;  reg           ptr_rdy_dly2;
reg  [11:0] new_entry_dly1;  reg  [11:0] new_entry_dly2;
reg  [39:0]   pkt_ptr_dly1;  reg  [39:0]   pkt_ptr_dly2;
always @ (posedge clk) begin // various delays/buffers to allow for 4 stage read/write
 	 ptr_rdy_dly1   <= ( tsm_state[1:0] == 2'h0 ) ? ptr_rdy_dly1 : ~queue_empty;
	 ptr_rdy_dly2   <= ( tsm_state[1:0] == 2'h0 ) ? ptr_rdy_dly2 : ptr_rdy_dly1;
	 
 	 new_entry_dly1 <= ( tsm_state[1:0] == 2'h0 ) ? new_entry_dly1 : new_entry;
	 new_entry_dly2 <= ( tsm_state[1:0] == 2'h0 ) ? new_entry_dly2 : new_entry_dly1;
	 
 	 pkt_ptr_dly1   <= ( tsm_state[1:0] == 2'h0 ) ? pkt_ptr_dly1 : pkt_ptr;
	 pkt_ptr_dly2   <= ( tsm_state[1:0] == 2'h0 ) ? pkt_ptr_dly2 : pkt_ptr_dly1;
end

// Compare the time stamps of previous elements in order to 
// take care of previous information not yet stored in the time slot memory. This is necessary only
// when the new event has the same time stamp as a previous one, not yet in memory.
reg [11:0] prev_head; always @ (posedge clk) prev_head <= (tsm_Wren) ? tsm_datain[27:16] : prev_head; // SAME TIMESTAMPS PROBLEM

wire [13:0] tstmp      = pkt_ptr     [17:4];   // 10ns
wire [13:0] tstmp_dly1 = pkt_ptr_dly1[17:4];
wire [13:0] tstmp_dly2 = pkt_ptr_dly2[17:4];
//reg tstmp_equal; always @ (posedge clk) tstmp_equal <= ((tstmp == tstmp_dly1) && (~queue_empty & ptr_rdy_dly1));
wire tstmp_equal = ((tstmp == tstmp_dly1) && (~queue_empty & ptr_rdy_dly1)); // equal (and both valid)

reg [12:0] prev_tail_dly1;  reg [12:0] prev_tail_dly2;
always @ (posedge clk) begin   //Calculate alternate Previous tail ... find closest event with same time stamp
   if (     tsm_state[0])
      prev_tail_dly1 <= (tstmp_equal) ? {1'b1,new_entry_dly1} : 13'h0 /*none found*/;
   else if (tsm_state[1])
      prev_tail_dly2 <= (tstmp_equal) ? {1'b1,new_entry_dly1} : 13'h0 /*none found*/;
   else if (tsm_state[2]) 
      prev_tail <= (prev_tail_dly1[12]) ? prev_tail_dly1 : {tsm_dataout[31],tsm_dataout[11:0]}; // same time stamp detected
   else if (tsm_state[3]) 
      prev_tail <= (prev_tail_dly2[12]) ? prev_tail_dly2 : {tsm_dataout[31],tsm_dataout[11:0]}; // same time stamp detected
   else
      prev_tail <= 12'h000; // undefined state
end

///////////////////////////           WRITE/READ of the time slot memory            ///////////////////////////
// Calculation of the proper head/tail pointers;  The last pointer set for that time slot (if any) is in now in tstmp_dly1
//  If there was already an entry for that time slot, the busy bit (bit 24) is set . In that case, only the tail is updated.
//  Otherwise, NewListElememt is written in both head and tail
assign tsm_rdaddr = (tsm_state[1:0] != 2'h0 ) ? tstmp : rdbk_time; // alternate between loading new ptrs[tstmp] and retreiving old ones
assign tsm_wraddr =    (tsm_state[2]) ? tstmp_dly2 :
                       (tsm_state[3]) ? tstmp_dly1 : rdbk_time_dly2; // delayed timestamp, or retreive timeslot (to clear)
assign tsm_Wren   = ( (tsm_state[1:0] != 2'h0) && RUN ) ? 1'b1 :
                      (  tsm_state[2]                 ) ? ptr_rdy_dly2 : ptr_rdy_dly1;

always @ * begin // **Combinatorial** (data to write to memory)
   if ( tsm_state[3:2] != 2'h0 ) begin
	   if (tsm_dataout[31] == 1 ) begin            // time slot not empty
		   tsm_datain <= (tsm_state[2]) ? {tsm_dataout[31:12],new_entry_dly2} : {tsm_dataout[31:12],new_entry_dly1};
		end else if (prev_tail_dly2[12] == 1 && tsm_state[3]) begin // not yet in memory
		   tsm_datain <= (tsm_state[2]) ? {ptr_rdy_dly2,3'h0,prev_head,4'h0,new_entry_dly2} :
			                               {ptr_rdy_dly1,3'h0,prev_head,4'h0,new_entry_dly1};
		end else begin                              // time slot empty
		   tsm_datain <= (tsm_state[2]) ? {ptr_rdy_dly2,3'h0,new_entry_dly2,4'h0,new_entry_dly2} :
		                                	{ptr_rdy_dly1,3'h0,new_entry_dly1,4'h0,new_entry_dly1};
		end	
	end		
	else tsm_datain  <= 0; // for clearing in RUN mode
end

always @ (posedge clk) begin ///  fill the time slot linked list elements    ////
   if (reset) begin
      head <=    0;  tail <=    0;  tsm_save <= 0;  cdb_ptr_out <= 0;
	end else begin
		head <= head;  tail <= tail;  tsm_save <= 0;  cdb_ptr_out <= cdb_ptr_out;
	   if ( tsm_state[3:2] != 2'h0 ) begin
	      if (tsm_Wren) begin
			   cdb_ptr_out <= ( tsm_state[3] ) ? pkt_ptr_dly1 : pkt_ptr_dly2;
		      head <= tsm_datain[27:16];  tail <= tsm_datain[11:0];  tsm_save <= 1;
		   end else begin
            head <= 0;  tail <= 0;  tsm_save <= 0;  cdb_ptr_out <= 0;
		   end
		end
	end
end

always @ (posedge clk) begin /////// recover elements from  time slot linked list  //////
	rdbk_head <= 0;  rdbk_tail <= 0;  rdbk_valid  <= 0;
   if ( RUN && (tsm_state[1:0] != 2'h0) && tsm_dataout[31] ) begin
	   rdbk_head <= tsm_dataout[27:16];  rdbk_tail <= tsm_dataout[11:0];  rdbk_valid <= 1;
	end
end
	
endmodule
