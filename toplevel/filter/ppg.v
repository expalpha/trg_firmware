// simple pattern generator - 32bit output pattern and 32bit dwell time per pattern
//   currently have 640bits of parameters assigned, which allows 10 patterns
//   wrap to first pattern at end, each dwell-time must be at least 1, or will wrap to 2^32-1
// if ppg clk is 1Mhz - 32bit dwelltime range is 4200s or 80minutes
// --------------------------------------------------------------------------------------------
// we would like to attach the ppg_pattern at time of event (which may be in the past)
//  - keep prev pattern and timestamp of change to current pattern
//  - need proper wraparound-compensated comparison
//  - will need to avoid changing pattern more than once per ~250us
// can use normal 64bit param for loading memories with program - 32bit dwell, 32bit pattern, 

// could have continuous comparison of input timestamp to individual times
//    and so instant identification of correct pattern for any timestamp 
module ppg ( output wire [31:0] ppg_address,
   input wire io_clk, input wire ppg_clk, input wire reset, input wire RUN, output wire debug,
	input wire [63:0]  param_in,  output wire [63:0] param_out,
	output wire [32:0] pattern_out // use bit[31] as active marker - needs to be zero in data
);                                // or just detect pattern changes in vme block
assign debug = 1'b0; //clk_count[6];
assign pattern_out = (RUN) ? {new_pattern,ppg_pattern} : {new_pattern,test_pattern};
wire   new_pattern = ppg_update || run_start || run_stop;

wire run_start = RUN && !run_reg;  wire run_stop = !RUN && run_reg;   // oneshots
reg run_reg;   always @(posedge io_clk) run_reg   <= RUN;  
wire ppg_reset_request = reset || run_start || run_stop;
 
wire [639:0] ppg_program; // 20 x 32bits - 10x64 - 64,128,192,256,320,384,448,512,576,640
param_io par (
   .clk(io_clk),  .reset(reset),  .param_in(param_in),  .param_out(param_out),  .pulser_ctrl(test_pattern),  .trig_deadtime(ppg_address),
	.coinc_param(ppg_program[511:0]), .prescales(ppg_program[639:512]) // 16 + 4 x 32bits
);
wire [31:0] test_pattern;

wire [31:0] ppg_pattern =  (ppg_addr == 4'h0) ? ppg_program[ 63: 32] : (ppg_addr == 4'h1) ? ppg_program[127: 96] :
                           (ppg_addr == 4'h2) ? ppg_program[191:160] : (ppg_addr == 4'h3) ? ppg_program[255:224] :
                           (ppg_addr == 4'h4) ? ppg_program[319:288] : (ppg_addr == 4'h5) ? ppg_program[383:352] :
                           (ppg_addr == 4'h6) ? ppg_program[447:416] : (ppg_addr == 4'h7) ? ppg_program[511:480] :
                           (ppg_addr == 4'h8) ? ppg_program[575:544] : ppg_program[639:608];

wire [31:0] nxt_ppg_time = (ppg_addr == 4'h0) ? ppg_program[ 95: 64] : (ppg_addr == 4'h1) ? ppg_program[159:128] :
 /* Next ppg time i.e.  */ (ppg_addr == 4'h2) ? ppg_program[223:192] : (ppg_addr == 4'h3) ? ppg_program[287:256] :
 /*   One After current */ (ppg_addr == 4'h4) ? ppg_program[351:320] : (ppg_addr == 4'h5) ? ppg_program[415:384] :
                           (ppg_addr == 4'h6) ? ppg_program[479:448] : (ppg_addr == 4'h7) ? ppg_program[543:512] :
                           (ppg_addr == 4'h8) ? ppg_program[607:576] : ppg_program[31:0];
									
// set ppg_update on io_clk where it will be used
reg pat_done, prv_done, prvprv_done, ppg_update;
always @ ( posedge io_clk ) begin
   pat_done    <= ppg_change;                  // state lasts for many clks, but metastable on change clk
	prv_done    <= pat_done;                    // prv_done should be a reliable version of pat_done
	prvprv_done <= prv_done;                    // so use prv_done != prvprv_done
   ppg_update  <= (prv_done && !prvprv_done);  // 
end                                            // 

// set reset-request on fast clock - wait on slow clock to clear it
reg ppg_reset;  always @ ( posedge io_clk ) ppg_reset <= ( ppg_reset_request ) ? 1'b1 : (ppg_reset_done) ? 1'b0 : ppg_reset;

reg [3:0] ppg_addr;  reg [31:0] ppg_count; reg ppg_reset_done;  reg ppg_change;
always @ ( posedge ppg_clk ) begin
   if( ppg_reset ) begin
	   ppg_addr <= 4'h0;  ppg_count <= ppg_program[31:0];  ppg_reset_done <= 1'b1;  ppg_change <= 1'b0;
	end else begin
	   ppg_addr <= ppg_addr;  ppg_reset_done <= 1'b0;  ppg_change <= 1'b0;
		if( RUN ) begin
		   ppg_count <= ppg_count - 32'h1;
		   if( ppg_count == 32'h1 ) begin
			   ppg_change <= 1'b1;
			   if( nxt_ppg_time == 32'h0 ) begin // zero duration signals the end of valid sequence data - wrap early
				   ppg_addr <= 4'h0; ppg_count <= ppg_program[31:0];
				end else begin
		         ppg_addr <= (ppg_addr == 4'h9) ? 4'h0 : ppg_addr + 4'h1; // wrap at 9
			      ppg_count <= nxt_ppg_time;
				end
		   end
		end else ppg_count <= ppg_count;  
   end
end

endmodule
