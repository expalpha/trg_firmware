// maintain 32 8-bit counters - Ge and bgo - for each of 16 clovers
module clov_counters ( input wire clk,   input wire reset,
   input wire         event_in,          input wire event_out,
	input wire   [3:0] dettype_in,        input wire [3:0] dettype_out,
	input wire   [3:0] flags_in,          input wire [3:0] flags_out,
	output wire [31:0] clovermult_out
);
assign clovermult_out = {crystal_mult_out,suppressed_multiplicity,bgo_multiplicity,clover_multiplicity};

// CUSTOM Clover multiplicity
//    maintain 16 small multiplicities for events in each of 16 clovers - allow 8bits - don't want to overflow
//    can then get clover multiplicity from #non-zero counts above
// do similar for bgo

wire [7:0] crystal_mult_out = (suppressed_crystal_multiplicity > 12'hFF) ? 8'hFF : suppressed_crystal_multiplicity[7:0]; // clip to 8bits

wire [11:0] suppressed_crystal_multiplicity =
   (bgo_present[ 0] ? 8'h0 : clover_mult[  7:  0]) + (bgo_present[ 1] ? 8'h0 : clover_mult[ 15:  8]) +
	(bgo_present[ 2] ? 8'h0 : clover_mult[ 23: 16]) + (bgo_present[ 3] ? 8'h0 : clover_mult[ 31: 24]) +
   (bgo_present[ 4] ? 8'h0 : clover_mult[ 39: 32]) + (bgo_present[ 5] ? 8'h0 : clover_mult[ 47: 40]) +
	(bgo_present[ 5] ? 8'h0 : clover_mult[ 55: 48]) + (bgo_present[ 7] ? 8'h0 : clover_mult[ 63: 56]) +
   (bgo_present[ 8] ? 8'h0 : clover_mult[ 71: 64]) + (bgo_present[ 9] ? 8'h0 : clover_mult[ 79: 72]) +
	(bgo_present[10] ? 8'h0 : clover_mult[ 87: 80]) + (bgo_present[11] ? 8'h0 : clover_mult[ 95: 88]) +
   (bgo_present[12] ? 8'h0 : clover_mult[103: 96]) + (bgo_present[13] ? 8'h0 : clover_mult[111:104]) +
	(bgo_present[14] ? 8'h0 : clover_mult[119:112]) + (bgo_present[15] ? 8'h0 : clover_mult[127:120]);

wire [15:0] clover_present = {
   (clover_mult[  7:  0] != 8'h0),  (clover_mult[ 15:  8] != 8'h0),  (clover_mult[ 23: 16] != 8'h0),  (clover_mult[ 31: 24] != 8'h0),
   (clover_mult[ 39: 32] != 8'h0),  (clover_mult[ 47: 40] != 8'h0),  (clover_mult[ 55: 48] != 8'h0),  (clover_mult[ 63: 56] != 8'h0),
   (clover_mult[ 71: 64] != 8'h0),  (clover_mult[ 79: 72] != 8'h0),  (clover_mult[ 87: 80] != 8'h0),  (clover_mult[ 95: 88] != 8'h0),
   (clover_mult[103: 96] != 8'h0),  (clover_mult[111:104] != 8'h0),  (clover_mult[119:112] != 8'h0),  (clover_mult[127:120] != 8'h0)
};
wire [15:0] bgo_present = {
   (bgo_mult[  7:  0] != 8'h0),  (bgo_mult[ 15:  8] != 8'h0),  (bgo_mult[ 23: 16] != 8'h0),  (bgo_mult[ 31: 24] != 8'h0),
   (bgo_mult[ 39: 32] != 8'h0),  (bgo_mult[ 47: 40] != 8'h0),  (bgo_mult[ 55: 48] != 8'h0),  (bgo_mult[ 63: 56] != 8'h0),
   (bgo_mult[ 71: 64] != 8'h0),  (bgo_mult[ 79: 72] != 8'h0),  (bgo_mult[ 87: 80] != 8'h0),  (bgo_mult[ 95: 88] != 8'h0),
   (bgo_mult[103: 96] != 8'h0),  (bgo_mult[111:104] != 8'h0),  (bgo_mult[119:112] != 8'h0),  (bgo_mult[127:120] != 8'h0)
};
wire [15:0] suppressed = {(clover_present[15] & !bgo_present[15]), (clover_present[14] & !bgo_present[14]),
                          (clover_present[13] & !bgo_present[13]), (clover_present[12] & !bgo_present[12]),
                          (clover_present[11] & !bgo_present[11]), (clover_present[10] & !bgo_present[10]),
                          (clover_present[ 9] & !bgo_present[ 9]), (clover_present[ 8] & !bgo_present[ 8]),
                          (clover_present[ 7] & !bgo_present[ 7]), (clover_present[ 6] & !bgo_present[ 6]),
                          (clover_present[ 5] & !bgo_present[ 5]), (clover_present[ 4] & !bgo_present[ 4]),
                          (clover_present[ 3] & !bgo_present[ 3]), (clover_present[ 2] & !bgo_present[ 2]),
                          (clover_present[ 1] & !bgo_present[ 1]), (clover_present[ 0] & !bgo_present[ 0])};

wire [7:0] clover_multiplicity = clover_present[ 0] + clover_present[ 1] + clover_present[ 2] + clover_present[ 3] + 
                                 clover_present[ 4] + clover_present[ 5] + clover_present[ 6] + clover_present[ 7] + 
                                 clover_present[ 8] + clover_present[ 9] + clover_present[10] + clover_present[11] + 
                                 clover_present[12] + clover_present[13] + clover_present[14] + clover_present[15];

wire [7:0] bgo_multiplicity = bgo_present[ 0] + bgo_present[ 1] + bgo_present[ 2] + bgo_present[ 3] + 
                              bgo_present[ 4] + bgo_present[ 5] + bgo_present[ 6] + bgo_present[ 7] + 
                              bgo_present[ 8] + bgo_present[ 9] + bgo_present[10] + bgo_present[11] + 
                              bgo_present[12] + bgo_present[13] + bgo_present[14] + bgo_present[15];

wire [7:0] suppressed_multiplicity = suppressed[ 0] + suppressed[ 1] + suppressed[ 2] + suppressed[ 3] + 
                                     suppressed[ 4] + suppressed[ 5] + suppressed[ 6] + suppressed[ 7] + 
                                     suppressed[ 8] + suppressed[ 9] + suppressed[10] + suppressed[11] + 
                                     suppressed[12] + suppressed[13] + suppressed[14] + suppressed[15];

wire [3:0] Ge_HighGain = 4'h1;  wire [3:0] Ge_BGO = 4'h7;
// 4 bits of flags contain clov# for Ge/BGO fragments
wire [5:0] clovnum_in  = {( (dettype_in  == Ge_HighGain) ? 2'h0 : (dettype_in  == Ge_BGO) ? 2'h1 : 2'h3 ), flags_in};
wire [5:0] clovnum_out = {( (dettype_out == Ge_HighGain) ? 2'h0 : (dettype_out == Ge_BGO) ? 2'h1 : 2'h3 ), flags_out};

wire [15:0] inc_clov;  wire [15:0] dec_clov;
assign inc_clov[ 0] = event_in & (clovnum_in == 6'h00);   assign dec_clov[ 0] = event_out & (clovnum_out == 6'h00);
assign inc_clov[ 1] = event_in & (clovnum_in == 6'h01);   assign dec_clov[ 1] = event_out & (clovnum_out == 6'h01);
assign inc_clov[ 2] = event_in & (clovnum_in == 6'h02);   assign dec_clov[ 2] = event_out & (clovnum_out == 6'h02);
assign inc_clov[ 3] = event_in & (clovnum_in == 6'h03);   assign dec_clov[ 3] = event_out & (clovnum_out == 6'h03);
assign inc_clov[ 4] = event_in & (clovnum_in == 6'h04);   assign dec_clov[ 4] = event_out & (clovnum_out == 6'h04);
assign inc_clov[ 5] = event_in & (clovnum_in == 6'h05);   assign dec_clov[ 5] = event_out & (clovnum_out == 6'h05);
assign inc_clov[ 6] = event_in & (clovnum_in == 6'h06);   assign dec_clov[ 6] = event_out & (clovnum_out == 6'h06);
assign inc_clov[ 7] = event_in & (clovnum_in == 6'h07);   assign dec_clov[ 7] = event_out & (clovnum_out == 6'h07);
assign inc_clov[ 8] = event_in & (clovnum_in == 6'h08);   assign dec_clov[ 8] = event_out & (clovnum_out == 6'h08);
assign inc_clov[ 9] = event_in & (clovnum_in == 6'h09);   assign dec_clov[ 9] = event_out & (clovnum_out == 6'h09);
assign inc_clov[10] = event_in & (clovnum_in == 6'h0A);   assign dec_clov[10] = event_out & (clovnum_out == 6'h0A);
assign inc_clov[11] = event_in & (clovnum_in == 6'h0B);   assign dec_clov[11] = event_out & (clovnum_out == 6'h0B);
assign inc_clov[12] = event_in & (clovnum_in == 6'h0C);   assign dec_clov[12] = event_out & (clovnum_out == 6'h0C);
assign inc_clov[13] = event_in & (clovnum_in == 6'h0D);   assign dec_clov[13] = event_out & (clovnum_out == 6'h0D);
assign inc_clov[14] = event_in & (clovnum_in == 6'h0E);   assign dec_clov[14] = event_out & (clovnum_out == 6'h0E);
assign inc_clov[15] = event_in & (clovnum_in == 6'h0F);   assign dec_clov[15] = event_out & (clovnum_out == 6'h0F);

wire [15:0] inc_bgo;  wire [15:0] dec_bgo;
assign inc_bgo [ 0] = event_in & (clovnum_in == 6'h10);   assign dec_bgo [ 0] = event_out & (clovnum_out == 6'h10);
assign inc_bgo [ 1] = event_in & (clovnum_in == 6'h11);   assign dec_bgo [ 1] = event_out & (clovnum_out == 6'h11);
assign inc_bgo [ 2] = event_in & (clovnum_in == 6'h12);   assign dec_bgo [ 2] = event_out & (clovnum_out == 6'h12);
assign inc_bgo [ 3] = event_in & (clovnum_in == 6'h13);   assign dec_bgo [ 3] = event_out & (clovnum_out == 6'h13);
assign inc_bgo [ 4] = event_in & (clovnum_in == 6'h14);   assign dec_bgo [ 4] = event_out & (clovnum_out == 6'h14);
assign inc_bgo [ 5] = event_in & (clovnum_in == 6'h15);   assign dec_bgo [ 5] = event_out & (clovnum_out == 6'h15);
assign inc_bgo [ 6] = event_in & (clovnum_in == 6'h16);   assign dec_bgo [ 6] = event_out & (clovnum_out == 6'h16);
assign inc_bgo [ 7] = event_in & (clovnum_in == 6'h17);   assign dec_bgo [ 7] = event_out & (clovnum_out == 6'h17);
assign inc_bgo [ 8] = event_in & (clovnum_in == 6'h18);   assign dec_bgo [ 8] = event_out & (clovnum_out == 6'h18);
assign inc_bgo [ 9] = event_in & (clovnum_in == 6'h19);   assign dec_bgo [ 9] = event_out & (clovnum_out == 6'h19);
assign inc_bgo [10] = event_in & (clovnum_in == 6'h1A);   assign dec_bgo [10] = event_out & (clovnum_out == 6'h1A);
assign inc_bgo [11] = event_in & (clovnum_in == 6'h1B);   assign dec_bgo [11] = event_out & (clovnum_out == 6'h1B);
assign inc_bgo [12] = event_in & (clovnum_in == 6'h1C);   assign dec_bgo [12] = event_out & (clovnum_out == 6'h1C);
assign inc_bgo [13] = event_in & (clovnum_in == 6'h1D);   assign dec_bgo [13] = event_out & (clovnum_out == 6'h1D);
assign inc_bgo [14] = event_in & (clovnum_in == 6'h1E);   assign dec_bgo [14] = event_out & (clovnum_out == 6'h1E);
assign inc_bgo [15] = event_in & (clovnum_in == 6'h1F);   assign dec_bgo [15] = event_out & (clovnum_out == 6'h1F);

reg [127:0] clover_mult; 
always @ (posedge clk or posedge reset) begin
   if( reset ) clover_mult <= 128'h0;
	else begin  clover_mult <= clover_mult;
	   if(      inc_clov[ 0] && !dec_clov[ 0] ) clover_mult[  7:  0] <= clover_mult[  7:  0] + 8'h1;
	   else if( dec_clov[ 0] && !inc_clov[ 0] ) clover_mult[  7:  0] <= clover_mult[  7:  0] - 8'h1;
	   if(      inc_clov[ 1] && !dec_clov[ 1] ) clover_mult[ 15:  8] <= clover_mult[ 15:  8] + 8'h1;
	   else if( dec_clov[ 1] && !inc_clov[ 1] ) clover_mult[ 15:  8] <= clover_mult[ 15:  8] - 8'h1;
	   if(      inc_clov[ 2] && !dec_clov[ 2] ) clover_mult[ 23: 16] <= clover_mult[ 23: 16] + 8'h1;
	   else if( dec_clov[ 2] && !inc_clov[ 2] ) clover_mult[ 23: 16] <= clover_mult[ 23: 16] - 8'h1;
	   if(      inc_clov[ 3] && !dec_clov[ 3] ) clover_mult[ 31: 24] <= clover_mult[ 31: 24] + 8'h1;
	   else if( dec_clov[ 3] && !inc_clov[ 3] ) clover_mult[ 31: 24] <= clover_mult[ 31: 24] - 8'h1;
	   if(      inc_clov[ 4] && !dec_clov[ 4] ) clover_mult[ 39: 32] <= clover_mult[ 39: 32] + 8'h1;
	   else if( dec_clov[ 4] && !inc_clov[ 4] ) clover_mult[ 39: 32] <= clover_mult[ 39: 32] - 8'h1;
	   if(      inc_clov[ 5] && !dec_clov[ 5] ) clover_mult[ 47: 30] <= clover_mult[ 47: 40] + 8'h1;
	   else if( dec_clov[ 5] && !inc_clov[ 5] ) clover_mult[ 47: 40] <= clover_mult[ 47: 40] - 8'h1;
	   if(      inc_clov[ 6] && !dec_clov[ 6] ) clover_mult[ 55: 48] <= clover_mult[ 55: 48] + 8'h1;
	   else if( dec_clov[ 6] && !inc_clov[ 6] ) clover_mult[ 55: 48] <= clover_mult[ 55: 48] - 8'h1;
	   if(      inc_clov[ 7] && !dec_clov[ 7] ) clover_mult[ 63: 56] <= clover_mult[ 63: 56] + 8'h1;
	   else if( dec_clov[ 7] && !inc_clov[ 7] ) clover_mult[ 63: 56] <= clover_mult[ 63: 56] - 8'h1;
	   if(      inc_clov[ 8] && !dec_clov[ 8] ) clover_mult[ 71: 64] <= clover_mult[ 71: 64] + 8'h1;
	   else if( dec_clov[ 8] && !inc_clov[ 8] ) clover_mult[ 71: 64] <= clover_mult[ 71: 64] - 8'h1;
	   if(      inc_clov[ 9] && !dec_clov[ 9] ) clover_mult[ 79: 72] <= clover_mult[ 79: 72] + 8'h1;
	   else if( dec_clov[ 9] && !inc_clov[ 9] ) clover_mult[ 79: 72] <= clover_mult[ 79: 72] - 8'h1;
	   if(      inc_clov[10] && !dec_clov[10] ) clover_mult[ 87: 80] <= clover_mult[ 87: 80] + 8'h1;
	   else if( dec_clov[10] && !inc_clov[10] ) clover_mult[ 87: 80] <= clover_mult[ 87: 80] - 8'h1;
	   if(      inc_clov[11] && !dec_clov[11] ) clover_mult[ 95: 88] <= clover_mult[ 95: 88] + 8'h1;
	   else if( dec_clov[11] && !inc_clov[11] ) clover_mult[ 95: 88] <= clover_mult[ 95: 88] - 8'h1;
	   if(      inc_clov[12] && !dec_clov[12] ) clover_mult[103: 96] <= clover_mult[103: 96] + 8'h1;
	   else if( dec_clov[12] && !inc_clov[12] ) clover_mult[103: 96] <= clover_mult[103: 96] - 8'h1;
	   if(      inc_clov[13] && !dec_clov[13] ) clover_mult[111:104] <= clover_mult[111:104] + 8'h1;
	   else if( dec_clov[13] && !inc_clov[13] ) clover_mult[111:104] <= clover_mult[111:104] - 8'h1;
	   if(      inc_clov[14] && !dec_clov[14] ) clover_mult[119:112] <= clover_mult[119:112] + 8'h1;
	   else if( dec_clov[14] && !inc_clov[14] ) clover_mult[119:112] <= clover_mult[119:112] - 8'h1;
	   if(      inc_clov[15] && !dec_clov[15] ) clover_mult[127:120] <= clover_mult[127:120] + 8'h1;
	   else if( dec_clov[15] && !inc_clov[15] ) clover_mult[127:120] <= clover_mult[127:120] - 8'h1;
   end
end
		
reg [127:0] bgo_mult;
always @ (posedge clk or posedge reset) begin
   if( reset ) bgo_mult <= 128'h0;
	else begin  bgo_mult <= bgo_mult;
	   if(      inc_bgo[ 0] && !dec_bgo[ 0] ) bgo_mult[  7:  0] <= bgo_mult[  7:  0] + 8'h1;
	   else if( dec_bgo[ 0] && !inc_bgo[ 0] ) bgo_mult[  7:  0] <= bgo_mult[  7:  0] - 8'h1;
	   if(      inc_bgo[ 1] && !dec_bgo[ 1] ) bgo_mult[ 15:  8] <= bgo_mult[ 15:  8] + 8'h1;
	   else if( dec_bgo[ 1] && !inc_bgo[ 1] ) bgo_mult[ 15:  8] <= bgo_mult[ 15:  8] - 8'h1;
	   if(      inc_bgo[ 2] && !dec_bgo[ 2] ) bgo_mult[ 23: 16] <= bgo_mult[ 23: 16] + 8'h1;
	   else if( dec_bgo[ 2] && !inc_bgo[ 2] ) bgo_mult[ 23: 16] <= bgo_mult[ 23: 16] - 8'h1;
	   if(      inc_bgo[ 3] && !dec_bgo[ 3] ) bgo_mult[ 31: 24] <= bgo_mult[ 31: 24] + 8'h1;
	   else if( dec_bgo[ 3] && !inc_bgo[ 3] ) bgo_mult[ 31: 24] <= bgo_mult[ 31: 24] - 8'h1;
	   if(      inc_bgo[ 4] && !dec_bgo[ 4] ) bgo_mult[ 39: 32] <= bgo_mult[ 39: 32] + 8'h1;
	   else if( dec_bgo[ 4] && !inc_bgo[ 4] ) bgo_mult[ 39: 32] <= bgo_mult[ 39: 32] - 8'h1;
	   if(      inc_bgo[ 5] && !dec_bgo[ 5] ) bgo_mult[ 47: 30] <= bgo_mult[ 47: 40] + 8'h1;
	   else if( dec_bgo[ 5] && !inc_bgo[ 5] ) bgo_mult[ 47: 40] <= bgo_mult[ 47: 40] - 8'h1;
	   if(      inc_bgo[ 6] && !dec_bgo[ 6] ) bgo_mult[ 55: 48] <= bgo_mult[ 55: 48] + 8'h1;
	   else if( dec_bgo[ 6] && !inc_bgo[ 6] ) bgo_mult[ 55: 48] <= bgo_mult[ 55: 48] - 8'h1;
	   if(      inc_bgo[ 7] && !dec_bgo[ 7] ) bgo_mult[ 63: 56] <= bgo_mult[ 63: 56] + 8'h1;
	   else if( dec_bgo[ 7] && !inc_bgo[ 7] ) bgo_mult[ 63: 56] <= bgo_mult[ 63: 56] - 8'h1;
	   if(      inc_bgo[ 8] && !dec_bgo[ 8] ) bgo_mult[ 71: 64] <= bgo_mult[ 71: 64] + 8'h1;
	   else if( dec_bgo[ 8] && !inc_bgo[ 8] ) bgo_mult[ 71: 64] <= bgo_mult[ 71: 64] - 8'h1;
	   if(      inc_bgo[ 9] && !dec_bgo[ 9] ) bgo_mult[ 79: 72] <= bgo_mult[ 79: 72] + 8'h1;
	   else if( dec_bgo[ 9] && !inc_bgo[ 9] ) bgo_mult[ 79: 72] <= bgo_mult[ 79: 72] - 8'h1;
	   if(      inc_bgo[10] && !dec_bgo[10] ) bgo_mult[ 87: 80] <= bgo_mult[ 87: 80] + 8'h1;
	   else if( dec_bgo[10] && !inc_bgo[10] ) bgo_mult[ 87: 80] <= bgo_mult[ 87: 80] - 8'h1;
	   if(      inc_bgo[11] && !dec_bgo[11] ) bgo_mult[ 95: 88] <= bgo_mult[ 95: 88] + 8'h1;
	   else if( dec_bgo[11] && !inc_bgo[11] ) bgo_mult[ 95: 88] <= bgo_mult[ 95: 88] - 8'h1;
	   if(      inc_bgo[12] && !dec_bgo[12] ) bgo_mult[103: 96] <= bgo_mult[103: 96] + 8'h1;
	   else if( dec_bgo[12] && !inc_bgo[12] ) bgo_mult[103: 96] <= bgo_mult[103: 96] - 8'h1;
	   if(      inc_bgo[13] && !dec_bgo[13] ) bgo_mult[111:104] <= bgo_mult[111:104] + 8'h1;
	   else if( dec_bgo[13] && !inc_bgo[13] ) bgo_mult[111:104] <= bgo_mult[111:104] - 8'h1;
	   if(      inc_bgo[14] && !dec_bgo[14] ) bgo_mult[119:112] <= bgo_mult[119:112] + 8'h1;
	   else if( dec_bgo[14] && !inc_bgo[14] ) bgo_mult[119:112] <= bgo_mult[119:112] - 8'h1;
	   if(      inc_bgo[15] && !dec_bgo[15] ) bgo_mult[127:120] <= bgo_mult[127:120] + 8'h1;
	   else if( dec_bgo[15] && !inc_bgo[15] ) bgo_mult[127:120] <= bgo_mult[127:120] - 8'h1;
   end
end
	
endmodule
