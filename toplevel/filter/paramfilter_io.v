module paramfilter_io ( input wire clk,     input wire reset,  // runs on io clock at the moment
   input wire [  63:0]      param_in,  output reg  [63:0]  param_out,   output reg [15:0] det_type_mask,
	output reg   [31:0] trig_deadtime,  output reg  [31:0]        csr,  // **csr[7:0] autocleared after cnt clks**
	output reg [2047:0]   coinc_param,  output reg [255:0]  prescales,   output reg [9:0] coinc_width,
	output reg    [5:0]    ge_address,  output reg  [31:0] pulser_ctrl
);

wire        par_valid = param_in[63];
wire        readback  = param_in[62];
wire [13:0] par_id    = param_in[61:48];
wire [15:0] chan      = param_in[47:32];
wire [31:0] par_value = param_in[31: 0];

wire par_active = par_valid && !parvalid_reg;  reg parvalid_reg; always @(posedge clk) parvalid_reg <= par_valid;  // oneshot

reg [2:0] csr_cnt;
always @ ( posedge clk or posedge reset ) begin
   if( reset ) begin
		param_out    <=   64'h0;      csr           <=  32'h0;  trig_deadtime <= 32'h0;   csr_cnt <= 3'h0;     ge_address <= 6'h0;                  
		coinc_param  <= 2048'h0;      prescales     <= 256'h0;  coinc_width   <= 10'h1;   det_type_mask <= 16'hFFFF;
		pulser_ctrl  <=   32'h0;
	end else if( csr[0] ) begin // programmable filter program reset
		param_out    <=   64'h0;      csr           <=  32'h0;  trig_deadtime <= 32'h0;   csr_cnt <= 3'h0;     ge_address <= 6'h0;                  
		coinc_param  <= 2048'h0;      prescales     <= 256'h0;  coinc_width   <= 10'h1;   det_type_mask <= 16'hFFFF;
		pulser_ctrl  <=   32'h0;
	end else begin
		param_out    <= {1'b0,param_out[62:0]};                 trig_deadtime <= trig_deadtime;   det_type_mask <= det_type_mask;
		coinc_param  <= coinc_param;  prescales  <= prescales;  coinc_width   <= coinc_width;     ge_address    <= ge_address;
	   pulser_ctrl  <= pulser_ctrl;
		// auto-clear csr after small delay
		csr <= ( csr_cnt != 3'h0 ) ? csr : {csr[31:8],8'h0};  csr_cnt <= ( csr_cnt != 3'h0 ) ? csr_cnt - 3'h1 : 3'h0;
		if( par_active ) begin
		   if( ! readback ) begin
            case( par_id )
            14'h02: pulser_ctrl          <= par_value;
            14'h03: trig_deadtime        <= par_value;
			   14'h3D: begin csr            <= csr & ~par_value;  csr_cnt <= 3'h7; end // Selective clear
			   14'h3E: begin csr            <= csr |  par_value;  csr_cnt <= 3'h7; end // Selective set
			   14'h3F: begin csr            <= par_value;         csr_cnt <= 3'h7; end 
				14'h80: coinc_param[ 31:  0] <= par_value;
				14'h81: coinc_param[ 63: 32] <= par_value;
				14'h82: coinc_param[ 95: 64] <= par_value;
				14'h83: coinc_param[127: 96] <= par_value;
				14'h84: coinc_param[159:128] <= par_value;
				14'h85: coinc_param[191:160] <= par_value;
				14'h86: coinc_param[223:192] <= par_value;
				14'h87: coinc_param[255:224] <= par_value;
				14'h88: coinc_param[287:256] <= par_value;
				14'h89: coinc_param[319:288] <= par_value;
				14'h8A: coinc_param[351:320] <= par_value;
				14'h8B: coinc_param[383:352] <= par_value;
				14'h8C: coinc_param[415:384] <= par_value;
				14'h8D: coinc_param[447:416] <= par_value;
				14'h8E: coinc_param[479:448] <= par_value;
				14'h8F: coinc_param[511:480] <= par_value;
				
				14'h90: coinc_param[543:512] <= par_value;
				14'h91: coinc_param[575:544] <= par_value;
				14'h92: coinc_param[607:576] <= par_value;
				14'h93: coinc_param[639:608] <= par_value;
				14'h94: coinc_param[671:640] <= par_value;
				14'h95: coinc_param[703:672] <= par_value;
				14'h96: coinc_param[735:704] <= par_value;
				14'h97: coinc_param[767:736] <= par_value;
				14'h98: coinc_param[799:768] <= par_value;
				14'h99: coinc_param[831:800] <= par_value;
				14'h9A: coinc_param[863:832] <= par_value;
				14'h9B: coinc_param[895:864] <= par_value;
				14'h9C: coinc_param[927:896] <= par_value;
				14'h9D: coinc_param[959:928] <= par_value;
				14'h9E: coinc_param[991:960] <= par_value;
				14'h9F: coinc_param[1023:992] <= par_value;

				14'hA0: coinc_param[1055:1024] <= par_value;
				14'hA1: coinc_param[1087:1056] <= par_value;
				14'hA2: coinc_param[1119:1088] <= par_value;
				14'hA3: coinc_param[1151:1120] <= par_value;
				14'hA4: coinc_param[1183:1152] <= par_value;
				14'hA5: coinc_param[1215:1184] <= par_value;
				14'hA6: coinc_param[1247:1216] <= par_value;
				14'hA7: coinc_param[1279:1248] <= par_value;
				14'hA8: coinc_param[1311:1280] <= par_value;
				14'hA9: coinc_param[1343:1312] <= par_value;
				14'hAA: coinc_param[1375:1344] <= par_value;
				14'hAB: coinc_param[1407:1376] <= par_value;
				14'hAC: coinc_param[1439:1408] <= par_value;
				14'hAD: coinc_param[1471:1440] <= par_value;
				14'hAE: coinc_param[1503:1472] <= par_value;
				14'hAF: coinc_param[1535:1504] <= par_value;
				
				14'hB0: coinc_param[1567:1536] <= par_value;
				14'hB1: coinc_param[1599:1568] <= par_value;
				14'hB2: coinc_param[1631:1600] <= par_value;
				14'hB3: coinc_param[1663:1632] <= par_value;
				14'hB4: coinc_param[1695:1664] <= par_value;
				14'hB5: coinc_param[1727:1696] <= par_value;
				14'hB6: coinc_param[1759:1728] <= par_value;
				14'hB7: coinc_param[1791:1760] <= par_value;
				14'hB8: coinc_param[1823:1792] <= par_value;
				14'hB9: coinc_param[1855:1824] <= par_value;
				14'hBA: coinc_param[1887:1856] <= par_value;
				14'hBB: coinc_param[1919:1888] <= par_value;
				14'hBC: coinc_param[1951:1920] <= par_value;
				14'hBD: coinc_param[1983:1952] <= par_value;
				14'hBE: coinc_param[2015:1984] <= par_value;
				14'hBF: coinc_param[2047:2016] <= par_value;

				14'hC0:   prescales[ 31:  0] <= par_value;
				14'hC1:   prescales[ 63: 32] <= par_value;
				14'hC2:   prescales[ 95: 64] <= par_value;
				14'hC3:   prescales[127: 96] <= par_value;
				14'hC4:   prescales[159:128] <= par_value;
				14'hC5:   prescales[191:160] <= par_value;
				14'hC6:   prescales[223:192] <= par_value;
				14'hC7:   prescales[255:224] <= par_value;
				14'hC8: coinc_width          <= par_value[ 9:0];
				14'hC9: det_type_mask        <= par_value[15:0];
				14'hCA: ge_address           <= par_value[ 5:0];
            endcase
			end else begin
            case( par_id )
            14'h002:  param_out <= {2'h3, par_id, chan,           pulser_ctrl};
            14'h00E:  param_out <= {2'h3, par_id, chan,         trig_deadtime};
            14'h03D:  param_out <= {2'h3, par_id, chan,                 csr  };
            14'h03E:  param_out <= {2'h3, par_id, chan,                 csr  };
            14'h03F:  param_out <= {2'h3, par_id, chan,                 csr  };
				
				14'h080:  param_out <= {2'h3, par_id, chan, coinc_param[ 31:  0] };
				14'h081:  param_out <= {2'h3, par_id, chan, coinc_param[ 63: 32] };
				14'h082:  param_out <= {2'h3, par_id, chan, coinc_param[ 95: 64] };
				14'h083:  param_out <= {2'h3, par_id, chan, coinc_param[127: 96] };
				14'h084:  param_out <= {2'h3, par_id, chan, coinc_param[159:128] };
				14'h085:  param_out <= {2'h3, par_id, chan, coinc_param[191:160] };
				14'h086:  param_out <= {2'h3, par_id, chan, coinc_param[223:192] };
				14'h087:  param_out <= {2'h3, par_id, chan, coinc_param[255:224] };
				14'h088:  param_out <= {2'h3, par_id, chan, coinc_param[287:256] };
				14'h089:  param_out <= {2'h3, par_id, chan, coinc_param[319:288] };
				14'h08A:  param_out <= {2'h3, par_id, chan, coinc_param[351:320] };
				14'h08B:  param_out <= {2'h3, par_id, chan, coinc_param[383:352] };
				14'h08C:  param_out <= {2'h3, par_id, chan, coinc_param[415:384] };
				14'h08D:  param_out <= {2'h3, par_id, chan, coinc_param[447:416] };
				14'h08E:  param_out <= {2'h3, par_id, chan, coinc_param[479:448] };
				14'h08F:  param_out <= {2'h3, par_id, chan, coinc_param[511:480] };
				
				14'h090:  param_out <= {2'h3, par_id, chan, coinc_param[543:512] };
				14'h091:  param_out <= {2'h3, par_id, chan, coinc_param[575:544] };
				14'h092:  param_out <= {2'h3, par_id, chan, coinc_param[607:576] };
				14'h093:  param_out <= {2'h3, par_id, chan, coinc_param[639:608] };
				14'h094:  param_out <= {2'h3, par_id, chan, coinc_param[671:640] };
				14'h095:  param_out <= {2'h3, par_id, chan, coinc_param[703:672] };
				14'h096:  param_out <= {2'h3, par_id, chan, coinc_param[735:704] };
				14'h097:  param_out <= {2'h3, par_id, chan, coinc_param[767:736] };
				14'h098:  param_out <= {2'h3, par_id, chan, coinc_param[799:768] };
				14'h099:  param_out <= {2'h3, par_id, chan, coinc_param[831:800] };
				14'h09A:  param_out <= {2'h3, par_id, chan, coinc_param[863:832] };
				14'h09B:  param_out <= {2'h3, par_id, chan, coinc_param[895:864] };
				14'h09C:  param_out <= {2'h3, par_id, chan, coinc_param[927:896] };
				14'h09D:  param_out <= {2'h3, par_id, chan, coinc_param[959:928] };
				14'h09E:  param_out <= {2'h3, par_id, chan, coinc_param[991:960] };
				14'h09F:  param_out <= {2'h3, par_id, chan, coinc_param[1023:992] };
				
				14'h0A0:  param_out <= {2'h3, par_id, chan, coinc_param[1055:1024] };
				14'h0A1:  param_out <= {2'h3, par_id, chan, coinc_param[1087:1056] };
				14'h0A2:  param_out <= {2'h3, par_id, chan, coinc_param[1119:1088] };
				14'h0A3:  param_out <= {2'h3, par_id, chan, coinc_param[1151:1120] };
				14'h0A4:  param_out <= {2'h3, par_id, chan, coinc_param[1183:1152] };
				14'h0A5:  param_out <= {2'h3, par_id, chan, coinc_param[1215:1184] };
				14'h0a6:  param_out <= {2'h3, par_id, chan, coinc_param[1247:1216] };
				14'h0A7:  param_out <= {2'h3, par_id, chan, coinc_param[1279:1248] };
				14'h0A8:  param_out <= {2'h3, par_id, chan, coinc_param[1311:1280] };
				14'h0A9:  param_out <= {2'h3, par_id, chan, coinc_param[1343:1312] };
				14'h0AA:  param_out <= {2'h3, par_id, chan, coinc_param[1375:1344] };
				14'h0AB:  param_out <= {2'h3, par_id, chan, coinc_param[1407:1376] };
				14'h0AC:  param_out <= {2'h3, par_id, chan, coinc_param[1439:1408] };
				14'h0AD:  param_out <= {2'h3, par_id, chan, coinc_param[1471:1440] };
				14'h0AE:  param_out <= {2'h3, par_id, chan, coinc_param[1503:1472] };
				14'h0AF:  param_out <= {2'h3, par_id, chan, coinc_param[1535:1504] };
				
				14'h0B0:  param_out <= {2'h3, par_id, chan, coinc_param[1567:1536] };
				14'h0B1:  param_out <= {2'h3, par_id, chan, coinc_param[1599:1568] };
				14'h0B2:  param_out <= {2'h3, par_id, chan, coinc_param[1631:1600] };
				14'h0B3:  param_out <= {2'h3, par_id, chan, coinc_param[1663:1632] };
				14'h0B4:  param_out <= {2'h3, par_id, chan, coinc_param[1695:1664] };
				14'h0B5:  param_out <= {2'h3, par_id, chan, coinc_param[1727:1696] };
				14'h0B6:  param_out <= {2'h3, par_id, chan, coinc_param[1759:1728] };
				14'h0B7:  param_out <= {2'h3, par_id, chan, coinc_param[1791:1760] };
				14'h0B8:  param_out <= {2'h3, par_id, chan, coinc_param[1823:1792] };
				14'h0B9:  param_out <= {2'h3, par_id, chan, coinc_param[1855:1824] };
				14'h0BA:  param_out <= {2'h3, par_id, chan, coinc_param[1887:1856] };
				14'h0BB:  param_out <= {2'h3, par_id, chan, coinc_param[1919:1888] };
				14'h0BC:  param_out <= {2'h3, par_id, chan, coinc_param[1951:1920] };
				14'h0BD:  param_out <= {2'h3, par_id, chan, coinc_param[1983:1952] };
				14'h0BE:  param_out <= {2'h3, par_id, chan, coinc_param[2015:1984] };
				14'h0BF:  param_out <= {2'h3, par_id, chan, coinc_param[2047:2016] };
				
				14'h0C0:  param_out <= {2'h3, par_id, chan,   prescales[ 31:  0] };
				14'h0C1:  param_out <= {2'h3, par_id, chan,   prescales[ 63: 32] };
				14'h0C2:  param_out <= {2'h3, par_id, chan,   prescales[ 95: 64] };
				14'h0C3:  param_out <= {2'h3, par_id, chan,   prescales[127: 96] };
				14'h0C4:  param_out <= {2'h3, par_id, chan,   prescales[159:128] };
				14'h0C5:  param_out <= {2'h3, par_id, chan,   prescales[191:160] };
				14'h0C6:  param_out <= {2'h3, par_id, chan,   prescales[223:192] };
				14'h0C7:  param_out <= {2'h3, par_id, chan,   prescales[255:224] };
				14'h0C8:  param_out <= {2'h3, par_id, chan, 22'h0,   coinc_width };
				14'h0C9:  param_out <= {2'h3, par_id, chan, 16'h0, det_type_mask };
				14'h0CA:  param_out <= {2'h3, par_id, chan, 26'h0,   ge_address  };
				
				default:  param_out <= 64'hdeaddeaddeaddead;
           endcase
			end
		end
   end
end

endmodule
