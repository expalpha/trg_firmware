module statistics(
   input wire clk, input wire reset,
   input wire [7:0] ccp_cok,     // satisfied coincidence pattern
   input wire mu_strobe,         // validates
   output wire [31:0] stat_rback
);
assign stat_rback = 32'h00000000;

endmodule
