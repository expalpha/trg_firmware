// Module to store and retrieve the packet pointers in the form of linked lists.
//    There is one linked list for each non empty time slot (corresponding to one time stamp value).
//    The linked lists are constructed as each event arrives scattered in time, posting
//    NewHead, Newtail, PreviousTail, PreviousTailUpdate, HeadtailSave.
//  After the Retrieve delay which is of the order of the maximum data skew, plus the
//    maximum concidence time window (~ 100 microseconds), the time slots that are not empty
//    are sequencialy requested to be retrieved, with RetrieveHead, RetrieveTail and RetrieveHeadTailStrobe. 
//       only read functions are performed in the memory; so it can be asynchronous with the writing.
// Write: One byte specific write cycle is used to update the tail pointer of the existing tail when prev_update flag is set
//        One write cycle is used to insert the new list element;
// Read: For the extraction of the list elements, one read cycle per output stream is used,
//       but there is a latency of two clocks before the address of the next element is known from the tail pointer.
// Element structure:
//    Address of next element (4 bit (= 0) + 12 bits) Packet Pointer (4 empty + 36 bits).
//    This allows writing the next element pointer without modifying the Packet Pointer 
//    Memory size for Packet Pointers: 4096 X 56 bits
// Input FIFO : 256 (or more) X 56 [**No flow control is possible - so make sure cannot overrun**]
//    Output on 1  or 2 streams (one per linked list, each corresponding to a time stamp)
//    or more simply: or a single stream. Each stream would have an output rate of 50 MHit/sec 
// Stand alone FMax: 260 MHz, Slack: 1.14 nsec
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
module TimeSlotLinkedList(
   input  wire clk,                input wire reset,
   input  wire [11:0] tsm_head,	  input wire [11:0] tsm_tail,      input wire tsm_save,       // incoming write requests
   input  wire [39:0] tsm_pkt_ptr, input wire [11:0] tsm_prevtail,                             //   ...
   input  wire [11:0] rdbk_head,   input wire [11:0] rdbk_tail,     input wire tsm_rdbk_valid, // incoming read requests
   output reg  [39:0] ll_ptr_out,  output reg        ll_ptr_rdy,    output wire ll_empty 
);
assign ll_empty = ((ll_state == 5'h0) & empty);

reg init_fifo;
wire [23:0] ll_FifoQ;  wire empty;  reg notempty;  wire rdreq = read_fifo || init_fifo;
// buffer read requests [head/tail/valid] - these can come consecutively and each can take a while to process
RetrieveCommandFIFO Retr_FIFO ( // **No Flow Control**
	.clock(clk),                   .aclr(reset),            //.almost_full(almost_full_sig),
	.data({rdbk_head,rdbk_tail}),  .wrreq(tsm_rdbk_valid),  //.full(full_sig), 
	.q(ll_FifoQ),                  .rdreq(rdreq),           .empty(empty)
);
// Using registered FIFO as look-ahead FIFO [Auto-init: auto-register output when leaving EMPTY state]
always @ (posedge clk) init_fifo <= ( (empty == 0) && (notempty == 0) && (reset == 0) && (init_fifo == 0) );  // Going out of empty
always @ (posedge clk or posedge reset) if (reset) notempty <= 0; else notempty <= (rdreq) ? ~empty : notempty;

wire [55:0] ll_DPMQ;
LinkedListDPM list ( // LINKED LIST dualPortMem (4096 X 56) one list per timestamp, each entry has pointer to next
	.clock(clk),
	.data(ll_data),  .wraddress(ll_wraddr),  .wren(ll_wren), 
	.q(ll_DPMQ),     .rdaddress(ll_rdaddr),  .byteena_a(ll_wrmask)
);
// ADD a new element to list - one or two words to write depending if previous tail needs to be updated  
// *** To allow 2 states to complete - writes need to be 2clks apart (no consecutive write pairs)
reg [55:0] ll_data;  reg [11:0] ll_wraddr;  reg  [6:0] ll_wrmask;  reg ll_wren;
always @ (posedge clk) begin // Define flag the head element (when Tail == Head)
   ll_data <= 56'h0;  ll_wraddr <= 12'h0;  ll_wrmask <= 7'h0;  ll_wren <= 1'h0;
   if ( tsm_save )  begin
      ll_wraddr <= tsm_tail;  ll_wrmask <= 7'h7f;  ll_wren <= 1'h1;
		ll_data <= {4'h0,tsm_tail,first_elem,tsm_pkt_ptr[38:0]};
   end else if (tsm_save_dly2) begin
      ll_wraddr <= prevtail_dly2;  ll_data <= {4'hF,tail_dly2,40'h0}; ll_wrmask <= 7'h60; // only update next_elem pointer
		ll_wren   <= update_prev_dly;
   end else if (tsm_save_dly) begin
      ll_wraddr <= prevtail_dly;  ll_data <= {4'hF,tail_dly,40'h0}; ll_wrmask <= 7'h60; // only update next_elem pointer
		ll_wren   <= update_prev;
	end					
end
reg [11:0] tail_dly;   reg [11:0] prevtail_dly;   reg tsm_save_dly;  reg update_prev;  // second write state for above process
reg [11:0] tail_dly2;  reg [11:0] prevtail_dly2;  reg tsm_save_dly2; reg update_prev_dly;  
always @ (posedge clk) begin            
   tsm_save_dly  <= tsm_save;      prevtail_dly  <= tsm_prevtail;  tail_dly  <= tsm_tail;  update_prev     <= ~first_elem;
   tsm_save_dly2 <= tsm_save_dly;  prevtail_dly2 <= prevtail_dly;  tail_dly2 <= tail_dly;  update_prev_dly <= update_prev;
end
wire first_elem = (tsm_head == tsm_tail);

reg  [4:0] ll_state;  reg read_fifo;
always @ (posedge clk or posedge reset) begin // process for sequential extraction of a complete linked list
   if (reset) begin
	   ll_state <= 0;  read_fifo <= 0;
   end else begin
	   ll_state <= 0;  read_fifo <= 0;
      case (ll_state)
		0: if ((notempty) && (ll_FifoQ[23:12] == ll_FifoQ[11:0]) ) begin //Single entry for time stamp
			   read_fifo <= 1;                    //  Idle state: check for a request out of the FIFO 
			end else if (notempty) ll_state <= 1; //  Idle state: check for a request out of the FIFO 
      1: ll_state <= 2; // read cycles
      2: ll_state <= 8;
      4: ll_state <= 5;
      5: ll_state <= 8;
      8: if ( ll_DPMQ[55:52]== 4'hF ) ll_state <= 4; // Check for continuation flag, loop until the tail is extracted
			else begin ll_state <= 9;  read_fifo <= 1; end // check FIFO if not equal	 
		endcase
	end
end

reg [11:0] ll_rdaddr;
always @ (posedge clk or posedge reset) begin
   if      (  reset                        ) ll_rdaddr <= 12'hFFF; // can't use zero as reset value
   else if ( (ll_state == 0) && (notempty) ) ll_rdaddr <= ll_FifoQ[23:12]; // read head 
   else if   (ll_state >= 4)                 ll_rdaddr <= ll_DPMQ[51:40];
   else                                      ll_rdaddr <= ll_rdaddr;
end
reg [11:0] ll_prevrdaddr;  always @ (posedge clk) ll_prevrdaddr <= ll_rdaddr;

reg ptr_rdy_dly1, ptr_rdy_dly2;
always @ (posedge clk) begin // Sync outputs to next stage
	ll_ptr_rdy <= ptr_rdy_dly1;   ptr_rdy_dly1 <= ptr_rdy_dly2;   ptr_rdy_dly2 <= (ll_rdaddr != ll_prevrdaddr);
	ll_ptr_out <= {1'b0,(ll_DPMQ[55:52]== 4'h0),ll_DPMQ[37:0]};
end

endmodule 