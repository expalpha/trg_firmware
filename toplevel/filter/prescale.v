// generate the prescale flag bit [39] when the prescale count for a detector type is reached
module prescale (
   input  wire               clk,  input  wire reset,        input wire [255:0] prescales,
   input  wire [39:0] ms_ptr_out,  input  wire ms_ptr_rdy,
   output reg  [39:0] ps_ptr_out,  output reg  ps_ptr_rdy
);

reg [255:0] pktcount; wire [3:0] dettype = ms_ptr_out[23:20]; // timing was poor between dettype and pktcount[*]
// can register prescale_passed or current_pktcount, current_prescale, which makes prescale_passed 1clk delayed
// can easily delay ps_ptr to update flag
// **but pktcount needs updating immediately for next clk (could already be new pkt_ptr)

wire [15:0] current_pktcount = (dettype == 4'h0) ? pktcount[ 15:  0] : (dettype == 4'h1) ? pktcount[ 31: 16] :
                               (dettype == 4'h2) ? pktcount[ 47: 32] : (dettype == 4'h3) ? pktcount[ 63: 48] :
                               (dettype == 4'h4) ? pktcount[ 79: 64] : (dettype == 4'h5) ? pktcount[ 95: 80] :
                               (dettype == 4'h6) ? pktcount[111: 96] : (dettype == 4'h7) ? pktcount[127:112] :
                               (dettype == 4'h8) ? pktcount[143:128] : (dettype == 4'h9) ? pktcount[159:144] :
		                         (dettype == 4'hA) ? pktcount[175:160] : (dettype == 4'hB) ? pktcount[191:176] :
		                         (dettype == 4'hC) ? pktcount[207:192] : (dettype == 4'hD) ? pktcount[223:208] :
		                         (dettype == 4'hE) ? pktcount[239:224] :                     pktcount[255:240];

wire [15:0] current_prescale = (dettype == 4'h0) ? prescales[ 15:  0] : (dettype == 4'h1) ? prescales[ 31: 16] :
                               (dettype == 4'h2) ? prescales[ 47: 32] : (dettype == 4'h3) ? prescales[ 63: 48] :
                               (dettype == 4'h4) ? prescales[ 79: 64] : (dettype == 4'h5) ? prescales[ 95: 80] :
                               (dettype == 4'h6) ? prescales[111: 96] : (dettype == 4'h7) ? prescales[127:112] :
                               (dettype == 4'h8) ? prescales[143:128] : (dettype == 4'h9) ? prescales[159:144] :
		                         (dettype == 4'hA) ? prescales[175:160] : (dettype == 4'hB) ? prescales[191:176] :
		                         (dettype == 4'hC) ? prescales[207:192] : (dettype == 4'hD) ? prescales[223:208] :
		                         (dettype == 4'hE) ? prescales[239:224] :                     prescales[255:240];
										 
wire prescale_passed = (current_pktcount >= current_prescale); // (only valid if ms_ptr_rdy)

always @ (posedge clk or posedge reset) begin
   if ( reset ) begin
	   ps_ptr_rdy <= 1'b0; ps_ptr_out <= 40'h0; pktcount <= {16{8'h1}}; // start counts at 1 not zero - to match parameter
	end else begin
	   ps_ptr_rdy <= 1'b0; ps_ptr_out <= 40'h0; pktcount <= pktcount;
	   if ( ms_ptr_rdy ) begin
	      ps_ptr_rdy <= 1'b1;  ps_ptr_out <= {prescale_passed,ms_ptr_out[38:0]};
	      case( dettype )
		   4'h0:  pktcount[ 15:  0] <= ( pktcount[ 15:  0] >= prescales[ 15:  0] ) ? 8'h1 : pktcount[ 15:  0] + 8'h1;
		   4'h1:  pktcount[ 31: 16] <= ( pktcount[ 31: 16] >= prescales[ 31: 16] ) ? 8'h1 : pktcount[ 31: 16] + 8'h1;
		   4'h2:  pktcount[ 47: 32] <= ( pktcount[ 47: 32] >= prescales[ 47: 32] ) ? 8'h1 : pktcount[ 47: 32] + 8'h1;
		   4'h3:  pktcount[ 63: 48] <= ( pktcount[ 63: 48] >= prescales[ 63: 48] ) ? 8'h1 : pktcount[ 63: 48] + 8'h1;
		   4'h4:  pktcount[ 79: 64] <= ( pktcount[ 79: 64] >= prescales[ 79: 64] ) ? 8'h1 : pktcount[ 79: 64] + 8'h1;
		   4'h5:  pktcount[ 95: 80] <= ( pktcount[ 95: 80] >= prescales[ 95: 80] ) ? 8'h1 : pktcount[ 95: 80] + 8'h1;
		   4'h6:  pktcount[111: 96] <= ( pktcount[111: 96] >= prescales[111: 96] ) ? 8'h1 : pktcount[111: 96] + 8'h1;
		   4'h7:  pktcount[127:112] <= ( pktcount[127:112] >= prescales[127:112] ) ? 8'h1 : pktcount[127:112] + 8'h1;
		   4'h8:  pktcount[143:128] <= ( pktcount[143:128] >= prescales[143:128] ) ? 8'h1 : pktcount[143:128] + 8'h1;
		   4'h9:  pktcount[159:144] <= ( pktcount[159:144] >= prescales[159:144] ) ? 8'h1 : pktcount[159:144] + 8'h1;
		   4'hA:  pktcount[175:160] <= ( pktcount[175:160] >= prescales[175:160] ) ? 8'h1 : pktcount[175:160] + 8'h1;
		   4'hB:  pktcount[191:176] <= ( pktcount[191:176] >= prescales[191:176] ) ? 8'h1 : pktcount[191:176] + 8'h1;
		   4'hC:  pktcount[207:192] <= ( pktcount[207:192] >= prescales[207:192] ) ? 8'h1 : pktcount[207:192] + 8'h1;
		   4'hD:  pktcount[223:208] <= ( pktcount[223:208] >= prescales[223:208] ) ? 8'h1 : pktcount[223:208] + 8'h1;
		   4'hE:  pktcount[239:224] <= ( pktcount[239:224] >= prescales[239:224] ) ? 8'h1 : pktcount[239:224] + 8'h1;
		   4'hF:  pktcount[255:240] <= ( pktcount[255:240] >= prescales[255:240] ) ? 8'h1 : pktcount[255:240] + 8'h1;
	      endcase
		end
   end
end

endmodule
