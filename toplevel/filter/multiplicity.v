// This module calculates the multiplicity for each detector type within a time period equal to the "coinc_width" parameter
//    multiplicity_out: 8bit counters containing # of occurences of each detector type within the coinc_width period
//	QueueFIFO: Contains time sequenced packet pointers.
//	    While the bottom element has its time stamp within the range of the coinc window,
//     it can be moved into the coinc window FIFO,and appropriate multiplicity counter incremented
//	CoincWinFIFO: contains stack of packet pointers with time stamps satisfying input time stamp - output time stamp <= coinc_width
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// timing failures between queue_q(time_in) -> timediff -> outwin -> add/dec-mult (both fifos are already registered)
module multiplicity( output reg error,     input  wire [15:0] coinc_passed,
   input  wire clk, input wire reset,      input  wire [13:0] coinc_width,   output wire [7:0] coinc_words,
   input  wire  [39:0] pkt_ptr_in,         input  wire pkt_ptr_rdy,          input wire ll_empty, input wire ms_empty,
   output wire [255:0] multiplicity_out,   output wire [39:0] mult_ptr_out,  output wire mult_ptr_rdy,
	output wire  [11:0] clover_mult_out
);
assign coinc_words = coincwin_words + (coincwin_valid && (coincwin_words != 8'hFF)) ? 8'h1 : 8'h0;
//always @ (posedge clk ) if( reset ) error <= 1'h0; else error <=
//     (!timeout && queue_empty && coinc_empty && pipe_empty && multiplicity_out != 128'h0) || (queue_words[7]);
always @ (posedge clk ) if( reset ) error <= 1'h0; else error <= (idle && multiplicity_out != 128'h0); // grifc version

// from signaltap - correct pattern is before we read event to output of coincWin
// - should correct this properly, but for now do the following ...
//reg  [7:0] coinc_save;  always @ (posedge clk) coinc_save <= (coinc_read) ? coinc_passed     : coinc_save;
//reg [39:0]   ptr_save;  always @ (posedge clk)   ptr_save <= (coinc_read) ? coincwin_q[39:0] :   ptr_save;
reg [15:0] coinc_save;  always @ (posedge clk) coinc_save <= coinc_passed    ;
reg [39:0]   ptr_save;  always @ (posedge clk)   ptr_save <= coincwin_q[39:0];
assign mult_ptr_out = {ptr_save[39:16],coinc_save}; // just make mu_strobe combinatorial to avoid this ...
// ** pkt_ptr[17:0] is timestamp which has been used already and is now spare

wire [71:0] queue_q; wire [7:0] queue_words; wire queue_full;  wire queue_empty;  wire queue_rdreq; 
FIFO36X256	QueueFIFO ( ////// **72**bit input QUEUE FIFO (*registered*)  /////
	.clock(clk),  .aclr(reset),  .usedw (queue_words),
	.data( {8'h0,pkt_ptr_in} ),  .wrreq(pkt_ptr_rdy),  .full(queue_full),
	.q(queue_q),                 .rdreq(queue_rdreq),  .empty(queue_empty)
);
wire   [12:0]    time_in =  queue_q[16: 4]; // 10ns slots (use [16:3] for 5 nsec time slots)
wire    [3:0] dettype_in =  queue_q[23:20];
wire    [3:0]   flags_in = {queue_q[37:36],queue_q[25:24]};
wire         last_stamp  =  queue_q[38];
wire prescale_passed_in  =  queue_q[39];

//////  COINC WINDOW FIFO - Holds Packet Pointers that have their time stamp within the defined coincidence window.
wire [12:0]      time_out =  coincwin_q [16: 4];  // 10ns slots (use [16:3] for 5 nsec time slots)
wire  [3:0]   dettype_out =  coincwin_q [23:20];
wire  [3:0]   flags_out   = {coincwin_q [37:36],coincwin_q [25:24]};
wire prescale_passed_out  =  coincwin_q[39];

wire [71:0] coincwin_q;  wire [7:0] coincwin_words;  wire coinc_read;
wire coinc_full;  wire coinc_empty;  wire coinc_add;  wire coincwin_valid;
FIFO36X256	coinc_window ( // **72**bit (*registered*) - same size as input queue
	.clock(clk),  .aclr(reset),  .usedw( coincwin_words ),
	.data(queue_q),    .wrreq(coinc_add),     .full(coinc_full),
	.q(coincwin_q),    .rdreq(coinc_read),  .empty(coinc_empty)
);

reg [2:0] ll_empty_dly; reg [1:0] ms_empty_dly;
always @ (posedge clk ) begin ll_empty_dly <= {ll_empty_dly[1:0],ll_empty};  ms_empty_dly <= {ms_empty_dly[0],ms_empty};  end

reg timeout_dly; always @ (posedge clk ) timeout_dly <= timeout;
wire idle = pipe_empty & timeout_dly & timeout & (!mult_ptr_rdy) & coinc_empty;

wire timeout;  // pipe_empty: no data in sight: nothing in queue or ll_mem (or inbetween in ms block) 
reg pipe_empty; always @ (posedge clk ) pipe_empty <= queue_empty & &(ll_empty_dly) & ll_empty & &(ms_empty_dly) & ms_empty; 
mu_control mu_control_inst (
	.clk(clk),  .reset(reset),     .coinc_width(coinc_width),
	.coinc_words(coincwin_words),  .coinc_empty(coinc_empty), .queue_words(queue_words),  .queue_empty(queue_empty),
	.tstamp_in(time_in),           .tstamp_out(time_out),	    .last_stamp(last_stamp),
	.pipe_empty(pipe_empty),       .coinc_read(coinc_read),   .queue_read(queue_rdreq),
	.coinc_add(coinc_add),         .mu_strobe(mult_ptr_rdy),  .timeout(timeout),          .RegValid(coincwin_valid)
);

mult_counters alltypes (  .clk(clk),      .reset(idle & error),
   .event_in(coinc_add),                  .event_out(mult_ptr_rdy),
	.prescale_ok_in(prescale_passed_in),   .prescale_ok_out(prescale_passed_out),
	.dettype_in(dettype_in),               .dettype_out(dettype_out),
	.multiplicity_out(multiplicity_out)
);

clov_counters clovers (  .clk(clk),       .reset(idle & error),
   .event_in(coinc_add),                  .event_out(mult_ptr_rdy),
	.dettype_in(dettype_in),               .dettype_out(dettype_out),
	.flags_in(flags_in),                   .flags_out(flags_out),
	.clovermult_out(clover_mult_out) // xtal,clov-supp,clov-bgo,clov-unsupp: each is 8bit mult
);

endmodule