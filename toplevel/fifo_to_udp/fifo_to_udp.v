//
// state machine sends data from FIFO to a UDP stream
//

`default_nettype none
module fifo_to_udp
  (
   input wire 	      clk,
   input wire 	      reset,

   // configuration
   input wire [31:0] udp_period,
   input wire [31:0] udp_threshold,
   
   // data into the fifo
   input wire [31:0] fifo_data,
   input wire fifo_wrreq,

   // fifo status
   input wire        tsc_fifo_full,
   output reg [31:0] fifo_status_out,
   output reg [31:0] fifo_data_out,
   input             wire fifo_rdack,

   // UDP avalon-st interface
   output reg [31:0] av_data_out,
   output reg        av_sop_out,
   output reg        av_eop_out,
   output reg        av_valid_out,
   input wire        av_ready
   );

   wire        fifo_empty;
   wire        fifo_full;
   reg         fifo_full_latch;
   wire [23:0] fifo_usedw;

   assign fifo_status_out = { fifo_full, fifo_empty, tsc_fifo_full, fifo_full_latch, 4'b0, fifo_usedw };

   assign fifo_usedw[23:17] = 0;
   assign fifo_usedw[16] = fifo_full;

   wire        udp_fifo_rdack;

   fifo	fifo_inst
     (
      .clock(clk),
      .sclr(reset),
      .data(fifo_data),
      .wrreq(fifo_wrreq),
      //.rdreq(fifo_rdreq),
      .rdreq(fifo_rdack | udp_fifo_rdack),
      .empty(fifo_empty),
      .full(fifo_full),
      .q(fifo_data_out),
      .usedw(fifo_usedw[15:0])
      );

   always_ff @(posedge clk) begin
      if (reset) begin
         fifo_full_latch <= 0;
      end else begin
         fifo_full_latch <= fifo_full_latch | fifo_full;
      end
   end

   ///////////////////////////////////////////////////////////

   reg udp_start;
   reg [31:0] udp_timer;

   always_ff @(posedge clk) begin
      udp_start <= 0;
      if (reset) begin
         udp_timer <= 0;
      end else if ((udp_threshold != 0) && (fifo_usedw >= udp_threshold[23:0])) begin
         udp_timer <= 0;
         udp_start <= 1;
      end else if (udp_period == 0) begin
         udp_timer <= 0;
      end else begin
         if ((udp_timer < udp_period)) begin // & fifo_usedw < NNN
            udp_timer <= udp_timer + 1;
         end else begin
            udp_timer <= 0;
            if (!fifo_empty) begin
               udp_start <= 1;
            end
         end
      end
   end

   ///////////////////////////////////////////////////////////
   //                   UDP packet transmitter
   ///////////////////////////////////////////////////////////

   parameter idle_state   = 8'h00;
   parameter write0_state = 8'h01;
   parameter write0a_state = 8'h11;
   parameter write1_state = 8'h02;
   parameter write2_state = 8'h03;
   parameter writeE_state = 8'hEE;

   reg [7:0]   tx_state;

   reg [31:0]  udp_packet_counter;
   reg [23:0]  udp_fifo_usedw;

   reg [23:0]  fifo_counter;

   // NOTE:
   // this state machine does has incorrect "av_ready"
   // and will lose data if av_ready is toggled in the middle
   // of a packet. the "write0a" kludge is needed to avoid
   // losing the first data word in the fifo when av_ready
   // is toggled right after av_sop_out (this is a quirk
   // of the avalon bus multiplexer). our data consumer
   // is Chris Pearson's UDP transmitter and it does not toggle
   // av_ready in the middle of a packet so we are good.
   // (it does remove av_ready on av_eop_out and this causes the
   // problem fixed by the write0a kludge). K.O. 25 July 2022.

   // NOTE:
   // correct code should not run "one clock ahead", instead
   // of clocked "av_data_out <= xxx" and "av_valid_out <= 1" it should
   // use muxed "av_data_out = xxx" and "av_valid_out = 1".
   // Instead of rewriting this code and retesting it, I use the write0a kludge.
   // K.O. 25 July 2022.

   always_ff @ (posedge clk) begin
      udp_fifo_rdack <= 0;
      if (av_ready) begin
         case (tx_state)
	   idle_state: begin
	      if (udp_start) begin
	         tx_state     <= write0_state;
	         av_valid_out <= 0;
                 av_sop_out   <= 0;
                 av_eop_out   <= 0;
                 fifo_counter <= 24'd1;
                 udp_packet_counter <= udp_packet_counter + 1;
                 udp_fifo_usedw <= fifo_usedw;
	      end else begin
	         tx_state     <= tx_state;
	         av_valid_out <= 0;
                 av_sop_out   <= 0;
                 av_eop_out   <= 0;
                 fifo_counter <= 24'd0;
	      end
	   end
	   write0_state: begin
	      tx_state     <= write0a_state;
	      av_data_out  <= {4'hD, udp_packet_counter[27:0]};
	      av_valid_out <= 1;
              av_sop_out   <= 1;
              av_eop_out   <= 0;
	   end
	   write0a_state: begin
	      tx_state     <= write1_state;
	      av_data_out  <= 32'hDEADBEEF;
	      av_valid_out <= 1;
              av_sop_out   <= 0;
              av_eop_out   <= 0;
	   end
	   write1_state: begin
	      tx_state     <= write2_state;
	      av_data_out  <= fifo_status_out;
	      av_valid_out <= 1;
              av_sop_out   <= 0;
              av_eop_out   <= 0;
              udp_fifo_rdack <= 1;
	   end
	   write2_state: begin
              if ((fifo_counter == udp_fifo_usedw) || (fifo_counter > 24'd350)) begin
	         tx_state     <= writeE_state;
                 udp_fifo_rdack <= 0;
              end else begin
	         tx_state     <= write2_state;
                 udp_fifo_rdack <= 1;
              end
	      av_data_out  <= fifo_data_out;
	      av_valid_out <= 1;
              av_sop_out   <= 0;
              av_eop_out   <= 0;
              fifo_counter <= fifo_counter + 24'd1;
	   end
	   writeE_state: begin	
	      tx_state     <= idle_state;
	      av_data_out  <= {4'hE, udp_packet_counter[27:0]};
	      av_valid_out <= 1;
              av_sop_out   <= 0;
              av_eop_out   <= 1;
	   end
         endcase // case (tx_state)
      end
   end // always @ (posedge clk)

endmodule
