//grey to binary ... upper bit unchanged, each lower ^= prev ...
//   x[11:0] ->  {x[11], x[10]^x[11], x[9]^{x[10]^x[11]}, .... }
// 
// to speed up - can break ripple chain in middle, and
// calculate both possibilities for lower half of chain in advance
// (and use actual value to select correct choice when available)
//
module grey2bin24 (input wire [23:0] grey_in, output wire [23:0] bin_out);

assign bin_out[23] = grey_in[23];
assign bin_out[22] = grey_in[22] ^ bin_out[23];
assign bin_out[21] = grey_in[21] ^ bin_out[22];
assign bin_out[20] = grey_in[20] ^ bin_out[21];
assign bin_out[19] = grey_in[19] ^ bin_out[20];
assign bin_out[18] = grey_in[18] ^ bin_out[19];
assign bin_out[17] = grey_in[17] ^ bin_out[18];
assign bin_out[16] = grey_in[16] ^ bin_out[17];
assign bin_out[15] = grey_in[15] ^ bin_out[16];
assign bin_out[14] = grey_in[14] ^ bin_out[15];
assign bin_out[13] = grey_in[13] ^ bin_out[14];
assign bin_out[12] = grey_in[12] ^ bin_out[13];
assign bin_out[11] = grey_in[11] ^ bin_out[12];
assign bin_out[10] = grey_in[10] ^ bin_out[11];
assign bin_out[ 9] = grey_in[ 9] ^ bin_out[10];
assign bin_out[ 8] = grey_in[ 8] ^ bin_out[ 9];
assign bin_out[ 7] = grey_in[ 7] ^ bin_out[ 8];
assign bin_out[ 6] = grey_in[ 6] ^ bin_out[ 7];
assign bin_out[ 5] = grey_in[ 5] ^ bin_out[ 6];
assign bin_out[ 4] = grey_in[ 4] ^ bin_out[ 5];
assign bin_out[ 3] = grey_in[ 3] ^ bin_out[ 4];
assign bin_out[ 2] = grey_in[ 2] ^ bin_out[ 3];
assign bin_out[ 1] = grey_in[ 1] ^ bin_out[ 2];
assign bin_out[ 0] = grey_in[ 0] ^ bin_out[ 1];

endmodule
