// reset is generated 40-80ms after pll locks (once on powerup)
// reset_in resets pll and then generates above reset
// poweron_time is time since pll locked after last reset or power on
module pll_reset
  (
   input wire 	     autoreset,
   input wire 	     inclk1,
   input wire 	     inclk2,
   input wire 	     clksel,
   input wire 	     reset_req,
   output reg 	     RSTn,
   output wire 	     slow_clk,
   output wire 	     sys_clk,
   output wire 	     filter_clk,
   output wire 	     filter_ddrclk,
   output wire 	     sigtap_clk,
   output wire 	     io_clk,
   output wire 	     io_ddrclk,
   //KO output wire ppg_clk,
   output wire 	     clk_625,
   output wire 	     sys_locked,
   //output wire 	     clk1_ok,
   //output wire 	     clk2_ok,
   output reg [29:0] poweron_time
);

// reset_req is registered on ethernet_clk, and only lasts 1 clk => care required transferring to another clk
// - have to transfer, because as soon as the pll is reset, eth_clk freezes, which would hold reset on
//   so stretch to 4 ioclks -> 32ns duration, and re-register on input clk
reg [3:0] reset_req_delay; always @ (posedge io_clk) reset_req_delay <= {reset_req_delay[2:0],reset_req};
reg reset; always @ (posedge inclk1) reset <= |(reset_req_delay);

reg reset_done;
always @ (posedge inclk1 or posedge reset) begin
   if( reset ) begin
	   reset_done <= 1'b0; poweron_time <= 30'h0; // 1Gig -> 8 seconds @ 125Mhz, 10 seconds @ 100
	end else begin
	   RSTn <= 1'b1; reset_done <= reset_done; poweron_time <= poweron_time + 30'h1;
      if( sys_locked  == 0                          ) poweron_time <= 30'h0; // keep at zero till locked
	   if( poweron_time[21] && reset_done == 0       ) RSTn         <=  1'b0; // reset     at 40ms
	   if( poweron_time[22]                          ) reset_done   <=  1'b1; // end reset at 80ms
	   if( poweron_time == 30'h3fffffff && autoreset ) reset_done   <=  1'b0; // re-arm for 10s later
	end
end

assign sigtap_clk = sys_clk;
wire clkact, clkbad1, clkbad2;
pll main_pll (
   .inclk0(inclk1),   .inclk1(inclk2),   .clkswitch(clksel), .activeclock(clkact),
	.clkbad0(clkbad1), .clkbad1(clkbad2), .locked(sys_locked),
	.c0(sys_clk),      .c1(filter_clk),   .c2(filter_ddrclk), /* x1,   x2   x4    50 100 200 */
   .c3(slow_clk),     .c4(io_clk),       .c5(io_ddrclk),     /* x0.5, x2.5 x5    25 125 250 */
	//.c6(ppg_clk),                                             /* x1/50             1         */
	.c6(clk_625) // 62.5 MHz (io_clk/2)
); 

endmodule

