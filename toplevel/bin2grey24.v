//binary to grey: (x>>1)^x   e.g for 12bit code ...  
//   {0,x[11:1]} ^ x[11:0]  =  {x[11], x[11]^x[10], ..., x[1]^x[0]}

module bin2grey24 (input wire [23:0] bin_in, output wire [23:0] grey_out);

assign grey_out[23] = bin_in[23];
assign grey_out[22] = bin_in[22] ^ bin_in[23];
assign grey_out[21] = bin_in[21] ^ bin_in[22];
assign grey_out[20] = bin_in[20] ^ bin_in[21];
assign grey_out[19] = bin_in[19] ^ bin_in[20];
assign grey_out[18] = bin_in[18] ^ bin_in[19];
assign grey_out[17] = bin_in[17] ^ bin_in[18];
assign grey_out[16] = bin_in[16] ^ bin_in[17];
assign grey_out[15] = bin_in[15] ^ bin_in[16];
assign grey_out[14] = bin_in[14] ^ bin_in[15];
assign grey_out[13] = bin_in[13] ^ bin_in[14];
assign grey_out[12] = bin_in[12] ^ bin_in[13];
assign grey_out[11] = bin_in[11] ^ bin_in[12];
assign grey_out[10] = bin_in[10] ^ bin_in[11];
assign grey_out[ 9] = bin_in[ 9] ^ bin_in[10];
assign grey_out[ 8] = bin_in[ 8] ^ bin_in[ 9];
assign grey_out[ 7] = bin_in[ 7] ^ bin_in[ 8];
assign grey_out[ 6] = bin_in[ 6] ^ bin_in[ 7];
assign grey_out[ 5] = bin_in[ 5] ^ bin_in[ 6];
assign grey_out[ 4] = bin_in[ 4] ^ bin_in[ 5];
assign grey_out[ 3] = bin_in[ 3] ^ bin_in[ 4];
assign grey_out[ 2] = bin_in[ 2] ^ bin_in[ 3];
assign grey_out[ 1] = bin_in[ 1] ^ bin_in[ 2];
assign grey_out[ 0] = bin_in[ 0] ^ bin_in[ 1];

endmodule
