// 4-wire i2c/smb [scl,sdi,sdo,sdoen] [was 2 wire but was passed through spartan]
//     normally data is changed when clk low,
//       if clk high, data hi-low => start, lo-hi => stop 
//     idle:       wait for trigger
//     start:      load data, drive init signals *wait for hold time*
//     setup/hold: set signals for #clks
//     finish:     drive high+turn off
// ----------------------- microchip 24aa02e48 8kbit eeprom ------------------------------
// device has 256 x 8bits  i.e. 8bit addr+data
// locations 0x80-0xFF (unwriteable) contain the MAC
//    i.e. FA-FF - and maybe duplicated in lower addresses
// smbus format - start, N*{8bits+ack}, stop
//    1st 8bits are ctrl [1010xxxR]   (for 24aa02e48 xxx are not connected)
// clk:Max400Khz, min 2.600us period
//    read will read prev addr+1, to read other location - send 8bit word address as write
// ---------------------------------------------------------------------------------------
// here - just want to read 6byte mac from starting address FA
//    1010xxxR => command is A1,FA
module smb_4wire (
   input  wire             clk,  input  wire        reset,
	input  wire         trigger,  output wire     busy_out,
   output reg              sck,  output  reg          sdo,
   input  wire             sdi,  output  reg       sdo_en, 
	output wire [47:0] data_out,  output wire        debug
);
assign debug = sdi;

wire  [7:0]     clk_time =  8'hA7; // 8ns clk, 1300ns setup/hold => min cnt=163[A3]
wire  [7:0]     num_bits =  8'h08;
wire [15:0] command_data  = 16'hA1FA;

parameter idle=3'h0, start=3'h1, setup=3'h2, hold=3'h3, finish=3'h4;

reg  [7:0] bit_cnt;   reg  [7:0] clk_cnt;
reg [15:0] command;  reg [47:0] response;
reg  [2:0]   state;  reg     read_active;

always @ (posedge clk or posedge reset) begin
   if( reset ) begin
		sck <= 1'bz;  sdo <= 1'b1;  sdo_en <= 1'b0;  busy_out <= 1'b0;
		command  <=  24'h0;	    response <=  8'h0;  read_bit <= 1'b0;
		state    <=   idle;      clk_cnt  <=  8'h0;  bit_cnt  <= 8'h0;	 
	end else begin
		sck <= sck;  sdo <= sdo;  sdo_en   <=    sdo_en;  busy_out <= busy_out;
		command  <=     command;  response <=  response;  read_bit <= read_bit;
		state    <=       state;  bit_cnt  <=      8'h0;  clk_cnt  <= clk_cnt - 8'h1;
	   case( state )
	      idle: begin // wait for trigger, then set clk high prepare to start
			      sck <= 1'bz; sdo <= 1'b1; sdo_en <= 1'b0; busy_out <= 1'b0;
			      if( trigger ) begin
			         sck <= 1'b1;  sdo <= 1'b1;  sdo_en <= 1'b1;  busy_out <= 1'b1;
					   command <=   command_data;  response <= 48'h0;
						clk_cnt <= clk_time;  state <= start_setup;
				   end
			   end
	      start_setup: // wait for setup time till changing sda
		      if ( clk_cnt == 8'h0 ) begin
			      sdo <= 1'b0;  clk_cnt <= clk_time;  state <= start_hold;
			   end
	      start_hold: // wait for hold time after changing sda before sck<=0
		      if ( clk_cnt == 8'h0 ) begin
			      sck <= 1'b0; clk_cnt <= clk_time;  state <= data_setup;
					bit_cnt <= num_bits;
			   end
	      data_setup: // wait for setup time (after prev clk transition) before changing data
		      if ( clk_cnt == 8'h0 ) begin
					sdo_en  <= !( read_bit && bit_cnt >= 8'h8 ); // leave off if reading ( read && data bits)
			      sdo <= command[15];  clk_cnt <= clk_time;  state <= data_hold;
			   end
	      data_hold: // wait for hold time (after data change) before changing clk
				if ( clk_cnt == 8'h0 ) begin
					if( bit_cnt == 8'h0 ) state <= finish;
					else begin
				      sck <= 1'b0; clk_cnt <= clk_time;  state <= clk_wait;
					   command <= {command[22:0],1'b0}; bit_cnt <= bit_cnt - 8'h1;
				   end
				end
			 clk_wait: // response <= {response[6:0],1'b0}; // after data hold time, can toggle clk high
	       finish: begin state <= idle; busy_out <= 1'b0; end
	      default: begin state <= idle; busy_out <= 1'b0; end
	   endcase
	end
end

endmodule
