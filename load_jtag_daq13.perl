#!/usr/bin/perl -w

$| = 1;

my $f = shift @ARGV;

die "Cannot read $f: $!\n" unless -r $f;

#my $quartus = "/home/quartus/quartus7.2";
#my $quartus = "/home/olchansk/altera7.2/quartus7.2";
#my $quartus = "/triumfcs/trshare/olchansk/altera/altera9.1/quartus";
#my $quartus = "/daq/daqshare/olchansk/altera/15.1/quartus";
#my $quartus = "/opt/intelFPGA/17.0/quartus";
my $quartus = "/opt/intelFPGA/20.1/quartus";

#my $q = `$quartus/bin/quartus_pgm -l`;
#print $q;

#$q =~ /1\) (.*)\n/m;
#my $b = $1;
#print "$b\n";

#my $b = "USB-Blaster [3-2.3]";
my $b = "2";

my $cmd = "$quartus/bin/quartus_pgm -c \"$b\" -m JTAG -o \"p;$f\@1\"";
print "Running $cmd\n";
system $cmd;

exit 0;
#end
