# Clock constraints

#create_clock -name "clk50" -period 20.000ns [get_ports {CLKIN_50}] -waveform {0.000 10.000}
#
#create_clock -name "sys_clk_50" -period 20ns {pll_rst1|main_pll|altpll_component|auto_generated|pll1|clk[0]}
#create_clock -name "io_clk1_125" -period 8ns {pll_rst1|main_pll|altpll_component|auto_generated|pll1|clk[4]}
#create_clock -name "io_clk2_125" -period 8ns {pll_rst2|main_pll|altpll_component|auto_generated|pll1|clk[4]}
#create_clock -name "clk_625" -period 16ns {pll_rst1|main_pll|altpll_component|auto_generated|pll1|clk[6]}
#
#create_clock -name "rx_clk_block1_0" -period 8ns {ag_sas|block1|FMC_Transceiver_sas_alt4gxb_component|receive_pcs0|clkout}
#create_clock -name "rx_clk_block1_1" -period 8ns {ag_sas|block1|FMC_Transceiver_sas_alt4gxb_component|receive_pcs1|clkout}
#create_clock -name "rx_clk_block1_2" -period 8ns {ag_sas|block1|FMC_Transceiver_sas_alt4gxb_component|receive_pcs2|clkout}
#create_clock -name "rx_clk_block1_3" -period 8ns {ag_sas|block1|FMC_Transceiver_sas_alt4gxb_component|receive_pcs3|clkout}
#
#create_clock -name "rx_clk_block2_0" -period 8ns {ag_sas|block2|FMC_Transceiver_sas_alt4gxb_component|receive_pcs0|clkout}
#create_clock -name "rx_clk_block2_1" -period 8ns {ag_sas|block2|FMC_Transceiver_sas_alt4gxb_component|receive_pcs1|clkout}
#create_clock -name "rx_clk_block2_2" -period 8ns {ag_sas|block2|FMC_Transceiver_sas_alt4gxb_component|receive_pcs2|clkout}
#create_clock -name "rx_clk_block2_3" -period 8ns {ag_sas|block2|FMC_Transceiver_sas_alt4gxb_component|receive_pcs3|clkout}
#
#create_clock -name "tx_clk_block3_0" -period 8ns {fmx_gx|block3|FMC_Transceiver_alt4gxb_component|transmit_pcs0|clkout}
#
#create_clock -name "rx_clk_block5_0" -period 8ns {ag_sas|block5|FMC_Transceiver_sas_alt4gxb_component|receive_pcs0|clkout}
#create_clock -name "rx_clk_block5_1" -period 8ns {ag_sas|block5|FMC_Transceiver_sas_alt4gxb_component|receive_pcs1|clkout}
#create_clock -name "rx_clk_block5_2" -period 8ns {ag_sas|block5|FMC_Transceiver_sas_alt4gxb_component|receive_pcs2|clkout}
#create_clock -name "rx_clk_block5_3" -period 8ns {ag_sas|block5|FMC_Transceiver_sas_alt4gxb_component|receive_pcs3|clkout}
#
#create_clock -name "rx_clk_block6_0" -period 8ns {ag_sas|block6|FMC_Transceiver_sas_alt4gxb_component|receive_pcs0|clkout}
#create_clock -name "rx_clk_block6_1" -period 8ns {ag_sas|block6|FMC_Transceiver_sas_alt4gxb_component|receive_pcs1|clkout}
#create_clock -name "rx_clk_block6_2" -period 8ns {ag_sas|block6|FMC_Transceiver_sas_alt4gxb_component|receive_pcs2|clkout}
#create_clock -name "rx_clk_block6_3" -period 8ns {ag_sas|block6|FMC_Transceiver_sas_alt4gxb_component|receive_pcs3|clkout}

# Automatically constrain PLL and other generated clocks
derive_pll_clocks -create_base_clocks

# Automatically calculate clock uncertainty to jitter and other effects.
derive_clock_uncertainty

# false-path all signal tap signals

set_false_path -to {sld_signaltap:*}

# set false_path for unrelated clocks

#set_false_path -to {rx_clk_block*}
#set_false_path -from {rx_clk_block*}

# set false path to IO pins

set_false_path -to {USER_LED*}
set_false_path -from {USER_DIP*}

set_false_path -to {VME_*}
set_false_path -from {VME_*}

set_false_path -from {FMC1_PRESENT}
set_false_path -from {FMC1_CFG_INITn}
set_false_path -from {FMC1_CFG_DONE}
set_false_path -from {FMC1_M2C[7]}
set_false_path -from {FMC1_M2C[8]}

set_false_path -to {FMC1_CFG_CCLK}
set_false_path -to {FMC1_CFG_DIN}
set_false_path -to {FMC1_CFG_PROGn}

set_false_path -from {altera_reserved_tdi}
set_false_path -from {altera_reserved_tms}
set_false_path -to {altera_reserved_tdo}

# eSATA clock input
#set_false_path -from {FMC1_MCLK[0]}
#set_false_path -from {FMC1_MCLK[1]}

# eSATA sync output
set_false_path -to {FMC1_C2M[4]}
set_false_path -to {FMC1_C2M[4](n)}

# FLASH interface
set_false_path -from {FLASH_A[*]}
set_false_path -from {FLASH_D[*]}
set_false_path -from {FLASH_BUSYn}
set_false_path -from {FLASH_BYTEn}
set_false_path -from {FLASH_CE0n}
set_false_path -from {FLASH_CE1n}
set_false_path -from {FLASH_OEn}
set_false_path -from {FLASH_RESETn}
set_false_path -from {FLASH_WEn}
set_false_path -from {FLASH_WPn}

set_false_path -to {FLASH_A[*]}
set_false_path -to {FLASH_D[*]}
set_false_path -to {FLASH_BUSYn}
set_false_path -to {FLASH_BYTEn}
set_false_path -to {FLASH_CE0n}
set_false_path -to {FLASH_CE1n}
set_false_path -to {FLASH_OEn}
set_false_path -to {FLASH_RESETn}
set_false_path -to {FLASH_WEn}
set_false_path -to {FLASH_WPn}

set_false_path -to {CFG_REQUEST}

#
# clock setup from yair
#

proc puts_and_post_message { x } {
    # set module [lindex $quartus(args) 0]                                                                                                                
    # if [string match "quartus_map" $module] {                                                                                                           
    # } else {                                                                                                                                            
    if { ([info commands post_message] eq "post_message") || ([info procs post_message] eq "post_message") } {
        puts $x
        post_message $x
    }
    # }                                                                                                                                                   
}

proc print_collection { col } {
    if {[catch {
        foreach_in_collection c $col  {
            puts_and_post_message "   [get_clock_info -name $c]";
        }
    } ] } {
        if {[catch {
            puts_and_post_message "[query_collection $col -all -report_format]"
        } ] } {
            puts_and_post_message "   $col"
        }
    }
}

proc timid_asynchronous { clock_name clockB { max_delay  50.0 } { min_delay  -50 } } {
    set_max_delay -from  $clock_name -to $clockB $max_delay
    set_max_delay -from $clockB -to  $clock_name $max_delay
    set_min_delay -from  $clock_name -to $clockB $min_delay
    set_min_delay -from $clockB -to  $clock_name $min_delay
    #puts_and_post_message "=========================================================================="
    #puts_and_post_message "timid_asynchronous $clock_name from/to $clockB, max_delay = $max_delay min_delay = $min_delay which means:"
    #print_collection $clock_name
    #puts_and_post_message "from/to: "
    #print_collection $clockB
    #puts_and_post_message "=========================================================================="
}

proc timid_asynchronous_group { clock_list { max_delay  50.0 } { min_delay  -50 } } {
    puts [concat "timid_asynchronous_group: " [llength $clock_list] ":" [join $clock_list]]
    for {set i 0 } { $i < [llength $clock_list] } { incr i } {
        for {set j [expr $i + 1] } { $j < [llength $clock_list] } { incr j }  {
            puts [concat "timid_asynchronous clocks " [lindex $clock_list $i] " and " [lindex $clock_list $j]" ]
            timid_asynchronous [lindex $clock_list $i] [lindex $clock_list $j] $max_delay $min_delay;
        }
    }
}

#
# auto-derived clocks:
#
# internal oscillator 50 MHz: CLKIN_50
#    50 MHz PLL: pll_rst1|main_pll|altpll_component|auto_generated|pll1|clk[0]
#   125 MHz PLL: pll_rst1|main_pll|altpll_component|auto_generated|pll1|clk[4]
#  62.5 MHz PLL: pll_rst1|main_pll|altpll_component|auto_generated|pll1|clk[6]
#   125 MHz PLL: pll_rst2|main_pll|altpll_component|auto_generated|pll1|clk[4]
#
# from CLKIN_50 derived 62.5 MHz clock:
#  62.5 MHz: ag|ag_pll_625_inst|altpll_component|auto_generated|pll1|clk[0]
#    10 MHz: ag|ag_pll_625_inst|altpll_component|auto_generated|pll1|clk[1]
#
# external clock 62.5 MHz: FMC1_MCLK[0]
#  62.5 MHz: ag|ag_pll_625_inst|altpll_component|auto_generated|pll1|clk[0]~1
#    10 MHz: ag|ag_pll_625_inst|altpll_component|auto_generated|pll1|clk[1]~1
#

timid_asynchronous_group {
CLKIN_50
FMC1_MCLK[0]
pll_rst1|main_pll|altpll_component|auto_generated|pll1|clk[4]
ag|ag_pll_625_inst|altpll_component|auto_generated|pll1|clk[0]
ag|ag_pll_625_inst|altpll_component|auto_generated|pll1|clk[1]
ag|ag_pll_625_inst|altpll_component|auto_generated|pll1|clk[0]~1
ag|ag_pll_625_inst|altpll_component|auto_generated|pll1|clk[1]~1
ag_sas|block1|FMC_Transceiver_sas_alt4gxb_component|receive_pcs0|clkout
ag_sas|block1|FMC_Transceiver_sas_alt4gxb_component|receive_pcs1|clkout
ag_sas|block1|FMC_Transceiver_sas_alt4gxb_component|receive_pcs2|clkout
ag_sas|block1|FMC_Transceiver_sas_alt4gxb_component|receive_pcs3|clkout
ag_sas|block2|FMC_Transceiver_sas_alt4gxb_component|receive_pcs0|clkout
ag_sas|block2|FMC_Transceiver_sas_alt4gxb_component|receive_pcs1|clkout
ag_sas|block2|FMC_Transceiver_sas_alt4gxb_component|receive_pcs2|clkout
ag_sas|block2|FMC_Transceiver_sas_alt4gxb_component|receive_pcs3|clkout
ag_sas|block5|FMC_Transceiver_sas_alt4gxb_component|receive_pcs0|clkout
ag_sas|block5|FMC_Transceiver_sas_alt4gxb_component|receive_pcs1|clkout
ag_sas|block5|FMC_Transceiver_sas_alt4gxb_component|receive_pcs2|clkout
ag_sas|block5|FMC_Transceiver_sas_alt4gxb_component|receive_pcs3|clkout
ag_sas|block6|FMC_Transceiver_sas_alt4gxb_component|receive_pcs0|clkout
ag_sas|block6|FMC_Transceiver_sas_alt4gxb_component|receive_pcs1|clkout
ag_sas|block6|FMC_Transceiver_sas_alt4gxb_component|receive_pcs2|clkout
ag_sas|block6|FMC_Transceiver_sas_alt4gxb_component|receive_pcs3|clkout
}

#
# per altera user guide https://www.intel.com/content/dam/www/programmable/us/en/pdfs/literature/ug/archives/ug-qpp-timing-analyzer-20-1.pdf
# page 69, "2.2.5.4. Deriving PLL Clocks" and "Example SDC Constraints for Internal Clock Mux"
# quoting: "If the PLL is in clock switchover mode, multiple clocks generate for the output clock of
# the PLL; one for the primary input clock (for example, inclk[0]), and one for the
# secondary input clock (for example, inclk[1]). Create exclusive clock groups for the
# primary and secondary output clocks since they are not active simultaneously."
#
# Set the two clocks as exclusive clocks
# set_clock_groups -exclusive -group {clk_100} -group {clk_125}
#
#
#set_clock_groups -exclusive -group {altpll_component|auto_generated|pll1|clk[0]} -group {altpll_component|auto_generated|pll1|clk[0]~1}
#set_clock_groups -exclusive -group {altpll_component|auto_generated|pll1|clk[1]} -group {altpll_component|auto_generated|pll1|clk[1]~1}
#
#set_clock_groups -exclusive -group {FMC1_MCLK[0]} -group {CLKIN_50}
#set_clock_groups -exclusive -group {FMC1_MCLK[0]} -group {pll_rst1|main_pll|altpll_component|auto_generated|pll1|clk[6]}
#
# all of this does not work, I still get clock transfers between the fake clocks. K.O.
#

# explicit clock transfers

set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block1|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs0_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[0]}
set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block1|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs1_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[1]}
set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block1|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs2_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[2]}
set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block1|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs3_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[3]}

set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block2|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs0_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[4]}
set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block2|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs1_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[5]}
set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block2|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs2_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[6]}
set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block2|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs3_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[7]}

set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block5|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs0_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[8]}
set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block5|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs1_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[9]}
set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block5|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs2_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[10]}
set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block5|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs3_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[11]}

set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block6|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs0_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[12]}
set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block6|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs1_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[13]}
set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block6|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs2_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[14]}
set_false_path -from {sas_links:ag_sas|FMC_Transceiver_sas:block6|FMC_Transceiver_sas_alt4gxb:FMC_Transceiver_sas_alt4gxb_component|wire_receive_pcs3_errdetect[0]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|rx_sd_async1[15]}

set_false_path -from {sas_links:ag_sas|sas_decode_bits:b0|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[0][*]}
set_false_path -from {sas_links:ag_sas|sas_decode_bits:b1|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[1][*]}
set_false_path -from {sas_links:ag_sas|sas_decode_bits:b2|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[2][*]}
set_false_path -from {sas_links:ag_sas|sas_decode_bits:b3|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[3][*]}

set_false_path -from {sas_links:ag_sas|sas_decode_bits:b4|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[4][*]}
set_false_path -from {sas_links:ag_sas|sas_decode_bits:b5|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[5][*]}
set_false_path -from {sas_links:ag_sas|sas_decode_bits:b6|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[6][*]}
set_false_path -from {sas_links:ag_sas|sas_decode_bits:b7|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[7][*]}

set_false_path -from {sas_links:ag_sas|sas_decode_bits:b8|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[8][*]}
set_false_path -from {sas_links:ag_sas|sas_decode_bits:b9|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[9][*]}
set_false_path -from {sas_links:ag_sas|sas_decode_bits:b10|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[10][*]}
set_false_path -from {sas_links:ag_sas|sas_decode_bits:b11|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[11][*]}

set_false_path -from {sas_links:ag_sas|sas_decode_bits:b12|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[12][*]}
set_false_path -from {sas_links:ag_sas|sas_decode_bits:b13|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[13][*]}
set_false_path -from {sas_links:ag_sas|sas_decode_bits:b14|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[14][*]}
set_false_path -from {sas_links:ag_sas|sas_decode_bits:b15|out[*]} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_async1[15][*]}

set_false_path -from {alphag:ag|frequency_counter:fc|reset} -to {alphag:ag|frequency_counter:fc|start2x}
set_false_path -from {alphag:ag|frequency_counter:fc|start1} -to {alphag:ag|frequency_counter:fc|start2a}

set_false_path -from {alphag:ag|frequency_counter:fc|clk2_counter_write} -to {alphag:ag|frequency_counter:fc|wr1}
set_false_path -from {alphag:ag|frequency_counter:fc|clk2_is_running} -to {alphag:ag|frequency_counter:fc|ru1}
set_false_path -from {alphag:ag|frequency_counter:fc|clk2_counter_tmp[*]} -to {alphag:ag|frequency_counter:fc|clk2_counter_out[*]}

set_false_path -from {alphag:ag|frequency_counter:fc_esata|reset} -to {alphag:ag|frequency_counter:fc_esata|start2x}
set_false_path -from {alphag:ag|frequency_counter:fc_esata|start1} -to {alphag:ag|frequency_counter:fc_esata|start2a}

set_false_path -from {alphag:ag|frequency_counter:fc_esata|clk2_counter_write} -to {alphag:ag|frequency_counter:fc_esata|wr1}
set_false_path -from {alphag:ag|frequency_counter:fc_esata|clk2_is_running} -to {alphag:ag|frequency_counter:fc_esata|ru1}
set_false_path -from {alphag:ag|frequency_counter:fc_esata|clk2_counter_tmp[*]} -to {alphag:ag|frequency_counter:fc_esata|clk2_counter_out[*]}

set_false_path -from {alphag:ag|frequency_counter:fc_eth|reset} -to {alphag:ag|frequency_counter:fc_eth|start2x}
set_false_path -from {alphag:ag|frequency_counter:fc_eth|start1} -to {alphag:ag|frequency_counter:fc_eth|start2a}

set_false_path -from {alphag:ag|frequency_counter:fc_eth|clk2_counter_write} -to {alphag:ag|frequency_counter:fc_eth|wr1}
set_false_path -from {alphag:ag|frequency_counter:fc_eth|clk2_is_running} -to {alphag:ag|frequency_counter:fc_eth|ru1}
set_false_path -from {alphag:ag|frequency_counter:fc_eth|clk2_counter_tmp[*]} -to {alphag:ag|frequency_counter:fc_eth|clk2_counter_out[*]}

set_false_path -from {gx_links:fmx_gx|FMC_Transceiver:block3|FMC_Transceiver_alt4gxb:FMC_Transceiver_alt4gxb_component|wire_receive_pcs0_syncstatus[0]} -to {gx_links:fmx_gx|xcvr_ready1}

set_false_path -to {alphag:ag|ag_pulser:ag_pulser|reset1}

#set_false_path -to {cb_main:cb_main|inputs1[*]}
#set_false_path -to {cb_main:cb_main|inputs1_clk_ts[*]}

set_false_path -to {cb_main:cb_main|sync_in1}
set_false_path -to {cb_main:cb_main|sync_in_clk_ts1}
set_false_path -to {cb_main:cb_main|sync_arm_clk_ts1}
set_false_path -to {cb_main:cb_main|sync_disarm_clk_ts1}

set_false_path -from {cb_main:cb_main|ts_overflow_data[*]} -to {cb_main:cb_main|tso_data[*]}

set_false_path -to {cb_main:cb_main|sync_slow_to_fast_clk:tso_sync_inst|in1}

set_false_path -to {cb_main:cb_main|sync_status_out[*]*}

set_false_path -to {led_stretch:*|in_async1}

# configuration into the 62.5MHz block

set_false_path -from {param_io:par|conf_pulser_period*}
set_false_path -from {param_io:par|conf_pulser_width*}
set_false_path -from {param_io:par|conf_trig_delay*}
set_false_path -from {param_io:par|conf_trig_width*}
set_false_path -from {param_io:par|conf_drift_width*}
set_false_path -from {param_io:par|conf_busy_width*}
set_false_path -from {param_io:par|conf_scaledown*}
set_false_path -from {param_io:par|conf_trig_enable*}
set_false_path -from {param_io:par|conf_adc32_masks*}
set_false_path -from {param_io:par|conf_adc16_masks*}

# configuration into chronobox

set_false_path -from {param_io:par|cb_sync_mask_a_out*}
set_false_path -from {param_io:par|cb_sync_mask_b_out*}
set_false_path -from {param_io:par|cb_enable_le_a_out*}
set_false_path -from {param_io:par|cb_enable_le_b_out*}
set_false_path -from {param_io:par|cb_enable_te_a_out*}
set_false_path -from {param_io:par|cb_enable_te_b_out*}
set_false_path -from {param_io:par|cb_invert_a_out*}
set_false_path -from {param_io:par|cb_invert_b_out*}

# configuration bits are constants for the purpose of timing analysis
#set_false_path -from {param_io:par|conf_trig_enable*}
#set_false_path -from {param_io:par|conf_adc16_masks*}
#set_false_path -from {param_io:par|conf_adc32_masks*}

# latch of 62.5MHz data

set_false_path -to {alphag:ag|sync_ss:s1xx|in1}
set_false_path -to {alphag:ag|sync_ss:s1zz|in1}
set_false_path -to {alphag:ag|ag_clk_625:ag_clk_625|sync_ss:s1|in1}

# latched 62.5MHz data to param_io block

set_false_path -from {alphag:ag|counter_scaledown_out[*]*}
set_false_path -from {alphag:ag|ts_625_out[*]*}
set_false_path -from {alphag:ag|counter_drift_out[*]*}
set_false_path -from {alphag:ag|counter_trig_in_out[*]*}
set_false_path -from {alphag:ag|counter_trig_out_out[*]*}
set_false_path -from {alphag:ag|counter_pulser_out[*]*}

# latched data to param_io block

set_false_path -from {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_out[*]}
set_false_path -from {alphag:ag|ag_sas_proc:ag_sas_proc|sas_sd_out[*]}
set_false_path -from {alphag:ag|ag_sas_proc:ag_sas_proc|sas_sd_counters_out[*]}

# constant data in param_io block

#set_false_path -from [get_registers {param_io:par|conf_trig_width[*]*}]
set_false_path -from [get_registers {param_io:par|conf_*[*]*}]

# latched 62.5MHz data to UDP packet generator

set_false_path -from {alphag:ag|ag_clk_625:ag_clk_625|*} -to {alphag:ag|udp_trig_ts_625[*]}
set_false_path -from {alphag:ag|ag_clk_625:ag_clk_625|*} -to {alphag:ag|udp_counter_trig_out[*]}
set_false_path -from {alphag:ag|ag_clk_625:ag_clk_625|*} -to {alphag:ag|udp_counter_trig_in[*]}
set_false_path -from {alphag:ag|ag_clk_625:ag_clk_625|*} -to {alphag:ag|udp_counter_pulser[*]}
set_false_path -from {alphag:ag|ag_clk_625:ag_clk_625|*} -to {alphag:ag|udp_counter_scaledown[*]}
set_false_path -from {alphag:ag|ag_clk_625:ag_clk_625|*} -to {alphag:ag|udp_counter_drift[*]}
set_false_path -from {alphag:ag|ag_clk_625:ag_clk_625|*} -to {alphag:ag|udp_counter_pulser[*]}
set_false_path -from {alphag:ag|ag_pulser:ag_pulser|*} -to {alphag:ag|udp_counter_pulser[*]}

# cut clock transfer paths reported by timequest report on "clocks transfer"
#set_false_path -from {param_io:par|reset_out*} -to {alphag:ag|ag_pulser:ag_pulser|reset1}
#set_false_path -from {alphag:ag|frequency_counter:fc_esata|start1} -to {alphag:ag|frequency_counter:fc|start2a}
#set_false_path -from {alphag:ag|frequency_counter:fc_esata|reset} -to {alphag:ag|frequency_counter:fc|start2x}
#set_false_path -from {alphag:ag|frequency_counter:fc_esata|start1} -to {alphag:ag|frequency_counter:fc_esata|start2a}
#set_false_path -from {alphag:ag|frequency_counter:fc_esata|reset} -to {alphag:ag|frequency_counter:fc_esata|start2x}
#set_false_path -from {alphag:ag|frequency_counter:fc_esata|reset} -to {alphag:ag|frequency_counter:fc_esata|clk2_counter*}
#set_false_path -from {alphag:ag|frequency_counter:fc_esata|reset} -to {alphag:ag|frequency_counter:fc_esata|clk2_is_running}

# cut unnecessary fast path
#set_false_path -from {param_io:par|latch_out~reg0} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_out[3][*]}
set_false_path -from {param_io:par|latch_out~reg0} -to {alphag:ag|ag_sas_proc:ag_sas_proc|sas_bits_out[*][*]}

#end
