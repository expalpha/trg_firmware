#####################################
# ALPHAg TRG trigger board (GRIF-C)
#####################################

#
# before running this script, execute
# /opt/intelFPGA/20.1/nios2eds/nios2_command_shell.sh
#

###########
# Targets #
###########

all:: quartus_compile

clean:: quartus_clean

###################
# Quartus Targets #
###################

quartus_clean: ## Remove temporary Quartus files
	quartus_sh --clean grifc
	rm -rf db
	rm -rf incremental_db

quartus_compile: ## Full compile of Quartus project
	/usr/bin/time quartus_sh --flow compile grifc

#quartus_map: ## Perform Analysis and Synthesis 
#	@cd $(hdl_dir) ; quartus_map $(quartus_project) -c $(quartus_project_rev)
#
#quartus_fit: ## Perform Fitter
#	@cd $(hdl_dir) ; quartus_fit $(quartus_project) -c $(quartus_project_rev)
#
#quartus_asm: ## Generate Assembly files and update memory contents
#	@cd $(hdl_dir) ; quartus_cdb --update_mif $(quartus_project) -c $(quartus_project_rev)
#	@cd $(hdl_dir) ; quartus_asm $(quartus_project) -c $(quartus_project_rev)

quartus_cof: ## Run Quartus COF file for project
	quartus_cpf -c grifc.cof

quartus_sta: ## Perform TimeQuest Timing Analysis
	quartus_sta grifc	

# end
